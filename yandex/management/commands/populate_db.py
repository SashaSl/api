# -*- coding: utf-8 -*-

import cfg
import json
from optparse import make_option
from django.core.management.base import BaseCommand

from yandex.models import *

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--app',
            action='store',
            dest='app',
            default=None,
            help='app.'),
        make_option('--filename',
            action='store',
            dest='filename',
            default=None,
            help='filename.'),
        make_option('--unique_field',
            action='store',
            dest='unique_field',
            default=None,
            help='unique_field.'),
        make_option('--model',
            action='store',
            dest='model',
            default=None,
            help='model.')
    )
    def handle(self, *args, **options):
        app = options.get('app')
        filename = options.get('filename')
        unique_field = options.get('unique_field')
        model = options.get('model')

        path = cfg.ROOT_DIR + '/' + app + '/fixtures/' + filename

        with open(path, 'r') as f:
            fixture = json.load(f)

        for item in fixture:
            object = eval(model).objects.filter(**{unique_field: item['fields'][unique_field]})

            if object:
                for field in item['fields']:
                    if not object[0].__getattribute__(field):
                        object.update(**{field: item['fields'][field]})
            else:
                eval(model).objects.create(**item['fields'])



