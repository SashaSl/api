# -*- coding: utf-8 -*-

from django.contrib import admin

from models import *


class SearchRequestAdmin(admin.ModelAdmin):
    list_display = [
        'request'
    ]

class AdwordsCatsAdmin(admin.ModelAdmin):
    def request(self, obj):
        return obj.request
    request.short_description = u'Запрос'

    list_display = [
        'request',
        'number',
        'result',
        'updated_at'
    ]

class GoogleAccsAdmin(admin.ModelAdmin):
    list_display = [
        'login',
        'password'
    ]


admin.site.register(SearchRequest, SearchRequestAdmin)
admin.site.register(AdwordsCats, AdwordsCatsAdmin)
admin.site.register(GoogleAccs, GoogleAccsAdmin)

