# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Region'
        db.create_table('geobase_region', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_ru', self.gf('django.db.models.fields.TextField')(unique=True)),
            ('name_en', self.gf('django.db.models.fields.TextField')(unique=True)),
            ('name_translit', self.gf('django.db.models.fields.TextField')(unique=True)),
            ('level', self.gf('django.db.models.fields.TextField')(db_index=True)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['geobase.Region'], null=True, blank=True)),
        ))
        db.send_create_signal('geobase', ['Region'])


    def backwards(self, orm):
        # Deleting model 'Region'
        db.delete_table('geobase_region')


    models = {
        'geobase.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'name_en': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_ru': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_translit': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['geobase']