# -*- coding: utf-8 -*-

import json
import random
from django.db import models
from engine.models import Engine
from django.db import transaction

class Parser(models.Model):
    MODE_SUGGEST = "suggest"
    MODE_SUGGEST_SCREENSHOT = "suggest_screenshot"
    MODE_SERP = "serp"
    MODE_WORDSTAT_WORDS = "wordstat_words"
    MODE_WORDSTAT_DIRECT_API = "wordstat_direct_api"
    MODE_WORDSTAT_REGIONS = "wordstat_regions"
    MODE_WORDSTAT_HISTORY = "wordstat_history"
    MODE_RELATED = "related"
    MODE_KERNEL_COLLECTOR = "kernel_collector"
    MODE_CRAWLER_NOJS = "crawler_nojs"
    MODE_DIRECT = 'direct'
    MODE_BUDGET = 'budget'
    MODE_VECTOR_MAKER = 'vector_maker'
    MODE_VECTOR_EXTRACTOR = 'vector_extractor'
    MODE_SEO_ANALYSIS_AGG = 'seo_analysis_agg'
    MODE_lINKPAD = 'linkpad'
    MODE_TITS = 'tits'
    MODE_PR_CY = 'pr_cy'
    MODE_NIC = 'nic'
    MODE_MEGAINDEX = 'megaindex'
    MODE_DIRECT_SERTIFICATE = "direct_sertificate"
    MODE_YANDEX_SPROS = "yandex_spros"
    MODE_CHOICE = (
        (MODE_SUGGEST, u"Саджест"),
        (MODE_SUGGEST_SCREENSHOT, u"Саджест скриншот"),
        (MODE_SERP, u"СЕРП"),
        (MODE_WORDSTAT_WORDS, u"Вордстат (режим words)"),
        (MODE_WORDSTAT_DIRECT_API, u"Вордстат (режим API Яндекс.Директ)"),
        (MODE_WORDSTAT_REGIONS, u"Вордстат (режим regions)"),
        (MODE_WORDSTAT_HISTORY, u"Вордстат (режим history)"),
        (MODE_RELATED, u"Рилейтед"),
        (MODE_KERNEL_COLLECTOR, u"Сборщик ядра"),
        (MODE_CRAWLER_NOJS, u"Краулер без JS"),
        (MODE_DIRECT, u"Директ"),
        (MODE_BUDGET, u"Бюджет"),
        (MODE_DIRECT_SERTIFICATE, u"Сертификат директа"),
        (MODE_VECTOR_MAKER, u"Сборщик вектора"),
        (MODE_lINKPAD, u'Линкпад'),
        (MODE_MEGAINDEX, u'Мегаиндекс'),
        (MODE_TITS, u'Тиц'),
        (MODE_PR_CY, u'Pr_cy'),
        (MODE_NIC, u'Nic'),
        (MODE_VECTOR_EXTRACTOR, u"Экстрактор вектора"),
        (MODE_SEO_ANALYSIS_AGG, u"Аггрегатор SEO-анализатора"),
        (MODE_YANDEX_SPROS, u"Яндекс-Спрос")
    )

    created = models.DateTimeField(auto_now_add=True, verbose_name=u"Момент кэширования", db_index=True)
    mode = models.TextField(choices=MODE_CHOICE, verbose_name=u"Тип парсера", db_index=True)
    operation = models.TextField(verbose_name=u"Код операции", db_index=True)

    @property
    def parser_data(self):
        return ParserData.objects.get(parser=self)

    @classmethod
    @transaction.commit_on_success
    def add_and_save(cls, mode, operation, request, region, engine, result, prev_request=None):
        parser = cls.objects.create(mode=mode, operation=operation)
        parser_data = ParserData.objects.create(
            parser=parser,
            region=region,
            engine=engine,
            request=request,
            prev_request=prev_request,
            _result=json.dumps(result, indent=4)
        )
        return parser, parser_data

    def __unicode__(self):
        return self.mode

    class Meta:
        verbose_name = u"парсер"
        verbose_name_plural = u"парсеры"

class ParserData(models.Model):
    parser = models.ForeignKey(Parser, verbose_name=u"Действие парсинга")
    region = models.ForeignKey('geobase.Region', null=True, blank=True, verbose_name=u"Регион", db_index=True)
    engine = models.TextField(null=True, blank=True, verbose_name=u"Поисковая система", choices=Engine.ENGINE_CHOICE, db_index=True)
    request = models.CharField(verbose_name=u"Запрос", db_index=True, max_length=1000)
    prev_request = models.CharField(verbose_name=u"Предыдущий запрос", db_index=True, max_length=1000, null=True, blank=True)
    _result = models.TextField(db_column='result', verbose_name=u"Результат")

    def set_result(self, result):
        self._result = json.dumps(result)

    def get_result(self):
        return json.loads(self._result)

    class Meta:
        verbose_name = u"результат парсинга"
        verbose_name_plural = u"результаты парсинга"

    result = property(get_result, set_result)

class ParentTask(models.Model):
    parent_task = models.CharField(max_length=40, verbose_name=u'Родительская таска', unique=True)

class ChildTask(models.Model):
    parent_task = models.ForeignKey(ParentTask)
    child_task = models.CharField(max_length=40, verbose_name=u'Дочерняя таска', unique=True)

class CaptchaMonitoring(models.Model):
    task_name = models.CharField(max_length=40, verbose_name=u'Название таски')
    time = models.DateTimeField(verbose_name=u"Время", auto_now_add=True)

