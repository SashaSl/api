# -*- coding: utf-8 -*-
import cfg
import os
from django.core.management.base import BaseCommand
from direct_api.models import User
from parser import tasks

class Command(BaseCommand):
    def handle(self, *args, **options):
        def _accs_checker():
            logins = set(User.objects.values_list('login', flat=True))
            dir_names = set(filter(lambda x: x, os.listdir(cfg.ROOT_DIR + '/api_data/direct_api/')))
            new_accs = logins - dir_names
            return list(new_accs)

        accs_to_fill = _accs_checker()
        if accs_to_fill:
            for item in accs_to_fill:
                tasks.accs_filler.apply(args=[item, User.objects.get(login=item).password], kwargs=None, queue='default')














