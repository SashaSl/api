# Django settings for e-core project.

import os
import sys

ROOT_DIR = os.path.dirname(__file__)
ROOT_DIR = os.path.abspath(os.path.join(ROOT_DIR, '..'))
sys.path.append(ROOT_DIR)
import cfg

SITE_URL = cfg.site_url


DEBUG = cfg.debug
TEMPLATE_DEBUG = DEBUG

ADMINS = cfg.admins

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': cfg.db_name,                      # Or path to database file if using sqlite3.
        'USER': cfg.db_user,                      # Not used with sqlite3.
        'PASSWORD': cfg.db_password,                  # Not used with sqlite3.
        'HOST': cfg.db_host,                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': cfg.db_port,                      # Set to empty string for default. Not used with sqlite3.
        'OPTIONS': {
            'autocommit': True,
        }
    }
}

CACHES = {
   'default':  {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'unique-snowflake'
    },
   'lock': {
       'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
       'LOCATION': 'api.evristic.ru:11211',
  }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = cfg.time_zone

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = cfg.language_code

SITE_ID = cfg.site_id

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = cfg.use_i18n

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = cfg.use_l10n

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = cfg.use_tz

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ROOT_DIR + cfg.media_root

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = cfg.media_url

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ROOT_DIR + cfg.static_root

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = cfg.static_url

# Additional locations of static files
STATICFILES_DIRS = [
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
] + [(ROOT_DIR + item) for item in cfg.staticfiles_dirs]

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = cfg.secret_key

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'middleware.crossdomainxhr.XsSharing',
)

XS_SHARING_ALLOWED_ORIGINS = "*"
XS_SHARING_ALLOWED_METHODS = ['POST','GET','OPTIONS', 'PUT', 'DELETE']

ROOT_URLCONF = 'system.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'system.wsgi.application'

TEMPLATE_DIRS = [
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
] + [(ROOT_DIR + item) for item in cfg.template_dirs]

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'south',
    'djcelery',
    'django_jenkins',
] + list(cfg.installed_apps)

FIXTURE_DIRS = []
for app in cfg.installed_apps:
    fixture_dir = os.path.join(ROOT_DIR, app, 'fixtures')
    if os.path.exists(fixture_dir):
        FIXTURE_DIRS.append(fixture_dir)

# mail config
SERVER_EMAIL = cfg.server_email
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = cfg.email_use_tls
EMAIL_HOST = cfg.email_host
EMAIL_HOST_USER = cfg.email_host_user
EMAIL_HOST_PASSWORD = cfg.email_host_password
EMAIL_PORT = cfg.email_port
DEFAULT_FROM_EMAIL = cfg.default_from_email

# celery
import djcelery
from kombu import Queue
djcelery.setup_loader()
CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
CELERY_DISABLE_RATE_LIMITS = True
BROKER_URL = cfg.broker_url
CELERY_DEFAULT_QUEUE = 'default'

'''
CELERY_QUEUES = (
    Queue('default'),
    Queue('urgent'),
    Queue('default2'),
    Queue('urgent2'),
    Queue('default3'),
    Queue('urgent3'),
    Queue('default4'),
    Queue('urgent4'),
    Queue('direct'),
)
'''

# jenkins
JENKINS_TASKS = (
    'django_jenkins.tasks.run_pylint',
    'django_jenkins.tasks.run_pep8',
    'django_jenkins.tasks.run_pyflakes',
    'django_jenkins.tasks.with_coverage',
    'django_jenkins.tasks.django_tests',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
