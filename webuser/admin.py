# -*- coding: utf-8 -*-

import json
import logging

from cookie.models import Cookie
from django.contrib import admin
from models import *

from string import join

logger = logging.getLogger(__name__)

class WebUserAdmin(admin.ModelAdmin):
    def cookie_count(self, obj):
        return Cookie.objects.filter(webuser=obj).count()
    
    cookie_count.short_description = u"Cookie count"

    list_display = [
        'id',
        'created_at',
        'modified_at',
        'cookie_count',
        'usage_count',
        'proxy'
    ]

admin.site.register(WebUser, WebUserAdmin)
