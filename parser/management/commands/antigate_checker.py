# -*- coding: utf-8 -*-

import os
import cfg
import urllib
import datetime
from django.core.management.base import BaseCommand

def antigate_checker():
    os.popen('mkdir -p ' + cfg.ROOT_DIR + '/api_data/monitoring')
    filename = cfg.ROOT_DIR + '/api_data/monitoring/antigate_checker'
    final_result = {}
    service = 'http://antigate.com/res.php?key={KEY}&action=getbalance'.format(KEY=cfg.antigate_key)
    result = urllib.urlopen(service).read()
    final_result['balance'] = float(result)
    final_result['timestamp'] = str(datetime.datetime.utcnow().strftime("%s"))
    print >> open(filename, 'w'), final_result

class Command(BaseCommand):
    def handle(self, *args, **options):
        antigate_checker()
