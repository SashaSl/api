# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Name'
        db.create_table('persons_name', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('is_first', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_male', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('nation', self.gf('django.db.models.fields.CharField')(default='russian', max_length=16)),
        ))
        db.send_create_signal('persons', ['Name'])

        # Adding unique constraint on 'Name', fields ['name', 'is_first', 'is_male', 'nation']
        db.create_unique('persons_name', ['name', 'is_first', 'is_male', 'nation'])

        # Adding model 'MailRu'
        db.create_table('persons_mailru', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('person', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['persons.Person'], unique=True)),
            ('login', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('mail', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('question_num', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('answer', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('lastmsg', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('persons', ['MailRu'])

        # Adding model 'Person'
        db.create_table('persons_person', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nickname', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('birthday', self.gf('django.db.models.fields.DateField')()),
            ('is_male', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('nation', self.gf('django.db.models.fields.CharField')(default='russian', max_length=16)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('proxy', self.gf('django.db.models.fields.CharField')(unique=True, max_length=256)),
            ('useragent', self.gf('django.db.models.fields.CharField')(max_length=1024)),
            ('is_used', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('persons', ['Person'])


    def backwards(self, orm):
        # Removing unique constraint on 'Name', fields ['name', 'is_first', 'is_male', 'nation']
        db.delete_unique('persons_name', ['name', 'is_first', 'is_male', 'nation'])

        # Deleting model 'Name'
        db.delete_table('persons_name')

        # Deleting model 'MailRu'
        db.delete_table('persons_mailru')

        # Deleting model 'Person'
        db.delete_table('persons_person')


    models = {
        'persons.mailru': {
            'Meta': {'object_name': 'MailRu'},
            'answer': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'lastmsg': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'mail': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'person': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['persons.Person']", 'unique': 'True'}),
            'question_num': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'persons.name': {
            'Meta': {'unique_together': "(('name', 'is_first', 'is_male', 'nation'),)", 'object_name': 'Name'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_first': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_male': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nation': ('django.db.models.fields.CharField', [], {'default': "'russian'", 'max_length': '16'})
        },
        'persons.person': {
            'Meta': {'object_name': 'Person'},
            'birthday': ('django.db.models.fields.DateField', [], {}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_male': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_used': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nation': ('django.db.models.fields.CharField', [], {'default': "'russian'", 'max_length': '16'}),
            'nickname': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'proxy': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'useragent': ('django.db.models.fields.CharField', [], {'max_length': '1024'})
        }
    }

    complete_apps = ['persons']