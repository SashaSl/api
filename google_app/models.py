# -*- coding: utf-8 -*-

import datetime
from jsonfield import JSONField

from django.db import models

class SearchRequest(models.Model):
    request = models.CharField(max_length=255, verbose_name=u'Запрос', unique=True)

    class Meta:
        verbose_name = u"Запрос"
        verbose_name_plural = u"Запросы"

    def __unicode__(self):
        return self.request

class AdwordsCatsManager(models.Manager):
    def create_or_update(self, **kwargs):
        sr = self.filter(request__request=kwargs['request'], number=kwargs['number'])
        if not sr:
            self.create(request=kwargs['request_obj'], number=kwargs['number'], result=kwargs['result'])
        else:
            sr.update(result=kwargs['result'], updated_at=datetime.datetime.now())

class AdwordsCats(models.Model):
    request = models.ForeignKey(SearchRequest, verbose_name=u"Запрос", db_index=True)
    number = models.IntegerField(verbose_name=u"Номер", max_length=10, blank=True, null=True)
    result = JSONField(verbose_name=u"Результат", blank=True, null=True)
    updated_at = models.DateTimeField(verbose_name=u'Время изменения', auto_now=True)

    objects = AdwordsCatsManager()

    class Meta:
        verbose_name = u"Результат категорий"
        verbose_name_plural = u"Результаты категорий"

class GoogleAccs(models.Model):
    login = models.CharField(max_length=50, verbose_name=u"Логин", unique=True, null=False)
    password = models.CharField(max_length=32, verbose_name=u"Пароль")

    class Meta:
        verbose_name = u"Гугловый акаунт"
        verbose_name_plural = u"Гугловые акаунты"


