# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'User'
        db.create_table('direct_api_user', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('login', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('limit_reached_at', self.gf('django.db.models.fields.DateTimeField')(default=None, null=True)),
        ))
        db.send_create_signal('direct_api', ['User'])

    def backwards(self, orm):
        # Deleting model 'User'
        db.delete_table('direct_api_user')

    models = {
        'direct_api.user': {
            'Meta': {'object_name': 'User'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'limit_reached_at': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'max_length': '32', 'unique': 'True', 'null': 'False'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        }
    }

    complete_apps = ['direct_api']
