# -*- coding: utf-8 -*-

import string
from linguistics import Morph
from seo.common import stop_words_cleaner

MORPH = Morph()


def similarity(tag, request):
    tag = tag.lower()
    request = request.lower()

    if tag == request:
        return 1.0
    if tag.startswith(request):
        return 0.9
    if tag.endswith(request):
        return 0.8
    if request in tag:
        return 0.7

    tag = MORPH.norm_to_string(tag)
    request = MORPH.norm_to_string(request)

    if tag == request:
        return 0.9
    if tag.startswith(request):
        return 0.8
    if tag.endswith(request):
        return 0.7
    if request in tag:
        return 0.6

    return 0.5


def similarity_615(h1, request, title):
    def _splitter(phrase):
        delimiters = ["-", "::", "|", ".", ",", "+", ";", ":"]
        phrase = [phrase]
        for delimiter in delimiters:
            new_phrase = []
            for item in phrase:
                new_phrase += item.split(delimiter)
            phrase = new_phrase
        return phrase

    def _similarity_615(request, tag, normalizer):
        def _cut_spaces(text):
            return u' '.join(text.split())

        def _punct_finder(text):
            punct_list = list(string.punctuation)
            for item in punct_list:
                text = text.replace(item, ' ' + item + ' ')
            return text

        request = request.strip()
        tag = tag.strip()

        # пунктуацию оставляем, воспринимаем её как отдельные слова
        request = _punct_finder(request)
        tag = _punct_finder(tag)

        # пробелы между словами - приводим к одному пробелу
        request = _cut_spaces(request)
        tag = _cut_spaces(tag)

        # приведение к нижнему регистру
        request = request.lower()
        tag = tag.lower()

        # нормализация в начальную форму слова
        request = normalizer.norm_to_string(request)
        tag = normalizer.norm_to_string(tag)

        # # выкидываем стоп-слова
        # request = stop_words_cleaner(request.split())
        # tag = stop_words_cleaner(tag.split())

        return int(set(tag) == set(request))

    if h1:
        return _similarity_615(request, h1, MORPH)
    else:
        for item in _splitter(title):
            if _similarity_615(request, item, MORPH):
                return 1
    return 0


# def similarity(text, request, power):
#     return power.run(request, text)
