import time
import cfg
import json
import urllib2
import httplib
import glob
import zipfile
import os
import utils.parser.serp.config
from selenium.common.exceptions import NoSuchElementException, UnexpectedAlertPresentException
from utils.selebot.surfer import Surfer
from selenium.webdriver.common.keys import Keys
from utils.common import Tor, DeleteDir, copy_file_to_api, random_string
from utils.parser.serp.local_utils import Xvfb
from urllib2 import HTTPError

from dblog.models import Error
import sys, traceback



class YandexSertificate:
    def check_sertificate(self, path_to_sertificate):
        sertif_folder = cfg.ROOT_DIR + str(path_to_sertificate)
        self.keyfile = sertif_folder + 'private.key'
        self.certfile = sertif_folder + 'cert.crt'
        that = self
        if os.path.exists(self.keyfile) and os.path.exists(self.certfile):
            # configure urlopener
            class YandexCertConnection(httplib.HTTPSConnection):
                def __init__(self, host, port=None, key_file=that.keyfile, cert_file=that.certfile, timeout=30):
                    httplib.HTTPSConnection.__init__(self, host, port, key_file, cert_file)
            class YandexCertHandler(urllib2.HTTPSHandler):
                def https_open(self, req):
                    return self.do_open(YandexCertConnection, req)
                https_request = urllib2.AbstractHTTPHandler.do_request_
            self.urlopener = urllib2.build_opener(*[YandexCertHandler()])
        else:
            return False
        try:
            resp = self.make_action("GetReportList")
            if 'error_code'in resp:
                return False
        except HTTPError:
            return False
        return True

    def make_action(self, method, live_url=None, param=None):
        url = 'https://api.direct.yandex.ru/json-api/v4/'
        live_url = 'https://api.direct.yandex.ru/live/v4/json/'
        if live_url:
            direct_url = live_url
        else:
            direct_url = url
        data = {
            "method": method,
        }
        if param:
            data["param"] = param
        response = self.get_response(data, direct_url)
        return json.load(response)

    def get_response(self, data, url):
        jdata = json.dumps(data, ensure_ascii=False).encode('utf8')
        response = self.urlopener.open(url, jdata)
        return response

    def _anti_captcha(self, surfer, browser, css):
        try:
            surfer.wait_for_css(css)
        except UnexpectedAlertPresentException:
            pass
        try:
            browser.find_element_by_css_selector('img[alt="captcha"]')
            while True:
                try:
                    captcha_text = surfer.solve_captcha('form[action] img[alt="captcha"]')
                except IndexError:
                    break
                if not captcha_text == 'ERROR_CAPTCHA_UNSOLVABLE':
                    browser.find_element_by_css_selector('input[type="text"]').send_keys(captcha_text)
                    browser.find_element_by_css_selector('input[type="submit"]').click()
                    try:
                        surfer.wait_for_css(css)
                    except UnexpectedAlertPresentException:
                        pass
                    try:
                        browser.find_element_by_css_selector('img[alt="captcha"]')
                    except NoSuchElementException:
                        break
        except NoSuchElementException:
            pass

    def _final_downloader(self, browser, surfer):
        surfer.wait_for_css('input[id="create_cert"]')
        bad_elements = browser.find_elements_by_css_selector('a[class="b-cert-list__action"]')
        if len(bad_elements) > 1:
            max_retry = 50
            retry_count = 0
            time.sleep(10)
            while True or retry_count < max_retry:
                bad_elements[-1].click()
                time.sleep(3)
                retry_count += 1
                try:
                    alert = browser.switch_to_alert()
                    time.sleep(3)
                    alert.accept()
                    self._anti_captcha(surfer, browser, 'div[class="b-head-line"]')
                    time.sleep(10)
                    break
                except Exception, msg:
                    print msg
                    self._anti_captcha(surfer, browser, 'div[class="b-head-line"]')

            surfer.wait_for_css('input[id="create_cert"]')

        browser.find_element_by_css_selector('input[id="create_cert"]').click()
        self._anti_captcha(surfer, browser, 'div[class="b-head-line"]')
        surfer.wait_for_css('form[class="b-cert-form__form"]')
        js_script = "document.getElementsByName('days')[0].value = 90"
        browser.execute_script(js_script)
        browser.find_element_by_css_selector('input[class="b-cert-form__submit_button"]').click()
        time.sleep(3)
        while True:
            try:
                alert = browser.switch_to_alert()
                time.sleep(3)
                alert.accept()
                self._anti_captcha(surfer, browser, 'div[class="b-head-line"]')
                time.sleep(10)
                break
            except:
                self._anti_captcha(surfer, browser, 'div[class="b-head-line"]')

    def _form_filler(self, browser, surfer):
        browser.find_element_by_css_selector('a[href="https://direct.yandex.ru/registered/main.pl?cmd=apiSettings&api_welcome=yes"]').click()
        self._anti_captcha(surfer, browser, 'div[class="b-head-line"]')
        surfer.wait_for_css('label[for="accept"]')
        browser.find_element_by_css_selector('label[for="accept"]').click()
        time.sleep(2)
        browser.find_element_by_css_selector('input[type="submit"]').click()
        self._anti_captcha(surfer, browser, 'div[class="b-head-line"]')
        surfer.wait_for_css('a[href="https://direct.yandex.ru/registered/main.pl?cmd=certList&ulogin="]')
        browser.find_element_by_css_selector('a[href="https://direct.yandex.ru/registered/main.pl?cmd=certList&ulogin="]').click()
        self._anti_captcha(surfer, browser, 'div[class="b-head-line"]')
        self._final_downloader(browser, surfer)

    def _certs_downloader(self, browser, surfer):
        browser.get('https://direct.yandex.ru/registered/main.pl?cmd=apiSettings')
        self._anti_captcha(surfer, browser, 'div[class="b-head-line"]')
        try:
            browser.find_element_by_css_selector('a[href="https://direct.yandex.ru/registered/main.pl?cmd=certList&ulogin="]')
            browser.find_element_by_css_selector('a[href="https://direct.yandex.ru/registered/main.pl?cmd=certList&ulogin="]').click()
            self._anti_captcha(surfer, browser, 'div[class="b-head-line"]')
            self._final_downloader(browser, surfer)
            return
        except NoSuchElementException:
            pass
        try:
            browser.find_element_by_css_selector('a[href="https://direct.yandex.ru/registered/main.pl?cmd=apiSettings&api_welcome=yes"]')
            self._form_filler(browser, surfer)
        except NoSuchElementException:
            self._final_downloader(browser, surfer)

    def _campaign_filler(self, login, password, folder):
        surfer = Surfer(useragent='Mozilla/5.0', folder=folder)
        surfer.open_browser('https://direct.yandex.ru/registered/main.pl?cmd=chooseInterfaceType')
        browser = surfer.get_browser()
        surfer.wait_for_css('input[name="login"]')
        browser.find_element_by_name('login').send_keys(login)
        browser.find_element_by_name('passwd').send_keys(password)
        browser.find_element_by_name('passwd').send_keys(Keys.ENTER)

        self._anti_captcha(surfer, browser, 'div[class="b-head-line"]')

        # time.sleep(3)
        surfer.wait_for_css('div[class="b-head-line"]')
        try:
            browser.find_element_by_css_selector('input[name="continue"]').click()
        except NoSuchElementException:
            pass
        surfer.wait_for_css('h1')
        try:
            browser.find_element_by_css_selector('a[href="https://direct.yandex.ru/registered/main.pl?cmd=apiSettings"]')
            self._certs_downloader(browser, surfer)
            return None
        except NoSuchElementException:
            js_script = "document.getElementsByName('client_country')[0].value = 225"
            browser.execute_script(js_script)
            js_script = "document.getElementsByName('currency')[0].value = 'RUB'"
            browser.execute_script(js_script)
            browser.find_element_by_css_selector('input[class="b-choose-interface-type__radio"][value="std"]').click()
            browser.find_element_by_css_selector('input[type="submit"]').click()
            self._anti_captcha(surfer, browser, 'div[class="b-head-line"]')
            surfer.wait_for_css('input[type="button"][class="b-campaign-edit__submit"]')
            browser.find_element_by_css_selector('input[type="button"][class="b-campaign-edit__submit"]').click()
            self._anti_captcha(surfer, browser, 'div[class="b-head-line"]')
            surfer.wait_for_css('div[class="b-head-line"]')
            self._certs_downloader(browser, surfer)

    @Tor(replace_only_none_proxy=False)
    @Xvfb(visible=utils.parser.serp.config.WORDSTAT_VISIBILITY, width=1200, height=600, timeout=30)
    def handler(self, login, password, **kwargs):

        path_to_direct_folder = cfg.ROOT_DIR + '/api_data/direct_api'
        if not os.path.exists(path_to_direct_folder):
            os.makedirs(path_to_direct_folder)
        folder_name = login + '_' + random_string()
        path_to_folder = path_to_direct_folder + '/' + folder_name
        if os.path.exists(path_to_folder):
            DeleteDir(path_to_folder)
        if not os.path.exists(path_to_folder):
            os.makedirs(path_to_folder)
        self._campaign_filler(login, password, path_to_folder)
        try:
            zip_file = glob.glob(path_to_folder + '/*.zip')[0]
        except Exception, e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Error.objects.create(
                tag='download sertificate error',
                message="".join(traceback.format_exception(exc_type, exc_value, exc_traceback)) + '///' + path_to_folder + repr(os.listdir(path_to_folder)) +'///'
            )
            return 'error'
        with zipfile.ZipFile(zip_file, 'r') as z:
            z.extractall(path_to_folder)
        os.remove(zip_file)

        for sertificate_file in os.listdir(path_to_folder):
            copy_file_to_api(path_to_folder + '/' + sertificate_file)

        return folder_name




