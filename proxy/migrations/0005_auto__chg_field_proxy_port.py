# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Proxy.port'
        db.alter_column('proxy_proxy', 'port', self.gf('django.db.models.fields.IntegerField')(null=True))

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Proxy.port'
        raise RuntimeError("Cannot reverse this migration. 'Proxy.port' and its values cannot be restored.")

    models = {
        'geobase.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'name_en': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_ru': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_translit': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        },
        'proxy.proxy': {
            'Meta': {'object_name': 'Proxy'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15', 'db_index': 'True'}),
            'is_tor': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'port': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'protocol': ('django.db.models.fields.TextField', [], {}),
            'proxy_type': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['proxy']