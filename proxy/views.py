# -*- coding: utf-8 -*-

import json

import re
import cfg
from utils.api import check_api_key, wrap_json

@wrap_json
@check_api_key("GET")
def proxy_checker(request):
    regex = re.compile('^HTTP_')
    headers = dict((regex.sub('', header), value) for (header, value) 
       in request.META.items() if header.startswith('HTTP_'))

    headers['REMOTE_ADDR'] = request.META.get('REMOTE_ADDR')

    return {
        "content": cfg.proxy_checker_content,
        "headers": headers,
    }

