#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os

ROOT_DIR = os.path.dirname(__file__) + '/..'
ROOT_DIR = os.path.abspath(ROOT_DIR)
sys.path.append(ROOT_DIR)

from utils.fast_suggest import FastSuggest

if __name__ == "__main__":
    FastSuggest.from_textfile(sys.argv[1], sys.argv[2])
