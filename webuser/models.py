# -*- coding: utf-8 -*-

import re
import json
import random
import datetime
import cookielib

from pprint import pprint

from django.db import models
from django.db import transaction
from django.db.models import F

import cfg
from cookie.models import Cookie
from proxy.models import Proxy
from utils.common import ts_to_datetime, datetime_to_ts

from surfbot.web_application import QNetworkCookie, QNetworkCookieJar

class WebUser(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u"Создан")
    modified_at = models.DateTimeField(auto_now=True, verbose_name=u"Изменен")
    usage_count = models.IntegerField(verbose_name=u"Запросов", default=0)
    proxy = models.ForeignKey('proxy.Proxy', null=True, blank=True, verbose_name=u"Последний IP", db_index=True)

    @classmethod
    def get_random_user(cls):
        while True:
            try:
                cnt = cls.objects.count()
                rand = random.randint(0, cnt-1)
                return cls.objects.all()[rand]
            except cls.DoesNotExist:
                pass

    @classmethod
    def generate(cls, proxy=None):
        thres = random.random()
        total_proxy_count = cls.objects.filter(proxy=Proxy.get_by_str(proxy)).count()
        print 'total_proxy_count: %s' % (total_proxy_count)

        total_count = cls.objects.count()

        if (total_count < cfg.webuser_max_users and thres < cfg.webuser_create_probability):
            return cls.objects.create()
        else:
            return cls.get_random_user()


    def generate_cookiejar(self, cj_type='raw'):
        cookie_data = Cookie.objects.filter(webuser=self)
        if cj_type == 'cookielib':
            cj = cookielib.CookieJar()
            if cookie_data:
                for cdata in cookie_data:
                    cookie = cookielib.Cookie(
                        version=0,
                        name=cdata.name,
                        value=cdata.value,
                        port=None,
                        port_specified=False,
                        domain=cdata.domain,
                        domain_specified=True,
                        domain_initial_dot=False,
                        path=cdata.path,
                        path_specified=True,
                        secure=cdata.secure,
                        expires=datetime_to_ts(cdata.expires),
                        discard=False,
                        comment=None,
                        comment_url=None,
                        rest=None
                    )
                    cj.set_cookie(cookie)
            return cj
        elif cj_type == 'qnetwork':
            cj = QNetworkCookieJar()
            cookie_list = []
            if cookie_data:
                for cdata in cookie_data:
                    cookie = QNetworkCookie()

                    cookie.setName(cdata.name)
                    cookie.setValue(cdata.value)
                    cookie.setDomain(cdata.domain)
                    cookie.setPath(cdata.path)
                    cookie.setSecure(cdata.secure)
                    cookie.setExpirationDate(cdata.expires)

                    cookie_list.append(cookie)

                cj.setAllCookies(cookie_list)
            return cj
        else:
            return cookie_data

    @transaction.commit_on_success
    def save_changes(self, cookie_list=None, proxy=None):
        Cookie.save_cookie_list(cookie_list, self)

        WebUser.objects.filter(id=self.id).update(
            modified_at=datetime.datetime.now(), 
            usage_count=F("usage_count") + 1,
            proxy = Proxy.get_by_str(proxy),
        )

    def __unicode__(self):
        return str(self.id)

    class Meta:
        verbose_name = u"Web-пользователь"
        verbose_name_plural = u"Web-пользователи"
