# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'ParserData.request'
        db.add_column('parser_parserdata', 'request',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=1000, db_index=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'ParserData.request'
        db.delete_column('parser_parserdata', 'request')


    models = {
        'geobase.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'name_en': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_ru': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_translit': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        },
        'parser.parser': {
            'Meta': {'object_name': 'Parser'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mode': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'operation': ('django.db.models.fields.TextField', [], {'db_index': 'True'})
        },
        'parser.parserdata': {
            'Meta': {'object_name': 'ParserData'},
            '_result': ('django.db.models.fields.TextField', [], {'db_column': "'result'"}),
            'engine': ('django.db.models.fields.TextField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parser': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['parser.Parser']"}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'}),
            'request': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'db_index': 'True'})
        }
    }

    complete_apps = ['parser']