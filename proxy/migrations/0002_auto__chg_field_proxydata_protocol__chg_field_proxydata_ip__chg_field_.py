# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'ProxyData.protocol'
        db.alter_column('proxy_proxydata', 'protocol', self.gf('django.db.models.fields.TextField')())

        # Changing field 'ProxyData.ip'
        db.alter_column('proxy_proxydata', 'ip', self.gf('django.db.models.fields.IPAddressField')(max_length=15))

        # Changing field 'ProxyData.is_tor'
        db.alter_column('proxy_proxydata', 'is_tor', self.gf('django.db.models.fields.BooleanField')())

        # Renaming column for 'ProxyData.proxy_type' to match new field type.
        db.rename_column('proxy_proxydata', 'type', 'proxy_type')
        # Changing field 'ProxyData.proxy_type'
        db.alter_column('proxy_proxydata', 'proxy_type', self.gf('django.db.models.fields.TextField')())

        # Changing field 'ProxyData.port'
        db.alter_column('proxy_proxydata', 'port', self.gf('django.db.models.fields.IntegerField')())

    def backwards(self, orm):

        # Changing field 'ProxyData.protocol'
        db.alter_column('proxy_proxydata', 'protocol', self.gf('django.db.models.fields.TextField')(db_column='protocol'))

        # Changing field 'ProxyData.ip'
        db.alter_column('proxy_proxydata', 'ip', self.gf('django.db.models.fields.IPAddressField')(max_length=15, db_column='ip'))

        # Changing field 'ProxyData.is_tor'
        db.alter_column('proxy_proxydata', 'is_tor', self.gf('django.db.models.fields.BooleanField')(db_column='is_tor'))

        # Renaming column for 'ProxyData.proxy_type' to match new field type.
        db.rename_column('proxy_proxydata', 'proxy_type', 'type')
        # Changing field 'ProxyData.proxy_type'
        db.alter_column('proxy_proxydata', 'type', self.gf('django.db.models.fields.TextField')(db_column='type'))

        # Changing field 'ProxyData.port'
        db.alter_column('proxy_proxydata', 'port', self.gf('django.db.models.fields.IntegerField')(db_column='port'))

    models = {
        'geobase.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'name_en': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_ru': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_translit': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        },
        'proxy.proxydata': {
            'Meta': {'object_name': 'ProxyData'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15', 'db_index': 'True'}),
            'is_tor': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'port': ('django.db.models.fields.IntegerField', [], {}),
            'protocol': ('django.db.models.fields.TextField', [], {}),
            'proxy_type': ('django.db.models.fields.TextField', [], {}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['proxy']