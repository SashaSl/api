# encoding: utf-8


class YandexDirectApiError(Exception):
    def __init__(self, code, message):
        self.code, self.message = code, message
        Exception.__init__(self, code, message)


class WordstatReportPending(YandexDirectApiError):
    pass


class ReportQueueLimitReached(YandexDirectApiError):
    pass


class RequestLimitReached(YandexDirectApiError):
    pass


class NotEnoughUnits(YandexDirectApiError):
    pass


class MethodDoesNotExist(YandexDirectApiError):
    pass


class ErrorFactory:
    """
    См.: http://api.yandex.ru/direct/doc/reference/ErrorCodes.xml
    """

    error_code_to_exception = {
        92: WordstatReportPending,
        55: MethodDoesNotExist,
        56: RequestLimitReached,
        31: ReportQueueLimitReached,
        152: NotEnoughUnits,
    }

    def __init__(self):
        pass

    @classmethod
    def create_error(cls, code, message):
        if code in cls.error_code_to_exception:
            return cls.error_code_to_exception[code](code, message)
        return YandexDirectApiError(code, message)
