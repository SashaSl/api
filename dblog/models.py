# -*- coding: utf-8 -*-

from django.db import models

class Error(models.Model):
    created = models.DateTimeField(verbose_name=u"Момент создания", db_index=True, auto_now_add=True)
    tag = models.TextField(verbose_name=u"Тэг ошибки", db_index=True)
    message = models.TextField(verbose_name=u"Сообщение об ошибке")

    @classmethod
    def to_db(cls, msg=''):
        import sys, traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        cls.objects.create(
            tag=exc_type,
            message="".join(traceback.format_exception(exc_type, exc_value, exc_traceback)) + '///' + msg
        )

def errors_to_db(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:
            Error.to_db()
    return wrapper
