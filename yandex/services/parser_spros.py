# -*- coding: utf-8 -*-

import json
import urllib
import urllib2


def spros(request, region):
    URL = 'http://direct.yandex.ru/public'
    VALUES = {'cmd': 'ajaxGetSuggestion', 'geo': region, 'get_stat': 1, 'srcPhrases': request.encode('utf8'), 'promo': 'spros'}

    data = urllib.urlencode(VALUES, doseq=True)
    req = urllib2.Request(URL, data)
    resp = urllib2.urlopen(req).read()

    return json.loads(resp)
