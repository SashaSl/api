from django.http import HttpResponse
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.admin.views.decorators import staff_member_required
from django.views.static import serve
from django.conf import settings
import cfg

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^api/', include('api.urls')),
    url(r'^api/', include('proxy.urls')),
    url(r'^yandex/', include('yandex.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

if not settings.DEBUG or True:
    urlpatterns += patterns('',
        url(r'^static/djcelery/style.css$', lambda request: HttpResponse(content_type='text/css')),
        url(r'^media/(.*)$', 'django.views.static.serve', kwargs={'document_root': cfg.ROOT_DIR + cfg.media_root.rstrip("/")}),
        url(r'^static/(.*)$', 'django.views.static.serve', kwargs={'document_root': cfg.ROOT_DIR + cfg.static_root.rstrip("/")}),
        url(r'^coverage/(.*)$', staff_member_required(serve), kwargs={'document_root': cfg.ROOT_DIR + '/htmlcov'}),
        url(r'^coverage$', 'django.views.generic.simple.redirect_to', kwargs={'url': "/coverage/index.html"}),
        url(r'^favicon\.ico$', 'django.views.generic.simple.redirect_to', kwargs={'url': "/static/img/favicon.ico"})
    )
