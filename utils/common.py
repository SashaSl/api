# -*- coding: utf-8 -*-

import hashlib
import time
import datetime
import multiprocessing
import socket
import random
import re
import os
import time
import signal
import sys
import traceback
import cfg
from django.core.cache import cache
import urllib
from django.forms.models import model_to_dict
from pyvirtualdisplay.display import Display
from celery.result import AsyncResult
from urlparse import urlparse
from pymorphy2 import MorphAnalyzer
from HTMLParser import HTMLParser, HTMLParseError
import urllib2
import string
import resource
import psutil
from datetime import timedelta
from collections import defaultdict
from operator import itemgetter
import itertools
import config
import json

from engine.models import Engine
from geobase.models import Region

SLEEP_TMT = cfg.api_sleep_timeout
SLEEP_TMT = 1
TOR_ON = cfg.tor_on
IP_ON = cfg.ip_on
SSH_EXTRA = '-v -o ConnectTimeout=30 -o ConnectionAttempts=1 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'
REGIONS_STOP_LIST = [
        u'Москва',
        u'Москва и Московская область',
        u'Балашиха',
        u'Воскресенск',
        u'Дмитров',
        u'Дмитровский район',
        u'Долгопрудный',
        u'Дубна',
        u'Железнодорожный',
        u'Жуковский',
        u'Ивантеевка',
        u'Климовск',
        u'Коломна',
        u'Королёв',
        u'Красногорск',
        u'Можайск',
        u'Орехово-Зуево',
        u'Подольск',
        u'Пущино',
        u'Раменское',
        u'Сергиев Посад',
        u'Серпухов',
        u'Фрязино',
        u'Черноголовка',
        u'Щелково',
        u'Юбилейный',
        u'Электросталь',
        u'Электроугли',
        u'Белоозерский',
        u'Бронницы',
        u'Ватутинки',
        u'Домодедово',
        u'Дубна',
        u'Егорьевск',
        u'Зеленоград',
        u'Истра',
        u'Истринский район',
        u'Клин',
        u'Клинский район',
        u'Коломна',
        u'Коммунарка',
        u'Королёв',
        u'Кубинка',
        u'Лобня',
        u'Лыткарино',
        u'Люберецкий район',
        u'Люберцы',
        u'Малаховка',
        u'Мосрентген',
        u'Мытищи',
        u'Наро-Фоминск',
        u'Ногинск',
        u'Ногинский район',
        u'Одинцово',
        u'Одинцовский район',
        u'Павловский Посад',
        u'Троицк',
        u'Фаустово',
        u'Фрязино',
        u'Электрогорск',
        u'Санкт-Петербург',
        u'Санкт-Петербург и Ленинградская область'
    ]


class XVFB_handler():
    def __init__(self, visible=cfg.wordstat_visible, width=1200, height=600, timeout=5):
        self.display = Display(visible=visible, size=(width, height))
        self.display.start()
        time.sleep(timeout)
    def __del__(self):
        self.display.stop()

def extract_domain(url):
    domains = re.findall(ur'^(?:(?:https?:)?//)?(?:www\.)?([^/?#\s]*)', url.lower())
    return domains[0].encode('idna')

def copy_file_to_api(filename):
    if cfg.install_type == 'local': return
    dirname = os.path.dirname(filename)
    dirname = os.path.abspath(dirname)
    assert not os.system('ssh {ssh_extra} -C root@{ip} -t "mkdir -p {dirname}"'.format(ssh_extra=SSH_EXTRA, ip=cfg.my_ip, dirname=dirname))
    assert not os.system('scp -r {ssh_extra} {filename} root@{ip}:{dirname}'.format(ssh_extra=SSH_EXTRA, ip=cfg.my_ip, dirname=dirname, filename=filename))

def copy_dir_to_worker(dirname):
    if cfg.install_type == 'local': return
    # Убираем '/' в конце чтобы команда scp перезаписывала каталоги
    if dirname[-1] == '/':
        dirname = dirname[:-1]
    new_dirname = cfg.ROOT_DIR + '/api_data/direct_api/'
    assert not os.system('scp -r {ssh_extra} root@{ip}:{dirname} {new_dirname}'.format(ssh_extra=SSH_EXTRA, ip=cfg.my_ip, dirname=dirname, new_dirname=new_dirname))

def wait_for_load(max_cpu=None, max_memory=80.0, max_loadavg=None, timeout=1.0):
    def _decorator(func):
        def wrapper(*args, **kwargs):
            while True:
                cpu = psutil.cpu_percent()
                mem = psutil.virtual_memory()
                mem = 100.0 * float(mem.used) / mem.total
                loadavg = os.getloadavg()[0]

                cpu_flag = max_cpu is None or cpu < max_cpu
                mem_flag = max_memory is None or mem < max_memory
                loadavg_flag = max_loadavg is None or loadavg < max_loadavg

                if cpu_flag and mem_flag and loadavg_flag:
                    return func(*args, **kwargs)
                time.sleep(timeout)
        return wrapper
    return _decorator

def limit_resources(max_memory=1*2**30):
    def _decorator(func):
        def wrapper(*args, **kwargs):
            old_res = resource.getrlimit(resource.RLIMIT_AS)
            try:
                resource.setrlimit(resource.RLIMIT_AS, (max_memory, resource.RLIM_INFINITY))
                return func(*args, **kwargs)
            finally:
                resource.setrlimit(resource.RLIMIT_AS, old_res)
        return wrapper
    return _decorator

def sanitize_stop_words(items):
    stop_list = [
        u"новости",
        u"о компании",
        u"отдел доставки",
        u"гарантии",
        u"вакансии",
        u"партнёры",
        u"обратная связь",
        u"услуги",
        u"консультация",
        u"доставка",
        u"условия сотрудничества",
        u"фотогалерея",
        u"статьи",
        u"правила сайта",
        u"на главную",
        u"акции",
        u"контакты",
        u"задать вопрос",
        u"контактная информация",
        u"оставьте отзыв",
        u"поделитесь ссылкой",
        u"продукция",
        u"полезная информация",
        u"находимся рядом с вами",
        u"работаем ежедневно",
        u"часто задаваемые вопросы",
        u"вопросы и ответы",
        u"гарантия",
        # строителям
        # дилерам
        # дизайнерам
    ]

    result = []
    for item in items:
        if not item in stop_list:
            result.append(item)

    return result

def sanitize_link_list(url, links):
    o = urlparse(url)
    normal_url = o.netloc
    pattern = re.compile('(?:http://)?(?:https://)?(?:www.)?(?:[a-zA-Z0-9]+.)?(?:[a-zA-Z0-9]+.)?' + normal_url.replace('www.',''), re.IGNORECASE)
    result = []
    for link in links:
        full_link = None
        if not 'http' in link and not link.startswith('//'):
            if link.startswith('/'):
                additinal_link = link
            elif link.startswith('./'):
                additinal_link = link.replace('./','/')
            elif link.startswith('../'):
                additinal_link = link.replace('../','/')
            else:
                additinal_link = '/' + link
            full_link = 'http://' + normal_url + additinal_link
        else:
            m = pattern.match(link)
            if m:
                full_link = link
        if full_link:
            result.append(full_link)
    return result

def wait_for_tasks(task_list, disregard_failure = False):
    for task_id in task_list:
        while True:
            async_result = AsyncResult(task_id)
            status = async_result.status.lower()

            if status == 'success':
                break
            elif status == 'failure':
                time.sleep(2)
                async_result = AsyncResult(task_id)
                status = async_result.status.lower()
                if status == 'failure':
                    if disregard_failure:
                        break
                    else:
                        class CommonWaitTaskFailed(Exception):
                            pass
                        raise CommonWaitTaskFailed('Task [ ' + task_id + '] failed while waiting!')
            else:
                time.sleep(SLEEP_TMT)


def wait_and_restart_tasks(task_info, task_type, time_limit = 600, max_restart_number = 10):
    def _select_bad_tasks(task_info):
        for task in task_info:
            async_result = AsyncResult(task['task_id'])
            status = async_result.status.lower()
            if status == 'success':
                continue
            elif status == 'failure':
                raise CommonWaitTaskFailed('Task [ ' + task['task_id'] + '] failed while waiting!')
            else:
                yield task

    def _restart_stuck_tasks(stuck_tasks, start=True):
        for task in stuck_tasks:
            async_result = AsyncResult(task['task_id'])
            async_result.revoke(terminate=True)
            if start:
                new_task_id = task_type.apply_async(args=task['task_args'], kwargs=task['task_kwargs'], queue=task['task_queue']).task_id
                task['task_id'] = new_task_id
                yield task

    class CommonWaitTaskFailed(Exception):
        pass

    def _check_tasks(task_info):
        start = int(datetime.datetime.now().strftime("%s"))
        for task in task_info:
            while True:
                if int(datetime.datetime.now().strftime("%s")) - start > time_limit:
                    return list(_select_bad_tasks(task_info))
                async_result = AsyncResult(task['task_id'])
                status = async_result.status.lower()
                if status == 'success':
                    break
                elif status == 'failure':
                    raise CommonWaitTaskFailed('Task [ ' + task['task_id'] + '] failed while waiting!')
                else:
                    time.sleep(SLEEP_TMT)

    restart_number = 1
    stuck_tasks = []
    while restart_number <= max_restart_number:
        stuck_tasks = _check_tasks(task_info)
        if not stuck_tasks:
            return
        else:
            task_info = list(_restart_stuck_tasks(stuck_tasks))
            restart_number += 1
    list(_restart_stuck_tasks(stuck_tasks, start=False))
    raise CommonWaitTaskFailed('Failed while waiting!')


def wait_for_tasks_time(task_list):
    def _time_adder(time_dict, task_id):
        if not task_id in time_dict:
            time_dict[task_id] = datetime.datetime.now()
        return time_dict

    time_dict = {}
    not_ready = True
    while not_ready:
        not_ready = False
        for task_id in task_list:
            async_result = AsyncResult(task_id)
            status = async_result.status.lower()

            if (status == 'success'):
                time_dict = _time_adder(time_dict, task_id)

            elif (status == 'failure'):
                class CommonWaitTaskFailed(Exception):
                    pass
                raise CommonWaitTaskFailed('Task [ ' + task_id + '] failed while waiting!')
            else:
                not_ready = True
                time.sleep(SLEEP_TMT)
    return time_dict

def pick_random_from_queryset(queryset):
    count = queryset.count()
    seed = random.randint(0, count - 1)
    return queryset[seed]

def connection_timeout(timeout):
    def timeout_decorator(func):
        def wrapper(*args, **kwargs):
            _timeout = socket.getdefaulttimeout()
            socket.setdefaulttimeout(timeout)
            result = func(*args, **kwargs)
            socket.setdefaulttimeout(_timeout)
            return result
        return wrapper
    return timeout_decorator

def Xvfb(visible=1, width=800, height=600, timeout=5.0):
    def decorator(func):
        def wrapper(*args, **kwargs):
            display = Display(visible=visible, size=(width, height))
            display.start()
            time.sleep(timeout)
            try:
                result = func(*args, **kwargs)
            finally:
                display.stop()
            return result
        return wrapper
    return decorator

class KWObject:
    def __init__(self, **kwargs):
        for k, v in kwargs.iteritems():
            setattr(self, k, v)

class Object:
    def __init__(self, dict_data):
        for k, v in dict_data.iteritems():
            setattr(self, k, v)

def model_filter(cls, timeout, *args, **kwargs):
    key = "FilterObjects__v1.0__" + cls.__name__ + "__" + urllib.quote_plus(str(args)) + "__" + urllib.quote_plus(str(kwargs))
    val = cache.get(key)
    if val is not None:
        return val
    val = [Object(model_to_dict(v)) for v in cls.objects.filter(*args, **kwargs)]
    cache.set(key, val, timeout)
    return val

def random_string(length=8):
    chars = '1234567890qwertyuiopasdfghjklzxcvbnm'
    return ''.join(random.choice(chars) for i in xrange(length))

def generate(records):
    norm = sum([r[0] for r in records])
    seed = random.random() * norm
    sub_sum = 0.0
    for freq, value in records:
        if sub_sum <= seed and seed <= sub_sum + freq:
            return value
        sub_sum += freq
    return generate(records)

def generate_id(records):
    _records = [(w, i) for i, w in enumerate(records)]
    return generate(_records)

def generate_from_dict(records):
    _records = [(w, i) for i, w in records.iteritems()]
    return generate(_records)

DOMAIN_MASK = re.compile('^(?:(?:https?)://)?(?:www\.)?([^/\s]+)', re.I)
def extract_domain_smart(url):
    return (DOMAIN_MASK.findall(url.strip()) + [''])[0].lower()

def exception_to_str():
    exc_type, exc_value, exc_traceback = sys.exc_info()
    message = "".join(traceback.format_exception(exc_type, exc_value, exc_traceback))
    return message

def random_string(length=8):
    chars = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890"
    return "".join([random.choice(chars) for i in xrange(length)])

def rm(target):
    cmd = 'rm -rf {TARGET}'.format(TARGET=target)
    print >> sys.stderr, cmd
    os.system(cmd)

def cp(src, dst):
    cmd = 'cp -r {SRC} {DST}'.format(SRC=src, DST=dst)
    print >> sys.stderr, cmd
    os.system(cmd)

def ts_to_datetime(ts):
    return datetime.datetime.fromtimestamp(int(ts))

def ts_to_date(ts):
    return datetime.date.fromtimestamp(int(ts))

def datetime_to_ts(dt):
    return int(time.mktime(dt.timetuple()))

def parser_task_hash(params):
    params_list = []
    for p in ['engine', 'type', 'region', 'request', 'page', 'prev_request', 'prefix_depth', 'serp_extra_data', 'max_serp_pos', 'max_direct_pos']:
        if p in params:
            params_list.append(str(params[p]))

    engine_str = '@@'.join(params_list)
    return hashlib.md5(engine_str).hexdigest()

def structure_gen(kernel, level=0):
    sep = re.compile('\s+')
    len_req = sorted((len(sep.split(req)), req) for req in kernel if len(sep.split(req)) >= 2)
    if not len_req:
        if level == 0:
            return {
                'structure': {},
                'attributes': {}
            }
        else:
            return {}

    min_len = len_req[0][0]
    len_req = sorted((len(r), r) for l, r in len_req if l == min_len)

    tree = {}
    _kernel = set(kernel)

    for l, r in len_req:
        group = [req for req in kernel if r in req and r != req]
        if not group: continue
        _kernel = _kernel - set(group)
        tree[r] = {
            'structure': structure_gen(group, level=level+1),
            'attributes': {}
        }

    tree[cfg.other_category] = {
        'structure': sorted(_kernel),
        'attributes': {}
    }

    if level == 0:
        tree = {
            'structure': tree,
            'attributes': {}
        }

    return tree

def reduce_list_by_prefixes(result, percentile=0.05):
    result.sort()

    total_count = len(result)
    threshold = int(total_count*percentile)

    cur_pos = 0
    cur_pref = result[cur_pos]

    groups = []
    for i in xrange(total_count):
        if not result[i].startswith(cur_pref + ' '):
            length = i - cur_pos
            if length > threshold:
                groups.append(cur_pref)

            cur_pos = i
            cur_pref = result[i]

    length = i - cur_pos
    if length > threshold:
        groups.append(cur_pref)

    return groups

class TorWrapper(object):
    TMP = "/tmp"
    TOR_DATA = TMP + "/{port}.tor.datadir"
    TOR_PID = TMP + "/{port}.tor.pid"
    TOR_STDOUT = TMP + "/{port}.tor.stdout"
    PRIVOXY_PID = TMP + "/{port}.privoxy.pid"
    PRIVOXY_CFG = TMP + "/{port}.privoxy.cfg"
    TOR_CMD = "tor SocksPort {port} RunAsDaemon 0 Log info DataDirectory '{datadir}' > '{outfile}' & echo $! > {pidfile}"
    TOR_READY = "Bootstrapped 100%"
    PRIVOXY_CFG_TEMPLATE = "forward-socks4a / 127.0.0.1:{tor_port} .\nlisten-address  127.0.0.1:{privoxy_port}"
    PRIVOXY_CMD = "privoxy --no-daemon '{cfgfile}' > /dev/null 2>/dev/null & echo $! > {pidfile}"

    @staticmethod
    def _tor_version():
        return os.popen("tor --version | awk '/^Tor version/{print $3}'").read().strip()

    @staticmethod
    def port_in_use(port):
        print "Checking port %d..." % port
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.bind(('127.0.0.1', port))
            sock.close()
            return False
        except socket.error, e:
            return True

    @staticmethod
    def generate_free_port(port_range=[1000, 32767]):
        print "Searching for free ports."
        port = random.randint(*port_range)
        while TorWrapper.port_in_use(port):
            port = random.randint(*port_range)
        return port

    @property
    def tor_port(self):
        if not hasattr(self, "_tor_port"):
            self._tor_port = self.generate_free_port()
        return self._tor_port

    @property
    def privoxy_port(self):
        if not hasattr(self, "_privoxy_port"):
            self._privoxy_port = self.generate_free_port()
        return self._privoxy_port

    def _is_tor_ready(self):
        print "Checking if Tor is up..."
        try:
            return self.TOR_READY in open(self.TOR_STDOUT.format(port=self.tor_port)).read()
        except IOError:
            return False

    def _is_privoxy_ready(self):
        print "Checking if Privoxy is up..."
        time.sleep(3)
        return True

    def _run_tor(self):
        print "Creating Tor data directory."
        datadir = self.TOR_DATA.format(port=self.tor_port)

        if self._tor_version() < "0.2.3":
            print "Old Tor."
            user = "debian-tor"
        else:
            print "New Tor."
            user = "root"
        os.system("sudo -u {user} mkdir -p -m 777 '{datadir}'".format(datadir=datadir, user=user))

        print "Starting Tor."
        cmd = self.TOR_CMD.format(
            port=self.tor_port,
            datadir=self.TOR_DATA.format(port=self.tor_port),
            pidfile=self.TOR_PID.format(port=self.tor_port),
            outfile=self.TOR_STDOUT.format(port=self.tor_port)
        )
        os.system(cmd)

    def _run_privoxy(self):
        print "Creating Privoxy config file."
        with open(self.PRIVOXY_CFG.format(port=self.tor_port), "w") as config:
            print >> config,  self.PRIVOXY_CFG_TEMPLATE.format(
                tor_port=self.tor_port,
                privoxy_port=self.privoxy_port
            )

        print "Starting Privoxy."
        cmd = self.PRIVOXY_CMD.format(
            pidfile=self.PRIVOXY_PID.format(port=self.tor_port),
            cfgfile=self.PRIVOXY_CFG.format(port=self.tor_port)
        )
        os.system(cmd)

    @property
    def tor_pid(self):
        try:
            if not hasattr(self, '_tor_pid'):
                print "Getting Tor pid from pid file."
                self._tor_pid = int(open(self.TOR_PID.format(port=self.tor_port)).read().strip())
                return self._tor_pid
            return self._tor_pid
        except IOError:
            return None

    @property
    def privoxy_pid(self):
        try:
            if not hasattr(self, '_privoxy_pid'):
                print "Getting Privoxy pid from pid file."
                self._privoxy_pid = int(open(self.PRIVOXY_PID.format(port=self.tor_port)).read().strip())
                return self._privoxy_pid
            return self._privoxy_pid
        except IOError:
            return None

    def _is_tor_alive(self):
        print "Checking if Tor process exists."
        if self.tor_pid is None:
            return False
        return self._check_pid(self.tor_pid)

    def _is_privoxy_alive(self):
        print "Checking if Privoxy process exists."
        if self.privoxy_pid is None:
            return False
        return self._check_pid(self.privoxy_pid)

    def _clean(self):
        print "Cleaning."
        items = [
            self.TOR_DATA.format(port=self.tor_port),
            self.TOR_PID.format(port=self.tor_port),
            self.TOR_STDOUT.format(port=self.tor_port),
            self.PRIVOXY_CFG.format(port=self.tor_port),
            self.PRIVOXY_PID.format(port=self.tor_port)
        ]
        for i in items:
            if os.path.exists(i):
                print "Removing " + i + "..."
                os.system("rm -rf '%s'" % i)

    @staticmethod
    def _check_pid(pid):
        try:
            os.kill(pid, 0)
            return True
        except OSError:
            return False

    def _kill_tor(self):
        print "Killing Tor."
        while self._is_tor_alive():
            print "Trying to kill Tor by pid..."
            os.kill(self.tor_pid, signal.SIGKILL)
            time.sleep(1)

    def _kill_privoxy(self):
        print "Killing Privoxy."
        while self._is_privoxy_alive():
            print "Trying to kill Privoxy by pid..."
            os.kill(self.privoxy_pid, signal.SIGKILL)
            time.sleep(1)

    @staticmethod
    def wait(condition, timeout):
        ts = time.time()
        while time.time() - ts <= timeout:
            if condition(): return True
            time.sleep(1)
        return False

    def stop(self):
        print "Stopping."
        if hasattr(self, "_cleaned"):
            return self._cleaned

        self._kill_tor()
        self._kill_privoxy()
        self._clean()
        self._cleaned = True
        return self._cleaned

    def __del__(self):
        self.stop()

    def start(self, timeout):
        print "Starting."
        self._run_tor()
        if not self.wait(self._is_tor_alive, timeout):
            self.stop()
            return False
        if not self.wait(self._is_tor_ready, timeout):
            self.stop()
            return False

        self._run_privoxy()
        if not self.wait(lambda : self._is_privoxy_alive, timeout):
            self.stop()
            return False
        if not self.wait(lambda : self._is_privoxy_ready, timeout):
            self.stop()
            return False
        return True

def Tor(replace_only_none_proxy=True, wait_for_tor_ready_timeout=60, require_proxy_kwarg=False):
    def Tor_decorator(func):
        def wrapper(*args, **kwargs):
            if TOR_ON:
                if 'tor_disable' in kwargs and kwargs['tor_disable'] is True:
                    result = func(*args, **kwargs)
                else:
                    if 'proxy' not in kwargs:
                        if require_proxy_kwarg:
                            raise Exception("There is no 'proxy=...' parameter in kwargs!")
                        else:
                            kwargs['proxy'] = None
                    proxy = kwargs['proxy']
                    if replace_only_none_proxy and proxy is not None:
                        return func(*args, **kwargs)

                    while True:
                        try:
                            tw = TorWrapper()
                            if tw.start(wait_for_tor_ready_timeout):
                                break
                        except:
                            tw.stop()

                    if 'tor_pid' in kwargs:
                        kwargs['tor_pid'] = tw.tor_pid


                    kwargs['proxy'] = '127.0.0.1:%d' % tw.privoxy_port

                    try:
                        result = func(*args, **kwargs)
                    except:
                        tw.stop()
                        raise
            else:
                result = func(*args, **kwargs)

            return result
        return wrapper
    return Tor_decorator

def Proxify(replace_only_none_proxy=True, require_proxy_kwarg=False):
    from proxy.models import Proxy
    def Proxify_decorator(func):
        def wrapper(*args, **kwargs):
            if 'proxy' not in kwargs:
                if require_proxy_kwarg:
                    raise Exception("There is no 'proxy=...' parameter in kwargs!")
                else:
                    kwargs['proxy'] = None
            proxy = kwargs['proxy']
            if replace_only_none_proxy and proxy is not None:
                return func(*args, **kwargs)

            p = Proxy.pick_random().ip_port()
            kwargs['proxy'] = p
            result = func(*args, **kwargs)
            return result
        return wrapper
    return Proxify_decorator

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    try:
        s.feed(html)
    except HTMLParseError:
        return ''
    return s.get_data()

def extra_data_downloader(advertising, engine):
    for page in advertising:
        for block in page:
            if block in ['page_number', 'time']:
                continue
            for element in page[block]:
                try:
                    if engine == 'yandex' or 'mail':
                        element['href'] = urllib2.urlopen(element['href']).geturl()
                    if engine == 'google':
                        element['href'] = link_preparator(element['href'])
                except urllib2.HTTPError:
                    pass
                except urllib2.URLError:
                    pass

                if 'sitelinks' in element:
                    for link in element['sitelinks']:
                        try:
                            if engine == 'yandex' or 'mail':
                                link[link.keys()[0]] = urllib2.urlopen(link[link.keys()[0]]).geturl()
                            if engine == 'google':
                                link[link.keys()[0]] = link_preparator(link[link.keys()[0]])
                        except urllib2.HTTPError:
                            pass
                        except urllib2.URLError:
                            pass

                if 'favicon' in element and element['favicon'] <> 'False':
                    try:
                        pic_size = urllib2.urlopen(element['favicon']).info().getheaders("Content-Length")[0]
                        if int(pic_size) == 43 or int(pic_size) == 99:
                            element['favicon'] = 'False'
                        else:
                            element['favicon'] = 'True'
                    except urllib2.HTTPError:
                        pass
                    except urllib2.URLError:
                        pass
    return advertising

def link_preparator(raw_html):
    return raw_html.split('url=')[1]

def slicer(reqs,length):
    sliced_requests = []
    new_request = ''
    while reqs:
        if not len(new_request) + len(reqs[0]) + 1 > length:
            new_request += reqs[0] + ','
            reqs = reqs[1:]
        else:
            sliced_requests.append(new_request[:len(new_request)-1])
            new_request = ''
    if len(new_request):
        sliced_requests.append(new_request[:len(new_request)-1])
    return sliced_requests

def kernel_detailed_cleaner(dic, reqs):
    for counter in xrange(len(reqs)-1):
        for sub in xrange(counter+1, len(reqs)):
            inter = set(dic[reqs[counter]]).intersection(set(dic[reqs[sub]]))
            if inter:
                for item in list(inter):
                    dic[reqs[sub]].remove(item)
    return dic

def punct_remover(str):
    punct_list = list(string.punctuation)
    for item in punct_list:
        str = str.replace(item, ' ')
    return str.split()

def DeleteDir(dir):
    for name in os.listdir(dir):
        file = os.path.join(dir, name)
        if not os.path.islink(file) and os.path.isdir(file):
            DeleteDir(file)
        else:

            os.remove(file)
    os.rmdir(dir)

def reduce_link(link):
    return re.sub('^(https?:\/\/)?(www\.)?', '', re.sub('\/$', '', link))

def link_normalizer(words):
    initial_words = words
    if not isinstance(words, list): words = [words]
    new_list = []
    for word in words:
        word = reduce_link(word)
        word = word.lower()
        word = 'http://www.' + word
        try:
            word = word.encode('idna')
        except UnicodeError:
            pass
        new_list.append(word)
    if not isinstance(initial_words, list): new_list = new_list[0]
    return new_list

def check_if_parsed(check_list, mode, Parser, cache_timeout, check_errors=False, **kwargs):
    if 'h' in cache_timeout:
        period = datetime.datetime.now()-timedelta(hours=int(cache_timeout.replace('h','')))
    elif 'd' in cache_timeout:
        period = datetime.datetime.now()-timedelta(int(cache_timeout.replace('d','')))
    else:
        period = datetime.datetime.now()-timedelta(int(cache_timeout))
    list_to_parse = []
    if not isinstance(check_list, list): check_list = [check_list]

    for item in check_list:
        if mode == 'screenshot':
            hash = parser_task_hash({
                'type': Parser.MODE_SUGGEST_SCREENSHOT,
                'request': item.encode('utf-8'),
                'region': kwargs['region'].name_en,
                'engine': kwargs['engine'],
            })
            parser_type = Parser.MODE_SUGGEST_SCREENSHOT

        if mode == 'crawler':
            hash = parser_task_hash({
                'type': Parser.MODE_CRAWLER_NOJS,
                'request': item.encode('utf-8'),
            })
            parser_type = Parser.MODE_CRAWLER_NOJS

        if mode == 'budget':
            hash = parser_task_hash({
                'type': Parser.MODE_BUDGET,
                'request': item.encode('utf-8'),
                'region': kwargs['region'].name_en,
            })
            parser_type = Parser.MODE_BUDGET

        if mode == 'direct':
            hash = parser_task_hash({
                'engine': kwargs['engine'],
                'type': Parser.MODE_DIRECT,
                'request': item.encode('utf-8'),
                'region': kwargs['region'].name_en,
                'max_direct_pos': kwargs['max_direct_pos'],
            })
            parser_type = Parser.MODE_DIRECT

        if mode == 'crawler':
            hash = parser_task_hash({
                            'type': Parser.MODE_CRAWLER_NOJS,
                            'request': item.encode('utf-8'),
                            })
            parser_type = Parser.MODE_CRAWLER_NOJS

        if mode == 'serp':
            hash = parser_task_hash({
                'engine': kwargs['engine'],
                'type': Parser.MODE_SERP,
                'request': item.encode('utf-8'),
                'region': kwargs['region'].name_en,
                'serp_extra_data': kwargs['serp_extra_data'],
                'max_serp_pos': kwargs['max_serp_pos'],
            })
            parser_type = Parser.MODE_SERP

        if mode == 'wordstat':
            for page in xrange(kwargs['maxpage']):
                hash = parser_task_hash({
                    'engine': kwargs['engine'],
                    'type': Parser.MODE_WORDSTAT_WORDS,
                    'request': item.encode('utf-8'),
                    'region': kwargs['region'].name_en,
                    'page': page + 1
                })
                parser_type = Parser.MODE_WORDSTAT_WORDS
                parse_result = Parser.objects.filter(
                    mode=parser_type,
                    operation=hash,
                    created__gt=period)
                if not parse_result:
                    list_to_parse.append({
                        item: page + 1
                    })
            continue

        parse_result = Parser.objects.filter(
            mode=parser_type,
            operation=hash,
            created__gt=period)
        if not parse_result:
            list_to_parse.append(item)
            continue
        if check_errors and 'error' in parse_result.latest('created').parser_data.result.keys():
            list_to_parse.append(item)
    return list_to_parse

def string_maker(items):
    new_req = ''
    for item in items:
        new_req += item
        new_req += ','
    new_req = new_req[:len(new_req)-1]
    return new_req

def budget_checker(items, region, Parser):
    list_to_parse = []
    for item in items:
        hash = parser_task_hash({
            'type': Parser.MODE_BUDGET,
            'request': item.encode('utf-8'),
            'region': region.name_en,
        })

        parse_result = Parser.objects.filter(
            mode=Parser.MODE_BUDGET,
            operation=hash).latest('created').parser_data.result
        if parse_result['budget'] == 'error':
            list_to_parse.append(item)
    return list_to_parse

def punct_remover_str(str):
    punct_list = list(string.punctuation)
    for item in punct_list:
        str = str.replace(item, ' ')
    return str

class KWExtractor(object):
    SPACE = re.compile(u'\s+')
    WEIGHTS = {
        'title': 2.0,
        'description': 0.5,
        'keywords': 0.5,
        'h1': 1.0,
        'h2': 0.7,
        'h3': 0.5,
        'a_text': 0.6,
        'strong': 0.5,
        'b': 0.5,
        'em': 0.5,
        'i': 0.5,
        'alt': 0.4,
        'a_title': 0.5,
        'parent_a_text': 0.5,
        'parent_a_title': 0.5
    }

    def __init__(self, MORPH):
        self.tags = set()
        self.MORPH = MORPH

    def push_item(self, tag, text):
        self.tags.add((tag, text))

    def _iter_variants_helper(self):
        for tag, text in self.tags:
            text = punct_remover_str(text)
            words = self.SPACE.split(text.strip())
            norm_words = self.MORPH.norm_to_list(text)
            assert len(words) == len(norm_words)
            for start_pos in xrange(len(words)):
                for length in xrange(len(words) - start_pos):
                    _words = words[start_pos:start_pos + length + 1]
                    _norm_words_key = u' '.join(norm_words[start_pos:start_pos + length + 1])
                    yield (_norm_words_key, tag), _words

    def _iter_variants(self):
        for (i0, i1), i2 in dict(self._iter_variants_helper()).iteritems():
            yield i0, self.WEIGHTS.get(i1, 0.0), i2

    def extract(self):
        agg_kw = defaultdict(list)
        for norm_words_key, weight, words in self._iter_variants():
            agg_kw[norm_words_key].append((weight, len(words), words))

        max_func = lambda i: (sum(map(itemgetter(0), i)), i[0][1])
        max_words = max(agg_kw.itervalues(), key=max_func)
        max_word = max(max_words, key=itemgetter(0))[2]
        return self.MORPH.norm_to_string(u' '.join(max_word)).lower(), self.MORPH

def split_list(somelist, parts):
    assert parts > 0
    splited = []
    _somelist = somelist[:]
    avg_part_len = float(len(somelist)) / parts
    int_part_len = int(avg_part_len)
    float_part_len = avg_part_len - int_part_len
    for part_id in xrange(parts - 1):
        part_len = int_part_len + int(random.random() < float_part_len)
        part = _somelist[:part_len]
        _somelist = _somelist[part_len:]
        splited.append(part)
    splited.append(_somelist)
    return splited

def split_int(someint, parts):
    somelist = [None] * someint
    splited = split_list(somelist, parts)
    return map(lambda part: len(part), splited)

def sublist_exists(somelist, sublist):
    sublen = len(sublist)
    for i in xrange(len(somelist) - sublen + 1):
        if somelist[i:i + sublen] == sublist:
            return True
    return False

def remove_subset(somelist, subset):
    return filter(lambda item: item not in subset, somelist)

def join_lists(list_of_lists):
    return list(itertools.chain.from_iterable(list_of_lists))

def sanitize_request4budget(request):
    request = request.replace('/', ' ').replace('_', '').replace(' ! ', ' ')
    if '-' in request and not ('- ' in request or ' -' in request or ' - ' in request):
        request = request.replace('-', ' ')
    request = request.replace('+','')
    request = ' '.join(request.split())
    request = re.sub('\+$', '', request)
    return request

def formatted_reqs_dict_maker(reqs):
    reqs_dict = {}
    for req in reqs:
        reqs_dict[req] = sanitize_request4budget(req)
    return reqs_dict

def formatted_reqs_list_maker(reqs):
    formatted_reqs = []
    for req in reqs:
        formatted_reqs.append(sanitize_request4budget(req))
    return formatted_reqs

class CachingMorphAnalyzer(MorphAnalyzer):
    _cache_parse = {}
    def parse(self, word):
        if word in self._cache_parse:
            return self._cache_parse[word]
        self._cache_parse[word] = super(self.__class__, self).parse(word)
        return self._cache_parse[word]
    def word_normalizer(self, word):
        morph_word = self.parse(word)
        if morph_word:
            return morph_word[0].normal_form
        else:
            return word

class AlphabetChecker:
    def __init__(self):
        alphabet = {
            'en': 'abcdefghigklmnopqrstuvwxyz',
            'ru': u'абвгдеёжзийклмнопрстуфхцчшщъыьэюя',
            'ua': u'абвгґдеєжзиіїйклмнопрстуфхцчшщьюя',
            'kz': u'аәбвгғдеёжзийкқлмнңоөпрстуұүфхһцчшщъыіьэюя',
            'tr': u'abcçdefgğhıijklmnoöprsştuüvyz',
            'decimal': '1234567890',
            'symbols': ' "-+!,.',
        }
        self.alphabet_sets = {}
        for lang, alph in alphabet.iteritems():
            self.alphabet_sets[lang] = set(alph)
        budget_parser_langs = ['en', 'ru', 'ua', 'kz', 'tr', 'decimal', 'symbols']
        bp_alphabet_str = ''
        for lang in budget_parser_langs:
            bp_alphabet_str += alphabet[lang]
        self.alphabet_sets['budget_parser'] = set(bp_alphabet_str)

    def check(self, string, lang):
        string = string.lower()
        if lang in self.alphabet_sets:
            diff = set(string) - self.alphabet_sets[lang]
            if not diff:
                return True, False
            else:
                return False, diff

def keys_finder(dct, item):
    keys = []
    for k, v in dct.iteritems():
        if v == item: keys.append(k)
    return keys

class HashableDict(dict):
    __hash__ = lambda self: hash(str(self))
    pass

class BudgetRequestsChecker:
    def __init__(self, reqs, reqs_dict):
        self.reqs = reqs
        self.reqs_dict = reqs_dict
        self.bad_reqs = defaultdict(set)
        self.morph = CachingMorphAnalyzer()

    def _symbols_checker(self):
        alphabet_checker = AlphabetChecker()
        for item in self.reqs:
            result = alphabet_checker.check(item, 'budget_parser')
            if result[0] is False:
                for key in keys_finder(self.reqs_dict, item):
                    self.bad_reqs[key].add(HashableDict(error_type='unsupported symbols', error_value=list(result[1])))

    def _length_checker(self):
        for item in self.reqs:
            if item.startswith('"') and item.endswith('"'):
                length = len(punct_remover(item))
                if length > 7:
                    for key in keys_finder(self.reqs_dict, item):
                        self.bad_reqs[key].add(HashableDict(error_type='too long', error_value=length))
            else:
                item_list = punct_remover(item)
                for word in item_list[:]:
                    morph_word = self.morph.parse(word)
                    if morph_word:
                        if morph_word[0].tag.POS in ['PREP', 'CONJ', 'PRCL']: item_list.remove(word)
                if len(item_list) > 7:
                    for key in keys_finder(self.reqs_dict, item):
                        self.bad_reqs[key].add(HashableDict(error_type='too long', error_value=len(item_list)))

    def _punct_checker(self):
        for item in self.reqs:
            for simbols in [' -', '- ', ' - ']:
                if simbols in item:
                    for key in keys_finder(self.reqs_dict, item):
                        self.bad_reqs[key].add(HashableDict(error_type='unsupported symbols', error_value=simbols))

    def _set_to_list(self, dct):
        new = {}
        for k, v in dct.iteritems():
            new[k] = list(v)
        return new

    def check(self):
        self._symbols_checker()
        self._length_checker()
        self._punct_checker()
        return self._set_to_list(self.bad_reqs)

def bad_reqs_save(bad_reqs, Parser, region, engine):
    for k, v in bad_reqs.iteritems():
        oper_hash = parser_task_hash({
            'type': Parser.MODE_BUDGET,
            'request': k.encode('utf-8'),
            'region': region.name_en,
        })
        final_result = {
            "request": k,
            "budget": 'error',
            "error": v,
            'formatted_request': sanitize_request4budget(k)
        }
        Parser.add_and_save(Parser.MODE_BUDGET, oper_hash, k, region, engine, final_result)

def screenshot_bad_reqs_save(bad_reqs, engine, region, Parser):
    for item in bad_reqs:
        oper_hash = parser_task_hash({
            'engine': engine,
            'type': Parser.MODE_SUGGEST_SCREENSHOT,
            'request': item.encode('utf-8'),
            'region': region.name_en,
        })
        final_result = {
            "success": True,
            "created": datetime_to_ts(datetime.datetime.now()),
            "request": item,
            "region": region.name_en,
            "error": 'no suggest',
        }
        Parser.add_and_save(Parser.MODE_SUGGEST_SCREENSHOT, oper_hash, item, region, engine, final_result)

def bad_reqs_remover(reqs, bad_reqs):
    for k in bad_reqs.iterkeys():
        reqs.remove(sanitize_request4budget(k))
    return reqs

def alert_text_parser(alert_text):
    errors_dict = {}
    alert_text = alert_text.replace('\n','')
    for item in alert_text.split(';'):
        item = item.decode('utf8')
        if item.endswith(')'):
            pattern = re.compile('\((.*?)\)')
            request = re.findall(pattern, item)[-1] if re.findall(pattern, item) else False
            if request:
                error = item.replace('(' + request + ')', '').strip()
                errors_dict[request] = error
            continue
        else:
            request = item[item.find(u'в ключевой фразе') + 18:len(item)-1]
            if request:
                error = item.replace('"' + request + '"', '').strip()
                errors_dict[request] = error

    return errors_dict

def reduce_link(link):
    return re.sub('^(https?:\/\/)?(www\.)?', '', re.sub('\/$', '', link))

def site_url_normalizer(words):
    initial_words = words
    if not isinstance(words, list): words = [words]
    new_list = []
    for word in words:
        word = reduce_link(word)
        word = word.lower()
        new_list.append(word.strip(u'\n\r\f\t\u200e'))
    if not isinstance(initial_words, list): new_list = new_list[0]
    return new_list

def values_sum(dct):
    lst = []
    for v in dct.itervalues():
        lst += v
    return lst

def req2norm(items, morph):
    norm_list = []
    dct = {}
    for item in items:
        norm_item = []
        for sub in punct_remover(item):
            word = morph.word_normalizer(sub)
            norm_item.append(word)
            dct[sub] = word
        norm_list.append(norm_item)
    return morph, norm_list, dct

def extract_first_domain(url):
    domains = re.findall(ur'^(?:(?:https?:)?//)?(?:www\.)?([^/?#\s]*)', url.lower())
    return domains[0].split('.')[-2] + '.' + domains[0].split('.')[-1]

def translit(locallangstring):
    conversion_one = {
        u'\u0410': 'A', u'\u0430': 'a',
        u'\u0411': 'B', u'\u0431': 'b',
        u'\u0412': 'V', u'\u0432': 'v',
        u'\u0413': 'G', u'\u0433': 'g',
        u'\u0414': 'D', u'\u0434': 'd',
        u'\u0415': 'E', u'\u0435': 'e',
        u'\u0401': 'Yo', u'\u0451': 'yo',
        u'\u0416': 'Zh', u'\u0436': 'zh',
        u'\u0417': 'Z', u'\u0437': 'z',
        u'\u0418': 'I', u'\u0438': 'i',
        u'\u0419': 'Y', u'\u0439': 'y',
        u'\u041a': 'K', u'\u043a': 'k',
        u'\u041b': 'L', u'\u043b': 'l',
        u'\u041c': 'M', u'\u043c': 'm',
        u'\u041d': 'N', u'\u043d': 'n',
        u'\u041e': 'O', u'\u043e': 'o',
        u'\u041f': 'P', u'\u043f': 'p',
        u'\u0420': 'R', u'\u0440': 'r',
        u'\u0421': 'S', u'\u0441': 's',
        u'\u0422': 'T', u'\u0442': 't',
        u'\u0423': 'U', u'\u0443': 'u',
        u'\u0424': 'F', u'\u0444': 'f',
        u'\u0425': 'H', u'\u0445': 'h',
        u'\u0426': 'Ts', u'\u0446': 'ts',
        u'\u0427': 'Ch', u'\u0447': 'ch',
        u'\u0428': 'Sh', u'\u0448': 'sh',
        u'\u0429': 'Sch', u'\u0449': 'sch',
        u'\u042a': '"', u'\u044a': '"',
        u'\u042b': 'Y', u'\u044b': 'y',
        u'\u042c': '\'', u'\u044c': '',
        u'\u042d': 'E', u'\u044d': 'e',
        u'\u042e': 'Yu', u'\u044e': 'yu',
        u'\u042f': 'Ya', u'\u044f': 'ya',
        u' ': '_',
    }
    conversion_two = {
        u'\u0410': 'A', u'\u0430': 'a',
        u'\u0411': 'B', u'\u0431': 'b',
        u'\u0412': 'V', u'\u0432': 'v',
        u'\u0413': 'G', u'\u0433': 'g',
        u'\u0414': 'D', u'\u0434': 'd',
        u'\u0415': 'E', u'\u0435': 'e',
        u'\u0401': 'Yo', u'\u0451': 'yo',
        u'\u0416': 'Zh', u'\u0436': 'zh',
        u'\u0417': 'Z', u'\u0437': 'z',
        u'\u0418': 'I', u'\u0438': 'i',
        u'\u0419': 'Y', u'\u0439': 'y',
        u'\u041a': 'K', u'\u043a': 'k',
        u'\u041b': 'L', u'\u043b': 'l',
        u'\u041c': 'M', u'\u043c': 'm',
        u'\u041d': 'N', u'\u043d': 'n',
        u'\u041e': 'O', u'\u043e': 'o',
        u'\u041f': 'P', u'\u043f': 'p',
        u'\u0420': 'R', u'\u0440': 'r',
        u'\u0421': 'C', u'\u0441': 'c',
        u'\u0422': 'T', u'\u0442': 't',
        u'\u0423': 'U', u'\u0443': 'u',
        u'\u0424': 'F', u'\u0444': 'f',
        u'\u0425': 'H', u'\u0445': 'h',
        u'\u0426': 'Ts', u'\u0446': 'ts',
        u'\u0427': 'Ch', u'\u0447': 'ch',
        u'\u0428': 'Sh', u'\u0448': 'sh',
        u'\u0429': 'Sch', u'\u0449': 'sch',
        u'\u042a': '"', u'\u044a': '"',
        u'\u042b': 'Y', u'\u044b': 'y',
        u'\u042c': '\'', u'\u044c': '',
        u'\u042d': 'E', u'\u044d': 'e',
        u'\u042e': 'Yu', u'\u044e': 'yu',
        u'\u042f': 'Ya', u'\u044f': 'ya',
        u' ': '_',
    }
    translitstring_one = []
    translitstring_two = []
    for c in locallangstring:
        translitstring_one.append(conversion_one.setdefault(c, c))
        translitstring_two.append(conversion_two.setdefault(c, c))
    return set([''.join(translitstring_one), ''.join(translitstring_two)])

def email_from_text_extractor(text):
    return list(set(re.findall(r'[\w\.-]+@[\w\.-]+', text)))

class IpWrapper(object):
    TMP = "/tmp"
    PRIVOXY_PID = TMP + "/{port}.privoxy.pid"
    PRIVOXY_CFG = TMP + "/{port}.privoxy.cfg"
    PRIVOXY_CFG_TEMPLATE = "forward-socks5 / xmoney.info:{xmoney_port} .\nlisten-address  127.0.0.1:{privoxy_port}"
    PRIVOXY_CMD = "privoxy --no-daemon '{cfgfile}' > /dev/null 2>/dev/null & echo $! > {pidfile}"

    @staticmethod
    def port_in_use(port):
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.bind(('127.0.0.1', port))
            sock.close()
            return False
        except socket.error, e:
            return True

    @staticmethod
    def generate_free_port(port_range=[1000, 32767]):
        port = random.randint(*port_range)
        while IpWrapper.port_in_use(port):
            port = random.randint(*port_range)
        return port

    @property
    def privoxy_port(self):
        if not hasattr(self, "_privoxy_port"):
            self._privoxy_port = self.generate_free_port()
        return self._privoxy_port

    def _is_privoxy_ready(self):
        time.sleep(3)
        return True

    def _run_privoxy(self):
        with open(self.PRIVOXY_CFG.format(port=self.xmoney_port), "w") as config:
            print >> config,  self.PRIVOXY_CFG_TEMPLATE.format(
                xmoney_port=self.xmoney_port,
                privoxy_port=self.privoxy_port
            )

        cmd = self.PRIVOXY_CMD.format(
            pidfile=self.PRIVOXY_PID.format(port=self.xmoney_port),
            cfgfile=self.PRIVOXY_CFG.format(port=self.xmoney_port)
        )
        os.system(cmd)

    @property
    def privoxy_pid(self):
        try:
            if not hasattr(self, '_privoxy_pid'):
                self._privoxy_pid = int(open(self.PRIVOXY_PID.format(port=self.xmoney_port)).read().strip())
                return self._privoxy_pid
            return self._privoxy_pid
        except IOError:
            return None

    def _is_privoxy_alive(self):
        if self.privoxy_pid is None:
            return False
        return self._check_pid(self.privoxy_pid)

    def _clean(self):
        items = [
            self.PRIVOXY_CFG.format(port=self.xmoney_port),
            self.PRIVOXY_PID.format(port=self.xmoney_port)
        ]
        for i in items:
            if os.path.exists(i):
                os.system("rm -rf '%s'" % i)

    @staticmethod
    def _check_pid(pid):
        try:
            os.kill(pid, 0)
            return True
        except OSError:
            return False

    def _kill_privoxy(self):
        while self._is_privoxy_alive():
            os.kill(self.privoxy_pid, signal.SIGKILL)
            time.sleep(1)

    @staticmethod
    def wait(condition, timeout):
        ts = time.time()
        while time.time() - ts <= timeout:
            if condition(): return True
            time.sleep(1)
        return False

    def stop(self):
        if hasattr(self, "_cleaned"):
            return self._cleaned
        self._kill_privoxy()
        self._clean()
        self._cleaned = True
        return self._cleaned

    def __del__(self):
        self.stop()

    def _get_ip(self):
        while True:
            resp, region = get_ip_response()
            if not region in REGIONS_STOP_LIST:
                self.xmoney_port = resp['port']
                return

    def start(self, timeout):
        self._get_ip()
        self._run_privoxy()
        if not self.wait(lambda: self._is_privoxy_alive, timeout):
            self.stop()
            return False
        time.sleep(5)
        return True

def Ip(replace_only_none_proxy=True, wait_for_ip_ready_timeout=60, require_proxy_kwarg=False):
    def Ip_decorator(func):
        def wrapper(*args, **kwargs):
            if IP_ON:
                if 'proxy' not in kwargs:
                    if require_proxy_kwarg:
                        raise Exception("There is no 'proxy=...' parameter in kwargs!")
                    else:
                        kwargs['proxy'] = None
                proxy = kwargs['proxy']
                if replace_only_none_proxy and proxy is not None:
                    return func(*args, **kwargs)

                while True:
                    try:
                        iw = IpWrapper()
                        if iw.start(wait_for_ip_ready_timeout):
                            break
                    except:
                        iw.stop()

                kwargs['proxy'] = '127.0.0.1:%d' % iw.privoxy_port

                try:
                    result = func(*args, **kwargs)
                except:
                    iw.stop()
                    raise
            else:
                result = func(*args, **kwargs)
            return result
        return wrapper
    return Ip_decorator

def get_ip_response():
    data = urllib.urlencode({'key': 'qi4dIuPv3jXI1APVDtKiUwGFLgjphCS1GXW8rYbIZka2wWuAmJjIW3n9P3FdG2DtVfICtCKlVBaJqrtT'})
    req = urllib2.Request('http://xmoney.info/get_used_port', data)
    resp = urllib2.urlopen(req).read()
    resp = json.loads(resp)
    region = resp['region']
    return resp, region

def get_ip():
    while True:
        resp, region = get_ip_response()
        if not region in REGIONS_STOP_LIST:
            return resp['port']

class PycurlBuffer:
   def __init__(self):
       self.contents = ''

   def body_callback(self, buf):
       self.contents = self.contents + buf

def check_phrases_equal(first, second):
    _second = second[:]
    _first = first[:]
    for word in first:
        if word in _second:
            _second.remove(word)
            _first.remove(word)
    if not _first and not _second:
        return True

def check_phrase_in_list(lst, phrase):
    phrase = punct_remover(phrase)
    for item in lst:
        if check_phrases_equal(phrase, punct_remover(item)):
            return True