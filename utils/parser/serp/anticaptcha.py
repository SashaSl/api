# -*- coding: utf-8 -*-

################################################################################

import os
import time
import config
import urllib
import httplib

from local_utils import random_string
from parser.models import CaptchaMonitoring

################################################################################

def recognize_captcha_url(captcha_url, is_russian=False, tag=None):
    captcha_image = config.TMP_DIR + "/" + random_string(16) + ".jpeg"
    urllib.urlretrieve(captcha_url, captcha_image)
    captcha_text = recognize_captcha(captcha_image, is_russian, tag=tag)
    os.remove(captcha_image)
    return captcha_text

################################################################################

def recognize_captcha(captcha_image, is_russian=False, tag=None):
    if tag:
        CaptchaMonitoring.objects.create(task_name=tag)
    status, captcha_text = _recognize_captcha(captcha_image, is_russian)
    if status != "OK":
        os.remove(captcha_image)

        class CaptchaFailed(Exception):
            pass

        raise CaptchaFailed('Captcha failed!')
    return captcha_text

################################################################################

def _recognize_captcha(captcha_image, is_russian=False):
    key = config.ANTI_CAPTCHA_KEY
    cap_id = __send_captcha_image(key, captcha_image, is_russian)
    time.sleep(config.ANTI_CAPTCHA_TIMEOUT)
    res_url = config.ANTI_CAPTCHA_API_URL
    res_url+= "?" + urllib.urlencode({'key': key, 'action': 'get', 'id': cap_id})
    while 1:
        res= urllib.urlopen(res_url).read()
        if res == config.ANTI_CAPTCHA_NOT_READY:
            time.sleep(1)
            continue
        break

    res= res.split('|')
    if len(res) == 2:
        return tuple(res)
    else:
        return ('ERROR', res[0])

################################################################################

def __send_captcha_image(key, captcha_image, is_russian=False):
    data = open(captcha_image, 'rb').read()
    boundary = config.ANTI_CAPTCHA_BOUNDARY
    body = '''--%s
Content-Disposition: form-data; name="method"

post
--%s
Content-Disposition: form-data; name="key"

%s
--%s
Content-Disposition: form-data; name="is_russian"

%s
--%s
Content-Disposition: form-data; name="file"; filename="capcha.jpg"
Content-Type: image/pjpeg

%s
--%s--

''' % (boundary, boundary, key, boundary, str(int(is_russian)), boundary, data, boundary)

    headers = {'Content-type' : 'multipart/form-data; boundary=%s' % boundary}
    h = httplib.HTTPConnection(config.ANTI_CAPTCHA_DOMAIN)
    h.request(config.ANTI_CAPTCHA_SEND_METHOD, config.ANTI_CAPTCHA_SEND_URL, body, headers)
    resp = h.getresponse()
    data = resp.read()
    h.close()
    if resp.status == 200:
        cap_id= int(data.split('|')[1])
        return cap_id
    else:
        return False

################################################################################
