from django.contrib import admin
from models import *


class DirectApiUserAdmin(admin.ModelAdmin):
    list_display = [
        '__unicode__',
        'id',
        'login',
        'password',
        'limit_reached_at',
        'first_usage_date',
        'success_counter',
        'fail_counter',
        'last_fail_date',
        'last_success_date',
        'is_used'
    ]

admin.site.register(User, DirectApiUserAdmin)
