# -*- coding: utf-8 -*-
import json

import cfg
from utils.api import check_api_key, wrap_json, wrap_does_not_exist, wrap_exception, check_and_pass_common
import tasks as yandex_tasks
from models import *
from utils.common import datetime_to_ts
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist


@csrf_exempt
@wrap_json
@check_api_key("POST", _json=True)
@wrap_exception
@check_and_pass_common("POST", _json=True, queue=True, date1=True, date2=True, counter_name=True, metrika_method=True)
def api_metrika_get_report(request, *args, **kwargs):
    queue = kwargs['queue']
    date1 = kwargs['date1']
    date2 = kwargs['date2']
    counter_name = kwargs['counter_name']
    metrika_method = kwargs['metrika_method']
    metrika_params = {
        'counter_name': counter_name,
        'date1': date1,
        'date2': date2,
    }
    task_id = yandex_tasks.api_metrika_get_report.apply_async(kwargs={'metrika_params': metrika_params, 'metrika_method': metrika_method}, queue=queue).task_id
    return {
        "success": True,
        "task_id": task_id
    }


@csrf_exempt
@wrap_json
@check_api_key("POST", _json=True)
@wrap_exception
@wrap_does_not_exist
def api_metrika_update_counters(request):
    task_id = yandex_tasks.api_metrika_update_counters.apply_async(queue='urgent').task_id
    return {
        "success": True,
        "task_id": task_id
    }
    
    
@csrf_exempt
@wrap_json
@check_api_key("GET")
@wrap_exception
@wrap_does_not_exist
@check_and_pass_common("GET", login=True, password=True, timestamp=True)
def sertificate_get(request, *args, **kwargs):
    login = kwargs['login']
    password = kwargs['password']
    try:
        yandex_user = YandexUser.objects.get(login=login, password=password)
        sertificate_path = yandex_user.sertificate_path
        success = True
    except ObjectDoesNotExist:
        sertificate_path = None
        success = False
    return {
        "success": success,
        "sertificate_path": sertificate_path
    }


@csrf_exempt
@wrap_json
@check_api_key("POST")
@wrap_exception
@check_and_pass_common("POST", login=True, password=True)
def sertificate_parse(request, *args, **kwargs):
    login = kwargs['login']
    password = kwargs['password']
    task = yandex_tasks.download_sertificate.apply_async(args=[login, password], kwargs=None, queue='default')
    task_id = task.task_id

    return {
        "success": True,
        "task_id": task_id
    }


@csrf_exempt
@wrap_json
@check_api_key("POST", _json=True)
@wrap_exception
@check_and_pass_common("POST", _json=True, queue=True, cache_timeout=True, extended=True)
def frequency_parse(request, *args, **kwargs):
    reqs = json.loads(request.body)['request']
    region = kwargs['region']
    queue = kwargs['queue']
    cache_timeout = kwargs['cache_timeout']
    task = yandex_tasks.parser_frequency.apply_async(args=[reqs, region], kwargs={'queue':queue, 'cache_timeout': cache_timeout}, queue=queue)
    task_id = task.task_id

    return {
        "success": True,
        "task_id": task_id
    }


@csrf_exempt
@wrap_json
@check_api_key("POST", _json=True)
@wrap_exception
@check_and_pass_common("POST", _json=True)
def frequency_get(request, *args, **kwargs):
    final_result = {
        'result': [],
    }
    reqs = json.loads(request.body)['request']
    region = kwargs['region']
    for req in reqs:
        frequency = SearchRequestBudget.objects.filter(
            region__name_en=region.name_en,
            search_request__phrase=req)[0].shows
        final_result['result'].append({
            'request': req,
            'frequency': frequency,
        })

    final_result["timestamp"] = datetime_to_ts(datetime.datetime.now())
    final_result['success'] = True

    return final_result


@csrf_exempt
@wrap_json
@check_api_key("POST", _json=True)
@wrap_exception
@check_and_pass_common("POST", _json=True, queue=True, date1=True, date2=True, counter_name=True, counter_id=True, denial=True)
def minus_words_metrika_get_report(request, *args, **kwargs):
    queue = kwargs['queue']
    date1 = kwargs['date1']
    date2 = kwargs['date2']
    counter_name = kwargs['counter_name']
    counter_id = kwargs['counter_id']
    denial = kwargs['denial']
    metrika_method = 'direct_summary'
    metrika_params = {
        'counter_name': counter_name,
        'id': counter_id,
        'date1': date1,
        'date2': date2,
    }
    task_id = yandex_tasks.minus_words_metrika.apply_async(kwargs={'metrika_params': metrika_params, 'metrika_method': metrika_method, 'denial': denial, 'queue': queue}, queue=queue).task_id
    return {
        "success": True,
        "task_id": task_id
    }


@csrf_exempt
@wrap_json
@check_api_key("POST", _json=True)
@wrap_exception
@check_and_pass_common("POST", _json=True, queue=True)
def api_direct_process(request, *args, **kwargs):
    queue = kwargs['queue']
    params = json.loads(request.body)
    params['queue'] = queue
    task = yandex_tasks.api_direct_process.apply_async(kwargs=params,
                                                       queue=queue)
    task_id = task.task_id

    return {
        "success": True,
        "task_id": task_id
    }

@csrf_exempt
@wrap_json
@check_api_key("POST", _json=True)
@wrap_exception
@check_and_pass_common("POST", _json=True)
def get_accounts_info(request, *args, **kwargs):
    agent_data = {}
    client_list = []
    all_accounts = YandexUser.objects.all()
    for account in all_accounts:
        if account.login not in cfg.agent_logins:
            if account.agent_login:
                agent_login = account.agent_login.login
                if agent_login in cfg.agent_logins:
                    agent_data[agent_login] = agent_data.get(agent_login, []) + [account.login]
                else:
                    client_list.append(account.login)
    agent_list = []
    for agent_login, client_data in agent_data.iteritems():
        agent_list.append({
            'login': agent_login,
            'client': client_data
        })
    return {
        'agent': agent_list,
        'client': client_list
    }


@csrf_exempt
@wrap_json
@check_api_key("POST", _json=True)
@wrap_exception
@check_and_pass_common("POST", login=True, password=True)
def get_campaign_list(request, *args, **kwargs):
    params = json.loads(request.body)
    login = params['login']
    yandex_user = YandexUser.objects.get(login=login)
    task = yandex_tasks.direct_account_info.apply_async(args=[yandex_user], kwargs=None, queue='default')
    task_id = task.task_id
    return {
        "success": True,
        "task_id": task_id
    }


@csrf_exempt
@wrap_json
@check_api_key("POST", _json=True)
@wrap_exception
@check_and_pass_common("POST", login=True, password=True)
def add_new_direct_client(request, *args, **kwargs):
    """
        Создаём нового клиента или отвечаем что такой уже есть
        Проверки на то валиден ли логин и пароль сейчас нет
    """
    params = json.loads(request.body)
    login = params['login']
    password = params['password']
    yandex_user, created = YandexUser.objects.get_or_create(login=login, password=password)
    yandex_user.agent_login = yandex_user
    yandex_user.save()
    task = yandex_tasks.get_sertificate_from_api.apply_async(args=[login, password], kwargs=None, queue='default')
    return {
        "success": True,
        "created": created
    }


@csrf_exempt
@wrap_json
@check_api_key("POST", _json=True)
@wrap_exception
@check_and_pass_common("POST", login=True, password=True)
def create_direct_report(request, *args, **kwargs):
    params = json.loads(request.body)
    login = params['login']
    id_list = params['id_list']
    start_date = params['start_date']
    end_date = params['end_date']
    yandex_user = YandexUser.objects.get(login=login)
    agent_login = yandex_user.agent_login.login
    agent_password = yandex_user.agent_login.password
    task = yandex_tasks.direct_report.apply_async(args=[id_list, agent_login, agent_password, start_date,  end_date], kwargs=None, queue='default')
    task_id = task.task_id
    return {
        "success": True,
        "task_id": task_id
    }


@csrf_exempt
@wrap_json
@check_api_key("POST", _json=True)
@wrap_exception
@check_and_pass_common("POST", login=True)
def get_clients_units(request, *args, **kwargs):
    """
        Возвращает количество баллов у пользователя с переданным login
    """
    params = json.loads(request.body)
    login = params['login']
    try:
        units = YandexUser.objects.get(login=login).units_rest
        result = {
            'units': units,
            'errors': None,
        }
        if units:
            result['status'] = 'success'
        else:
            result['status'] = 'pending'

    except ObjectDoesNotExist:
        result = {
            'status': 'failure',
            'units': None,
            'errors': 'Login not found',
        }

    return result