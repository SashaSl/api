#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os

ROOT_DIR = os.path.dirname(__file__) + '/..'
ROOT_DIR = os.path.abspath(ROOT_DIR)
sys.path.append(ROOT_DIR)

from utils.linguistics import Morph

assert __name__ == '__main__'

morph = Morph()

with open(sys.argv[-2]) as f, open(sys.argv[-1], 'w') as of:
    for line in f:
        line = line.strip().decode('utf-8')
        if not line: continue
        fields = line.split(u'\t')
        new_fields = []
        for field in fields:
            norm = morph.norm_to_string(field)
            new_field = norm + u'\t' + field
            new_fields.append(new_field)
        print >> of, u'\t\t'.join(new_fields).encode('utf-8')

