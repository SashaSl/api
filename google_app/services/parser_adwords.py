# -*- coding: utf-8 -*-

import os
import time
import random
import codecs
import shutil
import datetime

from django.db.models import Q
from celery.result import AsyncResult
from selenium.common.exceptions import NoSuchElementException

import cfg
from dblog.models import Error
from google_app.models import *
from utils.common import join_lists
from utils.selebot.surfer import Surfer

TOP_CATS = [10004,10009,10018,10011,10007,10013,10003,10108,10019,10010,10017,10016,10020,13575,10002,10014,10021,10006,10001,10005,10012,10015]

def get_user():
    cond = Q(login__istartswith='a') | Q(login__istartswith='b') | Q(login__istartswith='c') | Q(login__istartswith='d')
    users = GoogleAccs.objects.filter(cond)
    return random.choice(users)

def remove_folder(folder):
    try:
        shutil.rmtree(folder)
    except OSError:
        pass

class AdwordsException(Exception):
    pass

def dict_cleaner(dct, keys):
    for key in keys:
        if key in dct:
            dct.pop(key)
    return dct

def check_task_status(task_list):
    success_list = []
    fail_list = []
    for task_id in task_list:
        async_result = AsyncResult(task_id)
        status = async_result.status.lower()
        if status == 'success':
            success_list.append(task_id)
        elif status == 'failure':
            fail_list.append(task_id)
    return success_list, fail_list

def cats_to_parse_cleaner(cats_to_parse, tasks_and_cats):
    lst = []
    for k, v in tasks_and_cats.iteritems():
        lst += v
    return list(set(cats_to_parse) - set(lst))

def result_maker(request):
    items = AdwordsCats.objects.filter(request__request=request)
    return list(set(join_lists([i.result for i in items])))

def get_cat_children(number, structure):
    for item in structure:
        if item['i'] == number:
            return item.get('c', [])

def cats_analyzer(cats, request, cache_timeout):
    cats_to_parse = []
    full_cats = []
    for cat in cats:
        result = cat_result_checker(request, cat, cache_timeout)
        if result and len(result[0].result) >= 800:
            full_cats.append(cat)
        if not result:
            cats_to_parse.append(cat)
    return cats_to_parse, full_cats

def structure_crawler(structure, request, cache_timeout):
    cats = TOP_CATS
    total_cats_to_parse = []

    while cats:
        cats_to_parse, full_cats = cats_analyzer(cats, request, cache_timeout)
        total_cats_to_parse += cats_to_parse
        children_cats = join_lists([get_cat_children(i, structure) for i in full_cats])
        cats = children_cats

    return total_cats_to_parse

def cat_result_checker(request, number, cache_timeout):
    if 'h' in cache_timeout:
        period = datetime.datetime.now()-datetime.timedelta(hours=int(cache_timeout.replace('h','')))
    elif 'd' in cache_timeout:
        period = datetime.datetime.now()-datetime.timedelta(int(cache_timeout.replace('d','')))
    else:
        period = datetime.datetime.now()-datetime.timedelta(int(cache_timeout))

    return AdwordsCats.objects.filter(request__request=request, number=number, updated_at__gt=period)

def save_cat_result(request, number, result):
    request_obj = SearchRequest.objects.get_or_create(request=request)[0]
    AdwordsCats.objects.create_or_update(**{'request': request, 'number': number, 'result': result, 'request_obj': request_obj})

def wait_element_displayed(selector, browser, limit=15):
    counter = 0
    while counter < limit:
        if isinstance(selector, list):
            for sel in selector:
                try:
                    element = browser.find_element_by_css_selector(sel)
                    if element.is_displayed():
                        return
                except NoSuchElementException:
                    pass
            time.sleep(0.2)
            counter += 0.2
        else:
            try:
                element = browser.find_element_by_css_selector(selector)
                if element.is_displayed():
                    return
            except NoSuchElementException:
                    pass
            time.sleep(0.2)
            counter += 0.2
    raise AdwordsException("waited too long")

def wait_file_loaded(dirname, limit=60):
    counter = 0
    while counter < limit:
        try:
            filename = dirname + '/' + os.listdir(dirname)[0]
            with open(filename, 'r') as f:
                return
        except (IOError, IndexError, OSError):
            time.sleep(1)
            counter += 1
    raise AdwordsException("waited too long")

def phrases_extractor(filename):
    lst = []
    with codecs.open(filename, 'rb', 'utf16') as f:
        data = f.readlines()
        data = data[1:]
        for item in data:
            if 'Seed Keywords' in item:
                continue
            item = item.rstrip()
            item = item.replace('Keyword Ideas	','')
            item = item[:item.find('RUB')]
            lst.append(item.rstrip())
    return lst

def get_cat_name(cat, structure):
    for item in structure:
        if item['i'] == cat:
            return item['n']

class AdwordsParser():
    def parse(self, request, cat, folder, proxy=None):
        try:
            def _login(browser):
                user = get_user()
                login = user.login
                password = user.password
                browser.find_element_by_css_selector('input#Email').send_keys(login)
                browser.find_element_by_css_selector('input#Passwd').send_keys(password)
                browser.find_element_by_css_selector('input[id="signIn"]').click()
                return login

            def _download_file(browser, surfer, folder):
                browser.execute_script('document.querySelectorAll(\'div.goog-button-base-content > span.aw-toolbelt-downloadButton\')[1].click();')
                dialog = 'span[id*="debug-download-button-content"]'
                wait_element_displayed(dialog, browser)

                _click_checker(browser, dialog)

                surfer.wait_for_css('span[id*="debug-retrieve-download-content"]')
                wait_element_displayed('span[id*="debug-retrieve-download-content"]', browser)
                browser.execute_script('document.querySelector(\'span[id*="debug-retrieve-download-content"]\').click();')

                wait_file_loaded(folder)

            def _content_check(browser, limit=20):
                empty = False
                counter = 0
                while counter < limit:
                    el1 = 'div[id*="debug-seed-keyword-table"] > div[class][style]'
                    el2 = 'div[id*="debug-keyword-table"] > div[class][style]'
                    el3 = 'div[id*="debug-keyword-table"] div[id*="debug-table"]'
                    wait_element_displayed([el1, el2, el3], browser)

                    if u'не найдено вариантов' in browser.find_element_by_css_selector('div[id*="debug-keyword-table"] > div[class][style]').text:
                        return True

                    if browser.find_element_by_css_selector('div[id*="debug-keyword-table"] div[id*="debug-table"]').is_displayed():
                        return empty

                    browser.execute_script('document.querySelector(\'div[id*="debug-grouping-toggle"] > div[id*="debug-grouping-toggle-BUNDLED_IDEAS"] > div[class*="-Label"]\').click();')
                    time.sleep(3)
                    browser.execute_script('document.querySelector(\'div[id*="debug-grouping-toggle"] > div[id*="debug-grouping-toggle-KEYWORD_IDEAS"] > div[class*="-Label"]\').click();')
                    counter += 1
                raise AdwordsException("waited too long")

            def _click_checker(browser, dialog):
                counter = 0
                while counter < 90:
                    browser.execute_script('document.querySelector(\'span[id*="debug-download-button-content"]\').click();')
                    time.sleep(3)
                    if not browser.find_element_by_css_selector(dialog).is_displayed():
                        return
                    counter += 1
                raise AdwordsException("waited too long")

            if hasattr(self, 'surfer'):
                browser = self.browser
                surfer = self.surfer
                login = self.login
                browser.get('https://adwords.google.com/ko/KeywordPlanner/Home')
            else:
                surfer = Surfer(proxy=proxy, folder=folder)
                surfer._profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/csv, text/csv")
                surfer.open_browser()
                browser = surfer.get_browser()
                self.surfer = surfer
                self.browser = browser

                url = 'https://adwords.google.com/ko/KeywordPlanner'
                browser.get(url)

                browser.find_element_by_css_selector('a.maia-button').click()
                surfer.wait_for_css('input[id="signIn"]')

                login = _login(browser)
                self.login = login

            while True:
                try:
                    surfer.wait_for_css('img[id="aw-adwords-logo"]')
                    wait_element_displayed('div[id*="search-selection-input"]', browser, limit=3)
                    break
                except AdwordsException:
                    browser.get('https://adwords.google.com/ko/KeywordPlanner/Home')

            browser.execute_script('document.querySelector(\'div[id*="search-selection-input"]\').click();')

            surfer.wait_for_css('div[id*="splash-panel-form"] textarea[id*="keywords-text-area"]')

            ### продукт или услуга ###
            browser.find_element_by_css_selector('div[id*="splash-panel-form"] textarea[id*="keywords-text-area"]').send_keys(request)

            ### категория товаров ###
            browser.find_element_by_css_selector('div[id*="splash-panel-form"] input[id*="category-input"]').send_keys(cat)
            surfer.wait_for_css('div.suggestPopupMiddleCenterInner.suggestPopupContent')
            categories = browser.find_elements_by_css_selector('div.suggestPopupMiddleCenterInner.suggestPopupContent td[id*=uid]')
            for _cat in categories:
                if cat == _cat.text:
                    _cat.click()
                    break
            time.sleep(3)

            ### переходим на вкладку "Варианты ключевых слов" ###
            browser.execute_script('document.querySelector(\'span[id*="search-button-content"]\').click();')
            el1 = 'div[id*="debug-bundle-table"] > div[class][style]'
            el2 = 'div[id*="debug-bundle-table"] div[id*="debug-table"]'
            wait_element_displayed([el1, el2], browser)
            browser.execute_script('document.querySelector(\'div[id*="debug-grouping-toggle"] > div[id*="debug-grouping-toggle-KEYWORD_IDEAS"] > div[class*="-Label"]\').click();')

            empty = _content_check(browser)
            if empty:
                return []
            _download_file(browser, surfer, folder)
            filename = folder + '/' + os.listdir(folder)[0]
            result = phrases_extractor(filename)
            os.remove(filename)
            return result
        except Exception:
            Error.to_db(msg=login)
            raise

def wait_element_displayed_return_browser(selector, browser, limit=15):
    counter = 0
    while counter < limit:
        if isinstance(selector, list):
            for sel in selector:
                try:
                    element = browser.find_element_by_css_selector(sel)
                    if element.is_displayed():
                        return browser
                except NoSuchElementException:
                    pass
            time.sleep(0.2)
            counter += 0.2
        else:
            try:
                element = browser.find_element_by_css_selector(selector)
                if element.is_displayed():
                    return browser
            except NoSuchElementException:
                    pass
            time.sleep(0.2)
            counter += 0.2
    raise AdwordsException("waited too long")
