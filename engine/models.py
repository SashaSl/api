# -*- coding: utf-8 -*-

import json
from django.db import models
from django.core.exceptions import ValidationError

class Engine(object):
    ENGINE_YANDEX = "yandex"
    ENGINE_DIRECT_API = "direct_api"
    ENGINE_GOOGLE = "google"
    ENGINE_BING = "bing"
    ENGINE_MAIL = "mail"
    ENGINE_YOUTUBE = "youtube"
    ENGINE_CHOICE = (
        (ENGINE_YANDEX, u"Яндекс"),
        (ENGINE_DIRECT_API, u"Яндекс.Директ API"),
        (ENGINE_GOOGLE, u"Гугл"),
        (ENGINE_BING, u"Бинг"),
        (ENGINE_MAIL, u"Мэйл"),
        (ENGINE_YOUTUBE, u"Ютуб")
    )

def validate_json(value):
    try:
        json.loads(value)
    except ValueError:
        raise ValidationError(u"Строка '{value}' не является валидным json.")

class EngineRegionalParams(models.Model):
    region = models.ForeignKey('geobase.Region', verbose_name=u"Регион", db_index=True)
    engine = models.TextField(verbose_name=u"Поисковая система", choices=Engine.ENGINE_CHOICE, db_index=True)
    domain = models.TextField(verbose_name=u"Домен")
    region_code = models.TextField(verbose_name=u"Код региона в поисковой системе")
    params = models.TextField(verbose_name=u"Региональные параметры (json)", db_index=True, validators=[validate_json])
    page_region = models.TextField(null=True, blank=True, default=None, verbose_name=u"Название региона в поисковике", db_index=True)

    def __unicode__(self):
        return u"{R}: {E}".format(
            E = self.engine,
            R = self.region.name_ru
        )

    def loads_params(self):
        return json.loads(self.params)

    class Meta:
        unique_together = ("region", "engine")