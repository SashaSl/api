# -*- coding: utf-8 -*-

import random
from ..common import generate_from_dict, extract_domain

__all__ = ['LinksPlan']

class LinksPlan(object):
    ANCORS = {
        u'здесь': 15,
        u'тут': 14,
        u'ссылка': 20,
        u'сюда': 12,
        u'сайт': 7,
        u'линк': 7,
        u'цинк': 1,
        u'сцылка': 2,
        u'ссыль': 2,
        u'смотреть здесь': 9,
        u'взято отсюда': 10,
        u'источник': 11,
        u'ссылка на источник': 9,
        u'ссылка на статью': 9,
        u'ссылка на сайт': 10,
        u'найдено здесь': 9
    }

    class ANCOR_TYPE:
        random = 'random'
        random_capitalize = 'random_capitalize'
        random_upper = 'random_upper'
        url = 'url'
        domain = 'domain'
        request = 'request'
        request_capitalize = 'request_capitalize'
        request_upper = 'request_upper'

    GENERATORS = {
        (ANCOR_TYPE.random, lambda **kwargs: generate_from_dict(LinksPlan.ANCORS)): 0.2,
        (ANCOR_TYPE.random_capitalize, lambda **kwargs: generate_from_dict(LinksPlan.ANCORS).capitalize()): 0.04,
        (ANCOR_TYPE.random_upper, lambda **kwargs: generate_from_dict(LinksPlan.ANCORS).upper()): 0.01,
        (ANCOR_TYPE.url, lambda **kwargs: kwargs['url']): 0.05,
        (ANCOR_TYPE.domain, lambda **kwargs: kwargs['site']): 0.2,
        (ANCOR_TYPE.request, lambda **kwargs: kwargs['request']): 0.4,
        (ANCOR_TYPE.request_capitalize, lambda **kwargs: kwargs['request'].capitalize()): 0.08,
        (ANCOR_TYPE.request_upper, lambda **kwargs: kwargs['request'].upper()): 0.02,
    }

    EXTRA_WORDS_COUNT = {
        2: 0.2,
        3: 0.3,
        4: 0.3,
        5: 0.2
    }

    def __init__(self, data, site):
        """
        data = {
            'request_id': {
                '...(request)': \d,
                ...
            },
            'actions': [
                {
                    'request': '...',
                    'operation': '{link|article}',
                    'cost': float,
                    'request_id': 1..N,
                    'day': 0..D
                },
                ...
            ],
            'request_url': {
                '...(request)': '...(url)',
                ...
            },
            'unused_requests': ['...(request)', ...],
            'request_creation_day': {
                '...(request)': \d,
                ...
            },
            'request_frequency': {
                '...(request)': \d,
                ...
            },
            'request_creation_cost': {
                '...(request)': float,
                ...
            }
        }
        """
        self.site = site
        self.actions = data['actions']
        self.request_id = data['request_id']
        self.request_creation_day = data['request_creation_day']
        self.request_frequency = data['request_frequency']
        self.request_url = data['request_url']
        self.already_created_requests = set(self.request_url.iterkeys())

        self.kernel = sorted(
            self.request_frequency.iterkeys(),
            key=lambda r: self.request_frequency[r],
            reverse=True
        )

    def _url(self, request):
        return self.request_url[request] if request in self.already_created_requests else '#{}#'.format(self.request_id[request])

    def generate_links_plan(self):
        """
        return [
            {
                'request': '...',
                'request_id': 1..N,
                'cost': float,
                'day': 0..D,
                'url': '{url|#id#}',
                'ancor': '...',
                'extra_words': ['...', ...]
            },
            ...
        ]
        """
        from .predictor import SeoConst
        result = []
        for action in self.actions:
            if action['operation'] != SeoConst.OPERATION_LINK:
                continue
            request = action['request']
            url = self._url(request)
            ancor_type, ancor_func = generate_from_dict(self.GENERATORS)
            ancor = ancor_func(url=url, request=request, site=self.site)
            extra_words = self._generate_extra_words(ancor_type, request)
            result.append({
                'request': request,
                'request_id': action['request_id'],
                'cost': action['cost'],
                'day': action['day'],
                'url': url,
                'ancor': ancor,
                'extra_words': extra_words,
            })
        return result

    def _generate_extra_words_groups(self, request):
        remnants = []
        selected = []
        clean_kernel = self.kernel[:]
        for item in self.kernel:
            if (item.startswith(request) or item.endswith(request)) and item != request:
                remnants.append(item.replace(request, u'').strip())
                selected.append(item.strip())
                clean_kernel.remove(item)
        return remnants, selected, clean_kernel

    def _generate_extra_words(self, ancor_type, request):
        extra_words = []
        extra_words_count = generate_from_dict(self.EXTRA_WORDS_COUNT)
        if ancor_type in {self.ANCOR_TYPE.request, self.ANCOR_TYPE.request_capitalize, self.ANCOR_TYPE.request_upper}:
            remnants, selected, clean_kernel = self._generate_extra_words_groups(request)
            if remnants:
                random.shuffle(remnants)
                extra_words += remnants[:extra_words_count]
            if extra_words_count - len(extra_words) > 0:
                random.shuffle(clean_kernel)
                extra_words += clean_kernel[:(extra_words_count - len(extra_words))]
        elif ancor_type in {self.ANCOR_TYPE.random, self.ANCOR_TYPE.random_capitalize, self.ANCOR_TYPE.random_upper, self.ANCOR_TYPE.url, self.ANCOR_TYPE.domain}:
            extra_words.append(request)
            kernel = self.kernel[:]
            random.shuffle(kernel)
            extra_words += kernel[:extra_words_count - 1]
        else:
            raise ValueError
        return extra_words