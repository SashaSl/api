#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
from config import Config

ROOT_DIR = os.path.dirname(__file__)
ROOT_DIR = os.path.abspath(ROOT_DIR)

sys.path.append(ROOT_DIR)

install_type_path = os.path.join(ROOT_DIR, 'install_type')
if os.path.exists(install_type_path):
    with open(install_type_path) as f:
        install_type = f.read().strip()
else:
    install_type = 'local'

if install_type == 'local':
    __config = Config(open(os.path.join(ROOT_DIR, "local_cfg.cfg")))
elif install_type in ['prod', 'worker']:
    __config = Config(open(os.path.join(ROOT_DIR, "cfg.cfg")))

for __key, __val in __config.iteritems():
    locals()[__key] = __val

del __config
del __key
del __val
del Config

if __name__ == "__main__":
    opt_name = sys.argv[1]
    sys.stdout.write(str(locals()[opt_name]))
    sys.stdout.flush()