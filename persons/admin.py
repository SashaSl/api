from django.contrib import admin
from models import Name, Person, MailRu

class NameAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'name',
        'is_first',
        'is_male',
        'nation'
    ]

    list_editable = [
        'name',
        'is_first',
        'is_male',
        'nation'
    ]

class PersonAdmin(admin.ModelAdmin):
    using_mailru = lambda obj: obj.account(MailRu).email if obj.using(MailRu) else ''
    using_mailru.short_description = 'mail.ru'

    list_display = [
        'nickname',
        'is_used',
        'password',
        'first_name',
        'last_name',
        'birthday',
        'is_male',
        'nation',
        'created',
        'proxy',
        'useragent',
        using_mailru
    ]

    search_fields = [
        'nickname',
        'password',
        'first_name',
        'last_name',
        'birthday',
        'is_male',
        'nation',
        'created',
        'proxy',
        'useragent'
    ]

admin.site.register(Name, NameAdmin)
admin.site.register(Person, PersonAdmin)
admin.site.register(MailRu)
