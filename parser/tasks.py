# -*- coding: utf-8 -*-

import re
import time
import difflib
import config
import datetime
import operator

from pprint import pprint

from celery.task import task
from celery.utils.log import get_task_logger
from celery.result import AsyncResult
from direct_api.models import User as DirectApiUser
from geobase.models import Region
from celery.exceptions import SoftTimeLimitExceeded

from utils.linguistics import Morph
from utils.crawler.local_utils import ContentExtractor
from utils.parser import suggest, serp, direct_api
from utils.common import datetime_to_ts, parser_task_hash, \
    structure_gen, wait_for_tasks, sanitize_link_list, \
    sanitize_stop_words, Tor, slicer, kernel_detailed_cleaner, \
    link_normalizer, check_if_parsed, Xvfb, KWExtractor, \
    punct_remover_str, formatted_reqs_dict_maker, formatted_reqs_list_maker, \
    BudgetRequestsChecker, bad_reqs_save, bad_reqs_remover, wait_for_tasks_time, \
    screenshot_bad_reqs_save, site_url_normalizer, values_sum, CachingMorphAnalyzer, \
    req2norm, keys_finder, extract_domain, wait_and_restart_tasks, email_from_text_extractor, Ip, \
    check_phrase_in_list
from utils.crawler.nojs_crawler import NoJsBrowser
from utils.parser.direct_api.budget import YandexApiBudget, acc_randomizer
from google_app.tasks import kernel_collector_adwords

from parser.models import ParentTask, ChildTask
from engine.models import Engine, EngineRegionalParams
from dblog.models import Error
from models import Parser
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import F
import cfg
import os
import json
import random
import utils.parser.serp.config
import urllib2
from itertools import chain
from direct_api import certs_downloader
from readability.readability import Document

logger = get_task_logger(__name__)


RETRY = config.CELERY_RETRY_COUNT
API_BUDGET_RETRY = RETRY*2
DELAY = config.CELERY_DELAY
API_BUDGET_DELAY = DELAY/10
MAX_EMPTY_RETRY = config.CELERY_MAX_EMPTY_RETRY
MAX_PAGE = config.KERNEL_MAX_PAGE
SLEEP_TMT = config.KERNEL_API_DELAY
PERCENTILE = config.KERNEL_GROUPS_PERCENTILE
URL_LIMIT = config.KERNEL_SERP_URL_LIMIT
SERP_POS = utils.parser.serp.config.MIN_SERP_POS
MAX_SERP_POS = utils.parser.serp.config.MAX_SERP_POS
TMT = serp.config.MAX_TIMEOUT
SPACE = re.compile(u'\s+')
MAX_KERNEL = cfg.kernel_max_page

def revoke_tasks(parent_task_id):
    parent_task = ParentTask.objects.get(parent_task=parent_task_id)
    child_tasks = ChildTask.objects.filter(parent_task=parent_task)
    child_tasks_ids = [i.child_task for i in child_tasks]
    for task in child_tasks_ids:
        AsyncResult(task).revoke(terminate=True, signal='SIGKILL')
    child_tasks.delete()

def connect_tasks(parent_task, child_task):
    parent_task = ParentTask.objects.get(parent_task=parent_task)
    ChildTask.objects.get_or_create(parent_task=parent_task, child_task=child_task)

def check_tasks_connected(parent_task, child_task):
    if not parent_task:
        return
    connect_tasks(parent_task, child_task)

def child_queue(queue):
    try:
        last_char = str(int(queue[-1]) + 1)
        return queue[:-1] + last_char
    except ValueError:
        return queue + '2'


@task(name="parser.suggest.parse", default_retry_delay=DELAY, max_retries=RETRY)
def parser_suggest_parse(engine, region, request, operation, prev_request=None, iterations=1):
    try:
        if engine == Engine.ENGINE_YANDEX:
            suggest_class = suggest.YandexSuggest
        elif engine == Engine.ENGINE_GOOGLE:
            suggest_class = suggest.GoogleSuggest
        elif engine == Engine.ENGINE_BING:
            suggest_class = suggest.BingSuggest
        elif engine == Engine.ENGINE_MAIL:
            suggest_class = suggest.MailSuggest
        elif engine == Engine.ENGINE_YOUTUBE:
            raise Exception("Youtube suggest parser is not realized yet!")

        suggest_parser = suggest_class(region=region)
        fixed_request, result, efficiency = suggest_parser.get_suggest(request, prev_request=prev_request, iterations=iterations)
        final_result = {
            "success": True,
            "created": datetime_to_ts(datetime.datetime.now()),
            "result": result,
            "prev_request": prev_request,
            'efficiency': efficiency,
            'iterations': iterations
        }

        Parser.add_and_save(Parser.MODE_SUGGEST, operation, request, region, engine, final_result, prev_request=prev_request)
        return result
    except Exception, e:
        Error.to_db()
        raise parser_suggest_parse.retry(exc=e)

@task(name="parser.parent_crawler.parse", default_retry_delay=DELAY, max_retries=RETRY)
def parser_parent_crawler_parse(request, tor_disable=False, queue='default', cache_timeout=cfg.cache_timeout, parent_task=None):
    print 'parser parent crawler parse'
    try:
        check_tasks_connected(parent_task, parser_parent_crawler_parse.request.id)
        sliced_requests = []
        task_list = []
        cache_timeout = str(cache_timeout)
        if int(re.findall('\d+', cache_timeout)[0]) > 0:
            request = check_if_parsed(request, 'crawler', Parser, cache_timeout)
            if not request: return None

        while request:
            sliced_requests.append(request[:cfg.package_crawler_bunch_size])
            request = request[cfg.package_crawler_bunch_size:]

        for rq in sliced_requests:
            oper_hash = []
            for _rq in rq:
                oper_hash.append(parser_task_hash({
                    'type': Parser.MODE_CRAWLER_NOJS,
                    'request': _rq.encode('utf-8'),
                }))

            task = parser_crawler_parse.apply_async(args=[rq, oper_hash], kwargs={'tor_disable': tor_disable, 'parent_task': parent_task}, queue=child_queue(queue))
            task_id = task.task_id
            task_list.append(task_id)

        wait_for_tasks(task_list)
    except Exception, e:
        Error.to_db()
        raise parser_parent_crawler_parse.retry(exc=e)

@task(name="parser.crawler.parse", default_retry_delay=DELAY, max_retries=RETRY)
def parser_crawler_parse(request, operation_hash, tor_disable=False, parent_task=None):
    print 'parser_crawler_parse'
    if request and operation_hash:
        try:
            check_tasks_connected(parent_task, parser_crawler_parse.request.id)
            if not isinstance(request, list):
                request = [request]
                operation_hash = [operation_hash]

            browser = NoJsBrowser()
            request_bad_list = []
            hash_bad_list = []

            # @Tor(replace_only_none_proxy=False)
            def _process_chunk(request, proxy=None, tor_disable=False):
                first_result = []
                for i in xrange(len(request)):
                    try:
                        res = browser.parse(request[i], tor_disable=True, proxy=proxy)
                    except Exception:
                        Error.to_db()
                        request_bad_list.append(request[i])
                        hash_bad_list.append(operation_hash[i])
                        continue

                    first_result.append({
                        'request': request[i],
                        'data': res,
                        'hash': operation_hash[i],
                        })
                return first_result

            first_result = _process_chunk(request, tor_disable=tor_disable)

            for item in first_result:
                final_result = {
                    "success": True,
                    "created": datetime_to_ts(datetime.datetime.now()),
                }

                if 'result' in item['data']:
                    final_result['result'] = item['data']['result']

                if 'html' in item['data'] and item['data']['html']:
                    final_result['html'] = item['data']['html']

                if 'download_time' in item['data'] and item['data']['download_time']:
                    final_result['download_time'] = item['data']['download_time']

                if 'error' in item['data']:
                    current_retry = parser_crawler_parse.request.retries
                    print "################################ retry number: " + str(current_retry)
                    if current_retry < MAX_EMPTY_RETRY:
                        request_bad_list.append(item['request'])
                        hash_bad_list.append(item['hash'])
                        continue
                    else:
                        print "################################ Save result"
                        final_result['error'] = item['data']['error']

                Parser.add_and_save(Parser.MODE_CRAWLER_NOJS, item['hash'], item['request'][:20], None, None, final_result)
            if request_bad_list:
                raise Exception('Some crawlers are not parsed')
        except Exception, e:
            try:
                Error.to_db(msg=item['request'])
            except UnboundLocalError:
                Error.to_db()
            if request_bad_list:
                raise parser_crawler_parse.retry(args=[request_bad_list, hash_bad_list], kwargs={'tor_disable': tor_disable})
            else:
                raise parser_crawler_parse.retry(exc=e)

@task(name="parser.parent_direct.parse", default_retry_delay=DELAY, max_retries=RETRY)
def parser_parent_direct_parse(engine, region, request, queue='default', max_direct_pos=None, cache_timeout=0):
    print "parser parent direct parse"
    try:
        request = list(set(request))
        sliced_requests = []
        task_list = []
        cache_timeout = str(cache_timeout)
        if int(re.findall('\d+', cache_timeout)[0]) > 0:
            request = check_if_parsed(request, 'direct', Parser, cache_timeout, region=region, engine=engine, max_direct_pos=max_direct_pos)
            if not request: return None

        while request:
            sliced_requests.append(request[:cfg.package_direct_bunch_size])
            request = request[cfg.package_direct_bunch_size:]

        for rq in sliced_requests:
            oper_hash = []
            for _rq in rq:
                oper_hash.append(parser_task_hash({
                    'engine': engine,
                    'type': Parser.MODE_DIRECT,
                    'request': _rq.encode('utf-8'),
                    'region': region.name_en,
                    'max_direct_pos': max_direct_pos,
                }))

            task = parser_direct_parse_separate.apply_async(args=[engine, region, rq, oper_hash], kwargs={'max_direct_pos': max_direct_pos}, queue=child_queue(queue))
            task_id = task.task_id
            task_list.append(task_id)

        wait_for_tasks(task_list)
    except Exception, e:
        Error.to_db()
        raise parser_parent_direct_parse.retry(exc=e)

@task(name="parser.direct.parse_separate", default_retry_delay=DELAY, max_retries=RETRY)
def parser_direct_parse_separate(engine, region, request, operation, max_direct_pos=None):
    print "parser direct parse separate"
    try:
        direct_class = serp.YandexBrowser
        direct_parser = direct_class()

        request_good_list = []
        hash_good_list = []

        def _process_chunk(request):
            first_result = []
            for i in xrange(len(request)):
                try:
                    direct_data = direct_parser.direct_separate(request[i], region=region, max_direct_pos=max_direct_pos)
                    request_good_list.append(request[i])
                    hash_good_list.append(operation[i])
                    first_result.append({
                        'request': request[i],
                        'data': direct_data,
                        'hash': operation[i],
                    })
                except Exception:
                    Error.to_db()
                    break

            return first_result

        first_result = _process_chunk(request)
        for item in first_result:
            final_result = {
                "documents": len(item['data']['result']),
                "direct": [],
                'page_number': item['data']['page_number'],
            }
            for i in xrange(len(item['data']['result'])):
                element = {
                    'number': i+1,
                    'url': item['data']['result'][i]['link'],
                    'snippet': item['data']['result'][i]['snippet'],
                    'title': item['data']['result'][i]['title'],
                }
                if 'warning' in item['data']['result'][i]:
                    element['warning'] = item['data']['result'][i]['warning']
                final_result['direct'].append(element)

            Parser.add_and_save(Parser.MODE_DIRECT, item['hash'], item['request'], region, engine, final_result)
        for item in request_good_list:
            if item in request:
                request.remove(item)
        for item in hash_good_list:
            if item in operation:
                operation.remove(item)
        if request:
            class DirectParserFailed(Exception):
                pass
            raise DirectParserFailed('Some directs are not parsed')
    except Exception, e:
        Error.to_db()
        raise parser_direct_parse_separate.retry(args=[engine, region, request, operation], exc=e)

@task(name="parser.direct.parse", default_retry_delay=DELAY, max_retries=RETRY)
def parser_direct_parse(engine, region, request, operation, max_direct_pos=None):
    print "parser direct parse"
    if request and operation:
        try:
            if not isinstance(request, list):
                request = [request]
                operation = [operation]
            direct_class = serp.YandexBrowser
            direct_parser = direct_class()

            request_good_list = []
            hash_good_list = []

            @Ip(replace_only_none_proxy=False)
            def _process_chunk(request, proxy=None):
                first_result = []
                for i in xrange(len(request)):
                    try:
                        serp_data = direct_parser.direct(request[i], region=region, proxy=proxy, max_direct_pos=max_direct_pos)
                        request_good_list.append(request[i])
                        hash_good_list.append(operation[i])
                        first_result.append({
                            'request': request[i],
                            'data': serp_data,
                            'hash': operation[i],
                        })
                    except Exception:
                        Error.to_db()
                        break

                return first_result

            first_result = _process_chunk(request)
            for item in first_result:
                final_result = {
                    "documents": len(item['data']['result']),
                    "direct": [],
                    'page_number': item['data']['page_number'],
                }
                for i in xrange(len(item['data']['result'])):
                    element = {
                        'number': i+1,
                        'url': item['data']['result'][i]['link'],
                        'snippet': item['data']['result'][i]['snippet'],
                        'title': item['data']['result'][i]['title'],
                    }
                    if 'warning' in item['data']['result'][i]:
                        element['warning'] = item['data']['result'][i]['warning']
                    final_result['direct'].append(element)

                Parser.add_and_save(Parser.MODE_DIRECT, item['hash'], item['request'], region, engine, final_result)
            for item in request_good_list:
                if item in request:
                    request.remove(item)
            for item in hash_good_list:
                if item in operation:
                    operation.remove(item)
            if request:
                class DirectParserFailed(Exception):
                    pass
                raise DirectParserFailed('Some directs are not parsed')
        except Exception, e:
            Error.to_db()
            raise parser_direct_parse.retry(args=[engine, region, request, operation], exc=e)

@task(name="parser.budget.parse", default_retry_delay=DELAY, max_retries=RETRY, soft_time_limit=3600*3)
def parser_budget_parse(reqs, region, engine, queue='default', cache_timeout=0, parent_task=None):
    print "parser budget parse"
    try:
        check_tasks_connected(parent_task, parser_budget_parse.request.id)
        single = False
        if not parent_task:
            single = True
            parent_task = parser_budget_parse.request.id
            ParentTask.objects.get_or_create(parent_task=parent_task)
        reqs = [x.strip() for x in reqs]
        initial_reqs = reqs
        task_list = []
        cache_timeout = str(cache_timeout)
        if int(re.findall('\d+', cache_timeout)[0]) > 0:
            reqs = check_if_parsed(reqs, 'budget', Parser, cache_timeout, region=region)
            if not reqs: return None

        reqs_dict = formatted_reqs_dict_maker(reqs)
        reqs = formatted_reqs_list_maker(reqs)

        reqs_checker = BudgetRequestsChecker(reqs, reqs_dict)
        bad_reqs = reqs_checker.check()
        if bad_reqs:
            bad_reqs_save(bad_reqs, Parser, region, engine)
            reqs = bad_reqs_remover(reqs, bad_reqs)
            if not reqs: return None

        sliced_requests = slicer(reqs, 10000)

        ### Create tasks
        for partial_request in sliced_requests:
            task = parser_budget_parse_chunk.apply_async(args=[partial_request, region, engine, reqs_dict], kwargs={'parent_task': parent_task}, queue=child_queue(queue))
            task_id = task.task_id

            task_list.append(task_id)

        wait_for_tasks(task_list)

        cache_timeout = '1'
        while True:
            empty_reqs = check_if_parsed(initial_reqs, 'budget', Parser, cache_timeout, region=region)
            if empty_reqs:
                task_list = []
                sliced_requests = slicer(formatted_reqs_list_maker(empty_reqs), 10000)
                ### Create tasks
                for partial_request in sliced_requests:
                    task = parser_budget_parse_chunk.apply_async(args=[partial_request, region, engine, reqs_dict], kwargs={'parent_task': parent_task}, queue=child_queue(queue))
                    task_id = task.task_id

                    task_list.append(task_id)
                wait_for_tasks(task_list)
                initial_reqs = empty_reqs
            else:
                break
    except SoftTimeLimitExceeded, e:
        if single:
            revoke_tasks(parent_task)
        Error.to_db()
        raise parser_budget_parse.retry(exc=e)

    except Exception, e:
        Error.to_db()
        raise parser_budget_parse.retry(exc=e)

@task(name="parser.budget.parse_chunk", default_retry_delay=DELAY, max_retries=RETRY)
def parser_budget_parse_chunk(request, region, engine, reqs_dict, parent_task=None):
    try:
        check_tasks_connected(parent_task, parser_budget_parse_chunk.request.id)
        budget_parser = serp.YandexBrowser()
        budget_parser.budget_command(request, region.name_en, engine, reqs_dict)
    except Exception, e:
        Error.to_db()
        raise parser_budget_parse_chunk.retry(exc=e)



