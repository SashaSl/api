# -*- coding: utf-8 -*-

import cfg
import json
import time

from celery.task import task

from dblog.models import Error
from google_app.services import *
from utils.common import Tor, Xvfb, wait_for_tasks, kernel_detailed_cleaner


CACHE_TIMEOUT_FREQUENCY = cfg.cache_timeout_frequency

def child_queue(queue):
    try:
        last_char = str(int(queue[-1]) + 1)
        return queue[:-1] + last_char
    except ValueError:
        return queue + '2'

@task(name="google.parser_parent.adwords", default_retry_delay=cfg.celery_retry_delay, max_retries=cfg.celery_retry_count)
def parser_parent_adwords(request, queue='default', cache_timeout=CACHE_TIMEOUT_FREQUENCY):
    print 'parser parent adwords'
    try:
        def _start_tasks(cats_to_parse, structure):
            tasks_and_cats = {}
            task_list = []
            sliced_cats = []
            while cats_to_parse:
                sliced_cats.append(cats_to_parse[:cfg.cat_bunch_size])
                cats_to_parse = cats_to_parse[cfg.cat_bunch_size:]
            for item in sliced_cats:
                task_id = parser_child_adwords.apply_async(args=[request, item, structure], queue=child_queue(queue)).task_id
                tasks_and_cats[task_id] = item
                task_list.append(task_id)
            return task_list, tasks_and_cats

        filename = cfg.ROOT_DIR + '/api_data/google_adwords/adwords_cats.json'

        with open(filename, 'r') as f:
            structure = json.load(f)

        total_task_list = []
        total_tasks_and_cats = {}
        success_list = []
        fail_list = []
        first = True

        while True:
            if success_list:
                total_task_list = list(set(total_task_list) - set(success_list))
                total_tasks_and_cats = dict_cleaner(total_tasks_and_cats, success_list)

            if fail_list:
                total_task_list = list(set(total_task_list) - set(fail_list))
                total_tasks_and_cats = dict_cleaner(total_tasks_and_cats, fail_list)

            if success_list or fail_list or first:
                first = False
                cats_to_parse = structure_crawler(structure, request, cache_timeout)
                cats_to_parse = cats_to_parse_cleaner(cats_to_parse, total_tasks_and_cats)
                if not cats_to_parse and not total_tasks_and_cats:
                    break
                if cats_to_parse:
                    task_list, tasks_and_cats = _start_tasks(cats_to_parse, structure)
                    total_task_list += task_list
                    total_tasks_and_cats.update(tasks_and_cats)

            time.sleep(3)
            success_list, fail_list = check_task_status(total_task_list)

        return result_maker(request)

    except Exception:
        Error.to_db()
        pass

@task(name="google.parser_child.adwords", default_retry_delay=cfg.celery_retry_delay, max_retries=cfg.celery_retry_count)
def parser_child_adwords(request, cats, structure, queue='default'):
    print 'parser child adwords'
    try:
        folder = cfg.ROOT_DIR + '/api_data/google_adwords/reports/' + parser_child_adwords.request.id
        @Tor(replace_only_none_proxy=False)
        @Xvfb(visible=cfg.wordstat_visible, width=1200, height=600, timeout=30)
        def _process_chunk(request, cats, structure, folder, proxy=None):
            result = []
            cats_good_list = []
            for cat in cats:
                try:
                    data = adwords_parser.parse(request, get_cat_name(cat, structure), folder, proxy=None)
                    result.append({
                        'cat': cat,
                        'data': data
                    })
                    cats_good_list.append(cat)
                except Exception:
                    Error.to_db()
                    break
            remove_folder(folder)
            return result, cats_good_list
        adwords_parser = AdwordsParser()
        result, cats_good_list = _process_chunk(request, cats, structure, folder)

        for item in result:
            save_cat_result(request, item['cat'], item['data'])

        cats = list(set(cats) - set(cats_good_list))

        if cats:
            class AdwordsParserFailed(Exception):
                pass
            raise AdwordsParserFailed("Some cats are not parsed")

    except Exception, e:
        remove_folder(folder)
        Error.to_db()
        raise parser_child_adwords.retry(args=[request, cats, structure], exc=e)
    finally:
        remove_folder(folder)

@task(name="google.kernel_collector_adwords.parse", default_retry_delay=cfg.celery_retry_delay, max_retries=cfg.celery_retry_count)
def kernel_collector_adwords(request, queue='default'):
    print "kernel_collector_adwords"
    try:
        request = request.splitlines()
        task_list = []
        task_dict = {}
        ### Create tasks
        for req in request:
            task = parser_parent_adwords.apply_async(args=[req], kwargs={'queue': child_queue(queue)}, queue=child_queue(queue))
            task_id = task.task_id
            task_list.append(task_id)
            task_dict[req] = task_id

        ### Wait tasks for complete
        wait_for_tasks(task_list)

        ### Get result
        kernel = []
        kernel_detailed = {}
        for req, task_id in task_dict.iteritems():
            result = AsyncResult(task_id).result
            kernel += result
            kernel_detailed[req] = result

        kernel_detailed = kernel_detailed_cleaner(kernel_detailed, request)

        return {
            "kernel": kernel,
            "kernel_detailed": kernel_detailed,
            }

    except Exception, e:
        Error.to_db()
        raise kernel_collector_adwords.retry(exc=e)