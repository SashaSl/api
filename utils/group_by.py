# -*- coding: utf-8 -*-

from copy import deepcopy
from collections import defaultdict


class GroupByFunc(object):
    def __init__(self, *args, **kwargs):
        self.fields = args
        self.item_getter = None
        self.values = defaultdict(list)

    def set_item_getter(self, item_getter_func):
        self.item_getter = item_getter_func

    def get_value(self, item, field_id_or_name):
        if isinstance(field_id_or_name, basestring):
            field_name = field_id_or_name
        elif isinstance(field_id_or_name, int):
            field_name = self.fields[field_id_or_name]
        else:
            raise TypeError
        return self.item_getter(item, field_name)

    def push(self, item):
        for i, field_name in enumerate(self.fields):
            self.values[i].append(self.get_value(item, field_name))

    def aggregate(self):
        pass

    def reset(self):
        self.values = defaultdict(list)


class Last(GroupByFunc):
    def aggregate(self):
        return self.values[0][-1]


class First(GroupByFunc):
    def aggregate(self):
        return self.values[0][0]


class Max(GroupByFunc):
    def aggregate(self):
        return max(self.values[0])


class Min(GroupByFunc):
    def aggregate(self):
        return min(self.values[0])


class Count(GroupByFunc):
    def aggregate(self):
        return len(self.values[0])


class Sum(GroupByFunc):
    def aggregate(self):
        return sum(self.values[0])


class Avg(GroupByFunc):
    def __init__(self, *args, **kwargs):
        super(Avg, self).__init__(*args, **kwargs)
        self.base_type = kwargs.get('base_type', float)

    def aggregate(self):
        return self.base_type(sum(self.values[0]) / float(len(self.values[0])))


class AggList(GroupByFunc):
    def aggregate(self):
        return self.values[0]


class AggSortedList(GroupByFunc):
    def __init__(self, *args, **kwargs):
        super(AggSortedList, self).__init__(*args, **kwargs)
        self.reverse = kwargs.get('reverse', False)

    def aggregate(self):
        return sorted(self.values[0], reverse=self.reverse)


class GroupBy(object):
    def __init__(self, iterable, store_keys=True):
        self._iterable = iterable if isinstance(iterable, list) else list(iterable)
        self._operations = []
        self._result_items = []
        self._store_keys = store_keys

    def _operation_method(self, operation, *args, **kwargs):
        copy = self._copy()
        copy._operations.append({
            'type': operation,
            'args': args,
            'kwargs': kwargs
        })
        return copy

    def mul(self, *args, **kwargs):
        return self._operation_method(self.__class__.mul, *args, **kwargs)

    def map(self, *args, **kwargs):
        return self._operation_method(self.__class__.map, *args, **kwargs)

    def fold(self, *args, **kwargs):
        return self._operation_method(self.__class__.fold, *args, **kwargs)

    def unfold(self, *args, **kwargs):
        return self._operation_method(self.__class__.unfold, *args, **kwargs)

    def store_keys(self, *args, **kwargs):
        return self._operation_method(self.__class__.store_keys, *args, **kwargs)

    def not_store_keys(self, *args, **kwargs):
        return self._operation_method(self.__class__.not_store_keys, *args, **kwargs)

    def group_by(self, *args, **kwargs):
        return self._operation_method(self.__class__.group_by, *args, **kwargs)

    def group_by_mapper(self, *args, **kwargs):
        return self._operation_method(self.__class__.group_by_mapper, *args, **kwargs)

    def order_by(self, *args, **kwargs):
        return self._operation_method(self.__class__.order_by, *args, **kwargs)

    def join(self, *args, **kwargs):
        return self._operation_method(self.__class__.join, *args, **kwargs)

    def _copy(self):
        copy = self.__class__(self._iterable, store_keys=self._store_keys)
        copy._operations = deepcopy(self._operations)
        return copy

    @classmethod
    def _getter(cls, item, field):
        return item[field]

    @classmethod
    def _new_item(cls):
        return {}

    @classmethod
    def _setter(cls, item, field, value):
        item[field] = value

    @classmethod
    def _list_fields(cls, item):
        return item.keys()

    @classmethod
    def _perform_order_by(cls, items, operation, store_keys):
        def compare(a, b):
            for arg in operation['args']:
                if arg[0] == '-':
                    desc = True
                    arg = arg[1:]
                else:
                    desc = False
                av = cls._getter(a, arg)
                bv = cls._getter(b, arg)

                if av < bv and desc:
                    return 1
                if av < bv and not desc:
                    return -1
                if bv < av and desc:
                    return -1
                if bv < av and not desc:
                    return 1
            return 0

        class Key(object):
            def __init__(self, obj, *args, **kwargs):
                self.obj = obj

            def __lt__(self, other):
                return compare(self.obj, other.obj) < 0

            def __gt__(self, other):
                return compare(self.obj, other.obj) > 0

            def __eq__(self, other):
                return compare(self.obj, other.obj) == 0

            def __le__(self, other):
                return compare(self.obj, other.obj) <= 0

            def __ge__(self, other):
                return compare(self.obj, other.obj) >= 0

            def __ne__(self, other):
                return compare(self.obj, other.obj) != 0

        return sorted(items, key=Key)

    @classmethod
    def _perform_group_by(cls, items, operation, store_keys):
        _items = cls._perform_order_by(items, operation, store_keys)
        key_fields = map(lambda arg: (arg[1:] if arg[0] == '-' else arg), operation['args'])

        def key_func(item):
            new_item = cls._new_item()
            key_val = []
            for key_field in key_fields:
                if store_keys:
                    cls._setter(new_item, key_field, cls._getter(item, key_field))
                key_val.append(cls._getter(item, key_field))
            return key_val, new_item

        field_functor = []
        for field_name, functor in operation['kwargs'].iteritems():
            functor.set_item_getter(cls._getter)
            field_functor.append((field_name, functor))

        key = None
        last_new_item = None
        new_items = []
        for item in _items:
            _key, _new_item = key_func(item)
            if _key == key:
                for field_name, functor in field_functor:
                    functor.push(item)
            else:
                for field_name, functor in field_functor:
                    if last_new_item is not None:
                        val = functor.aggregate()
                        cls._setter(last_new_item, field_name, val)
                        functor.reset()
                    functor.push(item)
                if last_new_item is not None:
                    new_items.append(last_new_item)
                key = _key
                last_new_item = _new_item
        for field_name, functor in field_functor:
            val = functor.aggregate()
            cls._setter(last_new_item, field_name, val)
            functor.reset()
        new_items.append(last_new_item)
        return new_items

    @classmethod
    def _join(cls, items, operation, store_keys):
        left = operation['kwargs'].get('left', False)
        right = operation['kwargs'].get('right', False)
        inner = operation['kwargs'].get('inner', False)
        hide_nulls = operation['kwargs'].get('hide_nulls', False)
        if not (left or inner or right):
            inner = True
        by_fields = operation['kwargs']['by']
        right_items = operation['args'][0]

        def key_func(item):
            key_val = []
            for key_field in by_fields:
                key_val.append(cls._getter(item, key_field))
            return key_val

        all_fields = set()
        for left_item in items:
            all_fields |= set(cls._list_fields(left_item))
        for right_item in right_items:
            all_fields |= set(cls._list_fields(right_item))

        def make_new_item(*args):
            i = cls._new_item()
            for field in all_fields:
                val = None
                all_nulls = True
                for itm in args:
                    try:
                        val = cls._getter(itm, field)
                        all_nulls = False
                        break
                    except:
                        pass
                if not (hide_nulls and all_nulls):
                    cls._setter(i, field, val)
            return i

        new_items = []

        if left:
            for left_item in items:
                left_key = key_func(left_item)
                for right_item in right_items:
                    right_key = key_func(right_item)
                    if left_key == right_key:
                        break
                else:
                    new_items.append(make_new_item(left_item))

        if inner:
            for left_item in items:
                left_key = key_func(left_item)
                for right_item in right_items:
                    right_key = key_func(right_item)
                    if left_key == right_key:
                        new_items.append(make_new_item(left_item, right_item))
                        break

        if right:
            for right_item in right_items:
                right_key = key_func(right_item)
                for left_item in items:
                    left_key = key_func(left_item)
                    if left_key == right_key:
                        break
                else:
                    new_items.append(make_new_item(right_item))

        return new_items

    @classmethod
    def _unfold(cls, items, operation, store_keys):
        from_field = operation['kwargs'].get('from_field', False)
        to_field = operation['kwargs'].get('to_field', from_field)

        def make_new_item(item, subitem):
            i = cls._new_item()
            for field in cls._list_fields(item):
                if field == from_field:
                    continue
                val = cls._getter(item, field)
                cls._setter(i, field, val)
            cls._setter(i, to_field, subitem)
            return i

        new_items = []
        for item in items:
            for subitem in cls._getter(item, from_field):
                new_item = make_new_item(item, subitem)
                new_items.append(new_item)

        return new_items

    @classmethod
    def _map(cls, items, operation, store_keys):
        mapper = operation['args'][0]
        return map(mapper, items)

    @classmethod
    def _fold(cls, items, operation, store_keys):
        """Пока болванка"""
        return items

    @classmethod
    def _mul(cls, items, operation, store_keys):
        right_items = operation['args'][0]

        def make_new_item(*args):
            i = cls._new_item()
            for item in args:
                for field in cls._list_fields(item):
                    val = cls._getter(item, field)
                    cls._setter(i, field, val)
            return i

        new_items = []
        for left_item in items:
            for right_item in right_items:
                new_items.append(make_new_item(left_item, right_item))
        return new_items

    @classmethod
    def _perform_group_by_mapper(cls, items, operation, store_keys):
        mapper = operation['kwargs']['mapper']
        _items = cls._perform_order_by(items, operation, store_keys)
        key_fields = map(lambda arg: (arg[1:] if arg[0] == '-' else arg), operation['args'])

        def key_func(item):
            new_item = cls._new_item()
            key_val = []
            for key_field in key_fields:
                if store_keys:
                    cls._setter(new_item, key_field, cls._getter(item, key_field))
                key_val.append(cls._getter(item, key_field))
            return key_val, new_item

        key = None
        last_new_item = None
        items_window = defaultdict(list)
        new_items = []
        for item in _items:
            _key, _new_item = key_func(item)
            if _key != key:
                if items_window:
                    mapper_item = mapper(items_window)
                    for k, v in mapper_item.iteritems():
                        if k not in key_fields:
                            cls._setter(last_new_item, k, v)
                    new_items.append(last_new_item)
                items_window = defaultdict(list)
                key = _key
                last_new_item = _new_item
            for field in cls._list_fields(item):
                items_window[field].append(cls._getter(item, field))
        mapper_item = mapper(items_window)
        for k, v in mapper_item.iteritems():
            if k not in key_fields:
                cls._setter(last_new_item, k, v)
        new_items.append(last_new_item)
        return new_items

    @classmethod
    def _perform(cls, items, operations, store_keys):
        if not items:
            return []
        #assert items
        if not operations:
            return items
        operation = operations.pop(0)

        if operation['type'] == cls.mul:
            new_items = cls._mul(items, operation, store_keys)
        elif operation['type'] == cls.map:
            new_items = cls._map(items, operation, store_keys)
        elif operation['type'] == cls.order_by:
            new_items = cls._perform_order_by(items, operation, store_keys)
        elif operation['type'] == cls.group_by:
            new_items = cls._perform_group_by(items, operation, store_keys)
        elif operation['type'] == cls.group_by_mapper:
            new_items = cls._perform_group_by_mapper(items, operation, store_keys)
        elif operation['type'] == cls.join:
            new_items = cls._join(items, operation, store_keys)
        elif operation['type'] == cls.fold:
            new_items = cls._fold(items, operation, store_keys)
        elif operation['type'] == cls.unfold:
            new_items = cls._unfold(items, operation, store_keys)
        elif operation['type'] == cls.store_keys:
            store_keys = True
            new_items = items
        elif operation['type'] == cls.not_store_keys:
            store_keys = False
            new_items = items
        else:
            raise ValueError

        return cls._perform(new_items, operations, store_keys)

    def __iter__(self):
        self._result_items = []
        self._result_items = self._perform(self._iterable, deepcopy(self._operations), self._store_keys)
        return self

    def next(self):
        if not self._result_items:
            raise StopIteration
        return self._result_items.pop(0)

    def all(self):
        return list(self)


def main():
    test_data = [
        {
            'domain': 'dom1.ru',
            'request': 'some request #1',
            'traffic': 6
        },
        {
            'domain': 'dom3.ru',
            'request': 'some request #2',
            'traffic': 5
        },
        {
            'domain': 'dom1.ru',
            'request': 'some request #2',
            'traffic': 4
        },
        {
            'domain': 'dom2.ru',
            'request': 'some request #2',
            'traffic': 3
        },
        {
            'domain': 'dom2.ru',
            'request': 'some request #1',
            'traffic': 2
        },
        {
            'domain': 'dom3.ru',
            'request': 'some request #1',
            'traffic': 1
        },
    ]

    import json

    print json.dumps(
        GroupBy(test_data).order_by('request', '-traffic').all(),
        indent=4
    )

    print

    print json.dumps(
        GroupBy(test_data).group_by('-domain', traffic_list=AggSortedList('traffic')).all(),
        indent=4
    )

    print

    print json.dumps(
        GroupBy(test_data).group_by('request', traffic_sum=Sum('traffic'), traffic_avg=Avg('traffic'), count=Count('traffic')).all(),
        indent=4
    )

    ############################################################################

    class CustomGrouper(GroupBy):
        @classmethod
        def _getter(cls, item, field):
            i = item
            for f in field.split('___'):
                i = i[f]
            return i

        @classmethod
        def _setter(cls, item, field, value):
            i = item
            field_path = field.split('___')
            for f in field_path:
                if f == field_path[-1]:
                    i[f] = value
                else:
                    i[f] = i.get(f, {})
                    i = i[f]

    test_data = [
        {
            'domain': 'dom1.ru',
            'details': {
                'request': 'some request #1',
                'traffic': 6
            }
        },
        {
            'domain': 'dom3.ru',
            'details': {
                'request': 'some request #2',
                'traffic': 5
            }
        },
        {
            'domain': 'dom1.ru',
            'details': {
                'request': 'some request #2',
                'traffic': 4
            }
        },
        {
            'domain': 'dom2.ru',
            'details': {
                'request': 'some request #2',
                'traffic': 3
            }
        },
        {
            'domain': 'dom2.ru',
            'details': {
                'request': 'some request #1',
                'traffic': 2
            }
        },
        {
            'domain': 'dom3.ru',
            'details': {
                'request': 'some request #1',
                'traffic': 1
            }
        }
    ]

    print json.dumps(
        CustomGrouper(test_data).order_by('-details___request', '-details___traffic').all(),
        indent=4
    )

    print

    print json.dumps(
        CustomGrouper(test_data).group_by('-domain', traffic_list=AggSortedList('details___traffic')).all(),
        indent=4
    )

    print

    print json.dumps(
        CustomGrouper(test_data).not_store_keys().group_by(
            'details___request',
            request=First('details___request'),
            traffic___sum=Sum('details___traffic'),
            traffic___avg=Avg('details___traffic'),
            count=Count('details___traffic')
        ).join(
            [
                {'request': 'some request #1', 'somefield': 123},
                {'request': 'some request #3', 'somefield': 345},
            ],
            by=['request'],
            inner=True,
            left=True,
            right=True,
            hide_nulls=True
        ).all(),
        indent=4
    )

    print

    print json.dumps(
        GroupBy([
            {
                'request': 'some request #1',
                'serp': [{'position': 1}, {'position': 2}, {'position': 3}]
            },
            {
                'request': 'some request #2',
                'serp': [{'position': 1}, {'position': 2}, {'position': 3}]
            }
        ]).unfold(from_field='serp', to_field='serp_item').all(),
        indent=4
    )

    print

    test_items = [
        {'domain': '1.ru', 'request': '1'},
        {'domain': '1.ru', 'request': '2'},
        {'domain': '1.ru', 'request': '3'},
        {'domain': '2.ru', 'request': '1'},
        {'domain': '2.ru', 'request': '3'},
        {'domain': '2.ru', 'request': '4'},
    ]

    requests = GroupBy(test_items).group_by('request').all()
    pairs = GroupBy(test_items).group_by('domain').mul(requests).all()

    print json.dumps(
        GroupBy(test_items).join(pairs, by=['domain', 'request'], right=True).all(),
        indent=4
    )

    print

    print json.dumps(
        GroupBy(test_items).group_by_mapper('domain', mapper=lambda i: i).all(),
        indent=4
    )

if __name__ == '__main__':
    main()