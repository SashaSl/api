# -*- coding: utf-8 -*-

import os
import re
import cfg
import json
import time
import random
import urllib2
import datetime

from celery.task import task, periodic_task
from celery.schedules import crontab
from celery.result import AsyncResult
from celery.utils.log import get_task_logger

from models import *
from parser import tasks
from dblog.models import Error
from parser.models import Parser
from yandex.services.api.sertificate import YandexSertificate
from yandex.services.api.metrika import YaApiToken
from yandex.services.api.goals_preprocess import get_goals_names, goals_and_conversions_text, correct_goals

from engine.models import Engine, EngineRegionalParams
from utils.common import datetime_to_ts, parser_task_hash, wait_for_tasks, copy_dir_to_worker
from yandex.services import *
from yandex.services.api.direct.direct_preprocess import YandexDirectPreprocess
from django.core.exceptions import ObjectDoesNotExist

from django.core.cache import get_cache

logger = get_task_logger(__name__)
CACHE_TIMEOUT_FREQUENCY = cfg.cache_timeout_frequency


def get_sertificate_path(login, password):
    """
        По логину и паролю возвращает путь к сертификату из базы.
        Если сертификата нет или он не валиден запускает download_sertificate
    """

    def _check_sertificate(login, password):
        yandex_sertificate = YandexSertificate()
        yandex_user, created = YandexUser.objects.get_or_create(login=login, password=password)
        sertificate_path = yandex_user.sertificate_path
        # Копируем на данный воркер сертификат чтобы проверить валидность
        if sertificate_path and not os.path.exists(cfg.ROOT_DIR + sertificate_path):
            Error.objects.create(
                tag='log before_copy to worker',
                message='sert_path=' + cfg.ROOT_DIR + sertificate_path + '!' + 'exist=' + repr(os.path.exists(cfg.ROOT_DIR + sertificate_path))
            )
            copy_dir_to_worker(cfg.ROOT_DIR + sertificate_path)
        # Проверяем сертификат на валдиность
        valid_sertificate = yandex_sertificate.check_sertificate(sertificate_path)
        return valid_sertificate, sertificate_path

    valid_sertificate, sertificate_path = _check_sertificate(login, password)
    if valid_sertificate:
        return sertificate_path
    else:

        lock_id = '{0}-lock-{1}'.format('download_sertificate', login)
        lock_cache = get_cache('lock')

        def _dl_sertificate(login, password):
            lock_cache.add(lock_id, 'true', cfg.lock_time)

            task_id = download_sertificate.apply_async(args=[login, password])
            wait_for_tasks([task_id])
            sertificate_path = AsyncResult(task_id).result

            lock_cache.delete(lock_id)
            return sertificate_path

        if not lock_cache.get(lock_id):
            # Если нет скачиваний сертификатов другой task
            sertificate_path = _dl_sertificate(login, password)
            return sertificate_path
        else:
            while lock_cache.get(lock_id):
                # Ожидаем пока закончится скачивание сертификата
                time.sleep(cfg.celery_retry_delay)
            # Проверяем валиден ли сертификат, полученный другой таской
            valid_sertificate, sertificate_path = _check_sertificate(login, password)
            if valid_sertificate:
                return sertificate_path
            else:
                # Если сертификат невалиден, скачиваем
                sertificate_path = _dl_sertificate(login, password)
                return sertificate_path


@task(name="download_sertificate", default_retry_delay=cfg.celery_retry_delay, max_retries=cfg.dl_sertificate_retry_count)
def download_sertificate(login, password):
    try:
        yandex_sertificate = YandexSertificate()
        folder_name = yandex_sertificate.handler(login, password)
        if folder_name == 'error':
            # Сертификат может не скачаться из за ошибки браузера. Перестартуем задачу чтобы он скачался.
            raise download_sertificate.retry(exc='no sertificate')
        else:
            sertificate_path = '/api_data/direct_api/' + folder_name + '/'
            YandexUser.objects.filter(login=login, password=password).update(sertificate_path=sertificate_path)
            Error.objects.create(
                tag='log sert downloaded',
                message='sert_path=' + cfg.ROOT_DIR + sertificate_path + '!' + 'exist=' + repr(os.path.exists(cfg.ROOT_DIR + sertificate_path))
            )
            return sertificate_path
    except Exception, e:
        Error.to_db()
        raise download_sertificate.retry(exc=e)


@task(name="yandex.api.metrika.get_report", default_retry_delay=cfg.celery_retry_delay, max_retries=cfg.celery_retry_count)
def api_metrika_get_report(*args, **kwargs):
    try:
        print 'api_metrika_get_report'
        def _token_finder(counter_id, name=None):
            data_dict = {
                'counter_id': counter_id
            }
            if name:
                data_dict['name'] = name.lower()

            counters = Counters.objects.filter(**data_dict)
            if len(counters) == 0:
                api_metrika_update_counters.apply()
                counters = Counters.objects.filter(**data_dict)
            if counters:
                return YandexUser.objects.get(login=counters[0].user).token
            else:
                # Пробуем найти счётчик по его id
                users = YandexUser.objects.all()
                for user in users:
                    if not user.token:
                        continue
                    metrika = YaApiToken()
                    metrika_params = {
                        'oauth_token': user.token,
                        'counter_id': counter_id
                    }
                    try:
                        result = json.loads(metrika.get_report('counter', metrika_params))
                        if 'counter' in result:
                            Counters.objects.get_or_create(user=user, counter_id=counter_id, name=result['counter'].get('name', ''))
                            return user.token
                    except urllib2.HTTPError:
                        # HTTPError 403 возникает когда отправляется запрос по чужому токену
                        pass
                else:
                    return None

        if 'id' in kwargs['metrika_params']:
            counter_id = kwargs['metrika_params']['id']
            counter_name = None
            if 'counter_name' in kwargs['metrika_params'] and kwargs['metrika_params']['counter_name']:
                counter_name = kwargs['metrika_params']['counter_name']
                del kwargs['metrika_params']['counter_name']
            oauth_token = _token_finder(counter_id, counter_name)
            if oauth_token:
                kwargs['metrika_params']['oauth_token'] = oauth_token
            else:
                return None
        metrika = YaApiToken()
        kwargs['metrika_params']['table_mode'] = 'tree'
        result = metrika.get_report(kwargs['metrika_method'], kwargs['metrika_params'])
        return json.loads(result)

    except Exception, e:
        Error.to_db()
        raise api_metrika_get_report.retry(exc=e)

@task(name="yandex.api.metrika.update_counters", default_retry_delay=cfg.celery_retry_delay, max_retries=cfg.celery_retry_count)
def api_metrika_update_counters():
    try:
        print 'update counters'
        def _change_response(response):
            if not response['name']:
                response['name'] = ''
            else:
                response['name'] = response['name'].lower()
            response['counter_id'] = response['id']
            del response['id']
            return response

        users = YandexUser.objects.all()
        metrika_method = 'counters'
        total_result = []
        for user in users:
            if not user.token:
                continue
            metrika_params = {
                'oauth_token': user.token,
            }
            result = api_metrika_get_report.apply(kwargs={'metrika_params': metrika_params, 'metrika_method': metrika_method}).result['counters']
            if result:
                total_result += result
                filter(lambda x: Counters.objects.get_or_create(user=user, **_change_response(x)), result)
        return {'counters': total_counters_compiler(total_result)}
    except Exception, e:
        Error.to_db()
        raise api_metrika_update_counters.retry(exc=e)

@task(name="yandex.parser.spros", default_retry_delay=cfg.celery_retry_delay, max_retries=cfg.celery_retry_count)
def parser_spros(request, region):
    try:
        engine = Engine.ENGINE_YANDEX
        erp = EngineRegionalParams.objects.get(region=region, engine=engine)

        request_good_list = []
        result = []
        for req in request:
            try:
                data = spros(req, erp.region_code)
                result.append({
                    'request': req,
                    'data': data
                })
                request_good_list.append(req)
            except Exception:
                Error.to_db()
                break

        for item in result:
            final_result = {
                "success": True,
                "created": datetime_to_ts(datetime.datetime.now()),
                'data': item['data'],
            }
            params = {
                'engine': engine,
                'type': Parser.MODE_YANDEX_SPROS,
                'request': item['request'].encode('utf-8'),
                'region': region.name_en,
            }
            operation_hash = parser_task_hash(params)
            Parser.add_and_save(Parser.MODE_YANDEX_SPROS, operation_hash, item['request'], region, engine, final_result)

        request = list(set(request) - set(request_good_list))
        if request:
            class SprosParserFailed(Exception):
                pass
            raise SprosParserFailed("Some spros' are not parsed")
    except Exception, e:
        Error.to_db()
        raise parser_spros.retry(args=[request, region], exc=e)

@task(name="yandex.parser.frequency", default_retry_delay=cfg.celery_retry_delay, max_retries=cfg.celery_retry_count)
def parser_frequency(reqs, region, cache_timeout=CACHE_TIMEOUT_FREQUENCY, queue='default'):
    print 'parser frequency'
    try:
        zero = False
        if int(re.findall('\d+', cache_timeout)[0]) > 0:
            reqs = check_if_parsed_frequency(reqs, region, cache_timeout)
            if not reqs: return None
        else:
            zero = True
        engine = Engine.ENGINE_YANDEX
        task = tasks.parser_budget_parse.apply_async(args=[reqs, region, engine], kwargs={'queue': tasks.child_queue(queue), 'cache_timeout': 0}, queue=tasks.child_queue(queue))
        task_id = task.task_id
        wait_for_tasks([task_id])

        if zero: cache_timeout = '1'
        reqs = check_if_parsed_frequency(reqs, region, cache_timeout)
        if not reqs: return None

        kernel_wordstat_pages = 1
        task = tasks.parser_parent_wordstat_parse.apply_async(args=[engine, region, reqs, kernel_wordstat_pages], kwargs={'queue': tasks.child_queue(queue), 'cache_timeout': 0}, queue=tasks.child_queue(queue))
        task_id = task.task_id
        wait_for_tasks([task_id])

        wordstat_data_to_SRB(reqs, kernel_wordstat_pages, region, engine)

    except Exception, e:
        Error.to_db()
        raise parser_frequency.retry(exc=e)

@task(name="yandex.parser.minus_words_metrika", default_retry_delay=cfg.celery_retry_delay, max_retries=cfg.celery_retry_count)
def minus_words_metrika(*args, **kwargs):
    print 'minus_words_metrika'
    try:
        task_id = api_metrika_get_report.apply_async(kwargs={'metrika_params': kwargs['metrika_params'],
                                                             'metrika_method': kwargs['metrika_method']},
                                                     queue=tasks.child_queue(kwargs['queue'])).task_id
        wait_for_tasks([task_id])
        data = AsyncResult(task_id).result
        if data:
            extractor = Minus_words_extractor()
            minus_words = extractor.data_iterator(data, denial=kwargs['denial'])
            return {u'errors': None, u'status': u'success', u'result': minus_words}
        else:
            return {u'errors': 'counter_id not found', u'status': u'failure', u'result': []}
    except Exception, e:
        Error.to_db()
        raise minus_words_metrika.retry(exc=e)
        
@task(name="yandex.api.direct.process_request")
def api_direct_process_request(*args, **kwargs):
    login = kwargs['login']
    password = kwargs['password']
    part_path = get_sertificate_path(login, password)
    path = cfg.ROOT_DIR + part_path
    if not os.path.exists(path):
        copy_dir_to_worker(path)
    inst = YandexDirectApi(path)
    result = inst.make_action(kwargs['direct_method'], param=kwargs['direct_params'])
    return result

@task(name="yandex.api.direct.process")
def api_direct_process(*args, **kwargs):
    print 'direct process'
    try:
        def _ads_param_check(ads_param, campaign_id):
                if ads_param[0]['CampaignID'] == 0:
                    for banner in ads_param:
                        banner['CampaignID'] = campaign_id
                return ads_param

        def campaign_check(login, password, ids):
            response = api_direct_process_request.apply(kwargs={'login': login,
                                                        'password': password,
                                                        'direct_method': 'GetCampaignsParams',
                                                        'direct_params': {'CampaignIDS': ids},
                }).result
            return response

        def extract_images_data(ads_param, login):
            ad_image_data = []
            for ads in ads_param:
                if ads['image_url']:
                    # Новый тип передаваемых изображений - список
                    if isinstance(ads['image_url'], list):
                        for img_url in ads['image_url']:
                            ad_image_data.append(
                                {
                                    "Login": login,
                                    "URL": img_url,
                                    "Name": ads['headliner']
                                }
                            )
                    else:
                        # Старый тип - один хедлайнер, одно изображение
                        ad_image_data.append(
                            {
                                "Login": login,
                                "URL": ads['image_url'],
                                "Name": ads['headliner']
                            }
                        )
            return ad_image_data

        ### обрабатываем юзеров ###
        user_info = None
        agent_login = kwargs['agent_login']
        client_login = kwargs['client_login']
        issue_client = YandexUser.objects.filter(login=client_login)
        agent_password = YandexUser.objects.get(login=agent_login).password
        if 'client_info_param' in kwargs and not issue_client:
            response = process_user(agent_login, agent_password, client_login, kwargs['client_info_param'])
            if 'error_code' in response:
                return response
            else:
                agent_login, client_login = response['agent_login'], response['client_login']
            user_info = {'login': client_login,
                         'password': YandexUser.objects.get(login=client_login).password}

        ### создаём/изменяем кампанию ###
        campaign_id = None

        if 'campaign_param' in kwargs:
            response = api_direct_process_request.apply(kwargs={'login': agent_login,
                                                    'password': agent_password,
                                                    'direct_method': 'CreateOrUpdateCampaign',
                                                    'direct_params': kwargs['campaign_param'],
            }).result
            if 'error_code' in response:
                return response
            try:
                # Апи яндекса почему то возврщает строку вместо id(при этом отрабатывает без ошибок. Надо понять что возвращает апи
                campaign_id = response['data']
            except Exception, e:
                import sys, traceback
                exc_type, exc_value, exc_traceback = sys.exc_info()
                Error.objects.create(
                    tag=exc_type,
                    message="".join(traceback.format_exception(exc_type, exc_value, exc_traceback)) + '///' + repr(response) + '///'
                )
                return {'error_str': str(e) + repr(response)}

        ### загружаем объявления ###
        ads_ids = None
        check_campaign = False
        if 'ads_param' in kwargs:
            check_campaign = True
            ### проверка id кампании и объявлений ###
            ads_param = _ads_param_check(kwargs['ads_param'], campaign_id)
            response = api_direct_process_request.apply(kwargs={'login': agent_login,
                                                    'password': agent_password,
                                                    'direct_method': 'CreateOrUpdateBanners',
                                                    'direct_params': ads_param,
            }).result
            if 'error_code' in response:
                return response
            ads_ids = response['data']
            if campaign_id:
                id = campaign_id
            else:
                id = kwargs['ads_param'][0]['CampaignID']


        elif 'ads_with_images' in kwargs:
            check_campaign = True
            ads_ids = []
            if campaign_id:
                id = campaign_id
            else:
                id = kwargs['ads_with_images'][0]['ads_param'][0]['CampaignID']

            ad_image_data = extract_images_data(kwargs['ads_with_images'], client_login)
            if ad_image_data:
                images_hash, errors_dict = get_direct_images_hash.apply(kwargs={'login': agent_login,
                                                            'password': agent_password,
                                                            'client_login': client_login,
                                                            'ad_image_data': ad_image_data,
                }).result
                if errors_dict:
                    return {'error_str': repr(errors_dict)}
            else:
                images_hash = {}
            for ads in kwargs['ads_with_images']:
                # Создаём объявление
                ads_param = _ads_param_check(ads['ads_param'], campaign_id)
                response = api_direct_process_request.apply(kwargs={'login': agent_login,
                                                        'password': agent_password,
                                                        'direct_method': 'CreateOrUpdateBanners',
                                                        'direct_params': ads_param,
                }).result
                if 'error_code' in response:
                    return response
                new_ads_ids = response['data']
                if ads['image_url']:
                    # Если есть картинки добавляем привязки объявлений к картинке
                    ad_image_associations = []
                    for ads_id in new_ads_ids:
                        if isinstance(ads['image_url'], list):
                            img_url = random.choice(ads['image_url'])
                            image_hash = images_hash[img_url]
                        else:
                            image_hash = images_hash[ads['image_url']]
                        ad_image_associations.append(
                            {
                                "AdID": ads_id,
                                "AdImageHash": image_hash,
                            }
                        )
                    response = api_direct_process_request.apply(
                        kwargs={
                            'login': agent_login,
                            'password': agent_password,
                            'direct_method': "AdImageAssociation",
                            'direct_params': {
                                "Action": "Set",
                                "AdImageAssociations": ad_image_associations
                            },
                        }
                    ).result
                    if 'error_code' in response:
                        return response
                ads_ids.append({
                    'headliner': ads['headliner'],
                    'ads_ids': new_ads_ids
                })

        if check_campaign and campaign_check(agent_login, agent_password, [id])['data'][0]['StatusModerate'] == 'New':
            ### отправляем на модерацию ###
            response = api_direct_process_request.apply(kwargs={'login': agent_login,
                                                    'password': agent_password,
                                                    'direct_method': 'ModerateBanners',
                                                    'direct_params': {'CampaignID': id},
            }).result
            if 'error_code' in response:
                return response

        ### проверка статуса кампании ###
        if 'monitoring_param' in kwargs:
            return campaign_check(agent_login, agent_password, kwargs['monitoring_param']['CampaignIDS'])

        ### campaigns_list_param ###
        if 'campaigns_list_param' in kwargs:
            response = api_direct_process_request.apply(kwargs={'login': agent_login,
                                                    'password': agent_password,
                                                    'direct_method': 'GetCampaignsList',
                                                    'direct_params': kwargs['campaigns_list_param'],
            }).result
            return response

        ### get_banners_param ###
        if 'get_banners_param' in kwargs:
            response = api_direct_process_request.apply(kwargs={'login': agent_login,
                                                    'password': agent_password,
                                                    'direct_method': "GetBanners",
                                                    'direct_params': kwargs['get_banners_param'],
            }).result
            return response

        result = {}
        if user_info:
            result['user_info'] = user_info
        if ads_ids:
            result['ads_ids'] = ads_ids
        if campaign_id:
            result['campaign_id'] = campaign_id
        return result

    except Exception, e:
        Error.to_db()
        return {'error_str': str(e)}


@task(name="yandex_get_direct_images_hash", default_retry_delay=cfg.celery_retry_delay, max_retries=cfg.celery_retry_count)
def get_direct_images_hash(*args, **kwargs):
    """
        Вход вида:
            kwargs = {
                ad_image_data: [
                    {
                        "Login": (string),
                        "URL": (string),
                        "Name": (string)
                    }
                    ...
                ],
                'login': agent_login,
                'password': agent_password,
                'client_login': client_login
            }

        Таска смотрит есть ли хеш для картинки в модели. Все картинки без хеша загружаются на яндекс, в ответ приходит хеш
        Хеш нужен для привязки картинки к объявлению

        Выход:
        {
            'image_url1': image_hash1,
            'image_url2': image_hash2,
            ...
        }

    """
    try:
        ad_image_data = kwargs['ad_image_data']
        agent_login = kwargs['login']
        agent_password = kwargs['password']
        client_login = kwargs['client_login']
        max_pending_count = 30
        sleep_delay = 30

        result_dict = {}
        yandex_parse_list = []
        for image_data in ad_image_data:
            image = YandexImages.objects.filter(login=image_data["Login"], url=image_data["URL"])
            if image and image[0].hash and not image[0].error:
                result_dict[image[0].url] = image[0].hash
            else:
                yandex_parse_list.append(image_data)

        errors_dict = {}
        if yandex_parse_list:
            response = api_direct_process_request.apply(kwargs={'login': agent_login,
                'password': agent_password,
                'direct_method': "AdImage",
                'direct_params': {
                    "Action": "Upload",
                    "AdImageURLData": yandex_parse_list,
                }
            }).result
            image_upload_task_id = []
            if "data" in response and "ActionsResult" in response["data"]:
                data_list = response["data"]["ActionsResult"]
                for i in xrange(len(data_list)):
                    data = data_list[i]
                    if "Errors" in data and data["Errors"]:
                        errors_dict[yandex_parse_list[i]["URL"]] = data["Errors"]
                    else:
                        image_upload_task_id.append(data["AdImageUploadTaskID"])
            if image_upload_task_id:
                not_upload_flag = True
                pending_count = 0
                while not_upload_flag and pending_count < max_pending_count:
                    yandex_response = api_direct_process_request.apply(kwargs={'login': agent_login,
                        'password': agent_password,
                        'direct_method': "AdImage",
                        'direct_params': {
                            "Action": "CheckUploadStatus",
                            "SelectionCriteria": {
                                "Logins": [client_login],
                                "AdImageUploadTaskIDS": image_upload_task_id,
                            }
                        }
                    }).result
                    not_upload_flag = False
                    if 'data' in yandex_response and "AdImageUploads" in yandex_response['data']:
                        data_list = yandex_response['data']["AdImageUploads"]
                        for data in data_list:
                            status = data["Status"]
                            if status != 'Done':
                                if status == 'Error ':
                                    errors_dict[data["SourceURL"]] = data["Error"]
                                    image_upload_task_id.remove(data["AdImageUploadTaskID"])
                                else:
                                    not_upload_flag = True
                    pending_count += 1
                    time.sleep(sleep_delay)
                for data in yandex_response['data']["AdImageUploads"]:
                    if data["AdImageHash"]:
                        result_dict[data["SourceURL"]] = data['AdImageHash']
                        YandexImages.objects.create(
                            login=data["Login"],
                            url=data["SourceURL"],
                            name=data["Name"],
                            hash=data["AdImageHash"]
                        )
                    else:
                        errors_dict[data["SourceURL"]] = 'Yandex can not upload this url'
        return result_dict, errors_dict
    except Exception, e:
        Error.to_db()
        raise


@task(name="yandex_direct_account_info", default_retry_delay=cfg.celery_retry_delay, max_retries=cfg.celery_retry_count)
def direct_account_info(yandex_user):
    """
        Получение списка кампаний.
        Для клиентского логина дочернего агентскому посылается дочерние логин в параметрах
        Для клиенского логина не привязанного к агентским кампаниям параметры вызывать не надо
    """

    try:
        client_login = yandex_user.login
        if yandex_user.agent_login:
            agent_login = yandex_user.agent_login.login
        else:
            agent_login = client_login
        agent_password = yandex_user.agent_login.password

        ### Получаем список кампаний для клиентского логина ###
        request_param = {
            'login': agent_login,
            'password': agent_password,
            'direct_method': 'GetCampaignsList',
            'direct_params': [client_login],
        }

        # Если аккаунт клиентский то посылать параметры с логинами не надо
        if agent_login == client_login:
            request_param['direct_params'] = []

        direct_campaign_data = api_direct_process_request.apply(kwargs=request_param).result
        result_direct_campaign_list = []
        if 'data' in direct_campaign_data:
            for direct_campaign in direct_campaign_data['data']:
                result_direct_campaign_list.append({
                    'id': direct_campaign['CampaignID'],
                    'name': direct_campaign['Name'],
                    'status': direct_campaign['Status'],
                })
        return {
            'result': result_direct_campaign_list,
        }
    except Exception, e:
        Error.to_db()
        exc = direct_account_info.retry(exc=e)
        raise exc

@task(name="yandex.api.direct_report", default_retry_delay=cfg.celery_retry_delay, max_retries=cfg.celery_retry_count)
def direct_report(id_list, login, password, start_date, end_date):
    """
        Принимает список id компаний и данные о пользователе(нужен только сертификат)
        Возвращает данные отчётов яндекс директа + сохраняет файл по данному отчёту
        Структура выходных данных
        full_result = {
            'result': {
                # Нужно для единичного и суммарного отчётов
                'etalon_goals': etalon_goals,
                'goals': goals,
                'raw_summary_stat': sum_stat,
                'region_base': self.region_base,
                'start_date': self.start_date,
                'end_date': self.end_date,
                'campaign_name': self.campaign_details[int(campaign_id)]['name'],
                'campaign_status': self.campaign_details[int(campaign_id)]['status'],
                # Нужно для единичного отчёта
                'full_stat': body,
                'summary_stat': body_sum_data,
            },
            'errors': errors_list,
        }
        По этой структуре на тулзах можно будет составить единичный и суммарный отчёт

    """
    try:
        full_result = {}
        part_path = get_sertificate_path(login, password)
        path = cfg.ROOT_DIR + part_path
        if not os.path.exists(path):
            copy_dir_to_worker(path)
        direct_preprocess = YandexDirectPreprocess(campaign_id_list=id_list, path_to_sertificate=path, start_date=start_date, end_date=end_date)

        parse_result = direct_preprocess.parse()
        full_result['result'] = parse_result

        return full_result
    except Exception, e:
        print 'error'
        import sys, traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()

        message="".join(traceback.format_exception(exc_type, exc_value, exc_traceback))
        print message
        Error.to_db()
        exc = direct_report.retry(exc=e)
        raise exc


@periodic_task(name="get_direct_account_list", run_every=crontab(minute='*/30'))
def get_direct_account_list():
    try:
        for agent_login in cfg.agent_logins:
            agent_user = YandexUser.objects.get(login=agent_login)
            login = agent_user.login
            password = agent_user.password
            ### Получаем список не архивных пользоватей ###
            request_param = {
                'login': login,
                'password': password,
                'direct_method': "GetClientsList",
                'direct_params': {
                    'Filter': {
                        'StatusArch': 'No'
                    }
                }
            }
            result = api_direct_process_request.apply(kwargs=request_param).result
            logins_in_use = []
            if 'data' in result:
                account_data = result['data']
                for direct_account in account_data:
                    users = YandexUser.objects.filter(login=direct_account["Login"])
                    logins_in_use.append(direct_account["Login"])
                    if len(users) == 0:
                        new_user = YandexUser.objects.create(login=direct_account["Login"], password='', agent_login=agent_user)

                # Удаляем неиспользуемые логины
                api_user_list = YandexUser.objects.filter(agent_login=agent_user)
                api_login_list = []
                for user in api_user_list:
                    api_login_list.append(user.login)
                arhive_users = list(set(api_login_list) - set(logins_in_use))
                for login in arhive_users:
                    arhive_user = YandexUser.objects.get(login=login)
                    arhive_user.delete()

    except Exception, e:
        Error.to_db()
        exc = get_direct_account_list.retry(exc=e)
        raise exc


@periodic_task(name="periodic_get_clients_units", run_every=crontab(minute='*/30'))
def periodic_get_clients_units():
    """
        Периодически получает баллы для акаунта яндекса
    """
    try:
        # Для ускорения таски посылаем пачкой запросы для аккаунтов, которые привязаны к агентскому
        for agent_login in cfg.agent_logins:
            agent_user = YandexUser.objects.get(login=agent_login)
            password = agent_user.password
            client_account_list = YandexUser.objects.filter(agent_login=agent_user)
            client_login_list = []
            for client in client_account_list:
                client_login_list.append(client.login)
            if client_login_list:
                ### Получаем список кампаний для клиентского логина ###
                request_param = {
                    'login': agent_login,
                    'password': password,
                    'direct_method': "GetClientsUnits",
                    'direct_params': client_login_list

                }
                result = api_direct_process_request.apply(kwargs=request_param).result
                if 'data' in result:
                    result_data = result['data']
                    for item in result_data:
                        try:
                            user = YandexUser.objects.get(login=item["Login"])
                            user.units_rest = item["UnitsRest"]
                            user.save()
                        except ObjectDoesNotExist:
                            Error.objects.create(
                                tag='Error in client units',
                                message="Login + '" + repr(item["Login"]) + "' does not exist. client_login_list: " + repr(client_login_list)
                            )

        # Для неагентских аккаунтов баллы получаем для каждого в отдельности
        all_accounts = YandexUser.objects.all()
        for account in all_accounts:
            if account.agent_login and account.login not in cfg.agent_logins and \
                            account.agent_login.login not in cfg.agent_logins:
                request_param = {
                    'login': account.login,
                    'password': account.password,
                    'direct_method': "GetClientsUnits",
                    'direct_params': [account.login]
                }
                result = api_direct_process_request.apply(kwargs=request_param).result
                if 'data' in result:
                    account.units_rest = result['data'][0]["UnitsRest"]
                    account.save()
        return 'success'
    except Exception, e:
        Error.to_db()
        exc = periodic_get_clients_units.retry(exc=e)
        raise exc