# encoding: utf-8

from utils.parser.direct_api.client import YandexDirectApiClient
from utils.parser.serp import config
from dblog.models import errors_to_db
from utils.parser.serp.local_utils import connection_timeout
import errors
import time

TMT = config.MAX_TIMEOUT


class WordstatReportManager:

    def __init__(self, api_client):
        self.api_client = api_client

    @connection_timeout(TMT)
    @errors_to_db
    def clear_wordstat_queue(self):
        report_list = self.api_client.call('GetWordstatReportList')['data']
        for report_id in map(lambda a: a['ReportID'], report_list):
            self.api_client.call('DeleteWordstatReport', report_id)

    @connection_timeout(TMT)
    @errors_to_db
    def wordstat(self, request, yandex_region_id=None):
        report = self.retrieve_wordstat_report(request, yandex_region_id)

        freq = 0
        last_updated = None

        searching_with = []
        for wordstat_item in report['SearchedWith']:
            if wordstat_item['Phrase'] == request:
                freq = wordstat_item['Shows']
            searching_with.append({
                'frequency': wordstat_item['Shows'],
                'request': wordstat_item['Phrase']
            })

        searching_also = []
        if 'SearchedAlso' in report:
            for wordstat_item in report['SearchedAlso']:
                searching_also.append({
                    'frequency': wordstat_item['Shows'],
                    'request': wordstat_item['Phrase']
                })

        return [freq, searching_with, searching_also, last_updated]

    def retrieve_wordstat_report(self, request, yandex_region_id=None):
        """
        Возвращает WordstatReportInfo см. http://api.yandex.ru/direct/doc/reference/GetWordstatReport.xml
        """

        params = {'Phrases': [request]}

        if yandex_region_id:
            params['GeoID'] = [yandex_region_id]

        report_id = RetryMachine(
            lambda: self.api_client.call('CreateNewWordstatReport', params)['data'],
            errors.ReportQueueLimitReached,
            12
        ).run()

        report = RetryMachine(
            lambda: self.api_client.call('GetWordstatReport', report_id)['data'][0],
            errors.WordstatReportPending
        ).run()

        self.api_client.call('DeleteWordstatReport', report_id)
        return report


class RetryMachine:
    DEFAULT_MAX_STEPS = 8
    SLEEP_INTERVAL_MULTIPLIER = 2
    INITIAL_SLEEP_INTERVAL = 1

    def __init__(self, payload, exception_class=Exception, max_steps=DEFAULT_MAX_STEPS):
        self.sleep_step = 1
        self.max_steps = max_steps
        self.sleep_interval = self.INITIAL_SLEEP_INTERVAL
        self.payload = payload
        self.exception_class = exception_class

    def sleep_or_raise(self, e):
        if self.sleep_step > self.max_steps:
            raise e
        time.sleep(self.sleep_interval)
        self.sleep_step += 1
        self.sleep_interval *= self.SLEEP_INTERVAL_MULTIPLIER

    def run(self):
        while True:
            try:
                return self.payload()
            except self.exception_class as e:
                self.sleep_or_raise(e)
