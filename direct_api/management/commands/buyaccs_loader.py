# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from direct_api.models import User
from optparse import make_option

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--filename',
            action='store',
            dest='filename',
            default=None,
            help='Filename.'),)
    def handle(self, *args, **options):
        filename = options.get('filename')
        file = open(filename,'r')
        accs = [i.strip().split(':') for i in file.readlines() if i.strip()]
        file.close()
        for acc in accs:
            new_acc = User.objects.get_or_create(login=acc[0], password=acc[1])




