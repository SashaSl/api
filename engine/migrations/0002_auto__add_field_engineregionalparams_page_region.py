# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'EngineRegionalParams.page_region'
        db.add_column('engine_engineregionalparams', 'page_region',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True, default=None, db_index=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'EngineRegionalParams.page_region'
        db.delete_column('engine_engineregionalparams', 'page_region')


    models = {
        'engine.engineregionalparams': {
            'Meta': {'unique_together': "(('region', 'engine'),)", 'object_name': 'EngineRegionalParams'},
            'domain': ('django.db.models.fields.TextField', [], {}),
            'engine': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page_region': ('django.db.models.fields.TextField', [], {'default': 'None', 'db_index': 'True'}),
            'params': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']"}),
            'region_code': ('django.db.models.fields.TextField', [], {})
        },
        'geobase.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'name_en': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_ru': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_translit': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['engine']