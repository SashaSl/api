# -*- coding: utf-8 -*-

import sys
import os
from utils.common import exception_to_str, TorWrapper
from django.core.management.base import BaseCommand
import cfg
from proxy.models import Proxy
from persons.models import Person, MailRu
from optparse import make_option

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--action',
            action='store',
            dest='action',
            default=None,
            help='Action.'),
        make_option('--local',
            action='store_true',
            dest='local',
            default=False,
            help='Use no proxy.'),
        make_option('--change-proxy',
            action='store_true',
            dest='change_proxy',
            default=False,
            help='Change proxy.'),
        make_option('--visible',
            action='store_true',
            dest='visible',
            default=False,
            help='Visible.'),
        make_option('--person-id',
            action='store',
            dest='person_id',
            default=None,
            help='Person ID.'),
        make_option('--proxy',
            action='store',
            dest='proxy',
            default=None,
            help='Proxy.'),
        )

    def handle(self, *args, **options):
        #internet_assert()

        person_id = options.get('person_id')
        _proxy = options.get('proxy')
        person = Person.get_or_generate(id=person_id)
        print >> sys.stderr, 'PROCESS PERSON (ID=%d)' % person.id

        action = options.get('action')

        change_proxy = options.get('change_proxy')
        visible = options.get('visible')
        if change_proxy:
            person.change_proxy()

        local = options.get('local')
        if local:
            proxy = None
        else:
            proxy = Proxy.pick_random().ip_port() if _proxy is None else _proxy

        if action == 'mailru_signup':
            from persons.robots.mailru import generator
            generator(visible).signup(person, proxy)
        elif action == 'mailru_read_lastmsg':
            from persons.robots.mailru import generator
            generator(visible).read_lastmsg(person, proxy)