# -*- coding: utf-8 -*-

import cfg
import os
from local_utils import generate_login, generate_is_male, generate_birthday
from django.db import models
import random
from django.forms.models import model_to_dict
from utils.common import model_filter, random_string, TorWrapper
from proxy.models import Proxy

class Nations:
    NATION_RUSSIAN = "russian"

    NATION_CHOICE = (
        (NATION_RUSSIAN, u'Россия'),
    )

class Name(models.Model):
    name = models.CharField(verbose_name=u"Имя/Фамилия", max_length=100)
    is_first = models.BooleanField(verbose_name=u"Это имя")
    is_male = models.BooleanField(verbose_name=u"Мужской род")
    nation = models.CharField(
        default=Nations.NATION_RUSSIAN,
        choices=Nations.NATION_CHOICE,
        max_length=16, verbose_name=u'Национальность'
    )

    class Meta:
        unique_together = (
            'name',
            'is_first',
            'is_male',
            'nation'
        )
        verbose_name = u"имя"
        verbose_name_plural = u"имена"

    @classmethod
    def generate_full_name(cls, is_male, nation=Nations.NATION_RUSSIAN):
        first_names = model_filter(cls, 60 * 5, is_male=is_male, nation=nation, is_first=True)
        last_names = model_filter(cls, 60 * 5, is_male=is_male, nation=nation, is_first=False)
        first_name = random.choice(first_names).name
        last_name = random.choice(last_names).name
        return first_name, last_name

    @classmethod
    def generate_full_name_str(cls, is_male, nation=Nations.NATION_RUSSIAN):
        return ' '.join(cls.generate_full_name(is_male, nation))


class Account(models.Model):
    person = models.ForeignKey('persons.Person', unique=True)
    login = models.CharField(null=True, blank=True, max_length=100)
    password = models.CharField(null=True, blank=True, max_length=100)
    created = models.DateTimeField(null=True, blank=True)
    last_login = models.DateTimeField(null=True, blank=True)

    class Meta:
        abstract = True

    @property
    def dict(self):
        return model_to_dict(self)

    def refresh(self):
        return self.__class__.objects.get(id=self.id)


class MailRu(Account):
    mail = models.CharField(null=True, blank=True, max_length=100)
    question_num = models.IntegerField(null=True, blank=True)
    answer = models.TextField(null=True, blank=True)
    lastmsg = models.TextField(null=True, blank=True)

    @property
    def email(self):
        return self.login + '@' + self.mail

    class Meta:
        verbose_name = u"mail.ru"
        verbose_name_plural = u"mail.ru"


class Person(models.Model):
    nickname = models.CharField(unique=True, max_length=100)
    password = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    birthday = models.DateField()
    is_male = models.BooleanField()
    nation = models.CharField(
        default=Nations.NATION_RUSSIAN,
        choices=Nations.NATION_CHOICE,
        max_length=16, verbose_name=u'Национальность'
    )
    created = models.DateTimeField(auto_now_add=True)

    proxy = models.CharField(unique=True, max_length=256)
    useragent = models.CharField(max_length=1024)

    is_used = models.BooleanField(default=False)

    def using(self, account_class):
        try:
            account_class.objects.get(person=self)
            return True
        except account_class.DoesNotExist:
            return False

    def account(self, account_class):
        return account_class.objects.get(person=self)

    @property
    def dict(self):
        return model_to_dict(self)

    @classmethod
    def get_or_generate(cls, id=None):
        return cls.objects.get(id=id) if id is not None else cls.generate()

    def refresh(self):
        return self.__class__.objects.get(id=self.id)

    def do_action(self, action, proxy=None, visible=False):
        cmd = '{ROOT_DIR}/manage.py process_person --action={ACTION} --person-id={ID} {PROXY} {VIS}'.format(
            ROOT_DIR=cfg.ROOT_DIR,
            ACTION=action,
            ID=self.id,
            PROXY=('' if proxy is None else '--proxy="%s"' % proxy),
            VIS=('--visible' if visible else '')
        )
        result = os.system(cmd)
        if not result:
            return
        self.change_proxy()
        raise Exception('Cannot perform %s.' % action)

    @classmethod
    def generate_proxy(cls):
        proxies = set(cls.objects.all().values_list('proxy', flat=True))
        proxies = set(map(lambda p: p.ip_port(), Proxy.objects.all())) - proxies
        proxies = list(proxies)
        proxy = random.choice(proxies) if proxies else Proxy.pick_random()
        return proxy

    def change_proxy(self):
        self.proxy = self.__class__.generate_proxy()
        self.save()

    @classmethod
    def generate(cls):
        nation = Nations.NATION_RUSSIAN
        birthday = generate_birthday()
        is_male = generate_is_male()
        first_name, last_name = Name.generate_full_name(is_male, nation)
        login = generate_login(first_name, last_name, birthday)
        useragent = 'Mozilla/5.0'
        proxy = cls.generate_proxy()

        person = cls.objects.create(
            nickname=login,
            password=random_string(),
            first_name=first_name,
            last_name=last_name,
            birthday=birthday,
            is_male=is_male,
            nation=nation,
            proxy=proxy,
            useragent=useragent
        )

        return person

    def generate_login(self):
        return generate_login(self.first_name, self.last_name, self.birthday)

    class Meta:
        verbose_name = u"бот"
        verbose_name_plural = u"боты"

    def __unicode__(self):
        return '%s %s(nickname=%s, password=%s, using_mailru=%s)' % (
            self.first_name,
            self.last_name,
            self.nickname,
            self.password,
            str(self.using(MailRu)),
        )