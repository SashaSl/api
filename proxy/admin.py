# -*- coding: utf-8 -*-

from django.contrib import admin
from models import *

import json
import logging
from string import join

logger = logging.getLogger(__name__)

class ProxyAdmin(admin.ModelAdmin):
    pass

admin.site.register(Proxy, ProxyAdmin)
