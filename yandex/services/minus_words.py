# -*- coding: utf-8 -*-

from utils.linguistics import Morph
from utils.common import CachingMorphAnalyzer, punct_remover_str, HashableDict

def sec_to_min(sec):
    m, s = divmod(sec, 60)
    return "%2d:%02d" % ( m, s)

def total_counters_compiler(lst):
    dct = set()
    for item in lst:
        if not item['name']:
            item['name'] = ''
        dct.add(HashableDict(name=item['name'], id=item['counter_id']))
    return list(dct)

class Minus_words_extractor():
    def __init__(self):
        self.morph = CachingMorphAnalyzer()
        self.normalizer = Morph()
        self.result = []

    def data_iterator(self, data, **kwargs):
        for campaign in data['data']:
            if 'chld' in campaign:
                for ad in campaign['chld']:
                    if 'chld' in ad:
                        for ad_request in ad['chld']:
                            if 'chld' in ad_request:
                                for search_request in ad_request['chld']:
                                    if search_request['denial']*100 >= kwargs['denial'] and search_request['name'] != u'Прочие':
                                        self.result.append({
                                            'search_request': search_request['name'],
                                            'ad_request': ad_request['name'],
                                            'visits': search_request['visits'],
                                            'page_views': search_request['page_views'],
                                            'minus_words': self.subtrackor(search_request['name'], ad_request['name']),
                                            'bounce_rate': search_request['denial']*100,
                                            'elapsed_time': sec_to_min(search_request['visit_time']),
                                            'depth': search_request['depth']
                                        })
        return self.result

    # def prep_cleaner(self, lst):
    #     for word in lst:
    #         morph_word = self.morph.parse(word)
    #         if morph_word:
    #             if morph_word[0].tag.POS in ['PREP', 'CONJ', 'PRCL']: lst.remove(word)
    #     return lst

    def subtrackor(self, search_request, ad_request):
        search_request_forms_list = self.normalizer.norm_to_list(punct_remover_str(search_request), all_normal_forms=True)
        ad_request_forms_list = self.normalizer.norm_to_list(punct_remover_str(ad_request), all_normal_forms=True)
        minus_words = []
        for search_request_forms in search_request_forms_list:
            minus_word_flag = True
            for search_request_form in search_request_forms:
                for ad_request_forms in ad_request_forms_list:
                    if search_request_form in ad_request_forms:
                        minus_word_flag = False
            if minus_word_flag and search_request_forms:
                minus_words.append(search_request_forms[0])
        return minus_words