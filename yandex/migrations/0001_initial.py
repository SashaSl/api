# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'YandexUser'
        db.create_table('yandex_yandexuser', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('login', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32, db_index=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('token', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('token_expires', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('sertificate_path', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('sertificate_expires', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal('yandex', ['YandexUser'])

        # Adding model 'Counters'
        db.create_table('yandex_counters', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['yandex.YandexUser'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('permission', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('owner_login', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('site', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('code_status', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('counter_id', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
        ))
        db.send_create_signal('yandex', ['Counters'])


    def backwards(self, orm):
        # Deleting model 'YandexUser'
        db.delete_table('yandex_yandexuser')

        # Deleting model 'Counters'
        db.delete_table('yandex_counters')


    models = {
        'yandex.counters': {
            'Meta': {'object_name': 'Counters'},
            'code_status': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'counter_id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'owner_login': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'permission': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'site': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['yandex.YandexUser']"})
        },
        'yandex.yandexuser': {
            'Meta': {'object_name': 'YandexUser'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32', 'db_index': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'sertificate_expires': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sertificate_path': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'token_expires': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['yandex']