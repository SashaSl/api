import urllib
import urllib2
import socket

class YaApiToken:
    def __init__(self):
        self.url = 'http://api-metrika.yandex.ru'
        self.methods = {
            'direct_summary': '/stat/sources/direct/summary.json',
            'counters': '/counters.json',
            'counter': '/counter/%d.json'
        }

    def get_report(self, method, params):
        if method == 'counter':
            direct_method = self.methods[method] % params['counter_id']
            del params['counter_id']
        else:
            direct_method = self.methods[method]
        url = self.url + direct_method
        data = urllib.urlencode(params)
        url = url + "?" + data
        while True:
            try:
                result = urllib2.urlopen(url, timeout=300).read()
                break
            except socket.timeout:
                pass
        return result
