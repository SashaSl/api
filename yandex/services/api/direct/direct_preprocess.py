# -*- coding: utf-8 -*-

import os
from time import sleep, time
import xmltodict

import json
import urllib2
import httplib
import datetime

from yandex_direct import YandexDirectApi

class YandexDirectPreprocess:
    """
        Вход: список кампаний и путь к сертификату, начальная и конечная дата отчёта
        Выход: данные отчёта яндекса и суммарная статистика яндекса

        Принцип действия:
            Вызов - YandexDirectPreprocess.parse()
            Получаем данные по геобазе и целям.
            Посылаем запросы на создание отчёта на яндекс апи. Максимальное количество одновременных отчётов - 5
            Когда отчёт выполнен то мы его удаляем. Количество отчётов для одной кампании равно количеству её целей
            Время составления одного отчёта по одной цели - от 30 сек до 5 минут
            Если алгоритм сломается не успев удалить запрошенные отчёты то происходит следующее:
                Проверяем какие отчёты запрошенны на на яндекс директ. Если какой то отчёт держится там дольше
                8 минут то мы его удаляем.
            Сохраняются данные по кликам из первого запрошенного отчёта( в последующих они аналогичны) и данные по
            целям из каждого отчёта.

        По данным полученным этим классом можно составить excel документ с помощья класса DirectReport
    """
    def __init__(self, campaign_id_list=None, start_date=None, end_date=None, path_to_sertificate=None):
        self.direct_api = YandexDirectApi(path_to_sertificate)
        # Максимально возможное количество одновременных репортов на яндексе
        self.max_yandex_reports = 5
        # 8 минут - если за это время репорт не создастся то мы его удаляем с яндекса
        self.max_time = 60 * 8
        # Число рестартов при ожидании ответа чтобы не было бесконечного цикла
        self.max_count = 500
        self.campaign_id_list = campaign_id_list
        self.sleep_seconds = 20
        if start_date:
            self.start_date = start_date
            self.end_date = end_date
        else:
            # Указываем сегодняшнюю дату
            today = str(datetime.datetime.now()).split()
            self.start_date = self.end_date = today[0]
        self.sertificate_status = self.direct_api.check_sertificate()

    def parse(self):
        self.etalon_goals = self.get_etalon_goals()
        self.region_base = self.get_regions()
        # self.campaign_details = self.campaign_info()
        parse_results = {}
        for campaign_id in self.campaign_id_list:
            campaign_id = int(campaign_id)
            result = self.get_data(campaign_id)
            parse_results[campaign_id] = result
        return parse_results

    def campaign_info(self):
        direct_campaign_dict = {}
        direct_campaign_data = self.direct_api.make_action("GetCampaignsList", live_url=True)
        if 'data' in direct_campaign_data:
            for direct_campaign in direct_campaign_data['data']:
                direct_campaign_dict[direct_campaign['CampaignID']] = {
                    'name': direct_campaign['Name'],
                    'status': direct_campaign['Status'],
                    'tools_status': False,
                }
        return direct_campaign_dict

    def get_etalon_goals(self):
        etalon = {}
        for campaign_id in self.campaign_id_list:
            campaign_id = int(campaign_id)
            param = {
                "CampaignIDS": [campaign_id],
            }
            goals_data = self.direct_api.make_action("GetStatGoals", live_url=True, param=param)
            goals = self.get_goals(goals_data)
            # Новый способ нахождения эталонных целей
            etalon.update(goals)
        return etalon

    def get_mass_report_id(self, reports):
        mass_id = []
        for report in reports['data']:
            mass_id.append(report['ReportID'])
        return mass_id

    def delete_reports(self, mass_id):
        for rep_id in mass_id:
            self.direct_api.make_action("DeleteReport", param=rep_id)

    def get_regions(self):
        region_data = self.direct_api.make_action("GetRegions", live_url=True)
        region_base = {}
        for reg in region_data['data']:
            region_base[reg['RegionID']] = reg
        return region_base

    def get_goals(self, goals_data):
        goals = {}
        for goal in goals_data['data']:
            goals[goal['GoalID']] = {
                'name': goal['Name'],
                'search_goal_reached': goal['GoalsReached'],
                'context_goal_reached': goal['ContextGoalsReached']
            }
        return goals

    def get_new_reports_with_goal_stat(self, campaign_id, goals, min_value, max_value):
        parsed_goals = {}
        for goal_id in goals.keys()[min_value:max_value]:
            param = {
                'CampaignID': campaign_id,
                'StartDate': self.start_date,
                'EndDate': self.end_date,
                'GroupByColumns': ['clBanner', 'clPage', 'clDate', 'clGeo', 'clPhrase', 'clStatGoals', 'clPositionType'],
                'GroupByDate': 'day',
                'OrderBy': ['clBanner'],
                'TypeResultReport': 'xml',
            }
            if goal_id:
                param['Filter'] = {
                    'StatGoals': [goal_id]
                }
            response = self.direct_api.make_action("CreateNewReport", live_url=True, param=param)
            if 'error_code' in response and response['error_code'] == 31:
                sleep(self.sleep_seconds)
                break
            else:
                parsed_goals[response['data']] = goal_id
        return parsed_goals

    def get_docs(self, mass_id):
        self.direct_api.make_action("GetCampaignsList", live_url=True)
        rep_pending = True
        count = 0
        while rep_pending and count < self.max_count:
            rep_pending = False
            report_data = self.direct_api.make_action("GetReportList")
            for rep in report_data['data']:
                if rep['ReportID'] in mass_id:
                    if rep['StatusReport'] == 'Pending':
                        rep_pending = True
            count += 1
            sleep(self.sleep_seconds)
        mass_docs = {}
        for rep in report_data['data']:
            if rep['ReportID'] in mass_id:
                report_url = rep['Url']
                xml_doc = urllib2.urlopen(str(report_url)).read()
                doc = xmltodict.parse(xml_doc)
                mass_docs[rep['ReportID']] = doc
        return mass_docs

    def del_all_reports(self):
        reports = self.direct_api.make_action("GetReportList")
        mass_id = self.get_mass_report_id(reports)
        self.delete_reports(mass_id)

    def get_data(self, campaign_id):
        error_reports = {}
        param = {
            "CampaignIDS": [campaign_id],
        }
        goals_data = self.direct_api.make_action("GetStatGoals", live_url=True, param=param)
        goals = self.get_goals(goals_data)
        goals_for_parse = goals.copy()

        # Вписываем id цели = 0. Это означает что репорт будет будет собран вне зависимости от того есть ли в компании
        # цели. Данные этого репорта будем записывать в document_data(но не данные по его целям)
        goals_for_parse[0] = {
            'name': 'No Goals',
            'search_goal_reached': 0,
            'context_goal_reached': 0
        }
        if self.etalon_goals:
            etalon_goals = self.etalon_goals
        else:
            etalon_goals = goals
        slice_len_min = 0
        slice_len_max = self.max_yandex_reports
        not_etalon_docs = {}

        pending_count = 0
        document_data = []
        goals_data = {}
        while goals_for_parse and pending_count < self.max_count:
            parsed_goals = self.get_new_reports_with_goal_stat(campaign_id, goals_for_parse, slice_len_min, slice_len_max)
            if len(parsed_goals.keys()) < len(goals_for_parse.keys()[slice_len_min:slice_len_max]):
                pending_count += 1
                reports = self.direct_api.make_action("GetReportList")
                for report in reports['data']:
                    if report[u'ReportID'] not in error_reports.keys():
                        error_reports[report[u'ReportID']] = time()
                    report_time = error_reports[report[u'ReportID']]
                    delta = time() - report_time
                    if delta > self.max_time:
                        self.delete_reports([report[u'ReportID']])
            mass_id = []
            for response in parsed_goals.keys():
                mass_id.append(response)
            part_docs = self.get_docs(mass_id)
            if part_docs:
                for report_num, goal_id in parsed_goals.iteritems():
                    if goal_id == 0:
                        document_data = part_docs[report_num]

            # part_docs[id докум] = документ
            # parsed_goals[id документа] = id цели
            docs_with_goals = {}
            for response_num, parsed_doc in part_docs.iteritems():
                if parsed_goals[response_num] != 0:
                    goal_cost = []
                    goal_conversion = []
                    for_average_goals_price = 0.00
                    if 'row' in parsed_doc['report']['stat']:
                        for row in parsed_doc['report']['stat']['row']:
                            if '@goal_cost' in row:
                                goal_cost.append(row['@goal_cost'])
                            else:
                                goal_cost.append('-')
                            if '@goal_conversion' in row:
                                goal_conversion.append(row['@goal_conversion'])
                                # конверсия*клики по этой конверсии/100
                                for_average_goals_price += round(float(row['@goal_conversion'])/100, 2) * int(row['@clicks'])
                            else:
                                goal_conversion.append('-')
                    else:
                        goal_cost.append('-')
                        goal_conversion.append('-')

                    goals_data[parsed_goals[response_num]] = {
                        'goal_cost': goal_cost,
                        'goal_conversion': goal_conversion,
                        'for_average_goals_price': for_average_goals_price,
                    }
                # docs_with_goals[parsed_goals[key]] = value
            not_etalon_docs.update(docs_with_goals)
            for goal_id in parsed_goals.values():
                del goals_for_parse[goal_id]
            self.delete_reports(mass_id)

        param = {
            'CampaignIDS': [campaign_id],
            'StartDate': self.start_date,
            'EndDate': self.end_date,
        }
        sum_stat = self.direct_api.make_action("GetSummaryStat", live_url=True, param=param)
        result = {
            'etalon_goals': etalon_goals,
            'goals': goals,
            'goals_data': goals_data,
            'document_data': document_data,
            'raw_summary_stat': sum_stat,
            'region_base': self.region_base,
            'start_date': self.start_date,
            'end_date': self.end_date,
            # 'campaign_name': self.campaign_details[int(campaign_id)]['name'],
            # 'campaign_status': self.campaign_details[int(campaign_id)]['status'],
        }
        return result