# -*- coding: utf-8 -*-

import struct
import marisa_trie
from linguistics import Morph

MORPH = Morph()

class FastSuggest(object):
    _words_cache = {}

    def __init__(self, target_name):
        self._requests = marisa_trie.Trie().load(target_name + '.requests.dict')
        self._words = marisa_trie.BytesTrie().load(target_name + '.words.dict')

    def _get_request_ids(self, word):
        if word in self._words_cache:
            return self._words_cache[word]
        packed_ids = self._words.get(word, [])
        ids = set([struct.unpack('>I', packed_id)[0] for packed_id in packed_ids])
        self._words_cache[word] = ids
        return ids

    def iter_matching(self, phrases):
        target_ids = set()
        for phrase in phrases:
            words = MORPH.norm_to_set(phrase)
            ids_sets = [self._get_request_ids(word) for word in words]
            target_ids |= set.intersection(*ids_sets)

        for i in target_ids:
            prefix = unicode(i) + u' '
            request = self._requests.keys(prefix)[0].split(u' ', 1)[1]
            yield request

    @classmethod
    def from_textfile(cls, textfile_name, target_name):
        words_map = set()
        requests_set = set()
        request_id = 0
        with open(textfile_name) as f:
            for i, request in enumerate(f):
                if (i % 1000) == 0: print i
                request = request.strip().decode('utf-8')
                if not request: continue
                _request = unicode(request_id) + u' ' + request
                if _request in requests_set: continue
                requests_set.add(_request)
                words = MORPH.norm_to_set(request)
                for word in words:
                    key_val = word, struct.pack('>I', request_id)
                    words_map.add(key_val)
                request_id += 1
        _requests_set = marisa_trie.Trie(requests_set)
        _requests_set.save(target_name + '.requests.dict')
        _words_map = marisa_trie.BytesTrie(words_map)
        _words_map.save(target_name + '.words.dict')