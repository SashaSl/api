# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ProxyData'
        db.create_table('proxy_proxydata', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ip', self.gf('django.db.models.fields.IPAddressField')(max_length=15, db_column='ip', db_index=True)),
            ('port', self.gf('django.db.models.fields.IntegerField')(db_column='port')),
            ('protocol', self.gf('django.db.models.fields.TextField')(db_column='protocol')),
            ('proxy_type', self.gf('django.db.models.fields.TextField')(db_column='type')),
            ('is_tor', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='is_tor')),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['geobase.Region'], null=True, blank=True)),
        ))
        db.send_create_signal('proxy', ['ProxyData'])


    def backwards(self, orm):
        # Deleting model 'ProxyData'
        db.delete_table('proxy_proxydata')


    models = {
        'geobase.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'name_en': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_ru': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_translit': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        },
        'proxy.proxydata': {
            'Meta': {'object_name': 'ProxyData'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15', 'db_column': "'ip'", 'db_index': 'True'}),
            'is_tor': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'is_tor'"}),
            'port': ('django.db.models.fields.IntegerField', [], {'db_column': "'port'"}),
            'protocol': ('django.db.models.fields.TextField', [], {'db_column': "'protocol'"}),
            'proxy_type': ('django.db.models.fields.TextField', [], {'db_column': "'type'"}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['proxy']