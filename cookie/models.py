# -*- coding: utf-8 -*-

import json

from pprint import pprint

from django.db import models
from django.db import transaction

from engine.models import Engine
from utils.common import ts_to_datetime

class Cookie(models.Model):
    name = models.TextField(verbose_name=u"Название")
    value = models.TextField(verbose_name=u"Значение")
    path = models.TextField(verbose_name=u"Путь")
    domain = models.TextField(verbose_name=u"Домен")
    secure = models.BooleanField(verbose_name=u"Secure")
    expires = models.DateTimeField(verbose_name=u"Годен до")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u"Создан")
    webuser = models.ForeignKey('webuser.WebUser', null=True, blank=True, verbose_name=u"wuid", db_index=True)

    @classmethod
    def add_and_save(cls, cookie, webuser):
        cookie['expires'] = ts_to_datetime(cookie['expires'])
        cookie['webuser'] = webuser
        cls.objects.create(**cookie)

    @classmethod
    def save_cookie_list(cls, cookie_list, webuser):
        cls.objects.filter(webuser=webuser).delete()
        for cookie in cookie_list:
            cookie['expires'] = ts_to_datetime(cookie['expires'])
            # cookie['expires'] = cookie['expires']
            cookie['webuser'] = webuser
            cls.objects.create(**cookie)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u"Cookie"
        verbose_name_plural = u"Cookies"