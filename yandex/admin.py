# -*- coding: utf-8 -*-

import logging

from django.contrib import admin
from models import *

logger = logging.getLogger(__name__)


class YandexUserAdmin(admin.ModelAdmin):
    list_display = [
        'login',
        'password',
        'agent_login',
        'token',
        'token_expires',
        'sertificate_path',
        'sertificate_expires'
    ]

class CountersAdmin(admin.ModelAdmin):
    def login(self, obj):
        return obj.user
    login.short_description = u'Логин'

    list_display = [
        'login',
        'name',
        'counter_id',
        'permission',
        'owner_login',
        'site',
        'code_status',
        'type'
    ]

class SearchRequestAdmin(admin.ModelAdmin):
    list_display = [
        'phrase'
    ]

class SearchRequestBudgetHistoryAdmin(admin.ModelAdmin):
    def phrase(self, obj):
        return obj.search_request
    phrase.short_description = u'Запрос'

    list_display = [
        'phrase',
        'region',
        'shows',
        'clicks_premium',
        'clicks_firstplace',
        'clicks_guaranteed',
        'price_premium',
        'price_firstplace',
        'price_guaranteed',
        'ctr_premium',
        'ctr_firstplace',
        'ctr_guaranteed',
        'created_at',
    ]

class SearchRequestBudgetAdmin(SearchRequestBudgetHistoryAdmin):
    list_display = SearchRequestBudgetHistoryAdmin.list_display + ['updated_at']

admin.site.register(YandexUser, YandexUserAdmin)
admin.site.register(Counters, CountersAdmin)
admin.site.register(SearchRequest, SearchRequestAdmin)
admin.site.register(SearchRequestBudget, SearchRequestBudgetAdmin)
admin.site.register(SearchRequestBudgetHistory, SearchRequestBudgetHistoryAdmin)
