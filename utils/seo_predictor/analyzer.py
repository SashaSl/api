# -*- coding: utf-8 -*-

"""
Исходное письмо:
Давид,
Ниже описание параметров, которые мы обсуждали вчера и плюс мои какие-то дополнения.

1. Общедоменные показатели
С возможностью упорядочивания по параметрам на странице.

охват аудитории - (трафик/макс.трафик)*100%
покрытие ядра - по запросам: (кол-во запросов в поиске/кол-во запросов в ядре)*100%; по запросам, взвешенным на частоту: (сумма частот запросов в поиске/сумма частот запросов в ядре)*100%
скорость загрузки страниц - написать скорость и оценку от 0 до 5 этой скорости, где 5 офигенно, а 0 - сайт не грузится
соответствие рекомендациям Яндекса по появление быстрых ссылок - да/нет (возможно отдельные блоки написать, которые не будут выводиться)
месячный бюджет на продвижение - (кол-во ссылок на сайт*цену ссылки + кол-во статей на сайте*цену статьи)/срок жизни сайта
трафик - прогнозное кол-во трафика

2. Показатели под каждый запрос для домена
С возможностью упорядочивания по параметрам на странице.

запрос
выдача: заголовок; сниппет; быстрые ссылки в сниппете; домен; позиция Яндекс (представление аналогично БЕ)
броскость заголовка - функция от 0 до 1 с ростом от черноты, плотности черноты и "левости" черноты (по сути та мера, которую ты вводил для определения соответствия статьи запросу в предикторе)
броскость сниппета - функция от 0 до 1 с ростом от черноты, плотности черноты и "левости" черноты (по сути та мера, которую ты вводил для определения соответствия статьи запросу в предикторе)
соответствие статьи запросу: тайтл, Н1, кейвордс, дескриптион, Н2 - по каждой из этих частей (или что скажешь более нужное) оценку на вроде описанной выше (в броскости), а общая оценка собирается взвешенно
качество доноров - если у статьи некачественные доноры, то выводим упоминание об этом с расшифровкой, аля "на вас ссылается 1000 доноров, а на ТОП-1 по запросу всего лишь 10 доноров!"
частота по точной - частота с кавычками
частота по неточной - частота без кавычек

3. Непокрытые запросы под каждый домен
С возможностью упорядочивания по параметрам на странице.

запрос
выдача: ТОП-5 сайтов (доменов)
частота по точной
частота по неточной

################################################################################

Формат выходных данных:
data = {
    'requests': [
        {
            'request': '...'
            'top5': [
                {
                    'position': 1,
                    'url': '...',
                },
                ...
            ],
            'frequency': 0..(int),
            'frequency_precise': 0..(int),
            'top3_donors': 0..(int),
        },
        ...
    },
    'domains': [
        {
            'domain': '...unique canonized domain name...',
            'traffic_coverage': 0.0..1.0(float),
            'kernel_coverage': 0.0..1.0(float),
            'kernel_coverage_weighted': 0.0..1.0(float),
            'speed_score': 0..5(int),
            'fast_links_coverage': 0.0..1.0(float),
            'fast_links_coverage_weighted': 0.0..1.0(float),
            'month_budget': 0.0..(float),
            'month_traffic': 0.0..(float),
            'details': [
                {
                    'request_id': int,
                    'title': '...',
                    'snippet': '...',
                    'fast_links': '...',
                    'position': 1..50,
                    'title_power': 0.0..1.0(float),
                    'snippet_power': 0.0..1.0(float),
                    'article_power': 0.0..1.0(float),
                    'article_power_details': {
                        'title': 0.0..1.0(float),
                        'description': 0.0..1.0(float),
                        'keywords': 0.0..1.0(float),
                        'h1': 0.0..1.0(float),
                        'h2': 0.0..1.0(float),
                        'strong_em': 0.0..1.0(float),
                        'a': 0.0..1.0(float),
                        'parent_a': 0.0..1.0(float),
                        'img': 0.0..1.0(float),
                    },
                    'donors_ancors_power': 0.0..1.0(float),
                    'donors_ancors_fraudness': 0.0..1.0(float),
                    'donors_quality': 0.0..1.0(float),
                    'donors_count': 0..(int),
                },
                ...
            ],
            'not_covered_requests': [int, ...]
        },
        ...
    ]
}
"""

import re
import datetime
from utils.common import extract_domain
from utils.group_by import *
from collections import defaultdict
from .predictor import SeoConst, SeoPlan
from seo.common import compute_fraudness, Power, complex_power, agg_power

__all__ = ['analyze', 'get_speed_score']

def get_speed_score(download_time):
    if not isinstance(download_time, float):
        # return None
        return 5
    if download_time < 1.0:
        return 5
    if download_time < 2.0:
        return 4
    if download_time < 3.0:
        return 3
    if download_time < 5.0:
        return 2
    if download_time < 10.0:
        return 1
    return 0


def analyze(items):
    """
    items = [
        {
            'request': '...',
            'frequency': int,
            'frequency_precise': int,
            'frequency_agg': int,
            'direct_cost': {float|None},
            'serp': [
                {
                    'url': '...',
                    'position': 1..50,
                    'donors_ancors_fraudness': 0.0..1.0,
                    'donors_ancors_power': 0.0..1.0,
                    'article_power': 0.0..1.0,
                    'donors': 0..,
                    'links': 0..,
                    'main_donors': 0..,
                    'main_links': 0..,
                    'title': '...',
                    'snippet': '...',
                    'fast_links': bool,
                    'download_time': 0.0..,
                    'article_power_details': {
                        'title': 0.0..1.0,
                        'keywords': 0.0..1.0,
                        'description': 0.0..1.0,
                        'h1': 0.0..1.0,
                        'h2': 0.0..1.0,
                        'img': 0.0..1.0,
                        'strong_em': 0.0..1.0,
                        'a': 0.0..1.0,
                        'parent_a': 0.0..1.0
                    },
                    'whois_created': 'YYYYMMDD',
                    'whois_private': bool
                },
                ...
            ]
        },
        ...
    ]
    """

    power = Power()
    request_data = []
    request_id = {}
    direct_costs = []
    max_traffic = 0.0
    results = []
    domain_main_donors = {}
    domain_datalist = defaultdict(list)

    for request_item in items:
        request = request_item['request']
        frequency = request_item['frequency']
        frequency_precise = request_item['frequency_precise']
        frequency_agg = request_item['frequency_agg']
        direct_cost = request_item['direct_cost']

        if direct_cost is not None:
            direct_costs.append(direct_cost)

        top5 = []
        top3_donors = []
        _max_traffic = frequency_agg * SeoConst.SERP_CLICK_DISTR[0] / 100.0
        max_traffic += _max_traffic

        reqid = len(request_data)

        for serp_item in request_item['serp']:
            domain = extract_domain(serp_item['url'])
            position = serp_item['position']
            download_time = serp_item['download_time']
            traffic = frequency_agg * SeoConst.SERP_CLICK_DISTR[position - 1] / 100.0

            speed_score = get_speed_score(download_time)
            donors = serp_item['donors']
            domain_main_donors[domain] = serp_item['main_donors']

            if len(top5) < 5:
                top5.append({
                    'position': position,
                    'url': serp_item['url']
                })
            if len(top3_donors) < 3:
                top3_donors.append(donors)

            if serp_item['whois_created']:
                age = (datetime.date.today() - datetime.datetime.strptime(serp_item['whois_created'], '%Y%m%d').date()).days
            else:
                age = 365

            domain_item = {
                'url': serp_item['url'],
                'request_id': reqid,
                'position': position,
                'download_time': download_time,
                'speed_score': speed_score,
                'traffic': traffic,
                'fast_links': serp_item['fast_links'],
                'age': age,
                'donors': donors,
                'main_donors': serp_item['main_donors'],
                'title': serp_item['title'],
                'snippet': serp_item['snippet'],
                'title_power': power.run(request, serp_item['title']),
                'snippet_power': power.run(request, serp_item['snippet']),
                'article_power_details': serp_item['article_power_details'],
                'article_power': serp_item['article_power'],
                'donors_ancors_power': serp_item['donors_ancors_power'],
                'donors_ancors_fraudness': serp_item['donors_ancors_fraudness'],
            }

            domain_datalist[domain].append(domain_item)

        top3_donors = top3_donors or [0.0]
        top3_donors = float(sum(top3_donors)) / len(top3_donors)
        request_id[request] = reqid
        request_data.append({
            'request': request,
            'frequency': frequency,
            'frequency_precise': frequency_precise,
            'frequency_agg': frequency_agg,
            'direct_cost': direct_cost,
            'top3_donors': top3_donors,
            'top5': top5,
            'max_traffic': _max_traffic
        })

    direct_costs = direct_costs or [0.0]
    avg_direct_cost = float(sum(direct_costs)) / len(direct_costs)
    avg_link_cost = []
    for request in request_data:
        if request['direct_cost'] is None:
            request['direct_cost'] = avg_direct_cost
        request['link_cost'] = SeoConst.DIRECT_FUNC(request['direct_cost'])
        avg_link_cost.append(request['link_cost'])
    avg_link_cost = avg_link_cost or [0.0]
    avg_link_cost = float(sum(avg_link_cost)) / len(avg_link_cost)
    requests_set = set(request_id.itervalues())

    for domain in domain_datalist:
        traffic = 0.0
        domain_max_traffic = 0.0
        speed_score = []
        fast_links = []
        frequency_aggs = []
        month_budget = SeoConst.ARTICLE_COST * len(domain_datalist[domain]) + avg_link_cost * domain_main_donors[domain]
        month_traffic = 0.0
        covered_requests = set()
        details = []
        for request_item in domain_datalist[domain]:
            request_id = request_item['request_id']
            _request_item = request_data[request_id]
            request = _request_item['request']
            try:
                donors_quality = _request_item['top3_donors'] * (51 - request_item['position']) / float(50 * request_item['donors'])
            except ZeroDivisionError:
                donors_quality = 1.0

            if request_id not in covered_requests:
                traffic += request_item['traffic']
                domain_max_traffic += _request_item['max_traffic']

                details.append({
                    'request_id': request_id,
                    'url': request_item['url'],
                    'title': request_item['title'],
                    'snippet': request_item['snippet'],
                    'fast_links': request_item['fast_links'],
                    'position': request_item['position'],
                    'donors_quality': donors_quality,
                    'title_power': request_item['title_power'],
                    'snippet_power': request_item['snippet_power'],
                    'article_power': request_item['article_power'],
                    'article_power_details': request_item['article_power_details'],
                    'donors_ancors_power': request_item['donors_ancors_power'],
                    'donors_ancors_fraudness': request_item['donors_ancors_fraudness'],
                    'donors_count': request_item['donors'],
                })

                covered_requests.add(request_id)

            speed_score.append(request_item['speed_score'])
            fast_links.append(int(request_item['fast_links']))
            frequency_aggs.append(_request_item['frequency_agg'])

            month_budget += _request_item['link_cost'] * request_item['donors'] - avg_link_cost * request_item['donors']
            month_traffic += request_item['traffic']

        domain_item = {
            'domain': domain,
            'traffic_coverage': traffic / max_traffic,
            'kernel_coverage': float(len(covered_requests)) / len(request_data),
            'kernel_coverage_weighted': domain_max_traffic / max_traffic,
            'speed_score': float(sum(speed_score)) / len(speed_score),
            'fast_links_coverage': float(sum(fast_links)) / len(fast_links),
            'fast_links_coverage_weighted': sum([x * y for x, y in zip(fast_links, frequency_aggs)]) / (sum(frequency_aggs) or 1),
            'month_budget': month_budget,
            'month_traffic': month_traffic,
            'details': details,
            'not_covered_requests': list(requests_set - covered_requests)
        }

        results.append(domain_item)

    for request in request_data:
        del request['link_cost']
        del request['frequency_agg']
        del request['direct_cost']
        del request['max_traffic']

    return {
        'requests': request_data,
        'domains': results
    }