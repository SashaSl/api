# -*- coding: utf-8 -*-

import cfg
import os
import sys
import time
import json
import urllib
import logging
import tempfile
import datetime
import traceback
import chardet

from utils.common import Tor, ts_to_datetime, ts_to_date, connection_timeout
from utils.parser.serp.browser import Browser

from dblog.models import Error, errors_to_db

from local_utils import ContentExtractor
from seo.common import fix_url

TMT = cfg.crawler_timeout


class NoJsBrowser(Browser):
    @connection_timeout(TMT)
    @Tor(replace_only_none_proxy=False)
    def parse(self, url, proxy=None, tor_disable=False):
        if proxy is not None: self.set_proxy(proxy)
        browser = self.browser
        try:
            try:
                timer = time.time()
                browser.open(url, timeout = TMT)
                timer = time.time() - timer
            except:
                try:
                    timer = time.time()
                    browser.open(fix_url(url), timeout = TMT)
                    timer = time.time() - timer
                except Exception, e:
                    Error.to_db(msg=url)
                    raise

            browser._factory.is_html = True
            response = browser.response()
            resp = response.read()

            if int(response.code) != 200:
                return {'error': 'Response code: ' + str(response.code)}

            ce = ContentExtractor()
            enc = chardet.detect(resp)
            try:
                html = resp.decode(enc['encoding'].lower())
            except:
                html = []

            return {
                'result': ce.extract_from_page(resp),
                'html': html,
                'download_time': timer
            }

        except:
            Error.to_db()
            exc_type, exc_value, exc_traceback = sys.exc_info()
            return {'error': "".join(traceback.format_exception(exc_type, exc_value, exc_traceback))}

        return None

