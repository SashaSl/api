# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'WebUser.proxy'
        db.add_column('webuser_webuser', 'proxy',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['proxy.Proxy'], null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'WebUser.proxy'
        db.delete_column('webuser_webuser', 'proxy_id')


    models = {
        'geobase.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'name_en': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_ru': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_translit': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        },
        'proxy.proxy': {
            'Meta': {'object_name': 'Proxy'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15', 'db_index': 'True'}),
            'is_tor': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'port': ('django.db.models.fields.IntegerField', [], {}),
            'protocol': ('django.db.models.fields.TextField', [], {}),
            'proxy_type': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        },
        'webuser.webuser': {
            'Meta': {'object_name': 'WebUser'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'proxy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['proxy.Proxy']", 'null': 'True', 'blank': 'True'}),
            'usage_count': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['webuser']