# -*- coding: utf-8 -*-

from django.contrib import admin
from models import *

import json
import logging
from string import join

logger = logging.getLogger(__name__)

class CookieAdmin(admin.ModelAdmin):
    def cutted_value(self, obj):
        max_size = 50
        if (len(obj.value) < max_size):
            return obj.value
        else :
            return obj.value[0:max_size] + '...'

    cutted_value.short_description = u'Значение'

    list_display = [
        'name', 
        'cutted_value',
        'domain', 
        'path',
        'secure',
        'created_at',
        'expires',
        'webuser'
    ]

admin.site.register(Cookie, CookieAdmin)
