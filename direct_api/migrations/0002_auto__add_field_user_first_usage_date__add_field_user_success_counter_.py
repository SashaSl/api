# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'User.first_usage_date'
        db.add_column('direct_api_user', 'first_usage_date',
                      self.gf('django.db.models.fields.DateTimeField')(default=None, null=True, blank=True),
                      keep_default=False)

        # Adding field 'User.success_counter'
        db.add_column('direct_api_user', 'success_counter',
                      self.gf('django.db.models.fields.IntegerField')(default=None, null=True, blank=True),
                      keep_default=False)

        # Adding field 'User.fail_counter'
        db.add_column('direct_api_user', 'fail_counter',
                      self.gf('django.db.models.fields.IntegerField')(default=None, null=True, blank=True),
                      keep_default=False)

        # Adding field 'User.last_fail_date'
        db.add_column('direct_api_user', 'last_fail_date',
                      self.gf('django.db.models.fields.DateTimeField')(default=None, null=True, blank=True),
                      keep_default=False)

        # Adding field 'User.last_success_date'
        db.add_column('direct_api_user', 'last_success_date',
                      self.gf('django.db.models.fields.DateTimeField')(default=None, null=True, blank=True),
                      keep_default=False)

        # Adding field 'User.is_used'
        db.add_column('direct_api_user', 'is_used',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


        # Changing field 'User.login'
        db.alter_column('direct_api_user', 'login', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32))

    def backwards(self, orm):
        # Deleting field 'User.first_usage_date'
        db.delete_column('direct_api_user', 'first_usage_date')

        # Deleting field 'User.success_counter'
        db.delete_column('direct_api_user', 'success_counter')

        # Deleting field 'User.fail_counter'
        db.delete_column('direct_api_user', 'fail_counter')

        # Deleting field 'User.last_fail_date'
        db.delete_column('direct_api_user', 'last_fail_date')

        # Deleting field 'User.last_success_date'
        db.delete_column('direct_api_user', 'last_success_date')

        # Deleting field 'User.is_used'
        db.delete_column('direct_api_user', 'is_used')


        # Changing field 'User.login'
        db.alter_column('direct_api_user', 'login', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32, null=False))

    models = {
        'direct_api.user': {
            'Meta': {'object_name': 'User'},
            'fail_counter': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'first_usage_date': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_used': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_fail_date': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'last_success_date': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'limit_reached_at': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'success_counter': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['direct_api']