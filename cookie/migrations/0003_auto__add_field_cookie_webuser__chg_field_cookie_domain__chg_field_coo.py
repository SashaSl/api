# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Cookie.webuser'
        db.add_column('cookie_cookie', 'webuser',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['webuser.WebUser'], null=True, blank=True),
                      keep_default=False)


        # Changing field 'Cookie.domain'
        db.alter_column('cookie_cookie', 'domain', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Cookie.name'
        db.alter_column('cookie_cookie', 'name', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Cookie.expires'
        db.alter_column('cookie_cookie', 'expires', self.gf('django.db.models.fields.DateTimeField')())

        # Changing field 'Cookie.value'
        db.alter_column('cookie_cookie', 'value', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Cookie.path'
        db.alter_column('cookie_cookie', 'path', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Cookie.secure'
        db.alter_column('cookie_cookie', 'secure', self.gf('django.db.models.fields.BooleanField')())

    def backwards(self, orm):
        # Deleting field 'Cookie.webuser'
        db.delete_column('cookie_cookie', 'webuser_id')


        # Changing field 'Cookie.domain'
        db.alter_column('cookie_cookie', 'domain', self.gf('django.db.models.fields.TextField')(db_column='domain'))

        # Changing field 'Cookie.name'
        db.alter_column('cookie_cookie', 'name', self.gf('django.db.models.fields.TextField')(db_column='name'))

        # Changing field 'Cookie.expires'
        db.alter_column('cookie_cookie', 'expires', self.gf('django.db.models.fields.DateTimeField')(db_column='expires'))

        # Changing field 'Cookie.value'
        db.alter_column('cookie_cookie', 'value', self.gf('django.db.models.fields.TextField')(db_column='value'))

        # Changing field 'Cookie.path'
        db.alter_column('cookie_cookie', 'path', self.gf('django.db.models.fields.TextField')(db_column='path'))

        # Changing field 'Cookie.secure'
        db.alter_column('cookie_cookie', 'secure', self.gf('django.db.models.fields.BooleanField')(db_column='secure'))

    models = {
        'cookie.cookie': {
            'Meta': {'object_name': 'Cookie'},
            'domain': ('django.db.models.fields.TextField', [], {}),
            'expires': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'path': ('django.db.models.fields.TextField', [], {}),
            'secure': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'value': ('django.db.models.fields.TextField', [], {}),
            'webuser': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['webuser.WebUser']", 'null': 'True', 'blank': 'True'})
        },
        'webuser.webuser': {
            'Meta': {'object_name': 'WebUser'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['cookie']