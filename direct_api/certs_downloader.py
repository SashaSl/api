import time
import cfg
import glob
import zipfile
import os
import utils.parser.serp.config
from selenium.common.exceptions import NoSuchElementException, WebDriverException, UnexpectedAlertPresentException
from utils.selebot.surfer import Surfer
from selenium.webdriver.common.keys import Keys
from utils.common import Tor, DeleteDir, copy_file_to_api
from utils.parser.serp.local_utils import Xvfb
import shutil
from dblog.models import Error

def _anti_captcha(surfer, browser, css):
    try:
        surfer.wait_for_css(css)
    except UnexpectedAlertPresentException:
        pass
    try:
        browser.find_element_by_css_selector('img[alt="captcha"]')
        while True:
            try:
                captcha_text = surfer.solve_captcha('form[action] img[alt="captcha"]')
            except IndexError:
                break
            if not captcha_text == 'ERROR_CAPTCHA_UNSOLVABLE':
                browser.find_element_by_css_selector('input[type="text"]').send_keys(captcha_text)
                browser.find_element_by_css_selector('input[type="submit"]').click()
                try:
                    surfer.wait_for_css(css)
                except UnexpectedAlertPresentException:
                    pass
                try:
                    browser.find_element_by_css_selector('img[alt="captcha"]')
                except NoSuchElementException:
                    break
    except NoSuchElementException:
        pass

def _final_downloader(browser, surfer):
    surfer.wait_for_css('input[id="create_cert"]')
    bad_elements = browser.find_elements_by_css_selector('a[class="b-cert-list__action"]')
    if len(bad_elements) > 1:
        max_retry = 50
        retry_count = 0
        time.sleep(10)
        while True or retry_count < max_retry:
            bad_elements[-1].click()
            time.sleep(3)
            retry_count += 1
            try:
                alert = browser.switch_to_alert()
                time.sleep(3)
                alert.accept()
                _anti_captcha(surfer, browser, 'div[class="b-head-line"]')
                time.sleep(10)
                break
            except Exception, msg:
                print msg
                _anti_captcha(surfer, browser, 'div[class="b-head-line"]')

        surfer.wait_for_css('input[id="create_cert"]')

    browser.find_element_by_css_selector('input[id="create_cert"]').click()
    _anti_captcha(surfer, browser, 'div[class="b-head-line"]')
    surfer.wait_for_css('form[class="b-cert-form__form"]')
    js_script = "document.getElementsByName('days')[0].value = 90"
    browser.execute_script(js_script)
    browser.find_element_by_css_selector('input[class="b-cert-form__submit_button"]').click()
    time.sleep(3)
    while True:
        try:
            alert = browser.switch_to_alert()
            time.sleep(3)
            alert.accept()
            _anti_captcha(surfer, browser, 'div[class="b-head-line"]')
            time.sleep(10)
            break
        except:
            _anti_captcha(surfer, browser, 'div[class="b-head-line"]')

def _form_filler(browser, surfer):
    browser.find_element_by_css_selector('a[href="https://direct.yandex.ru/registered/main.pl?cmd=apiSettings&api_welcome=yes"]').click()
    _anti_captcha(surfer, browser, 'div[class="b-head-line"]')
    surfer.wait_for_css('label[for="accept"]')
    browser.find_element_by_css_selector('label[for="accept"]').click()
    time.sleep(2)
    browser.find_element_by_css_selector('input[type="submit"]').click()
    _anti_captcha(surfer, browser, 'div[class="b-head-line"]')
    surfer.wait_for_css('a[href="https://direct.yandex.ru/registered/main.pl?cmd=certList&ulogin="]')
    browser.find_element_by_css_selector('a[href="https://direct.yandex.ru/registered/main.pl?cmd=certList&ulogin="]').click()
    _anti_captcha(surfer, browser, 'div[class="b-head-line"]')
    _final_downloader(browser, surfer)

def _certs_downloader(browser, surfer):
    browser.get('https://direct.yandex.ru/registered/main.pl?cmd=apiSettings')
    _anti_captcha(surfer, browser, 'div[class="b-head-line"]')
    try:
        browser.find_element_by_css_selector('a[href="https://direct.yandex.ru/registered/main.pl?cmd=certList&ulogin="]')
        browser.find_element_by_css_selector('a[href="https://direct.yandex.ru/registered/main.pl?cmd=certList&ulogin="]').click()
        _anti_captcha(surfer, browser, 'div[class="b-head-line"]')
        _final_downloader(browser, surfer)
        return
    except NoSuchElementException:
        pass
    try:
        browser.find_element_by_css_selector('a[href="https://direct.yandex.ru/registered/main.pl?cmd=apiSettings&api_welcome=yes"]')
        _form_filler(browser, surfer)
    except NoSuchElementException:
        _final_downloader(browser, surfer)

def _campaign_filler(login, password, folder):
    surfer = Surfer(useragent='Mozilla/5.0', folder=folder)
    surfer.open_browser('https://direct.yandex.ru/registered/main.pl?cmd=chooseInterfaceType')
    browser = surfer.get_browser()
    surfer.wait_for_css('input[name="login"]')
    browser.find_element_by_name('login').send_keys(login)
    browser.find_element_by_name('passwd').send_keys(password)
    browser.find_element_by_name('passwd').send_keys(Keys.ENTER)

    _anti_captcha(surfer, browser, 'div[class="b-head-line"]')

    # time.sleep(3)
    surfer.wait_for_css('div[class="b-head-line"]')
    try:
        browser.find_element_by_css_selector('input[name="continue"]').click()
    except NoSuchElementException:
        pass
    surfer.wait_for_css('h1')
    try:
        browser.find_element_by_css_selector('a[href="https://direct.yandex.ru/registered/main.pl?cmd=apiSettings"]')
        _certs_downloader(browser, surfer)
        return None
    except NoSuchElementException:
        js_script = "document.getElementsByName('client_country')[0].value = 225"
        browser.execute_script(js_script)
        js_script = "document.getElementsByName('currency')[0].value = 'RUB'"
        browser.execute_script(js_script)
        browser.find_element_by_css_selector('input[class="b-choose-interface-type__radio"][value="std"]').click()
        browser.find_element_by_css_selector('input[type="submit"]').click()
        _anti_captcha(surfer, browser, 'div[class="b-head-line"]')
        surfer.wait_for_css('input[type="button"][class="b-campaign-edit__submit"]')
        browser.find_element_by_css_selector('input[type="button"][class="b-campaign-edit__submit"]').click()
        _anti_captcha(surfer, browser, 'div[class="b-head-line"]')
        surfer.wait_for_css('div[class="b-head-line"]')
        _certs_downloader(browser, surfer)

@Tor(replace_only_none_proxy=False)
@Xvfb(visible=utils.parser.serp.config.WORDSTAT_VISIBILITY, width=1200, height=600, timeout=30)
def handler(login, password, save_to_media,  **kwargs):
    folder = cfg.ROOT_DIR + '/api_data/direct_api/' + login
    if os.path.exists(folder):
        DeleteDir(folder)
    _campaign_filler(login, password, folder)
    zip_file = glob.glob(folder+'/*.zip')[0]

    if save_to_media:
        path_to_media = cfg.ROOT_DIR + cfg.media_root
        if not os.path.exists(path_to_media):
            os.makedirs(path_to_media)
        path_to_direct_folder = path_to_media + cfg.direct_sertificate
        if not os.path.exists(path_to_direct_folder):
            os.makedirs(path_to_direct_folder)
        path_to_folder = path_to_direct_folder + '/' + login
        if os.path.exists(path_to_folder):
            DeleteDir(path_to_folder)
        shutil.copytree(folder, path_to_folder)
        file_for_copy = glob.glob(path_to_folder+'/*.zip')[0]

        copy_file_to_api(file_for_copy)

    with zipfile.ZipFile(zip_file, 'r') as z:
        z.extractall(folder)
    os.remove(zip_file)
    return os.path.basename(zip_file)