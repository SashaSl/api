# -*- coding: utf-8 -*-


def direct_ctr(clicks, shows):
    if int(shows) != 0:
        res_ctr = round(float(clicks) * 100/float(shows), 2)
    else:
        res_ctr = 0
    return res_ctr


def get_goals_names(goals):
    goals_names = []
    for goal_id in goals.keys():
        goals_names.append(goals[goal_id]['name'])
    return goals_names


def goals_and_conversions_text(goals_names, average=False):
    text_list = []
    if not average:
        for name in goals_names:
            text_list.append(u'Цена цели ' + name + u' (у.е.)')
            text_list.append(u'Конверсия (%)')
    else:
        for name in goals_names:
            text_list.append(u'Средняя цена цели ' + name + u' (у.е.)')
            text_list.append(u'Средняя конверсия (%)')
    return text_list


def correct_goals(company_one_goals, company_two_goals):
    goals_set_one, goals_set_two = set(company_one_goals.keys()), set(company_two_goals.keys())
    if len(goals_set_one) > len(goals_set_two):
        return goals_set_two.issubset(goals_set_one)
    else:
        return goals_set_one.issubset(goals_set_two)

