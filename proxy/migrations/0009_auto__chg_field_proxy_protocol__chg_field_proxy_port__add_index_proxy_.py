# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Proxy.protocol'
        db.alter_column('proxy_proxy', 'protocol', self.gf('django.db.models.fields.CharField')(max_length=10))

        # Changing field 'Proxy.port'
        db.alter_column('proxy_proxy', 'port', self.gf('django.db.models.fields.IntegerField')())
        # Adding index on 'Proxy', fields ['port']
        db.create_index('proxy_proxy', ['port'])

        # Adding unique constraint on 'Proxy', fields ['ip', 'port']
        db.create_unique('proxy_proxy', ['ip', 'port'])


    def backwards(self, orm):
        # Removing unique constraint on 'Proxy', fields ['ip', 'port']
        db.delete_unique('proxy_proxy', ['ip', 'port'])

        # Removing index on 'Proxy', fields ['port']
        db.delete_index('proxy_proxy', ['port'])


        # Changing field 'Proxy.protocol'
        db.alter_column('proxy_proxy', 'protocol', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Proxy.port'
        db.alter_column('proxy_proxy', 'port', self.gf('django.db.models.fields.IntegerField')(null=True))

    models = {
        'geobase.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'name_en': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_ru': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_translit': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        },
        'proxy.proxy': {
            'Meta': {'unique_together': "(('ip', 'port'),)", 'object_name': 'Proxy'},
            'fail_counter': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15', 'db_index': 'True'}),
            'is_tor': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_check': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'last_fail': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'last_usage': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'port': ('django.db.models.fields.IntegerField', [], {'default': '80', 'db_index': 'True'}),
            'protocol': ('django.db.models.fields.CharField', [], {'default': "'http'", 'max_length': '10'}),
            'proxy_type': ('django.db.models.fields.CharField', [], {'default': "'unchecked'", 'max_length': '50', 'db_index': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['proxy']