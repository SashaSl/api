# -*- coding: utf-8 -*-

import gc
import cfg
import json
from django.core.management.base import BaseCommand
from parser.models import Parser
from yandex.models import *
from yandex.services import SRB_save


def queryset_iterator(queryset, chunksize=1000):
    pk = 0
    last_pk = queryset.order_by('-pk')[0].pk
    queryset = queryset.order_by('pk')
    while pk < last_pk:
        for row in queryset.filter(pk__gt=pk)[:chunksize]:
            pk = row.pk
            item = row.parser_data
            budget = item.result['budget']
            if isinstance(budget, dict):
                SRB_save(item.request, item.region, budget )

        gc.collect()

class Command(BaseCommand):
    def handle(self, *args, **options):
        qset = Parser.objects.filter(mode=Parser.MODE_BUDGET)
        queryset_iterator(qset)






