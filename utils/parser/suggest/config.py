# -*- coding: utf-8 -*-

import re

SERVERS = {
    "morda_ru": "morda_ru",
    "serp_ru": "serp_ru",
    }

SUGGEST_URL = {
    'yandex':   'http://suggest.yandex.{DOMAIN}/suggest-ya.cgi?v=4&lr={REGION}&srv={SERVER}&part={PREFIX}',
    'google':   'https://www.google.{DOMAIN}/s?gs_ri=heirloom-hp&sclient={CLIENT}&q={PREFIX}',
    'mail':     'http://suggests.go.mail.ru/sg_u?q={PREFIX}',
    'bing':     'http://api.bing.com/qsonhs.aspx?q={PREFIX}',
}

PREV_REQUEST_URL = {
    'yandex':   'http://yandex.{DOMAIN}/yandsearch?lr={REGION}&text={REQUEST}',
    'google':   None,
    'mail':     None,
    'bing':     None,
}

RESULT_MASK = {
    'yandex': None,
    'google': re.compile("^window\.google\.ac\.h\((.*)\)$"),
}

HEADERS = {
    'User-Agent': 'Mozilla/5.0',
}

REGIONS = {
    'moscow': {
        'yandex': {
            'domain': 'ru',
            'code': '213',
        },
        'google': {
            'domain': 'ru',
            'code': '0J6sXuwc',
        },
    },
    'peterburg': {
        'yandex': {
            'domain': 'ru',
            'code': '2',
        },
        'google': {
            'domain': 'ru',
            'code': '10L_QtdGC0LXRgNCx0YPRgNCz',
        },
    },
    'ekaterinburg': {
        'yandex': {
            'domain': 'ru',
            'code': '54',
        },
        'google': {
            'domain': 'ru',
            'code': '10JXQutCw0YLQtdGA0LjQvdCx0YPRgNCz',
        },
    },
    'kazan': {
        'yandex': {
            'domain': 'ru',
            'code': '43',
        },
        'google': {
            'domain': 'ru',
            'code': '10LrQsNC30LDQvdGM',
        },
    },
    'novosibirsk': {
        'yandex': {
            'domain': 'ru',
            'code': '65',
        },
        'google': {
            'domain': 'ru',
            'code': '10L3QvtCy0L7RgdC40LHQuNGA0YHQug',
        },
    },
    'krasnodar': {
        'yandex': {
            'domain': 'ru',
            'code': '35',
        },
        'google': {
            'domain': 'ru',
            'code': '10LrRgNCw0YHQvdC-0LTQsNGA',
        },
    },
    'rostov': {
        'yandex': {
            'domain': 'ru',
            'code': '39',
        },
        'google': {
            'domain': 'ru',
            'code': '10YDQvtGB0YLQvtCy',
        },
    },
    'voronezh': {
        'yandex': {
            'domain': 'ru',
            'code': '193',
        },
        'google': {
            'domain': 'ru',
            'code': '10LLQvtGA0L7QvdC10LY',
        },
    },
    'ufa': {
        'yandex': {
            'domain': 'ru',
            'code': '172',
        },
        'google': {
            'domain': 'ru',
            'code': '10YPRhNCw',
        },
    },
    'omsk': {
        'yandex': {
            'domain': 'ru',
            'code': '66',
        },
        'google': {
            'domain': 'ru',
            'code': '10L7QvNGB0Lo',
        },
    },
    'astana': {
        'yandex': {
            'domain': 'kz',
            'code': '163',
        },
        'google': {
            'domain': 'kz',
            'code': '10JDRgdGC0LDQvdCw',
        },
    },
    'kiev': {
        'yandex': {
            'domain': 'ua',
            'code': '143',
        },
        'google': {
            'domain': 'com.ua',
            'code': '10JrQuNC10LI',
        },
    },
    'minsk': {
        'yandex': {
            'domain': 'by',
            'code': '157',
        },
        'google': {
            'domain': 'by',
            'code': '10JzQuNC90YHQug',
        },
    },
}
