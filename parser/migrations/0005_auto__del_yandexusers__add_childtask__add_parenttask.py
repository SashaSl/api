# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'YandexUsers'
        db.delete_table('parser_yandexusers')

        # Adding model 'ChildTask'
        db.create_table('parser_childtask', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parent_task', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['parser.ParentTask'])),
            ('child_task', self.gf('django.db.models.fields.CharField')(unique=True, max_length=40)),
        ))
        db.send_create_signal('parser', ['ChildTask'])

        # Adding model 'ParentTask'
        db.create_table('parser_parenttask', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parent_task', self.gf('django.db.models.fields.CharField')(unique=True, max_length=40)),
        ))
        db.send_create_signal('parser', ['ParentTask'])


    def backwards(self, orm):
        # Adding model 'YandexUsers'
        db.create_table('parser_yandexusers', (
            ('password', self.gf('django.db.models.fields.CharField')(max_length=32, unique=True, db_index=True)),
            ('login', self.gf('django.db.models.fields.CharField')(max_length=32, unique=True, db_index=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('parser', ['YandexUsers'])

        # Deleting model 'ChildTask'
        db.delete_table('parser_childtask')

        # Deleting model 'ParentTask'
        db.delete_table('parser_parenttask')


    models = {
        'geobase.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'name_en': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_ru': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_translit': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        },
        'parser.childtask': {
            'Meta': {'object_name': 'ChildTask'},
            'child_task': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent_task': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['parser.ParentTask']"})
        },
        'parser.parenttask': {
            'Meta': {'object_name': 'ParentTask'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent_task': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'})
        },
        'parser.parser': {
            'Meta': {'object_name': 'Parser'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mode': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'operation': ('django.db.models.fields.TextField', [], {'db_index': 'True'})
        },
        'parser.parserdata': {
            'Meta': {'object_name': 'ParserData'},
            '_result': ('django.db.models.fields.TextField', [], {'db_column': "'result'"}),
            'engine': ('django.db.models.fields.TextField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parser': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['parser.Parser']"}),
            'prev_request': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'}),
            'request': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'db_index': 'True'})
        }
    }

    complete_apps = ['parser']