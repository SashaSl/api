from django.contrib import admin
from models import *

class RegionAdmin(admin.ModelAdmin):
    pass

admin.site.register(Region, RegionAdmin)
