from django.contrib import admin
from models import *

class EngineRegionalParamsAdmin(admin.ModelAdmin):
    list_display = [
        '__unicode__',
        'region',
        'engine',
        'domain',
        'region_code',
        'params',
        'page_region'
    ]

admin.site.register(EngineRegionalParams, EngineRegionalParamsAdmin)