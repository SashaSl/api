# -*- coding: utf-8 -*-

import re

class Morph(object):
    space = re.compile(u'\s+')
    cache = {}

    def __init__(self, use_cache=True):
        import pymorphy2
        self.morph = pymorphy2.MorphAnalyzer()
        self.use_cache = use_cache

    def norm_to_list(self, text, all_normal_forms=False):
        words = []
        for word in self.space.split(text):
            if not word: continue
            norm = None
            if self.use_cache and not all_normal_forms:
                norm = self.cache.get(word, None)
            if norm is None:
                norm = self.morph.parse(word)
                if all_normal_forms:
                    if norm:
                        norm = [morph_word.normal_form for morph_word in norm]
                    else:
                        norm = [word]
                else:
                    norm = norm[0].normal_form if norm else word
                if self.use_cache and not all_normal_forms:
                    self.cache[word] = norm
            words.append(norm)
        return words

    def norm_to_set(self, text):
        return set(self.norm_to_list(text))

    def norm_to_string(self, text):
        return u' '.join(self.norm_to_list(text))