# -*- coding: utf-8 -*-

################################################################################

from .common import generate_id, KWObject
import random

################################################################################

class Morpher:
    __KB_LAYOUT = """ёЁйцукенгшщзхъфывапролджэячсмитьбюЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ""".decode("utf-8"),\
                  """`~qwertyuiop[]asdfghjkl;'zxcvbnm,.QWERTYUIOP{}ASDFGHJKL:"ZXCVBNM<>""".decode("utf-8")

    __TRANSLIT = {
        "й" : ["j", "y", "i"],
        "ц" : ["ts"],
        "у" : ["u"],
        "к" : ["k"],
        "е" : ["e", "ye", "ie"],
        "н" : ["n"],
        "г" : ["g"],
        "ш" : ["sh"],
        "щ" : ["sch"],
        "з" : ["z"],
        "х" : ["h"],
        "ъ" : ["", "''"],
        "ф" : ["f"],
        "ы" : ["y", "i"],
        "в" : ["v", "w"],
        "а" : ["a"],
        "п" : ["p"],
        "р" : ["r"],
        "о" : ["o"],
        "л" : ["l"],
        "д" : ["d"],
        "ж" : ["zh", "j", "z"],
        "э" : ["e"],
        "я" : ["ya", "ja", "ia"],
        "ч" : ["ch"],
        "с" : ["s"],
        "м" : ["m"],
        "и" : ["i"],
        "т" : ["t"],
        "ь" : ["", "'"],
        "б" : ["b"],
        "ю" : ["yu", "ju", "iu"],
        "ё" : ["yo", "jo", "io"]
    }

    ########################################

    def __init__(self):
        KB_MAP = {}
        for i, c in enumerate(self.__KB_LAYOUT[0]):
            KB_MAP[c] = self.__KB_LAYOUT[1][i]
        self.__KB_LAYOUT = KB_MAP

        TR_MAP = {}
        for k, vs in self.__TRANSLIT.iteritems():
            k = k.decode("utf-8")
            vs = [v.decode("utf-8") for v in vs]
            TR_MAP[k] = vs
        self.__TRANSLIT = TR_MAP

    ########################################

    def __keyboard_layout_ru_to_en(self, text):
        res = ""
        for c in text:
            res += self.__KB_LAYOUT.get(c, c)
        return res

    ########################################

    def __translit_ru_to_en(self, text):
        res = ""
        for c in text:
            if c == c.upper():
                c = c.lower()
                l = self.__TRANSLIT.get(c, [c])
                random.shuffle(l)
                res += l[0].upper()
            else:
                l = self.__TRANSLIT.get(c, [c])
                random.shuffle(l)
                res += l[0]
        return res

    ########################################

    def translit(self, text):
        return self.__translit_ru_to_en(text)

    ########################################

    def __misspell_remove(self, text):
        if len(text) == 0:
            return text
        i = random.randrange(0, len(text))
        return text[:i] + text[i+1:]

    ########################################

    def __misspell_replace(self, text):
        if len(text) < 2:
            return text
        i = random.randrange(0, len(text) - 1)
        return text[:i] + text[i+1] + text[i] + text[i+2:]

    ########################################

    def generate_form(self, request):
        if isinstance(request, basestring):
            request = KWObject(
                morpher_capitalize=3,
                morpher_upper=1.5,
                morpher_title=1.5,
                morpher_remove=1,
                morpher_replace=1,
                morpher_kb_layout=1,
                morpher_translit=1,
                request=request
            )

        ws = [
            request.morpher_capitalize,
            request.morpher_upper,
            request.morpher_title,
            request.morpher_remove,
            request.morpher_replace,
            request.morpher_kb_layout,
            request.morpher_translit,
        ]
        w_normal = max(0.0, 100.0 - sum(ws))
        ws.append(w_normal)
        idx = generate_id(ws)
        if idx == 0:
            return request.request.capitalize()
        elif idx == 1:
            return request.request.upper()
        elif idx == 2:
            return request.request.title()
        elif idx == 3:
            return self.__misspell_remove(request.request)
        elif idx == 4:
            return self.__misspell_replace(request.request)
        elif idx == 5:
            return self.__keyboard_layout_ru_to_en(request.request)
        elif idx == 6:
            return self.__translit_ru_to_en(request.request)
        elif idx == 7:
            return request.request

################################################################################

def morph(request):
    return Morpher().generate_form(request)

def translit(text):
    return Morpher().translit(text)

################################################################################
