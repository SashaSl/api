# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'EngineRegionalParams'
        db.create_table('engine_engineregionalparams', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['geobase.Region'])),
            ('engine', self.gf('django.db.models.fields.TextField')(db_index=True)),
            ('domain', self.gf('django.db.models.fields.TextField')()),
            ('region_code', self.gf('django.db.models.fields.TextField')()),
            ('params', self.gf('django.db.models.fields.TextField')(db_index=True)),
        ))
        db.send_create_signal('engine', ['EngineRegionalParams'])

        # Adding unique constraint on 'EngineRegionalParams', fields ['region', 'engine']
        db.create_unique('engine_engineregionalparams', ['region_id', 'engine'])


    def backwards(self, orm):
        # Removing unique constraint on 'EngineRegionalParams', fields ['region', 'engine']
        db.delete_unique('engine_engineregionalparams', ['region_id', 'engine'])

        # Deleting model 'EngineRegionalParams'
        db.delete_table('engine_engineregionalparams')


    models = {
        'engine.engineregionalparams': {
            'Meta': {'unique_together': "(('region', 'engine'),)", 'object_name': 'EngineRegionalParams'},
            'domain': ('django.db.models.fields.TextField', [], {}),
            'engine': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'params': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']"}),
            'region_code': ('django.db.models.fields.TextField', [], {})
        },
        'geobase.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'name_en': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_ru': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_translit': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['engine']