# -*- coding: utf-8 -*-
import re
import random
import json
from django.db import models
from engine.models import Engine
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import transaction

import cfg

class Proxy(models.Model):
    PROXY_TRANSPARENT = "transparent"
    PROXY_ANONYMOUS = "anonymous"
    PROXY_ELITE = "elite"
    PROXY_UNCHECKED = "unchecked"
    PROXY_CHOICE = (
        (PROXY_TRANSPARENT, PROXY_TRANSPARENT),
        (PROXY_ANONYMOUS, PROXY_ANONYMOUS),
        (PROXY_ELITE, PROXY_ELITE),
        (PROXY_UNCHECKED, PROXY_UNCHECKED),
    )

    PROTO_HTTP = 'http'
    PROTO_SOCKS4 = 'socks4'
    PROTO_SOCKS5 = 'socks5'
    PROTO_CHOICE = (
        (PROTO_HTTP, 'http'),
        (PROTO_SOCKS4, 'socks4'),
        (PROTO_SOCKS5, 'socks5')
    )

    ip = models.IPAddressField(verbose_name=u"IP", db_index=True)
    port = models.IntegerField(verbose_name=u"Port", default=80, db_index=True, validators=[MinValueValidator(1), MaxValueValidator(32767)])
    protocol = models.CharField(verbose_name=u"Протокол", max_length=10, choices=PROTO_CHOICE, default=PROTO_HTTP)
    proxy_type = models.CharField(choices=PROXY_CHOICE, verbose_name=u"Тип", db_index=True, default=PROXY_UNCHECKED, max_length=50)
    is_tor = models.BooleanField(verbose_name=u"isTor", default=False)
    region = models.ForeignKey('geobase.Region', null=True, blank=True, verbose_name=u"Регион", db_index=True)
    last_check = models.DateTimeField(verbose_name=u"Последняя проверка", null=True, blank=True)
    last_usage = models.DateTimeField(verbose_name=u"Последнее использование", null=True, blank=True)
    last_fail = models.DateTimeField(verbose_name=u"Последний фейл", null=True, blank=True)
    fail_counter = models.IntegerField(verbose_name=u"Количество фейлов", null=True, blank=True)

    def ip_port(self):
        return self.ip + ":" + str(self.port)

    @classmethod
    def pick_random(cls, is_tor=False):
        qs = cls.objects.filter(is_tor=is_tor)
        count = qs.count()
        seed = random.randint(0, count - 1)
        return qs[seed]

    @classmethod
    def get_by_str(self, proxy_str):
        ip, port = None, None
        is_tor = True

        if proxy_str: 
            m = re.match(cfg.proxy_mask, proxy_str)
            if m:
                ip, port = m.group(1), m.group(3)

        if (ip and port):
            is_tor = False

        print ip
        print port
        print is_tor

        if ip:
            proxy_obj, created = Proxy.objects.get_or_create(ip=ip, port=port, is_tor=is_tor)
            return proxy_obj
        else:
            return None

    def __unicode__(self):
        result = ''
        if self.is_tor:
            return 'tor: %s' % (self.ip)
        else:
            return 'proxy: %s:%s' % (self.ip, self.port)

    class Meta:
        verbose_name = u"Прокси"
        verbose_name_plural = u"Прокси"

        unique_together = ('ip', 'port')

