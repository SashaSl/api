# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Proxy.proxy_type'
        db.alter_column('proxy_proxy', 'proxy_type', self.gf('django.db.models.fields.TextField')())
        # Adding index on 'Proxy', fields ['proxy_type']
        db.create_index('proxy_proxy', ['proxy_type'])


    def backwards(self, orm):
        # Removing index on 'Proxy', fields ['proxy_type']
        db.delete_index('proxy_proxy', ['proxy_type'])


        # Changing field 'Proxy.proxy_type'
        db.alter_column('proxy_proxy', 'proxy_type', self.gf('django.db.models.fields.TextField')(null=True))

    models = {
        'geobase.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'name_en': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_ru': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_translit': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        },
        'proxy.proxy': {
            'Meta': {'object_name': 'Proxy'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15', 'db_index': 'True'}),
            'is_tor': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_check': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'last_fail': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'last_usage': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'port': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'protocol': ('django.db.models.fields.TextField', [], {}),
            'proxy_type': ('django.db.models.fields.TextField', [], {'default': "'unchecked'", 'db_index': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['proxy']