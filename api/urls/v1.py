# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from api.views import v1
from yandex.views import sertificate_parse, sertificate_get

urlpatterns = patterns('',
    url(r'^task/status$', v1.task_status, name='api_v1_task_status'),
    url(r'^task/result$', v1.task_result, name='api_v1_task_result'),
    url(r'^task/result_v2$', v1.task_result_v2, name='api_v1_task_result_v2'),

    url(r'^parser/suggest/parse$', v1.parser_suggest_parse, name='api_v1_parser_suggest_parse'),
    url(r'^parser/suggest/get$', v1.parser_suggest_get, name='api_v1_parser_suggest_get'),

    url(r'^parser/crawler/parse$', v1.parser_crawler_parse, name='api_v1_parser_crawler_parse'),
    url(r'^parser/crawler/get$', v1.parser_crawler_get, name='api_v1_parser_crawler_get'),

    url(r'^parser/direct/parse$', v1.parser_direct_parse, name='api_v1_parser_direct_parse'),
    url(r'^parser/direct/get$', v1.parser_direct_get, name='api_v1_parser_direct_get'),
    url(r'^parser/direct/get_chunk$', v1.parser_direct_get_chunk, name='api_v1_parser_direct_get_chunk'),

    url(r'^parser/budget/parse$', v1.parser_budget_parse, name='api_v1_parser_budget_parse'),
    url(r'^parser/budget/get$', v1.parser_budget_get, name='api_v1_parser_budget_get'),
    url(r'^parser/budget/get_chunk$', v1.parser_budget_get_chunk, name='api_v1_parser_budget_get_chunk')
)