# -*- coding: utf-8 -*-

import random
from collections import defaultdict

from ..linguistics import Morph
from seo.common import stop_words_finder, stop_words_cleaner_list, remove_subset_stop_words
from ..common import generate_from_dict, sublist_exists, remove_subset, join_lists, punct_remover, split_list

__all__ = ['ArticlesPlan']

class ArticlesPlan(object):
    OPERATION_REWRITE = 'rewrite'
    OPERATION_WRITE = 'write'

    MORPH = Morph()

    H2_COUNT = {
        3: 0.5,
        4: 0.5
    }

    LINKS_COUNT = {
        1: 0.25,
        2: 0.5,
        3: 0.25
    }

    @classmethod
    def from_kernel(cls, site, items, used_requests, days, tags=None):
        from .predictor import SeoPlan, SeoConst
        seo_plan = SeoPlan(items, 0, site, 6)
        sorted_requests = sorted(seo_plan.requests, key=lambda r: seo_plan.request_frequency[r], reverse=True)

        used_requests = set(used_requests)
        buy_article = {}
        request_id = {}
        request_url = {}
        article_cost = {}
        unused_requests = set(seo_plan.requests)
        rid = 1
        for request, target in seo_plan.request_target.iteritems():
            if target:
                buy_article[request] = 0
                request_id[request] = rid
                request_url[request] = target['url']
                article_cost[request] = 0.0
                used_requests.add(request)
                unused_requests -= {request}
                rid += 1

        used_sorted_requests = []
        for request in sorted_requests:
            if request not in used_requests:
                continue
            if request in request_id:
                continue
            used_sorted_requests.append(request)

        day_requests = split_list(used_sorted_requests, days)

        for day, requests in enumerate(day_requests):
            for request in requests:
                buy_article[request] = day
                article_cost[request] = SeoConst.ARTICLE_COST
                unused_requests -= {request}
                request_id[request] = rid
                rid += 1

        data = {
            'request_id': request_id,
            'request_url': request_url,
            'unused_requests': list(unused_requests),
            'request_creation_day': buy_article,
            'request_frequency': seo_plan.request_frequency,
            'request_creation_cost': article_cost
        }

        return cls(data, tags=tags)

    def __init__(self, data, tags=None):
        """
        data = {
            'request_id': {
                '...(request)': \d,
                ...
            },
            'actions': [
                {
                    'request': '...',
                    'operation': '{link|article}',
                    'cost': float,
                    'request_id': 1..N,
                    'day': 0..D
                },
                ...
            ],
            'request_url': {
                '...(request)': '...(url)',
                ...
            },
            'unused_requests': ['...(request)', ...],
            'request_creation_day': {
                '...(request)': \d,
                ...
            },
            'request_frequency': {
                '...(request)': \d,
                ...
            },
            'request_creation_cost': {
                '...(request)': float,
                ...
            }
        }
        """
        self.tags = tags
        self.request_id = data['request_id']
        self.request_creation_day = data['request_creation_day']
        self.request_frequency = data['request_frequency']
        self.request_url = data['request_url']
        self.request_cost = data['request_creation_cost']
        self.already_created_requests = set(self.request_url.iterkeys())

        self.ordered_used_requests = sorted(
            self.request_creation_day.iterkeys(),
            key=lambda request: (self.request_creation_day[request], self.request_id[request]),
        )

        self.ordered_unused_requests = sorted(
            set(data['unused_requests']),
            key=lambda request: self.request_frequency[request],
            reverse=True
        )

        norm_unused_requests = set()
        self.uniq_ordered_unused_requests = []
        for request in self.ordered_unused_requests:
            norm_request = self.MORPH.norm_to_string(request)
            if norm_request in norm_unused_requests:
                continue
            norm_unused_requests.add(norm_request)
            self.uniq_ordered_unused_requests.append(request)

    def _generate_subrequests(self):
        request_subrequests = {}
        unused = []
        for request in self.ordered_used_requests:
            request_stop_words = stop_words_finder(request.split())
            if not self.uniq_ordered_unused_requests:
                request_subrequests[request] = []
                continue
            if not unused:
                unused = self.uniq_ordered_unused_requests[:]
            h2_count = generate_from_dict(self.H2_COUNT)
            norm_request = self.MORPH.norm_to_list(request)
            subrequests = filter(
                lambda r: sublist_exists(self.MORPH.norm_to_list(r), norm_request),
                stop_words_cleaner_list(unused, redundant_stop_words=request_stop_words)
            )[:h2_count]
            unused = remove_subset_stop_words(unused, set(subrequests), redundant_stop_words=request_stop_words)
            while len(subrequests) < h2_count:
                if not unused:
                    unused = self.uniq_ordered_unused_requests[:]
                subrequests.append(stop_words_cleaner_list([unused.pop(0)], redundant_stop_words=request_stop_words)[0])
            random.shuffle(subrequests)  # вероятно, это не нужно
            request_subrequests[request] = subrequests
        return request_subrequests

    def _generate_subrequests_with_tags(self):
        def _extract_reqs(reqs, tags, norm):
            def _normalize_list(lst, norm):
                if not norm:
                    return [i.lower() for i in lst]
                return [self.normalizer.norm_to_string(item).lower() for item in lst]

            extracted_reqs = []
            for req in reqs:
                if filter(lambda i: i in _normalize_list(punct_remover(req), norm), _normalize_list(tags, norm)):
                    extracted_reqs.append(req)
            return extracted_reqs

        garbage_kernel = _extract_reqs(self.uniq_ordered_unused_requests, self.tags['garbage_tags'], self.tags['norm'])
        transact_kernel = _extract_reqs(self.uniq_ordered_unused_requests, self.tags['transaction_tags'], self.tags['norm'])
        main_kernel = list(set(self.uniq_ordered_unused_requests) - set(garbage_kernel) - set(transact_kernel))
        transact_kernel = list(set(transact_kernel) - set(garbage_kernel))
        self.uniq_ordered_unused_requests = transact_kernel + main_kernel

        request_subrequests = {}
        unused = []
        for request in self.ordered_used_requests:
            request_stop_words = stop_words_finder(request.split())
            current_trans_tags = self.tags['transaction_tags'][:]
            if not self.uniq_ordered_unused_requests:
                request_subrequests[request] = []
                continue
            if not unused:
                unused = self.uniq_ordered_unused_requests[:]
            h2_count = generate_from_dict(self.H2_COUNT)
            norm_request = self.MORPH.norm_to_list(request)
            subrequests = filter(
                lambda r: sublist_exists(self.MORPH.norm_to_list(r), norm_request),
                stop_words_cleaner_list(unused, redundant_stop_words=request_stop_words)
            )[:h2_count]
            unused = remove_subset_stop_words(unused, set(subrequests), redundant_stop_words=request_stop_words)
            while len(subrequests) < h2_count:
                if not current_trans_tags:
                    if not unused:
                        unused = self.uniq_ordered_unused_requests[:]
                    subrequests.append(stop_words_cleaner_list([unused.pop(0)], redundant_stop_words=request_stop_words)[0])
                else:
                    random_tag = random.choice(current_trans_tags)
                    current_trans_tags.remove(random_tag)
                    subrequests.append(request + ' ' + random_tag)
            random.shuffle(subrequests)  # вероятно, это не нужно
            request_subrequests[request] = subrequests
        return request_subrequests

    def _generate_possible_links(self):
        subrequest_requests = defaultdict(set)
        for subrequest in self.ordered_unused_requests:
            norm_subrequest = self.MORPH.norm_to_list(subrequest)
            for request in self.ordered_used_requests:
                norm_request = self.MORPH.norm_to_list(request)
                if sublist_exists(norm_subrequest, norm_request):
                    subrequest_requests[subrequest].add(request)
        return subrequest_requests

    def _generate_linking(self, requests_subrequests):
        subrequest_requests = self._generate_possible_links()
        request_links = {}
        for request, subrequests in requests_subrequests.iteritems():
            links_count = generate_from_dict(self.LINKS_COUNT)
            link_requests = set(join_lists(map(lambda s: subrequest_requests[s], subrequests))) - {request}
            link_requests = sorted(
                link_requests,
                key=lambda r: self.request_frequency[r],
                reverse=True
            )[:links_count]
            if len(link_requests) < links_count:
                available_requests = list(set(self.ordered_used_requests) - set(link_requests))
                random.shuffle(available_requests)
                link_requests.extend(available_requests[:links_count - len(link_requests)])
            random.shuffle(link_requests)
            request_links[request] = link_requests
        return request_links

    def _url(self, request):
        return self.request_url[request] if request in self.already_created_requests else '#{}#'.format(self.request_id[request])

    def _generate_articles_plan(self):
        if self.tags is None:
            request_subrequests = self._generate_subrequests()
        else:
            self.normalizer = Morph()
            request_subrequests = self._generate_subrequests_with_tags()

        request_links = self._generate_linking(request_subrequests)

        plan = []
        currently_created_requests = set()
        to_request_from_requests = defaultdict(list)
        for request in self.ordered_used_requests:
            future_links_keywords = []
            to_links = []
            for link_request in request_links[request]:
                if link_request in currently_created_requests:
                    to_links.append({
                        'ancor': link_request,
                        'from_link': self._url(request),
                        'to_link': self._url(link_request),
                    })
                else:
                    to_request_from_requests[link_request].append(request)
                    future_links_keywords.append(link_request)

            from_links = []
            for from_request in to_request_from_requests[request]:
                from_links.append({
                    'ancor': request,
                    'from_link': self._url(from_request),
                    'to_link': self._url(request),
                })

            day = self.request_creation_day[request]
            operation = self.OPERATION_REWRITE if request in self.already_created_requests else self.OPERATION_WRITE
            url = self._url(request)
            h1 = request.capitalize()
            h2 = map(lambda r: r.capitalize(), request_subrequests[request])

            currently_created_requests.add(request)

            plan.append({
                'day': day,
                'id': self.request_id[request],
                'request': request,
                'url': url,
                'cost': self.request_cost[request],
                'operation': operation,
                'internal_links': to_links,
                'external_links': from_links,
                'future_links_keywords': future_links_keywords,
                'tags': {
                    'title': h1,
                    'keywords': u', '.join([request] + request_subrequests[request]),
                    'description': u'. '.join([h1] + h2),
                    'h1': h1,
                    'h2': h2,
                    'strong_em': [request] + request_subrequests[request],
                    'img_alt_title': [h1] + h2,
                }
            })

        h2_requests = sorted(
            set(join_lists(request_subrequests.itervalues())),
            key=lambda r: self.request_frequency[r],
            reverse=True
        )

        return plan, h2_requests

    def generate_articles_plan(self):
        """
        return {
            'h1': ['...(request)', ...],  # ordered by frequency desc
            'h2': ['...(request)', ...],  # ordered by frequency desc
            'not_used': ['...(request)', ...],  # ordered by frequency desc
            'plan': [
                {
                    'day': 0..D,
                    'id': 1..N,
                    'request': '...',
                    'url': '{url|#id#}',
                    'cost': float,
                    'operation': '{write|rewrite}',
                    'internal_links': [
                        {
                            'ancor': '...',
                            'from_link': '{url|#id#}',
                            'to_link': '{url|#id#}'
                        },
                        ...
                    ],
                    'external_links': [
                        {
                            'ancor': '...',
                            'from_link': '{url|#id#}',
                            'to_link': '{url|#id#}'
                        },
                        ...
                    ],
                    'future_links_keywords': ['...', ...],
                    'tags': {
                        'title': '...',
                        'keywords': '...',
                        'description': '...',
                        'h1': '...',
                        'h2': ['...', ...],
                        'strong_em': ['...', ...],
                        'img_alt_title': ['...', ...],
                    }
                },
                ...
            ]
        }
        """
        plan, h2_requests = self._generate_articles_plan()
        return {
            'plan': plan,
            'h1': self.ordered_used_requests,
            'h2': h2_requests,
            'not_used': filter(lambda r: r not in h2_requests, self.ordered_unused_requests)
        }