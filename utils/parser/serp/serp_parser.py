# -*- coding: utf-8 -*-

import cfg
import os
import time
import json
import config
import signal
import pycurl
import logging
import tempfile
import datetime
import lxml.html
import urllib2
import re
import urllib
import urlparse

from parser.config import CELERY_MAX_EMPTY_RETRY

from lxml import etree
from urllib import quote_plus

from anticaptcha import recognize_captcha, recognize_captcha_url
from local_utils import random_string, connection_timeout, url_from_url, Xvfb, top_and_bottom_extractor, mail_ad_extractor
from browser import Browser
from surfbot import WebApplication
from utils.common import Tor, datetime_to_ts, ts_to_date, strip_tags, \
    extra_data_downloader, parser_task_hash, copy_file_to_api, alert_text_parser, keys_finder, XVFB_handler, extract_first_domain, get_ip, PycurlBuffer
from engine.models import EngineRegionalParams, Engine
from dblog.models import Error
from geobase.models import Region
from dblog.models import errors_to_db
from surfbot.web_application import QNetworkCookie, QNetworkCookieJar
from utils.selebot.surfer import Surfer
from parser.models import Parser
from direct_api.models import User
from yandex.services import SRB_save

from selenium.webdriver.remote.errorhandler import NoSuchElementException, NoAlertPresentException
from cookielib import Cookie, CookieJar
from selenium.webdriver.common.keys import Keys
from httplib import BadStatusLine
from selenium.common.exceptions import StaleElementReferenceException, UnexpectedAlertPresentException, WebDriverException
from google_app.services.parser_adwords import wait_element_displayed, wait_element_displayed_return_browser
from parser.models import CaptchaMonitoring

TMT = config.MAX_TIMEOUT
MAX_EMPTY_RETRY = CELERY_MAX_EMPTY_RETRY

class YandexBrowser(Browser):
    @connection_timeout(TMT)
    def direct(self, request, region=Region.extract('Moscow'), proxy=None, max_direct_pos=None):
        if proxy is not None: self.set_proxy(proxy)

        if max_direct_pos is None:
            max_page = 1000
        else:
            max_page = max_direct_pos/20 + 1 if max_direct_pos%20 != 0 else max_direct_pos/20

        direct = []
        page = 0
        request = request.encode("utf-8")
        erp = EngineRegionalParams.objects.get(region=region, engine=Engine.ENGINE_YANDEX)
        page_number = None
        while page < max_page:
            page += 1
            #############
            print page
            #############
            url = config.SEARCH_URL['yandex_direct'].format(
                REGION = erp.region_code,
                DOMAIN = erp.domain,
                REQUEST = quote_plus(request),
                PAGE = page,
            )
            br, page_result, page_number_current = self._direct_page(url)
            if br:
                break
            direct = direct + page_result
            if page_number_current:
                page_number = page_number_current
        if max_direct_pos is not None: direct = direct[:max_direct_pos]
        return {
            'result': direct,
            'user_profile': self.user_profile(),
            'page_number': page_number
        }

    def _direct_page(self, url):
        def _extract_page_number(doc):
            number = len(doc.cssselect('div.b-pager__pages > b')) + len(doc.cssselect('div.b-pager__pages > a'))
            if not number:
                number = 1
            return number

        page_number = None
        page_result = []
        self.browser.open(url, timeout = TMT)
        self.browser._factory.is_html = True

        html = self.browser.response().read()

        while True:
            tree = etree.HTML(html)
            captcha = tree.xpath("//img[@alt='captcha']")
            if captcha:
                print "captcha!"
                img_url = captcha[0].attrib['src']
                text = recognize_captcha_url(img_url, True, tag='direct')
                self.browser.select_form(nr=1)
                self.browser.form['captcha_code']=text
                self.browser.submit()
            else:
                break
            self.browser._factory.is_html = True
            html = self.browser.response().read()

        if config.YA_INDICATOR_TEXT not in html:
            class PageIsNotYandex(Exception):
                pass
            raise PageIsNotYandex('Page is not Yandex!')

        doc = lxml.html.document_fromstring(html.decode('utf-8'))
        block = doc.cssselect('.b-map-page__banner-list li')

        if not block:
            br = True
            return br, [], page_number
        else:
            br = False

        if int(url.split('=')[-1]) == 1:
            page_number = _extract_page_number(doc)

        for item in block:
            element = {}

            try:
                ad_title = item.cssselect('.ann .banner-selection .ad .ad-link a')[0].text_content()
            except IndexError:
                ad_title = ''

            try:
                ad_snippet = item.cssselect('.ann .banner-selection .ad div')[1].text_content()
            except IndexError:
                ad_snippet = ''

            try:
                ad_link = item.cssselect('.ann .banner-selection .ad .url .domain')[0].text_content()
            except IndexError:
                ad_link = ''

            element['title'] = ad_title
            element['snippet'] = ad_snippet
            element['link'] = ad_link

            warning = item.cssselect('.ann .banner-selection .ad .url span.b-adv-alert__text')
            if warning:
                element['warning'] = warning[0].text_content()

            page_result.append(element)
        return br, page_result, page_number

    @Xvfb(visible=config.WORDSTAT_VISIBILITY, width=1200, height=600, timeout=30)
    @Tor(replace_only_none_proxy=False)
    def budget(self, request, region, proxy=None):
        def _budget_data_extractor(html):
            result = []
            doc = lxml.html.document_fromstring(html)

            rows = doc.cssselect('tr')
            for row in rows:
                row_result = {}
                req_data = {}
                if not 'tdata' in row.attrib['class']: continue
                request = row.cssselect('div.my_div em')[0].text_content()

                td_labels = row.cssselect('td[nowrap][align] label')
                for td_label in td_labels:
                    checker = [val for val in ['id','for','style'] if val in td_label.attrib.keys()]
                    if not checker:
                        displays = td_label.text_content()
                        break

                req_data['special_placement'] = {
                    'click_cost': float(row.cssselect('td[nowrap][align] label[id^="price_text1"]')[0].text_content()),
                    'CTR': float(row.cssselect('td[nowrap][align] label[id^="ctr_text1"]')[0].text_content()),
                    'displays': float(displays),
                    'budget': float(row.cssselect('td[nowrap][align] span[id^="sum_text1"]')[0].text_content()),
                    }

                req_data['1st_place'] = {
                    'click_cost': float(row.cssselect('td[nowrap][align] label[id^="price_text2"]')[0].text_content()),
                    'CTR': float(row.cssselect('td[nowrap][align] label[id^="ctr_text2"]')[0].text_content()),
                    'budget': float(row.cssselect('td[nowrap][align] span[id^="sum_text2"]')[0].text_content()),
                    }

                req_data['guaranteed_display'] = {
                    'click_cost': float(row.cssselect('td[nowrap][align] label[id^="price_text3"]')[0].text_content()),
                    'CTR': float(row.cssselect('td[nowrap][align] label[id^="ctr_text3"]')[0].text_content()),
                    'budget': float(row.cssselect('td[nowrap][align] span[id^="sum_text3"]')[0].text_content()),
                    }

                row_result['request'] = request
                row_result['result'] = req_data
                result.append(row_result)
            return result

        def _budget_html_extractor(request, proxy):
            bad_list = []
            if proxy is not None: self.set_proxy(proxy)
            surfer = Surfer(proxy=proxy, useragent='Mozilla/5.0')
            surfer.open_browser('https://direct.yandex.ru/registered/main.pl?cmd=ForecastByWords')
            browser = surfer.get_browser()
            surfer.wait_for_css('input[name="login"]')
            account = User.get_active_user()
            input_login = browser.find_element_by_name('login').send_keys(account.login)
            input_password = browser.find_element_by_name('passwd').send_keys(account.password)
            browser.find_element_by_name('passwd').send_keys(Keys.ENTER)
            surfer.wait_for_css('div[class="b-head-line"]')
            try:
                browser.find_element_by_css_selector('input[name="continue"]').click()
            except NoSuchElementException:
                pass

            surfer.wait_for_css('h2[class="hide_this_for_print"]')
            js_script = "document.getElementById('ad-words').value = '{REQUEST}';".format(REQUEST=request.encode('utf-8'))
            browser.execute_script(js_script)
            js_script = "document.getElementById('geo').value = '{REGION}';".format(REGION=EngineRegionalParams.objects.get(region=region, engine=Engine.ENGINE_YANDEX).region_code)
            browser.execute_script(js_script)
            browser.find_element_by_css_selector('input[id="btn_submit"]').click()

            time.sleep(3)
            try:
                alert = browser.switch_to_alert()
                alert_text = alert.text
                alert_text = alert_text.replace(u'Ключевая фраза не может состоять более чем из 7 слов ','').replace('(','').replace(')','').replace(';','splitter').replace('\n','')
                bad_list = alert_text.split('splitter')
                for bad_req in bad_list:
                    request = request.replace(bad_req,'')
                alert.accept()
                browser.find_element_by_css_selector('textarea[id="ad-words"]').send_keys(Keys.CONTROL, 'a')
                browser.find_element_by_css_selector('textarea[id="ad-words"]').send_keys(Keys.BACK_SPACE)
                js_script = "document.getElementById('ad-words').value = '{REQUEST}';".format(REQUEST=request.encode('utf-8'))
                browser.execute_script(js_script)
                browser.find_element_by_css_selector('input[id="btn_submit"]').click()
            except NoAlertPresentException:
                pass

            surfer.wait_for_css('div[class="b-head-line"]')
            time.sleep(3)
            try:
                browser.find_element_by_css_selector('form[action] img[alt="captcha"]')
                print 'CAPTCHA'
                while True:
                    captcha_text = surfer.solve_captcha('form[action] img[alt="captcha"]')
                    if not captcha_text == 'ERROR_CAPTCHA_UNSOLVABLE':
                        browser.find_element_by_css_selector('input[type="text"]').send_keys(captcha_text)
                        browser.find_element_by_css_selector('input[type="submit"]').click()
                        surfer.wait_for_css('div[class="b-head-line"]')
                        try:
                            browser.find_element_by_css_selector('span[style="color: red;"]')
                        except NoSuchElementException:
                            surfer.wait_for_css('tr[class="tdata old-tr-top"]')
                            break

            except NoSuchElementException:
                pass

            surfer.wait_for_css('tr[class="tdata old-tr-top"]')
            html = browser.find_element_by_css_selector('table[id="result_table"]').get_attribute('innerHTML')
            return html, bad_list

        html, bad_list = _budget_html_extractor(request, proxy)
        result = _budget_data_extractor(html)
        final_result = {
            'result': result,
            'user_profile': self.user_profile()
        }
        if bad_list:
            final_result['bad_list'] = bad_list
        return final_result

    @Tor(replace_only_none_proxy=False)
    @Xvfb(visible=config.WORDSTAT_VISIBILITY, width=1200, height=600, timeout=30)
    def budget_surfbot(self, request, region, reqs_dict, proxy=None, tor_pid=None):
        try:
            def _ajax_captcha(page):
                try:
                    page.find_elements('div.i-popup.i-popup_autoclosable_yes.i-popup_fixed_yes.i-bem.i-popup_js_inited.i-popup_visibility_visible img.b-ajax-captcha__img')[0]
                    captcha_text = page.solve_captcha('div.i-popup.i-popup_autoclosable_yes.i-popup_fixed_yes.i-bem.i-popup_js_inited.i-popup_visibility_visible img.b-ajax-captcha__img')
                    CaptchaMonitoring.objects.create(task_name='budget')
                    page.evaluate('document.querySelector(\'div.i-popup.i-popup_autoclosable_yes.i-popup_fixed_yes.i-bem.i-popup_js_inited.i-popup_visibility_visible input[class="b-form-input__input"][name="captcha"]\').value={VALUE}'.format(VALUE=captcha_text))
                    time.sleep(3)
                    page.evaluate('document.querySelector(\'div.i-popup.i-popup_autoclosable_yes.i-popup_fixed_yes.i-bem.i-popup_js_inited.i-popup_visibility_visible input[class="b-form-button__input"][type="submit"]\').click();')
                    time.sleep(4)
                except IndexError:
                    return

            def _anticaptcha(page):
                def _ordinary_captcha(page):
                    try:
                        while True:
                            page.find_elements('form[action] img[alt="captcha"]')[0]
                            print 'ordinary captcha'
                            captcha_text = page.solve_captcha('form[action] img[alt="captcha"]')
                            CaptchaMonitoring.objects.create(task_name='budget')
                            page.evaluate('document.querySelector(\'input[type="text"]\').value={VALUE}'.format(VALUE=captcha_text))
                            page.evaluate('document.querySelector(\'input[type="submit"]\').click();')
                            page.wait_for_page_loaded()
                    except IndexError:
                        return

                _ordinary_captcha(page)

            def _budget_data_extractor(html):
                result = []
                doc = lxml.html.document_fromstring(html)
                doc = doc.cssselect('table[id="result_table"]')[0]
                rows = doc.cssselect('tr')
                for row in rows:
                    row_result = {}
                    req_data = {}
                    if not 'tdata' in row.attrib['class']: continue
                    request = row.cssselect('div.my_div em')[0].text_content()

                    td_labels = row.cssselect('td[nowrap][align] label')
                    for td_label in td_labels:
                        checker = [val for val in ['id','for','style'] if val in td_label.attrib.keys()]
                        if not checker:
                            displays = td_label.text_content()
                            break

                    req_data['special_placement'] = {
                        'click_cost': float(row.cssselect('td[nowrap][align] label[id^="price_text1"]')[0].text_content()),
                        'CTR': float(row.cssselect('td[nowrap][align] label[id^="ctr_text1"]')[0].text_content()),
                        'displays': float(displays),
                        'budget': float(row.cssselect('td[nowrap][align] span[id^="sum_text1"]')[0].text_content()),
                        }

                    req_data['1st_place'] = {
                        'click_cost': float(row.cssselect('td[nowrap][align] label[id^="price_text2"]')[0].text_content()),
                        'CTR': float(row.cssselect('td[nowrap][align] label[id^="ctr_text2"]')[0].text_content()),
                        'budget': float(row.cssselect('td[nowrap][align] span[id^="sum_text2"]')[0].text_content()),
                        }

                    req_data['guaranteed_display'] = {
                        'click_cost': float(row.cssselect('td[nowrap][align] label[id^="price_text3"]')[0].text_content()),
                        'CTR': float(row.cssselect('td[nowrap][align] label[id^="ctr_text3"]')[0].text_content()),
                        'budget': float(row.cssselect('td[nowrap][align] span[id^="sum_text3"]')[0].text_content()),
                        }

                    row_result['request'] = request
                    row_result['result'] = req_data
                    result.append(row_result)
                return result

            def _budget_html_extractor(request):
                errors_dict = {}
                cache_dir = os.path.join(tempfile.gettempdir(), 'webkit-network-cache-' + random_string())
                if not os.path.exists(cache_dir): os.makedirs(cache_dir)
                app = WebApplication(
                        open_links_in_new_tabs=False,
                        wait_for_page_load_when_open=True,
                        proxy=proxy,
                        antigate_key=config.ANTI_CAPTCHA_KEY,
                        viewport_size=(1200, 600),
                        user_agent='Mozilla/5.0',
                        cache_dir=cache_dir,
                        homedir=os.path.join(tempfile.gettempdir(), 'webkit-home-dir-' + random_string())
                )
                url = 'https://direct.yandex.ru/registered/main.pl?cmd=ForecastByWords'
                app.load_url_in_tab(url, True)
                page = app.get_current_web_page()
                account = User.get_active_user()
                js_script = "document.getElementById('login').value = '{LOGIN}';".format(LOGIN=account.login)
                page.evaluate(js_script)
                js_script = "document.getElementById('passwd').value = '{PASSWORD}';".format(PASSWORD=account.password)
                page.evaluate(js_script)
                page.evaluate('document.forms[0].submit();')
                page.wait_for_selector('div[class="b-head-line"]')
                page.wait_for_page_loaded()

                try:
                    attention = page.find_elements('input[name="continue"]')[0]
                    page.evaluate('document.querySelector(\'input[name="continue"]\').click();')
                except IndexError:
                    pass

                _anticaptcha(page)

                page.wait_for_selector('h2[class="hide_this_for_print"]')
                page.wait_for_page_loaded()
                js_script = u"document.getElementById('ad-words').value = '{REQUEST}';".format(REQUEST=request.replace("'","\\'"))
                page.evaluate(js_script)
                js_script = "document.getElementById('geo').value = '{REGION}';".format(REGION=EngineRegionalParams.objects.get(region=Region.extract(region), engine=Engine.ENGINE_YANDEX).region_code)
                page.evaluate(js_script)
                page.evaluate('window.alert = function(msg) {window.fsm = msg;}')
                page.evaluate('document.querySelector(\'input[id="btn_submit"]\').click();')

                def _cond():
                    while True:
                        if page.evaluate('window.fsm')[0]:
                            break
                        if page.find_elements('tr[class="tdata old-tr-top"]'):
                            break
                        if page.find_elements('form[action] img[alt="captcha"]'):
                            break
                        page.sleep(20)
                _cond()
                page.wait_for_page_loaded()

                if None in page.evaluate('window.fsm'):
                    pass
                else:
                    alert_text = str(page.evaluate('window.fsm')[0].toUtf8())
                    errors_dict = alert_text_parser(alert_text)
                    if errors_dict:
                        for k, v in errors_dict.iteritems():
                            request = request.replace(k,'')
                        if not request: return 'empty', errors_dict
                        js_script = u"document.getElementById('ad-words').value = '{REQUEST}';".format(REQUEST=request.replace("'","\\'"))
                        page.evaluate(js_script)
                        page.evaluate('window.fsm = []')
                        page.evaluate('document.querySelector(\'input[id="btn_submit"]\').click();')
                        _cond()
                        page.wait_for_page_loaded()

                _anticaptcha(page)
                old_timeout = page.wait_timeout
                page.wait_timeout = 10
                try:
                    page.wait_for_selector('tr[class="tdata old-tr-top"]')
                except Exception:
                    _ajax_captcha(page)
                    page.evaluate('document.querySelector(\'input[id="btn_submit"]\').click();')
                    page.wait_timeout = old_timeout
                    page.wait_for_selector('tr[class="tdata old-tr-top"]')
                page.wait_for_page_loaded()

                return page.content, errors_dict

            html, errors_dict = _budget_html_extractor(request)
            if html != 'empty':
                result = _budget_data_extractor(html)
                region_obj = Region.extract(region)
                for item in result:
                    for key in keys_finder(reqs_dict, item['request']):
                        oper_hash = parser_task_hash({
                            'type': Parser.MODE_BUDGET,
                            'request': key.encode('utf-8'),
                            'region': region,
                        })
                        final_result = {
                            "request": key,
                            "budget": item['result'],
                            'formatted_request': item['request']
                        }
                        Parser.add_and_save(Parser.MODE_BUDGET, oper_hash, key, Region.extract(region), None, final_result)
                        SRB_save(key, region_obj, item['result'])

            if errors_dict:
                for k, v in errors_dict.iteritems():
                    for key in keys_finder(reqs_dict, k):
                        oper_hash = parser_task_hash({
                            'type': Parser.MODE_BUDGET,
                            'request': key.encode('utf-8'),
                            'region': region,
                        })
                        final_result = {
                            "request": key,
                            "budget": 'error',
                            'formattetd_request': k,
                            'error': {'error_type': 'yandex error',
                                      'error_value': v}
                        }
                        Parser.add_and_save(Parser.MODE_BUDGET, oper_hash, key, Region.extract(region), None, final_result)
            print 'abbastanza'
            if tor_pid:
                os.kill(tor_pid, signal.SIGKILL)
        except Exception:
            Error.to_db()
            raise

    def budget_command(self, request, region, engine, reqs_dict):
        _request = urllib.quote_plus(request.encode("utf-8"))

        with tempfile.NamedTemporaryFile() as reqs_dict_file:
            json.dump(reqs_dict, reqs_dict_file, indent=4)
            reqs_dict_file.flush()
            cmd = "{ROOT}/../../manage.py budget_parser {REGION} {ENGINE} {REQS_DICT} --request=\"{REQUEST}\"".format(
                ROOT=config.ROOT_DIR,
                REQUEST=_request,
                REGION='--region="%s"' % region,
                ENGINE='--engine="%s"' % engine,
                REQS_DICT='--reqs_dict="%s"' % reqs_dict_file.name,
            )
            from subprocess import Popen, PIPE
            out, err = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE).communicate()

        if not 'abbastanza' in out:
            class BudgetParserFailed(Exception):
                pass
            raise BudgetParserFailed("Failed to parse budget!" + ' ' + request.split(',')[0].encode('utf-8'))

    def direct_selenium(self, request, region=Region.extract('Moscow'), proxy=None, max_direct_pos=None):
        if not hasattr(self, 'direct_surfer'):
            self.direct_surfer = Surfer(proxy=proxy, useragent='Mozilla/5.0 (X11; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/24.0')
            self.direct_surfer.open_browser()
            self.direct_browser = self.direct_surfer.get_browser()
            self.direct_browser.set_page_load_timeout(300)
        page_number = None
        if max_direct_pos is None:
            max_page = 1000
        else:
            max_page = max_direct_pos/20 + 1 if max_direct_pos%20 != 0 else max_direct_pos/20

        direct = []
        page = 0
        request = request.encode("utf-8")
        erp = EngineRegionalParams.objects.get(region=region, engine=Engine.ENGINE_YANDEX)

        while page < max_page:
            page += 1
            print page
            url = config.SEARCH_URL['yandex_direct'].format(
                REGION = erp.region_code,
                DOMAIN = erp.domain,
                REQUEST = quote_plus(request),
                PAGE = page,
            )
            br, page_result, page_number_current = self._direct_selenium_page(url)
            direct = direct + page_result
            if page_number_current:
                page_number = page_number_current
            if br:
                break
        if max_direct_pos is not None: direct = direct[:max_direct_pos]
        return {
            'result': direct,
            'user_profile': self.user_profile(),
            'page_number': page_number
        }

    def _direct_selenium_page(self, url):
        def _solve_captcha(html, url):
            doc = lxml.html.document_fromstring(html.decode('utf-8'))
            captcha = doc.cssselect("img[alt='captcha']")
            if captcha:
                print 'captcha'
                right_captcha_ok = False
                while not right_captcha_ok:
                    unsolv_captcha_ok = False
                    while not unsolv_captcha_ok:
                        img_url = captcha[0].attrib['src']
                        captcha_text = recognize_captcha_url(img_url, tag='direct_selenium')
                        if captcha_text != 'ERROR_CAPTCHA_UNSOLVABLE':
                            unsolv_captcha_ok = True
                        else:
                            self.direct_browser.get(url)
                            html = self.direct_browser.page_source.encode('utf8')
                            doc = lxml.html.document_fromstring(html.decode('utf-8'))
                            captcha = doc.cssselect("img[alt='captcha']")

                    self.direct_browser.find_element_by_css_selector('input[type="text"][name="captcha_code"]').send_keys(captcha_text.decode('utf8').lower())
                    self.direct_browser.find_element_by_css_selector('input[type="text"][name="captcha_code"]').send_keys(Keys.ENTER)
                    self.direct_browser = wait_element_displayed_return_browser(["img[alt='captcha']", 'div.refblock'], self.direct_browser, limit=220)
                    time.sleep(3)


                    html = self.direct_browser.page_source.encode('utf8')
                    doc = lxml.html.document_fromstring(html.decode('utf-8'))
                    captcha = doc.cssselect("img[alt='captcha']")
                    if not captcha:
                        right_captcha_ok = True
            return html

        def _extract_page_number(doc):
            number = len(doc.cssselect('div.b-pager__pages > b')) + len(doc.cssselect('div.b-pager__pages > a'))
            if not number:
                number = 1
            return number

        def _check_next_page(doc, url):
            current_page = int(url.split('=')[-1])
            pages = doc.cssselect('div.b-pager__pages')
            if not pages:
                return True

            pages = pages[0].text_content()
            last_page = int(pages.replace(u'\u2026','').lstrip().rstrip().split(' ')[-1])
            if current_page == last_page:
                return True

        page_number = None
        page_result = []

        while True:
            try:
                self.direct_browser.get(url)
                break
            except BadStatusLine:
                pass

        html = self.direct_browser.page_source.encode('utf8')
        html = _solve_captcha(html, url)

        doc = lxml.html.document_fromstring(html.decode('utf-8'))
        block = doc.cssselect('.b-map-page__banner-list li')

        if not block:
            br = True
            return br, [], page_number
        else:
            br = False

        if int(url.split('=')[-1]) == 1:
            page_number = _extract_page_number(doc)

        br = _check_next_page(doc, url)

        for item in block:
            element = {}

            try:
                ad_title = item.cssselect('.ann .banner-selection .ad .ad-link a')[0].text_content()
            except IndexError:
                ad_title = ''

            try:
                ad_snippet = item.cssselect('.ann .banner-selection .ad div')[1].text_content()
            except IndexError:
                ad_snippet = ''

            try:
                ad_link = item.cssselect('.ann .banner-selection .ad .url .domain')[0].text_content()
            except IndexError:
                ad_link = ''

            element['title'] = ad_title
            element['snippet'] = ad_snippet
            element['link'] = ad_link

            warning = item.cssselect('.ann .banner-selection .ad .url span.b-adv-alert__text')
            if warning:
                element['warning'] = warning[0].text_content()

            page_result.append(element)

        return br, page_result, page_number

    def direct_separate(self, request, region=Region.extract('Moscow'), max_direct_pos=None):
        if max_direct_pos is None:
            max_page = 1000
        else:
            max_page = max_direct_pos/20 + 1 if max_direct_pos%20 != 0 else max_direct_pos/20

        direct = []
        page = 0
        request = request.encode("utf-8")
        erp = EngineRegionalParams.objects.get(region=region, engine=Engine.ENGINE_YANDEX)
        page_number = None
        self.region_code = erp.region_code
        while page < max_page:
            page += 1
            print page
            url = config.SEARCH_URL['yandex_direct'].format(
                DOMAIN = erp.domain,
                REQUEST = quote_plus(request),
                PAGE = page,
            )

            exc = None
            counter = 0
            while counter <= 10:
                counter += 1
                try:
                    br, page_result, page_number_current = self._direct_page_separate(url)
                    exc = None
                    break
                except Exception, e:
                    exc = e
                    pass
            if exc:
                raise exc
            direct = direct + page_result
            if page_number_current:
                page_number = page_number_current
            if br:
                break

        if max_direct_pos is not None: direct = direct[:max_direct_pos]
        return {
            'result': direct,
            'user_profile': self.user_profile(),
            'page_number': page_number
        }

    def _direct_page_separate(self, url):
        def _check_not_yandex(html):
            if config.YA_INDICATOR_TEXT not in html:
                return True

        def _check_captcha(html):
            tree = etree.HTML(html)
            captcha = tree.xpath("//img[@alt='captcha']")
            if captcha:
                return True

        class DirectSeparateParser(Exception):
            pass

        def _extract_page_number(doc):
            number = len(doc.cssselect('div.b-pager__pages > b')) + len(doc.cssselect('div.b-pager__pages > a'))
            if not number:
                number = 1
            return number

        def _check_next_page(doc, url):
            current_page = int(url.split('=')[-1])
            pages = doc.cssselect('div.b-pager__pages')
            if not pages:
                return True

            pages = pages[0].text_content()
            last_page = int(pages.replace(u'\u2026','').lstrip().rstrip().split(' ')[-1])
            if current_page == last_page:
                return True
        page_number = None
        page_result = []

        ip = get_ip()

        buffer = PycurlBuffer()
        curl = pycurl.Curl()
        curl.setopt(curl.URL, url)
        curl.setopt(pycurl.TIMEOUT, 5)
        curl.setopt(pycurl.PROXY, 'xmoney.info')
        curl.setopt(pycurl.PROXYPORT, ip)
        curl.setopt(pycurl.PROXYTYPE, pycurl.PROXYTYPE_SOCKS5)
        curl.setopt(pycurl.COOKIE, "yandex_gid={val}".format(val=self.region_code))
        curl.setopt(curl.WRITEFUNCTION, buffer.body_callback)
        curl.perform()
        curl.close()

        html = buffer.contents

        if _check_captcha(html):
            raise DirectSeparateParser('Captcha, Sir!')

        if _check_not_yandex(html):
            raise DirectSeparateParser('Page is not Yandex!')

        doc = lxml.html.document_fromstring(html.decode('utf-8'))
        block = doc.cssselect('.b-map-page__banner-list li')

        if not block:
            br = True
            return br, [], page_number
        else:
            br = False

        if int(url.split('=')[-1]) == 1:
            page_number = _extract_page_number(doc)

        br = _check_next_page(doc, url)

        for item in block:
            element = {}

            try:
                ad_title = item.cssselect('.ann .banner-selection .ad .ad-link a')[0].text_content()
            except IndexError:
                ad_title = ''

            try:
                ad_snippet = item.cssselect('.ann .banner-selection .ad div')[1].text_content()
            except IndexError:
                ad_snippet = ''

            try:
                ad_link = item.cssselect('.ann .banner-selection .ad .url .domain')[0].text_content()
            except IndexError:
                ad_link = ''

            element['title'] = ad_title
            element['snippet'] = ad_snippet
            element['link'] = ad_link

            warning = item.cssselect('.ann .banner-selection .ad .url span.b-adv-alert__text')
            if warning:
                element['warning'] = warning[0].text_content()

            page_result.append(element)
        return br, page_result, page_number