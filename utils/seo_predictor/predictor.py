# -*- coding: utf-8 -*-

import math
import random
from collections import defaultdict, Counter
from copy import deepcopy
from ..common import extract_domain
from ..features import similarity, similarity_615
from seo.common import remove_double_urls


class SeoConst:
    OPERATION_ARTICLE = 'article'
    OPERATION_LINK = 'link'
    DIRECT_FUNC = staticmethod(lambda direct_cost: 200.0 * (1 + math.log(1 + direct_cost, 10)))
    MIN_SIMILARITY = 0.0  # 0.85  # Хотфикс для amocrm.ru. Для запросов типа "амо срм", где в выдаче в тайтле AmoCrm
    ARTICLE_COST = 200.0
    SERP_CLICK_DISTR = [
        20.0, 16.0, 12.8, 10.24, 8.19, 6.55, 5.24, 2.69, 3.37, 4.2,
        2.144, 1.7152, 1.37216, 1.097728, 0.877968, 0.70216, 0.561728, 0.288368, 0.36126400000000003, 0.45024000000000003,
        0.22983680000000004, 0.18386944000000002, 0.14709555200000002, 0.11767644160000001, 0.09411816960000001, 0.075271552, 0.06021724160000001, 0.030913049600000003, 0.03872750080000001, 0.04826572800000001,
        0.02463850496, 0.019710803968000002, 0.015768643174400004, 0.012614914539520001, 0.01008946778112, 0.008069110374400001, 0.006455288299520001, 0.0033138789171200003, 0.004151588085760001, 0.005174086041600001,
        0.0026412477317120002, 0.0021129981853696004, 0.0016903985482956804, 0.0013523188386365442, 0.0010815909461360641, 0.0008650086321356801, 0.0006920069057085442, 0.00035524781991526404, 0.00044505024279347207, 0.0005546620236595201,
        0.0
    ]
    BAD_DONORS_CONST = 0.3
    MONEY_PERIOD = 6
    ARTICLES_PER_MONTH_CONST = 0.5
    MIN_ARTICLES_PER_MONTH = 30
    LINKS_PER_MONTH_CONST = 0.5
    MIN_LINKS_PER_MONTH = 30
    CONCURENT_GROWTH_CONST = 0.05
    TRAFFIC_CONST = 1.5
    INDEXING_LAG = 1
    LINKS_INDEXING_CONST = 1.0 / 3
    LINKS_INDEXING_PERIOD = 6
    LINK_MONTH_WEIGHT = 1.0 / LINKS_INDEXING_PERIOD
    TOP_POSITIONS = [1, 2, 3]
    MIN_TOP_DONORS = 1.0
    MAX_SERP = 51
    MUTATION_MAX_SIZE = 10
    TRAILING_MONTHS = 6
    NOT_ALLOWED_CHANCE = 0.5
    FREQ_MUL_CONST = 0.1


class SeoPlan(object):
    def _get_main_donors(self):
        for serp in self.request_serp.itervalues():
            for serp_item in serp:
                if extract_domain(serp_item['url']) == self.domain:
                    return serp_item['main_donors']
        return 0

    def _get_all_targets(self, items):
        result = defaultdict(list)
        for item in items:
            request = item['request']
            for si in item['serp']:
                url = si['url']
                pos = si['position']
                if extract_domain(url) != self.domain:
                    continue
                result[request].append({
                    'url': url,
                    'position': pos
                })
        return result

    def _get_target(self, request):
        target = filter(lambda serp_item: extract_domain(serp_item['url']) == self.domain, self.request_serp[request])
        if not target:
            return None
        target = max(target, key=lambda serp_item: similarity_615(serp_item['h1'], request, serp_item['site_title']) if serp_item['site_title'] else similarity_615(serp_item['h1'], request, serp_item['title']))

        if target['site_title']:
            if similarity_615(target['h1'], request, target['site_title']) != 1:
                return None
        else:
            if similarity_615(target['h1'], request, target['title']) != 1:
                return None
        return target

    def _get_not_target(self, request):
        target = self._get_target(request)
        all_target = filter(
            lambda serp_item: extract_domain(serp_item['url']) == self.domain and serp_item != target,
            self.request_serp[request]
        )
        return all_target

    def _get_top_donors(self, request):
        top = filter(lambda serp_item: serp_item['position'] in SeoConst.TOP_POSITIONS, self.request_serp[request])
        if not top:
            return SeoConst.MIN_TOP_DONORS
        donors = map(lambda serp_item: serp_item['donors'] * similarity(serp_item['title'], request), top)
        return max(float(sum(donors)) / len(donors), SeoConst.MIN_TOP_DONORS)

    @staticmethod
    def _filter_empty_serps(items):
        serps = []
        empty_serps = []
        for item in items:
            if item['serp']:
                serps.append(item)
            else:
                empty_serps.append(item)
        return serps, empty_serps

    def _check_bad_donors(self):
        requests_with_bad_donors = {}
        for request in self.requests:
            donors = self.get_initial_donors(request)
            if donors < self.request_top_donors[request]:
                continue

            position = self.get_initial_position(request)
            if position <= 1:
                continue

            donors = int(self.request_top_donors[request] * SeoConst.BAD_DONORS_CONST)
            requests_with_bad_donors[request] = {
                'url': self.request_target[request]['url'],
                'donors': self.request_target[request]['donors'],
                'top3_avg_donors': self.request_top_donors[request],
                'position': self.request_target[request]['position']
            }
            self.request_target[request]['donors'] = donors
        return requests_with_bad_donors

    @staticmethod
    def _extract_direct_info(items):
        formatted = defaultdict(dict)
        freq = defaultdict(int)
        costs = {}
        for item in items:
            for di in item['direct_info']:
                mid = di['mode_id']
                request = item['request']

                formatted[request][mid] = di

                if mid == 1:
                    freq[request] += int(di['frequency'] * SeoConst.FREQ_MUL_CONST)
                elif mid == 2:
                    freq[request] += di['frequency']
                    costs[request] = di['special_cost']

        _costs = list(filter(lambda i: i is not None, costs.itervalues()))
        avg_cost = float(sum(_costs)) / len(_costs)
        for request, cost in costs.iteritems():
            if cost is None:
                costs[request] = avg_cost

        return formatted, freq, costs

    def _get_operation_variants(self):
        variants = []
        for request in self.requests:
            frequency = self.request_frequency[request] * SeoConst.TRAFFIC_CONST
            position = self.get_position(self.month - 1, request)
            probability = SeoConst.SERP_CLICK_DISTR[position - 1] / 100.0
            donors = self.get_donors(self.month - 1, request)
            top_donors = self.get_top_donors(self.month - 1 + SeoConst.LINKS_INDEXING_PERIOD, request)
            has_article = self.has_article(self.month - 1, request)

            if position == 1:
                continue
            if donors >= top_donors and int(self.get_effective_donors(self.month - 1, request)) < donors:
                continue

            assert donors < top_donors

            for possible_position in xrange(1, position):
                possible_probability = SeoConst.SERP_CLICK_DISTR[possible_position - 1] / 100.0
                part = float(position - possible_position) / (position - 1)
                donors_increase = int((top_donors - donors) * part) or 1
                article_cost = int(not has_article) * SeoConst.ARTICLE_COST
                links_cost = donors_increase * self.link_cost(request)
                costs = article_cost + links_cost
                traffic_increase = frequency * (possible_probability - probability)
                tc = traffic_increase / costs
                variants.append({
                    'request': request,
                    'traffic_per_costs': tc,
                    'costs': costs,
                    'target_positoin': possible_position,
                    'buy_article': not has_article,
                    'buy_links': donors_increase,
                    'link_cost': self.link_cost(request),
                    'article_cost': SeoConst.ARTICLE_COST,
                    'before_traffic': frequency * probability,
                    'after_traffic': frequency * possible_probability
                })
        variants.sort(key=lambda i: (-i['traffic_per_costs'], i['costs']))
        return variants

    def _plan_from_variants(self, variants):
        choosen_requests = set()
        plan = []
        money_limit = min(self.cost_per_month, self.money)
        for i in variants:
            request = i['request']
            if request in choosen_requests:
                continue
            if money_limit - i['costs'] < 0:
                continue
            money_limit -= i['costs']
            choosen_requests.add(i['request'])
            plan.append(i)

            self.month_main_donors_increase[self.month] += i['buy_links']
            self.agg_month_main_donors_increase[self.month] += i['buy_links']
            self.month_articles_increase[self.month] += int(i['buy_article'])
            self.agg_month_articles_increase[self.month] += int(i['buy_article'])

            self.month_plan[self.month][request][SeoConst.OPERATION_LINK] += i['buy_links']
            self.agg_month_plan[self.month][request][SeoConst.OPERATION_LINK] += i['buy_links']
            if i['buy_article']:
                self.month_plan[self.month][request][SeoConst.OPERATION_ARTICLE] += 1
                self.agg_month_plan[self.month][request][SeoConst.OPERATION_ARTICLE] += 1

            self.month_costs[self.month] += i['costs']
            self.money -= i['costs']
        return plan

    def _generate_month_plan(self):
        self.month += 1
        if self.month > 1:
            self.agg_month_main_donors_increase[self.month] = self.agg_month_main_donors_increase[self.month - 1]
            self.agg_month_articles_increase[self.month] = self.agg_month_articles_increase[self.month - 1]
            self.agg_month_plan[self.month] = deepcopy(self.agg_month_plan[self.month - 1])
            self.agg_month_costs[self.month] = self.agg_month_costs[self.month - 1]

        variants = self._get_operation_variants()
        plan = self._plan_from_variants(variants)

        self.update_positions()
        self.update_traffic()

        return plan

    def generate_plan(self, trailing_months=SeoConst.TRAILING_MONTHS):
        self.money = self.money_limit
        self.cost_per_month = self.money_limit / self.months
        self.month = 0

        self.month_main_donors_increase = Counter()
        self.month_articles_increase = Counter()
        self.month_plan = defaultdict(lambda: defaultdict(Counter))
        self.month_costs = defaultdict(float)

        self.agg_month_main_donors_increase = Counter()
        self.agg_month_articles_increase = Counter()
        self.agg_month_plan = defaultdict(lambda: defaultdict(Counter))
        self.agg_month_costs = defaultdict(float)

        self.month_request_position = defaultdict(dict)
        self.month_request_traffic = defaultdict(dict)

        self.update_positions()
        self.update_traffic()

        while self._generate_month_plan():
            pass

        for month in xrange(trailing_months - 1):
            self._generate_month_plan()

    def __init__(self, items, money_limit, site, months):
        """
        money_limit - float
        site - unicode
        items = [
            {
                'request': '...',
                'direct_info': [
                    {
                        'mode_id': {1|2|3|4},
                        'frequency': int,
                        'special_cost': {float|None},
                        'first_cost': {float|None},
                        'guaranteed_cost': {float|None}
                    },
                    ...
                ]
                'serp': [
                    {
                        'url': '...',
                        'position': 1..50,
                        'donors': 0..,
                        'links': 0..,
                        'main_donors': 0..,
                        'main_links': 0..,
                        'title': '...'
                    }
                ]
            }
        ]
        months - int (months count)
        """

        self.money_limit = float(money_limit)
        self.months = int(months)
        self.domain = extract_domain(site)

        self.request_direct_info, self.request_frequency, self.request_cost = self._extract_direct_info(items)
        self.items, self.empty_serp_items = self._filter_empty_serps(items)
        self.request_serp = dict(map(lambda item: (item['request'], item['serp']), items))
        self.requests = self.request_cost.keys()[:]
        self.request_target = dict(map(lambda r: (r, self._get_target(r)), self.requests))
        self.request_target = remove_double_urls(self.request_target, self.request_frequency)
        self.request_top_donors = dict(map(lambda r: (r, self._get_top_donors(r)), self.requests))
        self.main_donors = self._get_main_donors()
        self.articles = len(list(filter(lambda item: item, self.request_target.itervalues())))
        self.request_not_target = dict(map(lambda r: (r, self._get_not_target(r)), self.requests))
        self.request_all_targets = self._get_all_targets(items)

        # Проверяем целевые страницы на плохих доноров.
        # Считаем что если ссылочная масса как у топовых
        # страниц, но урл не в топе - ссылочная масса у него плохая.
        self.requests_with_bad_donors = self._check_bad_donors()

        self.generate_plan()

    def link_cost(self, request):
        return SeoConst.DIRECT_FUNC(self.request_cost[request])

    def get_initial_donors(self, request):
        return self.request_target[request]['donors'] if self.request_target[request] else 0

    def get_donors(self, month, request):
        initial_donors = self.get_initial_donors(request)
        if month < 1:
            return initial_donors
        return self.agg_month_plan[month][request][SeoConst.OPERATION_LINK] + initial_donors

    def get_top_donors(self, month, request):
        top_donors = self.request_top_donors[request]
        for m in xrange(month):
            top_donors += top_donors * SeoConst.CONCURENT_GROWTH_CONST
        return float(top_donors)

    def get_initial_position(self, request):
        return self.request_target[request]['position'] if self.request_target[request] else SeoConst.MAX_SERP

    def get_position(self, month, request):
        if month < 1:
            return self.get_initial_position(request)
        return self.month_request_position[month][request]

    def get_effective_donors(self, month, request):
        donors = 0.0
        for m in xrange(month):
            age = month - m
            m += 1
            weight = age * SeoConst.LINK_MONTH_WEIGHT
            if weight > 1.0:
                weight = 1.0
            if m == month:
                weight *= SeoConst.LINKS_INDEXING_CONST
            donors += self.month_plan[m][request][SeoConst.OPERATION_LINK] * weight
        return donors

    def update_positions(self):
        for request, target in self.request_target.iteritems():
            position = self.get_initial_position(request)
            if not self.has_article(self.month - SeoConst.INDEXING_LAG, request):
                self.month_request_position[self.month][request] = position
                continue
            donors = self.get_initial_donors(request)
            top_donors = self.get_top_donors(self.month, request)
            if position == 1:
                k = (1.0 - SeoConst.MAX_SERP) / top_donors
            else:
                assert donors < top_donors
                k = (1.0 - position) / (top_donors - donors)
            donors_increase = self.get_effective_donors(self.month, request)
            new_position = int(k * donors_increase + position)
            if new_position < 1:
                new_position = 1
            if new_position > SeoConst.MAX_SERP:
                new_position = SeoConst.MAX_SERP
            self.month_request_position[self.month][request] = new_position

    @staticmethod
    def get_traffic(position, frequency, frequency_precise):
        probability = SeoConst.SERP_CLICK_DISTR[position - 1] / 100.0
        freq = frequency * SeoConst.FREQ_MUL_CONST + frequency_precise
        traffic = probability * freq
        return traffic

    def update_traffic(self):
        for request in self.requests:
            frequency = self.request_frequency[request] * SeoConst.TRAFFIC_CONST
            position = self.get_position(self.month, request)
            probability = SeoConst.SERP_CLICK_DISTR[position - 1] / 100.0
            self.month_request_traffic[self.month][request] = frequency * probability
            for serp_item in self.request_not_target[request]:
                _position = serp_item['position']
                _probability = SeoConst.SERP_CLICK_DISTR[_position - 1] / 100.0
                self.month_request_traffic[self.month][request] += frequency * _probability

    def get_max_possible_traffic(self):
        traffic = 0.0
        for request in self.requests:
            frequency = self.request_frequency[request] * SeoConst.TRAFFIC_CONST
            probability = SeoConst.SERP_CLICK_DISTR[0] / 100.0
            traffic += frequency * probability
            for serp_item in self.request_not_target[request]:
                _position = serp_item['position']
                _probability = SeoConst.SERP_CLICK_DISTR[_position - 1] / 100.0
                traffic += frequency * _probability
        return traffic

    def get_month_traffic(self, month):
        return sum(self.month_request_traffic[month].itervalues())

    def get_month_costs(self, month):
        return self.month_costs[month]

    def get_total_costs(self, month):
        return self.agg_month_costs[month]

    def get_avg_link_cost(self, month):
        if self.month_main_donors_increase[month] == 0:
            return 0.0
        costs = 0.0
        for request, plan in self.month_plan[month].iteritems():
            costs += self.link_cost(request) * plan[SeoConst.OPERATION_LINK]
        return costs / self.month_main_donors_increase[month]

    def get_month_links_costs(self, month):
        total = 0.0
        for request, operations in self.month_plan[month].iteritems():
            total += self.link_cost(request) * operations[SeoConst.OPERATION_LINK]
        return total

    def get_month_articles_costs(self, month):
        total = 0.0
        for request, operations in self.month_plan[month].iteritems():
            total += SeoConst.ARTICLE_COST * operations[SeoConst.OPERATION_ARTICLE]
        return total

    def get_history(self):
        history = [{
            'month': 0,
            'month_traffic': self.get_month_traffic(0),
            'max_possible_traffic': self.get_max_possible_traffic()
        }]
        for month, plan in sorted(self.month_plan.iteritems()):
            history.append({
                'month': month,
                'month_traffic': self.get_month_traffic(month),
                'month_costs': self.get_month_costs(month),
                'month_links_costs': self.get_month_links_costs(month),
                'month_articles_costs': self.get_month_articles_costs(month),
                'total_costs': self.get_total_costs(month),
                'max_possible_traffic': self.get_max_possible_traffic(),
                'month_buy_links': self.month_main_donors_increase[month],
                'month_buy_articles': self.month_articles_increase[month],
                'month_avg_link_cost': self.get_avg_link_cost(month),
                'details': []
            })
            for request, operations in sorted(plan.iteritems()):
                history[-1]['details'].append({
                    'request': request,
                    'direct_info': self.request_direct_info[request],
                    'frequency': self.request_frequency[request],
                    'month_buy_articles': operations[SeoConst.OPERATION_ARTICLE],
                    'total_buy_articles': self.agg_month_plan[month][request][SeoConst.OPERATION_ARTICLE],
                    'month_buy_links': operations[SeoConst.OPERATION_LINK],
                    'total_buy_links': self.agg_month_plan[month][request][SeoConst.OPERATION_LINK],
                    'link_cost': self.link_cost(request),
                    'position': self.month_request_position[month][request],
                    'initial_position': self.get_initial_position(request),
                    'has_article': self.has_article(month, request),
                    'has_article_initially': bool(self.request_target[request]),
                    'total_links': self.get_donors(month, request),
                    'initial_links': self.get_initial_donors(request),
                    'top3_avg_links': int(self.get_top_donors(month, request)),
                    'initial_top3_avg_links': self.request_top_donors[request]
                })
        return history

    def get_request_history(self):
        """
        return [
            {
                'request': '...',
                'direct_info': {
                    1: {
                        'mode_id': 1,
                        'frequency': int,
                        'special_cost': {float|None},
                        'first_cost': {float|None},
                        'guaranteed_cost': {float|None}
                    },
                    2: {...},
                    3: {...},
                    4: {...}

                },
                'link_cost': float,
                'months': [
                    {
                        'month': 0..,
                        'month_buy_links': int,
                        'total_buy_links': int,
                        'total_links': int,
                        'position': 1..51,
                        'month_buy_articles': int,
                        'has_article': bool,
                        'top3_avg_links': float,
                        'traffic': float,
                        'extra_traffic': float
                    },
                    ...
                ]
            }
        ]
        """
        history = []
        for request in self.requests:
            i = {
                'request': request,
                'direct_info': self.request_direct_info[request],
                'link_cost': self.link_cost(request),
            }
            m = []
            frequency = self.request_frequency[request] * SeoConst.TRAFFIC_CONST
            for month in xrange(self.month + 1):
                mi = {
                    'month': month,
                    'month_buy_links': self.month_plan[month][request][SeoConst.OPERATION_LINK],
                    'total_buy_links': self.agg_month_plan[month][request][SeoConst.OPERATION_LINK],
                    'total_links': self.get_donors(month, request),
                    'position': self.get_position(month, request),
                    'month_buy_articles': self.month_plan[month][request][SeoConst.OPERATION_ARTICLE],
                    'has_article': self.has_article(month, request),
                    'top3_avg_links': int(self.get_top_donors(month, request)),
                    'traffic': frequency * SeoConst.SERP_CLICK_DISTR[self.get_position(month, request) - 1] / 100.0,
                    'extra_traffic': sum([0.0] + map(
                        lambda item: frequency * SeoConst.SERP_CLICK_DISTR[item['position'] - 1] / 100.0,
                        self.request_not_target[request]
                    ))
                }

                m.append(mi)
            i['months'] = m
            history.append(i)
        return history

    def has_article(self, month, request):
        if month < 1:
            return bool(self.request_target[request])
        return bool(self.request_target[request] or self.agg_month_plan[month][request][SeoConst.OPERATION_ARTICLE] > 0)

    def extract_actions(self):
        """
        return {
            'request_id': {
                '...(request)': \d,
                ...
            },
            'actions': [
                {
                    'request': '...',
                    'operation': '{link|article}',
                    'cost': float,
                    'request_id': 1..N,
                    'day': 0..D
                },
                ...
            ],
            'request_url': {
                '...(request)': '...(url)',
                ...
            },
            'unused_requests': ['...(request)', ...],
            'request_creation_day': {
                '...(request)': \d,
                ...
            },
            'request_frequency': {
                '...(request)': \d,
                ...
            },
            'request_creation_cost': {
                '...(request)': float,
                ...
            }
        }
        """
        buy_article = {}
        request_id = {}
        request_url = {}
        article_cost = {}
        unused_requests = set(self.requests[:])
        rid = 1
        for request, target in self.request_target.iteritems():
            if target:
                buy_article[request] = 0
                request_id[request] = rid
                request_url[request] = target['url']
                article_cost[request] = 0.0
                rid += 1

        days = map(lambda i: [], xrange(30 * self.month))
        for month, plan in self.month_plan.iteritems():
            for request, operations in plan.iteritems():
                if operations[SeoConst.OPERATION_ARTICLE] == 0:
                    continue
                if month == self.month:
                    extra_days = 15
                else:
                    extra_days = 0
                day = random.randint((month - 1) * 30, month * 30 - 1 - extra_days)
                day_plan = days[day]
                day_plan.append({
                    'request': request,
                    'operation': SeoConst.OPERATION_ARTICLE,
                    'cost': SeoConst.ARTICLE_COST
                })
                buy_article[request] = day
                article_cost[request] = SeoConst.ARTICLE_COST
                unused_requests -= {request}
        for month, plan in self.month_plan.iteritems():
            for request, operations in plan.iteritems():
                for link_id in xrange(operations[SeoConst.OPERATION_LINK]):
                    day = random.randint(buy_article[request], min(buy_article[request] + 30 - 1, len(days) - 1))
                    day_plan = days[day]
                    day_plan.append({
                        'request': request,
                        'operation': SeoConst.OPERATION_LINK,
                        'cost': self.link_cost(request)
                    })
                    unused_requests -= {request}

        result_plan = []
        for day, day_plan in enumerate(days):
            for operation in day_plan:
                if operation['operation'] == SeoConst.OPERATION_ARTICLE:
                    request_id[operation['request']] = rid
                    rid += 1
                operation['request_id'] = request_id[operation['request']]
                operation['day'] = day
                result_plan.append(operation)
        return {
            'request_id': request_id,
            'actions': result_plan,
            'request_url': request_url,
            'unused_requests': list(unused_requests),
            'request_creation_day': buy_article,
            'request_frequency': self.request_frequency,
            'request_creation_cost': article_cost
        }
