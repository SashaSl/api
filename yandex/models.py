# -*- coding: utf-8 -*-

import datetime
from django.db import models
from geobase.models import Region
from jsonfield import JSONField

class YandexUser(models.Model):
    login = models.CharField(verbose_name=u"Логин", max_length=32, db_index=True, unique=True)
    agent_login = models.ForeignKey('self', verbose_name=u'Агентский логин', blank=True, null=True)
    password = models.CharField(verbose_name=u"Пароль", max_length=32)
    token = models.CharField(verbose_name=u"Токен", max_length=40, blank=True, null=True)
    token_expires = models.DateTimeField(verbose_name=u"Token_expires", blank=True, null=True)
    sertificate_path = models.CharField(verbose_name=u"Путь к сертификату", max_length=256, blank=True, null=True)
    sertificate_expires = models.DateTimeField(verbose_name=u"Sertificate_expires", blank=True, null=True)
    created_at = models.DateTimeField(verbose_name=u"Время создания", blank=True, null=True, auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=u"Время последнего обновления", blank=True, null=True, auto_now=True)
    units_rest = models.IntegerField(verbose_name=u"Количество доступных баллов",  null=True, default=None, blank=True)

    class Meta:
        verbose_name = u"Пользователь яндекса"
        verbose_name_plural = u"Пользователи яндекса"

    def __unicode__(self):
        return self.login

class Counters(models.Model):
    user = models.ForeignKey(YandexUser, verbose_name=u"Пользователь яндекса")
    name = models.TextField(verbose_name=u"Name", blank=True, null=True)
    permission = models.TextField(verbose_name=u"Permission", blank=True, null=True)
    owner_login = models.TextField(verbose_name=u"Owner_login", blank=True, null=True)
    site = models.TextField(verbose_name=u"Site", blank=True, null=True)
    code_status = models.TextField(verbose_name=u"Code_status", blank=True, null=True)
    type = models.TextField(verbose_name=u"Type", blank=True, null=True)
    counter_id = models.CharField(verbose_name=u"Counter_id", max_length=40, blank=True, null=True)

    class Meta:
        verbose_name = u"Счётчик"
        verbose_name_plural = u"Счётчики"

class SearchRequest(models.Model):
    phrase = models.CharField(max_length=255, verbose_name=u'Фраза', unique=True)

    class Meta:
        verbose_name = u"Запрос"
        verbose_name_plural = u"Запросы"

    def __unicode__(self):
        return self.phrase

class SearchRequestManager(models.Manager):
    def create_or_update(self, **kwargs):
        sr, created = self.get_or_create(search_request=kwargs['search_request'], region=kwargs['region'], defaults=kwargs)
        if not created:
            sr.save()

class SearchRequestBudgetBase(models.Model):
    '''
    "budget": {
            "special_placement": {
                "click_cost": 2.35,
                "displays": 625.0,
                "CTR": 3.36,
                "budget": 49.35
            },
            "1st_place": {
                "click_cost": 0.87,
                "budget": 8.7,
                "CTR": 1.6
            },
            "guaranteed_display": {
                "click_cost": 0.87,
                "budget": 7.83,
                "CTR": 1.44
            }
    shows = displays
    ctr_premium, ctr_firstplace, ctr_guaranteed = CTR
    price_premium, ... = click_cost
    clicks_premium, ... = ctr_premium * shows / 100
    '''

    class Meta:
        abstract = True

    search_request = models.ForeignKey(SearchRequest, db_index=True)
    region = models.ForeignKey(Region, db_index=True)

    created_at = models.DateTimeField(verbose_name=u'Время создания', auto_now_add=True)

    shows = models.IntegerField(verbose_name=u'Показы', blank=True, null=True)

    clicks_premium = models.IntegerField(verbose_name=u'Клики спеца', blank=True, null=True)
    clicks_firstplace = models.IntegerField(verbose_name=u'Клики первого места', blank=True, null=True)
    clicks_guaranteed = models.IntegerField(verbose_name=u'Клики гарантии', blank=True, null=True)

    price_premium = models.DecimalField(verbose_name=u'Цена спеца', blank=True, null=True, max_digits=8, decimal_places=4)
    price_firstplace = models.DecimalField(verbose_name=u'Цена первого места', blank=True, null=True, max_digits=8, decimal_places=4)
    price_guaranteed = models.DecimalField(verbose_name=u'Цена гарантии', blank=True, null=True, max_digits=8, decimal_places=4)

    ctr_premium = models.DecimalField(verbose_name=u'CTR спеца, %', blank=True, null=True, max_digits=8, decimal_places=4)
    ctr_firstplace = models.DecimalField(verbose_name=u'CTR первого места, %', blank=True, null=True, max_digits=8, decimal_places=4)
    ctr_guaranteed = models.DecimalField(verbose_name=u'CTR гарантии, %', blank=True, null=True, max_digits=8, decimal_places=4)

    objects = SearchRequestManager()

class SearchRequestBudget(SearchRequestBudgetBase):
    updated_at = models.DateTimeField(verbose_name=u'Время изменения', auto_now=True)
    class Meta:
        unique_together = ('search_request', 'region')
        verbose_name = u"Budget data"
        verbose_name_plural = u"Budget data"

class SearchRequestBudgetHistory(SearchRequestBudgetBase):
    class Meta:
        verbose_name = u"Budget history"
        verbose_name_plural = u"Budget history"

class YandexImages(models.Model):
    login = models.CharField(verbose_name=u"Пользователь яндекса", max_length=50, db_index=True)
    url = models.CharField(verbose_name=u'URL изображения', max_length=100, db_index=True)
    name = models.CharField(verbose_name=u'Описание изображения', max_length=100, blank=True)
    hash = models.CharField(verbose_name=u'URL изображения', max_length=100, blank=True)
    error = JSONField(verbose_name=u'Описание ошибки', blank=True, null=True)

    class Meta:
        unique_together = ('login', 'url')
        verbose_name = u"Загруженное в Директ изображение"
        verbose_name_plural = u"Загруженные в Директ изображения"

