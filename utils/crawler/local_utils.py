# -*- coding: utf-8 -*-

import re
import sys
import chardet
import lxml.html
from pprint import pprint

class ContentExtractor(object):
    def __init__(self):
        stop_regex = [
            '^\d+\.$',
        ]

        self.compiled = []
        for regex in stop_regex:
            self.compiled.append(re.compile(regex))

    def _sanitize_text(self, text):
        text = text.strip().lower()

        if not text:
            return False

        if text.isdigit():
            return False

        for compiled in self.compiled:
            if compiled.search(text):
                return False

        return text

    def extract_from_page(self, content):
        enc = chardet.detect(content)
        if not enc['encoding']:
            return []
        
        try:
            doc = lxml.html.document_fromstring(content.decode(enc['encoding'].lower()).replace('encoding',''))
        except:
            doc = lxml.html.document_fromstring(content)

        extracted = {}
        extracted['links'] = []

        ### title
        if doc.cssselect('title'):
            extracted["title"] = doc.cssselect('title')[0].text_content()

        ### meta
        selector = 'meta'
        if doc.cssselect(selector):
            for elem in doc.cssselect(selector):
                if 'name' in elem.attrib and elem.attrib['name'].lower() in ['keywords', 'description']:
                    meta_type = elem.attrib['name'].lower()
                    for line in elem.attrib['content'].splitlines():
                        if meta_type not in extracted:
                            extracted[meta_type] = []

                        for w in line.split(','):
                            extracted[meta_type].append(w.strip())

        ### headers
        for level in xrange(6): 
            selector = 'h' + str(level + 1)
            if doc.cssselect(selector):
                extracted[selector] = []
                for elem in doc.cssselect(selector):
                    sanitized = self._sanitize_text(elem.text_content())
                    if sanitized:
                        extracted[selector].append(sanitized)

        ### text tags
        for selector in ('b', 'strong', 'em', 'i', 'a', 'img'):
        # for selector in ('a'):
            if doc.cssselect(selector):
                extracted[selector] = []
                for elem in doc.cssselect(selector):
                    if selector == 'a' and 'href' in elem.attrib:
                        if 'title' in elem.attrib:
                            title = elem.attrib['title']
                        else:
                            title = ''

                        if elem.cssselect('img'):
                            img = elem.cssselect('img')[0].attrib.get('src', None)
                            alt = elem.cssselect('img')[0].attrib.get('alt', None)
                        else:
                            img = None
                            alt = None

                        text = elem.text_content()
                        element = {
                            'href': elem.attrib['href'],
                            'title': title,
                            'text': text,
                            'img': img,
                            'alt': alt,
                        }
                        if 'rel' in elem.attrib:
                            element['rel'] = elem.attrib['rel']

                        extracted['links'].append(element)

                    if selector == 'img':
                        extracted[selector].append({
                            'src': elem.attrib.get('src', None),
                            'alt': elem.attrib.get('alt', None),
                            'title': elem.attrib.get('title', None)
                        })

                    sanitized = self._sanitize_text(elem.text_content())
                    if sanitized:
                        extracted[selector].append(sanitized)

        ### list anchors
        selector = 'li > a'
        if doc.cssselect(selector):
            extracted['li a'] = []
            for elem in doc.cssselect(selector):
                sanitized = self._sanitize_text(elem.text_content())
                if sanitized:
                    extracted['li a'].append(sanitized)

        return extracted
