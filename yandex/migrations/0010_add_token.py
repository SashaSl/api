# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models
from yandex.models import YandexUser

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        # Note: Don't use "from appname.models import ModelName". 
        # Use orm.ModelName to refer to models in this application,
        # and orm['appname.ModelName'] for models in other applications.
        data_list = [
            {
                'login': 'direct.evristic@yandex.ru',
                'password': 'Evristic2014Only',
                'token': '218143351c544221943e3cb262df3880'
            },
            {
                'login': 'direct.lionit@yandex.ru',
                'password': 'lionit2014',
                'token': 'd10ca569e4104cc78f8c57a3dd510d8a'
            },
            {
                'login': 'evristic',
                'password': 'Only1988',
                'token': '02f58d9782404d27908e49ae16f97677'
            },
            {
                'login': 'suggest.evristic',
                'password': 'Evristic2013',
                'token': '08b37cbbafb74967b78bb1bc36dbb7fd'
            },
            {
                'login': 'it.evristic',
                'password': 'Development2014',
                'token': '09e0e67ae98f41ffa7a87119e4b2b0c4'
            }
        ]

        for data in data_list:
            user, created = YandexUser.objects.get_or_create(login=data['login'], password=data['password'])
            print data['login'], created
            if not user.token:
                print 'not token'
                user.token = data['token']
                user.save()

    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        'geobase.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'name_en': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_ru': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_translit': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        },
        'yandex.counters': {
            'Meta': {'object_name': 'Counters'},
            'code_status': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'counter_id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'owner_login': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'permission': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'site': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['yandex.YandexUser']"})
        },
        'yandex.searchrequest': {
            'Meta': {'object_name': 'SearchRequest'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phrase': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'yandex.searchrequestbudget': {
            'Meta': {'unique_together': "(('search_request', 'region'),)", 'object_name': 'SearchRequestBudget'},
            'clicks_firstplace': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'clicks_guaranteed': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'clicks_premium': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ctr_firstplace': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'ctr_guaranteed': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'ctr_premium': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price_firstplace': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'price_guaranteed': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'price_premium': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']"}),
            'search_request': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['yandex.SearchRequest']"}),
            'shows': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'yandex.searchrequestbudgethistory': {
            'Meta': {'object_name': 'SearchRequestBudgetHistory'},
            'clicks_firstplace': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'clicks_guaranteed': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'clicks_premium': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ctr_firstplace': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'ctr_guaranteed': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'ctr_premium': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price_firstplace': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'price_guaranteed': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'price_premium': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']"}),
            'search_request': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['yandex.SearchRequest']"}),
            'shows': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'yandex.yandexuser': {
            'Meta': {'object_name': 'YandexUser'},
            'agent_login': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['yandex.YandexUser']", 'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32', 'db_index': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'sertificate_expires': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sertificate_path': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'token_expires': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'units_rest': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['yandex']
    symmetrical = True
