# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SearchRequestBudgetHistory'
        db.create_table('yandex_searchrequestbudgethistory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('search_request', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['yandex.SearchRequest'])),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('region', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('shows', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('clicks_premium', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('clicks_firstplace', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('clicks_guaranteed', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('price_premium', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=4, blank=True)),
            ('price_firstplace', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=4, blank=True)),
            ('price_guaranteed', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=4, blank=True)),
            ('ctr_premium', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=4, blank=True)),
            ('ctr_firstplace', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=4, blank=True)),
            ('ctr_guaranteed', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=4, blank=True)),
        ))
        db.send_create_signal('yandex', ['SearchRequestBudgetHistory'])

        # Adding model 'SearchRequestBudget'
        db.create_table('yandex_searchrequestbudget', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('search_request', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['yandex.SearchRequest'])),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('region', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('shows', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('clicks_premium', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('clicks_firstplace', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('clicks_guaranteed', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('price_premium', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=4, blank=True)),
            ('price_firstplace', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=4, blank=True)),
            ('price_guaranteed', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=4, blank=True)),
            ('ctr_premium', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=4, blank=True)),
            ('ctr_firstplace', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=4, blank=True)),
            ('ctr_guaranteed', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=4, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('yandex', ['SearchRequestBudget'])

        # Adding model 'SearchRequest'
        db.create_table('yandex_searchrequest', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('phrase', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
        ))
        db.send_create_signal('yandex', ['SearchRequest'])


    def backwards(self, orm):
        # Deleting model 'SearchRequestBudgetHistory'
        db.delete_table('yandex_searchrequestbudgethistory')

        # Deleting model 'SearchRequestBudget'
        db.delete_table('yandex_searchrequestbudget')

        # Deleting model 'SearchRequest'
        db.delete_table('yandex_searchrequest')


    models = {
        'yandex.counters': {
            'Meta': {'object_name': 'Counters'},
            'code_status': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'counter_id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'owner_login': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'permission': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'site': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['yandex.YandexUser']"})
        },
        'yandex.searchrequest': {
            'Meta': {'object_name': 'SearchRequest'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phrase': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'yandex.searchrequestbudget': {
            'Meta': {'object_name': 'SearchRequestBudget'},
            'clicks_firstplace': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'clicks_guaranteed': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'clicks_premium': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ctr_firstplace': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'ctr_guaranteed': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'ctr_premium': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price_firstplace': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'price_guaranteed': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'price_premium': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'region': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'search_request': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['yandex.SearchRequest']"}),
            'shows': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'yandex.searchrequestbudgethistory': {
            'Meta': {'object_name': 'SearchRequestBudgetHistory'},
            'clicks_firstplace': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'clicks_guaranteed': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'clicks_premium': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ctr_firstplace': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'ctr_guaranteed': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'ctr_premium': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price_firstplace': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'price_guaranteed': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'price_premium': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'region': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'search_request': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['yandex.SearchRequest']"}),
            'shows': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'yandex.yandexuser': {
            'Meta': {'object_name': 'YandexUser'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32', 'db_index': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'sertificate_expires': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sertificate_path': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'token_expires': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['yandex']