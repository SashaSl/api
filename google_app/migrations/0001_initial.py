# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SearchRequest'
        db.create_table('google_app_searchrequest', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('request', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
        ))
        db.send_create_signal('google_app', ['SearchRequest'])

        # Adding model 'AdwordsCats'
        db.create_table('google_app_adwordscats', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cat', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['google_app.SearchRequest'])),
            ('number', self.gf('django.db.models.fields.IntegerField')(max_length=10, null=True, blank=True)),
            ('checked', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('result', self.gf('jsonfield.fields.JSONField')(null=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('google_app', ['AdwordsCats'])


    def backwards(self, orm):
        # Deleting model 'SearchRequest'
        db.delete_table('google_app_searchrequest')

        # Deleting model 'AdwordsCats'
        db.delete_table('google_app_adwordscats')


    models = {
        'google_app.adwordscats': {
            'Meta': {'object_name': 'AdwordsCats'},
            'cat': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['google_app.SearchRequest']"}),
            'checked': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'result': ('jsonfield.fields.JSONField', [], {'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'google_app.searchrequest': {
            'Meta': {'object_name': 'SearchRequest'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'request': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        }
    }

    complete_apps = ['google_app']