# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from views import *

urlpatterns = patterns('',
    url(r'^api/metrika/get_report$', api_metrika_get_report, name='yandex_api_metrika_get_report'),
    url(r'^api/metrika/update_counters$', api_metrika_update_counters, name='yandex_api_metrika_update_counters'),
    url(r'^sertificate/parse$', sertificate_parse, name='yandex_sertificate_parse'),
    url(r'^sertificate/get$', sertificate_get, name='yandex_sertificate_get'),
    url(r'^frequency/parse$', frequency_parse, name='frequency_parse'),
    url(r'^frequency/get', frequency_get, name='frequency_get'),
    url(r'^minus_words_metrika/get_report$', minus_words_metrika_get_report, name='minus_words_metrika_get_report'),
    url(r'^api/direct/process/parse', api_direct_process, name='yandex_api_direct_process'),
    url(r'^api/direct/get_accounts_info', get_accounts_info, name='yandex_api_get_accounts_info'),
    url(r'^api/direct/get_campaign_list/parse', get_campaign_list, name='yandex_api_get_campaign_list'),
    url(r'^api/direct/add_new_client', add_new_direct_client, name='yandex_add_new_direct_client'),
    url(r'^api/direct/create_report', create_direct_report, name='yandex_api_create_direct_report'),
    url(r'^api/direct/get_clients_units', get_clients_units, name='yandex_api_get_clients_units'),
    )