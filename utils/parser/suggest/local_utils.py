# -*- coding: utf-8 -*-

import config
import urllib
import urllib2
import json
import operator
from collections import defaultdict

from geobase.models import Region
from engine.models import EngineRegionalParams, Engine

class KWObject(object):
    def __init__(self, **kwargs):
        for k, v in kwargs.iteritems():
            setattr(self, k, v)

class YandexSuggest(KWObject):
    def __init__(self, **kwargs):
        super(YandexSuggest, self).__init__(**kwargs)

        if not hasattr(self, 'server'): self.server = 'serp_ru'
        if not hasattr(self, 'region'): self.region = Region.extract('Moscow')

        erp = EngineRegionalParams.objects.get(region=self.region, engine=Engine.ENGINE_YANDEX)

        self.__server = config.SERVERS[self.server]
        self.__region = erp.region_code
        self.__domain = erp.domain

    def get_suggest(self, prefix, prev_request=None, iterations=1):
        def _true_suggest_get(suggests):
            suggest_dict = defaultdict(int)
            for item in suggests:
                suggest_dict[json.dumps(item)] += 1
            _max = max(suggest_dict.iteritems(), key=operator.itemgetter(1))
            return json.loads(_max[0]), _max[1]

        prefix = urllib.quote_plus(prefix.encode("utf-8"))
        url = config.SUGGEST_URL['yandex'].format(
            REGION = self.__region,
            SERVER = self.__server,
            DOMAIN = self.__domain,
            PREFIX = prefix
        )

        headers = {
            'User-Agent': 'Mozilla/5.0',
        }

        if prev_request is not None:
            headers['Referer'] = config.PREV_REQUEST_URL['yandex'].format(
                DOMAIN=self.__domain,
                REGION=self.__region,
                REQUEST=urllib.quote_plus(prev_request.encode('utf-8'))
            )
        suggests = []
        request = urllib2.Request(url, None, headers)
        for it in xrange(iterations):
            result = urllib2.urlopen(request).read()
            result = json.loads(result)
            req, anss = result[0], result[1]

            _anss = []
            for ans in anss:
                if isinstance(ans, basestring):
                    _anss.append(ans)
                elif isinstance(ans, list) and len(ans) >= 2:
                    _anss.append(ans[1])
            suggests.append(_anss)
        true_suggest, value = _true_suggest_get(suggests)
        return req, true_suggest, float(value)/iterations

    def get_request_position_for_prefix(self, request, prefix):
        _pref, anss = self.get_suggest(prefix)
        if request in anss:
            return anss.index(request) + 1

class GoogleSuggest(KWObject):
    def __init__(self, **kwargs):
        super(GoogleSuggest, self).__init__(**kwargs)

        if not hasattr(self, 'hl'): self.hl = 'ru'
        if not hasattr(self, 'region'): self.region = Region.extract('Moscow')
        if not hasattr(self, 'is_instant'): self.is_instant = False

        print self.region

        erp = EngineRegionalParams.objects.get(region=self.region, engine=Engine.ENGINE_GOOGLE)

        print self.region.id
        print erp.domain, erp.region_code, erp.params
        self.__region = erp.region_code
        self.__domain = erp.domain
        self.__hl = erp.loads_params().get("hl", "ru")

        self.__client = 'heirloom-hp'
        if (self.is_instant):
            self.__client = 'psy-ab'

    def get_suggest(self, prefix, prev_request=None, iterations=1):
        prefix = urllib.quote_plus(prefix.encode("utf-8"))
        url = config.SUGGEST_URL['google'].format(
            DOMAIN = self.__domain, 
            PREFIX = prefix,
            CLIENT = self.__client,
        )
        print url
        headers = {
            'User-Agent': config.HEADERS['User-Agent'],
            'Cookie': 'PREF=ID=0000000000000000:LD={HL}:L={REGION}'.format(
                HL = self.__hl,
                REGION = self.__region
            ),
        }
        print headers
        result = urllib2.urlopen(urllib2.Request(url, headers = headers)).read()

        result = config.RESULT_MASK['google'].findall(result)[0]
        result = json.loads(result)

        req = result[0]
        anss = list(map(lambda x: x[0].replace('<b>', '').replace('</b>', ''), result[1]))
        return req, anss, 1.0

    def get_request_position_for_prefix(self, request, prefix):
        _pref, anss = self.get_suggest(prefix)
        if request in anss:
            return anss.index(request) + 1

class MailSuggest(KWObject):
    def __init__(self, **kwargs):
        super(MailSuggest, self).__init__(**kwargs)
        if not hasattr(self, 'region'): self.region = Region.extract('Moscow')

    def get_suggest(self, prefix, prev_request=None, iterations=1):
        prefix = urllib.quote_plus(prefix.encode("utf-8"))
        url = config.SUGGEST_URL['mail'].format(
            PREFIX = prefix,
        )
        erp = EngineRegionalParams.objects.get(region=self.region, engine=Engine.ENGINE_MAIL)

        headers = {
            'User-Agent': config.HEADERS['User-Agent'],
            'Cookie': 's=geo={REGION}'.format(
                REGION=erp.region_code
            ),
        }

        result = urllib2.urlopen(urllib2.Request(url, headers = headers)).read()
        _json = json.loads(result)

        result = [item['text'] for item in _json['items']]
        return _json['terms']['query'], result, 1.0

    def get_request_position_for_prefix(self, request, prefix):
        _pref, anss = self.get_suggest(prefix)
        if request in anss:
            return anss.index(request) + 1

class BingSuggest(KWObject):
    def __init__(self, **kwargs):
        super(BingSuggest, self).__init__(**kwargs)

    def get_suggest(self, prefix, prev_request=None, iterations=1):
        prefix = urllib.quote_plus(prefix.encode("utf-8"))
        url = config.SUGGEST_URL['bing'].format(
            PREFIX = prefix,
        )
        result = urllib2.urlopen(urllib2.Request(url, headers = config.HEADERS)).read()
        _json = json.loads(result)
        result = [item['Txt'] for item in _json['AS']['Results'][0]['Suggests']]
        return _json['AS']['Query'], result, 1.0

    def get_request_position_for_prefix(self, request, prefix):
        _pref, anss = self.get_suggest(prefix)
        if request in anss:
            return anss.index(request) + 1
