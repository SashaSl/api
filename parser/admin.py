# -*- coding: utf-8 -*-

from django.contrib import admin
from models import *

import json
import logging

logger = logging.getLogger(__name__)


class ParserAdmin(admin.ModelAdmin):
    def parser_datetime(self, obj):
        return obj.created.strftime('%T %d.%m.%Y')
    parser_datetime.short_description = u"Время"


    list_display = [
        'parser_datetime', 
        'mode', 
        'operation', 
    ]

    list_filter = ['mode']


class ParserDataAdmin(admin.ModelAdmin):
    def parser_result(self, obj):
        result = json.loads(obj._result)
        if obj.parser.mode == Parser.MODE_SUGGEST:
            return "<br />".join(unicode(item) for item in result['result'])
        elif obj.parser.mode == Parser.MODE_SERP:
            out = ''
            for el in result['serp'][:10]:
                out += '%s. %s <br />' % (el['number'], el['url'])

            out += '... <br /> <b>Total: %s </b>' % (result['documents'])
            return out
        elif obj.parser.mode in [Parser.MODE_WORDSTAT_WORDS, Parser.MODE_WORDSTAT_DIRECT_API]:
            out = '<b>Main frequency</b>: ' + str(result['frequency'])
            if 'page' in result:
                out += "<br /><br /><b>Page: %s</b>" % (result['page'])
            if 'searching_with' in result:
                out += '<br /><b>Searching with:</b><br />'
                for el in result['searching_with'][:10]:
                    out += '%s: %s <br />' % (el['request'], el['frequency'])
            return out
        if obj.parser.mode == Parser.MODE_RELATED:
            return "<br />".join(result['related'])
        else:
            return "Unknown format for result json"
    parser_result.short_description = u"Результат"
    parser_result.allow_tags = True


    def parser_datetime(self, obj):
        return obj.parser.created.strftime('%T %d.%m.%Y')
    parser_datetime.short_description = u"Время"


    def parser_mode(self, obj):
        return obj.parser.mode
    parser_mode.short_description = u"Тип парсера"

    def parser_operation_hash(self, obj):
        return obj.parser.operation
    parser_operation_hash.short_description = u"Operation hash"

    list_display = [
        'request',
        'prev_request',
        'parser_datetime',
        'parser_mode',
        'region',
        'engine',
        'parser_result',
        'parser_operation_hash',
    ]


class CaptchaMonitoringAdmin(admin.ModelAdmin):
    list_display = [
        'task_name',
        'time'

    ]

admin.site.register(Parser, ParserAdmin)
admin.site.register(ParserData, ParserDataAdmin)
admin.site.register(CaptchaMonitoring, CaptchaMonitoringAdmin)
