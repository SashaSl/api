# -*- coding: utf-8 -*-


from utils.common import ts_to_datetime, parser_task_hash, datetime_to_ts
import os
import cfg
import time
import json
import urllib
import datetime
from django.http import HttpResponse, Http404
from utils.api import check_api_key, wrap_json, wrap_does_not_exist, wrap_exception, check_and_pass_common
from engine.models import Engine
from parser import tasks
from seo import tasks as seo_tasks
from celery.result import AsyncResult
from geobase.models import Region
from parser.models import Parser
from engine.models import EngineRegionalParams, Engine
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist


@wrap_json
@check_api_key("GET")
@wrap_exception
def task_status(request):
    task_id = request.GET.get('task_id')
    async_result = AsyncResult(task_id)
    status = async_result.status.lower()
    return {
        "success": True,
        "status": status
    }


@wrap_json
@check_api_key("GET")
@wrap_exception
def task_result(request):
    task_id = request.GET.get('task_id')
    async_result = AsyncResult(task_id)
    status = async_result.status.lower()

    if status != "success":
        return {
            "success": False,
            "errors": "Incorrect target task status: %s != success. Current result: %s." % (status, str(async_result.result))
        }

    return {
        "success": True,
        "result": async_result.result
    }


@wrap_json
@check_api_key("GET")
@wrap_exception
def task_result_v2(request):
    task_id = request.GET.get('task_id')
    async_result = AsyncResult(task_id)
    status = async_result.status.lower()

    if status != "success":
        return {
            "success": False,
            "errors": "Incorrect target task status: %s != success. Current result: %s." % (status, str(async_result.result))
        }
    final_result = async_result.result
    final_result['success'] = True
    return final_result


@csrf_exempt
@wrap_json
@check_api_key("POST")
@wrap_exception
@check_and_pass_common("POST",queue=True, iterations=True)
def parser_suggest_parse(request, *args, **kwargs):
    req = request.POST.get('request')
    prev_req = request.POST.get('prev_request', None)
    region = kwargs['region']
    engine = kwargs['engine']
    queue = kwargs['queue']
    iterations = kwargs['iterations']

    params = {
        'engine': engine,
        'type': Parser.MODE_SUGGEST,
        'request': req.encode('utf-8'),
        'region': region.name_en,
    }
    if prev_req is not None:
        params['prev_request'] = prev_req.encode('utf-8')

    operation_hash = parser_task_hash(params)

    task = tasks.parser_suggest_parse.apply_async(args=[engine, region, req, operation_hash], kwargs={'prev_request':prev_req, 'iterations': iterations}, queue=queue)
    task_id = task.task_id

    return {
        "success": True,
        "task_id": task_id
    }


@csrf_exempt
@wrap_json
@check_api_key("GET")
@wrap_exception
@wrap_does_not_exist
@check_and_pass_common("GET", timestamp=True)
def parser_suggest_get(request, *args, **kwargs):
    req = request.GET.get('request')
    prev_req = request.GET.get('prev_request', None)
    timestamp = kwargs['timestamp']
    region = kwargs['region']
    engine = kwargs['engine']

    params = {
        'engine': engine,
        'type': Parser.MODE_SUGGEST,
        'request': req.encode('utf-8'),
        'region': region.name_en,
    }
    if prev_req is not None:
        params['prev_request'] = prev_req.encode('utf-8')

    operation_hash = parser_task_hash(params)

    if timestamp is None:
        return Parser.objects.filter(
            mode=Parser.MODE_SUGGEST,
            operation=operation_hash
        ).latest('created').parser_data.result
    else:
        return Parser.objects.filter(
            mode=Parser.MODE_SUGGEST,
            operation=operation_hash, created__lte=ts_to_datetime(timestamp)).latest('created').parser_data.result


@csrf_exempt
@wrap_json
@check_api_key("POST")
@wrap_exception
@check_and_pass_common("POST", check_tor_disable=True, max_serp_pos=False, region_engine=False, queue=True, cache_timeout=True)
def parser_crawler_parse(request, *args, **kwargs):
    request = request.POST.getlist('request')
    tor_disable = kwargs['tor_disable']
    queue = kwargs['queue']
    cache_timeout = kwargs['cache_timeout']

    task = tasks.parser_parent_crawler_parse.apply_async(args=[request], kwargs={'tor_disable': tor_disable, 'queue': queue, 'cache_timeout': cache_timeout}, queue=queue)
    task_id = task.task_id

    return {
        "success": True,
        "task_id": task_id
    }


@csrf_exempt
@wrap_json
@check_api_key("GET")
@wrap_exception
@wrap_does_not_exist
@check_and_pass_common("GET", timestamp=True)
def parser_crawler_get(request, *args, **kwargs):
    request = request.GET.get('request')
    timestamp = kwargs['timestamp']

    operation_hash = parser_task_hash({
        'type': Parser.MODE_CRAWLER_NOJS,
        'request': request.encode('utf-8'),
    })

    if timestamp is None:
        return Parser.objects.filter(
            mode=Parser.MODE_CRAWLER_NOJS,
            operation=operation_hash
        ).latest('created').parser_data.result
    else:
        return Parser.objects.filter(
            mode=Parser.MODE_CRAWLER_NOJS,
            operation=operation_hash, created__lte=ts_to_datetime(timestamp)).latest('created').parser_data.result


@csrf_exempt
@wrap_json
@check_api_key("POST")
@wrap_exception
@check_and_pass_common("POST", queue=True, max_direct_pos=True, cache_timeout=True, zero=True)
def parser_direct_parse(request, *args, **kwargs):
    req = request.POST.getlist('request')
    region = kwargs['region']
    engine = kwargs['engine']
    queue = kwargs['queue']
    max_direct_pos = kwargs['max_direct_pos']
    cache_timeout = kwargs['cache_timeout']

    task = tasks.parser_parent_direct_parse.apply_async(args=[engine, region, req], kwargs={'max_direct_pos': max_direct_pos, 'cache_timeout': cache_timeout}, queue=queue)
    task_id = task.task_id

    return {
        "success": True,
        "task_id": task_id
    }


@csrf_exempt
@wrap_json
@check_api_key("GET")
@wrap_exception
@wrap_does_not_exist
@check_and_pass_common("GET", timestamp=True, max_direct_pos=True)
def parser_direct_get(request, *args, **kwargs):
    reqs = request.GET.get('request')
    region = kwargs['region']
    engine = kwargs['engine']
    timestamp = kwargs['timestamp']
    max_direct_pos = kwargs['max_direct_pos']
    try:
        reqs = json.loads(reqs)
    except ValueError:
        reqs = [reqs]
    final_result = []

    for req in reqs:
        operation_hash = parser_task_hash({
            'engine': engine,
            'type': Parser.MODE_DIRECT,
            'request': req.encode('utf-8'),
            'region': region.name_en,
            'max_direct_pos': max_direct_pos,
        })
        try:
            if timestamp is None:
                result = Parser.objects.filter(
                    mode=Parser.MODE_DIRECT,
                    operation=operation_hash
                ).latest('created').parser_data.result
            else:
                result = Parser.objects.filter(
                    mode=Parser.MODE_DIRECT,
                    operation=operation_hash, created__lte=ts_to_datetime(timestamp)).latest('created').parser_data.result
        except ObjectDoesNotExist:
            result = {
                 "success": False,
                 "errors": "Object does not exist!"
                }
        final_result.append({
            req: result
        })
    return final_result


@csrf_exempt
@wrap_json
@check_api_key("POST")
@wrap_exception
@check_and_pass_common("POST", queue=True, cache_timeout=True, zero=True)
def parser_budget_parse(request, *args, **kwargs):
    req = request.POST.getlist('request')
    region = kwargs['region']
    engine = kwargs['engine']
    queue = kwargs['queue']
    cache_timeout = kwargs['cache_timeout']

    task = tasks.parser_budget_parse.apply_async(args=[req, region, engine], kwargs={'queue':queue, 'cache_timeout': cache_timeout}, queue=queue)
    task_id = task.task_id

    return {
        "success": True,
        "task_id": task_id
    }


@csrf_exempt
@wrap_json
@check_api_key("GET")
@wrap_exception
@wrap_does_not_exist
@check_and_pass_common("GET", timestamp=True)
def parser_budget_get(request, *args, **kwargs):
    req = request.GET.get('request')
    timestamp = kwargs['timestamp']
    region = kwargs['region']

    req = req.strip()
    req = req.replace(u'\xa0',' ')

    operation_hash = parser_task_hash({
        'type': Parser.MODE_BUDGET,
        'request': req.encode('utf-8'),
        'region': region.name_en,
    })

    if timestamp is None:
        return Parser.objects.filter(
            mode=Parser.MODE_BUDGET,
            operation=operation_hash
        ).latest('created').parser_data.result
    else:
        return Parser.objects.filter(
            mode=Parser.MODE_BUDGET,
            operation=operation_hash, created__lte=ts_to_datetime(timestamp)).latest('created').parser_data.result


@csrf_exempt
@wrap_json
@check_api_key("POST")
@wrap_exception
@wrap_does_not_exist
@check_and_pass_common("POST", timestamp=True)
def parser_budget_get_chunk(request, *args, **kwargs):
    reqs = request.POST.get('request')
    timestamp = kwargs['timestamp']
    region = kwargs['region']
    reqs = json.loads(reqs)
    final_result = {}
    first_result = []
    empty = False
    for req in reqs:
        req = req.strip()
        req = req.replace(u'\xa0',' ')

        operation_hash = parser_task_hash({
            'type': Parser.MODE_BUDGET,
            'request': req.encode('utf-8'),
            'region': region.name_en,
        })
        try:
            if timestamp is None:
                result = Parser.objects.filter(
                    mode=Parser.MODE_BUDGET,
                    operation=operation_hash
                ).latest('created').parser_data.result
            else:
                result = Parser.objects.filter(
                    mode=Parser.MODE_BUDGET,
                    operation=operation_hash, created__lte=ts_to_datetime(timestamp)).latest('created').parser_data.result
        except ObjectDoesNotExist:
            empty = True
            continue
        first_result.append({
            req: result
        })
    if empty:
        final_result = {
            "created": datetime_to_ts(datetime.datetime.now()),
            'success': False,
            'error': "Data for one or more request doesn't exist in db",
            'result': first_result
        }
    else:
        final_result = {
            "created": datetime_to_ts(datetime.datetime.now()),
            'success': True,
            'result': first_result
        }
    return final_result


@csrf_exempt
@wrap_json
@check_api_key("POST")
@wrap_exception
@wrap_does_not_exist
@check_and_pass_common("POST", timestamp=True, max_direct_pos=True)
def parser_direct_get_chunk(request, *args, **kwargs):
    reqs = request.POST.get('request')
    region = kwargs['region']
    engine = kwargs['engine']
    timestamp = kwargs['timestamp']
    max_direct_pos = kwargs['max_direct_pos']

    reqs = json.loads(reqs)
    final_result = {}
    first_result = []
    empty = False
    for req in reqs:
        operation_hash = parser_task_hash({
            'engine': engine,
            'type': Parser.MODE_DIRECT,
            'request': req.encode('utf-8'),
            'region': region.name_en,
            'max_direct_pos': max_direct_pos,
        })
        try:
            if timestamp is None:
                result = Parser.objects.filter(
                    mode=Parser.MODE_DIRECT,
                    operation=operation_hash
                ).latest('created').parser_data.result
            else:
                result = Parser.objects.filter(
                    mode=Parser.MODE_DIRECT,
                    operation=operation_hash, created__lte=ts_to_datetime(timestamp)).latest('created').parser_data.result
        except ObjectDoesNotExist:
            empty = True
            continue
        first_result.append({
            req: result
        })
    if empty:
        final_result = {
            "created": datetime_to_ts(datetime.datetime.now()),
            'success': False,
            'error': "Data for one or more request doesn't exist in db",
            'result': first_result
        }
    else:
        final_result = {
            "created": datetime_to_ts(datetime.datetime.now()),
            'success': True,
            'result': first_result
        }
    return final_result