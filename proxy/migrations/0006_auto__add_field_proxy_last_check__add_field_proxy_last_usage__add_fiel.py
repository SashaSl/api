# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Proxy.last_check'
        db.add_column('proxy_proxy', 'last_check',
                      self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Proxy.last_usage'
        db.add_column('proxy_proxy', 'last_usage',
                      self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Proxy.last_fail'
        db.add_column('proxy_proxy', 'last_fail',
                      self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Proxy.last_check'
        db.delete_column('proxy_proxy', 'last_check')

        # Deleting field 'Proxy.last_usage'
        db.delete_column('proxy_proxy', 'last_usage')

        # Deleting field 'Proxy.last_fail'
        db.delete_column('proxy_proxy', 'last_fail')


    models = {
        'geobase.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'name_en': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_ru': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_translit': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        },
        'proxy.proxy': {
            'Meta': {'object_name': 'Proxy'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15', 'db_index': 'True'}),
            'is_tor': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_check': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'last_fail': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'last_usage': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'port': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'protocol': ('django.db.models.fields.TextField', [], {}),
            'proxy_type': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['proxy']