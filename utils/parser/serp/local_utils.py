# -*- coding: utf-8 -*-

import re
import config
import random
import socket
import time
import requests
import lxml.html

from pyvirtualdisplay import Display

from utils.common import extract_domain

def random_string(length = 8):
    chars = list("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890")
    result = ""
    for i in xrange(length):
        random.shuffle(chars)
        result += chars[i]
    return result

def connection_timeout(timeout):
    def timeout_decorator(func):
        def wrapper(*args, **kwargs):
            _timeout = socket.getdefaulttimeout()
            socket.setdefaulttimeout(timeout)
            result = func(*args, **kwargs)
            socket.setdefaulttimeout(_timeout)
            return result
        return wrapper
    return timeout_decorator

def url_from_url(url, params=['url', 'q'], try_raw=True):
    if try_raw and not (url.startswith('/url?') or bool(config.G_SERP_URL_MASK.match(url))):
        return url
    _params = urlparse.parse_qs(urlparse.urlparse(url).query)
    for param in params:
        if param in _params: return _params[param][0]

def Xvfb(visible=1, width=800, height=600, timeout=5.0):
    def decorator(func):
        def wrapper(*args, **kwargs):
            display = Display(visible=visible, size=(width, height))
            display.start()
            time.sleep(timeout)
            try:
                result = func(*args, **kwargs)
            finally:
                display.stop()
            return result
        return wrapper
    return decorator

def top_and_bottom_extractor(ads):
    def _smart_domain_extractor(url):
        resp = requests.get(url, allow_redirects=False)
        return resp.headers['location']

    block_result = []
    position = 0
    for ad in ads:
        position += 1
        try:
            domain = ad.cssselect('a[class="b-link serp-url__link"]')[0].text_content()
        except IndexError:
            domain = ''
        if domain.startswith('...'):
            url = ad.cssselect('a.b-link.serp-url__link')[0].attrib['href']
            domain = _smart_domain_extractor(url)
        domain = extract_domain(domain)
        title = ad.cssselect('h2.serp-item__title')[0].text_content()
        text = ad.cssselect('div.serp-item__text')[0].text_content()
        href = ad.cssselect('h2.serp-item__title.clearfix > a')[0].attrib['href']
        element = {
            'domain': domain,
            'title': title,
            'text': text,
            'href': href,
            'position': position,
        }
        sitelinks_list = ad.cssselect('.serp-sitelinks a')
        if sitelinks_list:
            sitelinks = []
            for sitelink in sitelinks_list:
                sitelinks.append({sitelink.text_content(): sitelink.attrib['href']})
            element['sitelinks'] = sitelinks

        element['contact_info_filled'] = 'False'
        lnks = [i.text_content().lower() for i in ad.cssselect('div.serp-item__greenurl a')]
        for lnk in lnks:
            if u'адрес и телефон' in lnk:
                element['contact_info_filled'] = 'True'
            if u'м. ' in lnk:
                element['metro'] = lnk.split(', ')[-1]

        warning_text= u''
        warning = ad.cssselect('div.serp-meta span.serp-meta__item div.serp-adv__warning')
        if warning:
            warning_text = warning[0].text_content()
            element['warning'] = warning_text

        hours = ad.cssselect('div.serp-meta span.serp-meta__item')
        if hours:
            hours = hours[0].text_content()
            pattern = re.compile('(-\d\d-\d\d)')
            phone = pattern.findall(hours)
            if phone:
                hours = hours.split(phone[-1])
            else:
                hours = [hours]
            for hour in hours:
                if ':' in hour:
                    element['hours'] = hour.replace(warning_text,u'')
        warning_side = ad.cssselect('div.serp-item__text div.serp-adv__warning')
        if warning_side:
            warning_text = warning_side[0].text_content()
            element['warning'] = warning_text
            element['text'] = element['text'].replace(warning_text,u'')
        block_result.append(element)
    return block_result

def mail_ad_extractor(ads, type):
    block_result = []
    position = 0
    for ad in ads:
        position += 1
        title = ad.cssselect('*[class*=title]')[0].text_content()
        text = ad.cssselect('*[class*=text]')[0].text_content().strip()
        try:
            domain = ad.cssselect('*[class*=url] > a')[0].text_content()
        except IndexError:
            domain = ''
        href = ad.cssselect('*[class*=title] > a')[0].attrib['href']
        element = {
            'domain': domain,
            'title': title,
            'text': text,
            'href': href,
            'position': position,
        }
        sitelinks_list = ad.cssselect('*[class*=sitelinks] > li > a')
        if sitelinks_list:
            sitelinks = []
            for sitelink in sitelinks_list:
                sitelinks.append({sitelink.text_content(): sitelink.attrib['href']})
            element['sitelinks'] = sitelinks
        if not type == 'side':
            element['favicon'] = 'http://favicon.yandex.net/favicon/' + element['domain']
        block_result.append(element)
    return block_result
