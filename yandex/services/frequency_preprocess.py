# -*- coding: utf-8 -*-

import datetime

from yandex.models import *
from utils.common import parser_task_hash

def get_wordstat_frequency(req, page, region, engine):
    from parser.models import Parser
    params = {
            'engine': engine,
            'request': req.encode('utf-8'),
            'type': Parser.MODE_WORDSTAT_WORDS,
            'page': page,
            'region': region.name_en
        }
    operation_hash = parser_task_hash(params)

    frequency = Parser.objects.filter(
        mode=Parser.MODE_WORDSTAT_WORDS,
        operation=operation_hash
    ).latest('created').parser_data.result['frequency']
    return frequency


def wordstat_item_to_SRB(frequency, req, region):
    data = {
        "special_placement": {
            "click_cost": 0,
            "displays": frequency,
            "CTR": 0,
            "budget": 0,
        },
        "1st_place": {
            "click_cost": 0,
            "budget": 0,
            "CTR": 0,
        },
        "guaranteed_display": {
            "click_cost": 0,
            "budget": 0,
            "CTR": 0,
        }
    }
    SRB_save(req, region, data)


def wordstat_data_to_SRB(reqs, page, region, engine):
    for req in reqs:
        frequency = get_wordstat_frequency(req, page, region, engine)
        wordstat_item_to_SRB(frequency, req, region)


def SRB_save(request, region, data):
    request = SearchRequest.objects.get_or_create(phrase=request)[0]
    data_dict = {
        'search_request': request,
        'region': region,
        'shows': data['special_placement']['displays'],

        'clicks_premium': data['special_placement']['CTR']*data['special_placement']['displays']/100,
        'clicks_firstplace': data['1st_place']['CTR']*data['special_placement']['displays']/100,
        'clicks_guaranteed': data['guaranteed_display']['CTR']*data['special_placement']['displays']/100,

        'price_premium': data['special_placement']['click_cost'],
        'price_firstplace': data['1st_place']['click_cost'],
        'price_guaranteed': data['guaranteed_display']['click_cost'],

        'ctr_premium': data['special_placement']['CTR'],
        'ctr_firstplace': data['1st_place']['CTR'],
        'ctr_guaranteed': data['guaranteed_display']['CTR']
    }

    SearchRequestBudgetHistory.objects.create(**data_dict)
    SearchRequestBudget.objects.create_or_update(**data_dict)

def check_if_parsed_frequency(check_list, region, cache_timeout):
    cache_timeout = str(cache_timeout)
    if 'h' in cache_timeout:
        period = datetime.datetime.now()-datetime.timedelta(hours=int(cache_timeout.replace('h','')))
    elif 'd' in cache_timeout:
        period = datetime.datetime.now()-datetime.timedelta(int(cache_timeout.replace('d','')))
    else:
        period = datetime.datetime.now()-datetime.timedelta(int(cache_timeout))

    list_to_parse = []
    for item in check_list:
        result = SearchRequestBudget.objects.filter(region__name_en=region.name_en, search_request__phrase=item, updated_at__gt=period)
        if not result:
            list_to_parse.append(item)
    return list_to_parse

