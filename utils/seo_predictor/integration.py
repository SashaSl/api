# -*- coding: utf-8 -*-

from .predictor import *
from .links import *
from .articles import *

__all__ = ['predict']


def predict(site, money, period, vector_extractor_data):
    # SeoPlan
    seo_plan = SeoPlan(vector_extractor_data, money, site, period)

    actions_plan = seo_plan.extract_actions()
    # actions_plan = {
    #     'request_id': {
    #         '...(request)': \d,
    #         ...
    #     },
    #     'actions': [
    #         {
    #             'request': '...',
    #             'operation': '{link|article}',
    #             'cost': float,
    #             'request_id': 1..N,
    #             'day': 0..D
    #         },
    #         ...
    #     ],
    #     'request_url': {
    #         '...(request)': '...(url)',
    #         ...
    #     },
    #     'unused_requests': ['...(request)', ...],
    #     'request_creation_day': {
    #         '...(request)': \d,
    #         ...
    #     },
    #     'request_frequency': {
    #         '...(request)': \d,
    #         ...
    #     },
    #     'request_creation_cost': {
    #         '...(request)': float,
    #         ...
    #     }
    # }

    seo_plan_history = seo_plan.get_history()
    # [
    #     {
    #         'month': month,
    #         'month_traffic': self.get_month_traffic(month),
    #         'month_costs': self.get_month_costs(month),
    #         'month_links_costs': float,
    #         'month_articles_costs': float,
    #         'total_costs': self.get_total_costs(month),
    #         'max_possible_traffic': self.get_max_possible_traffic(),
    #         'month_buy_links': self.month_main_donors_increase[month],
    #         'month_buy_articles': self.month_articles_increase[month],
    #         'month_avg_link_cost': self.get_avg_link_cost(month),
    #         'details': [
    #             {
    #                 'request': request,
    #                 'direct_info': {
    #                     'mode_id': {1|2|3|4},
    #                     'frequency': int,
    #                     'special_cost': {float|None},
    #                     'first_cost': {float|None},
    #                     'guaranteed_cost': {float|None}
    #                 },
    #                 'frequency': self.request_frequency[request],
    #                 'month_buy_articles': operations[SeoConst.OPERATION_ARTICLE],
    #                 'total_buy_articles': self.agg_month_plan[month][request][SeoConst.OPERATION_ARTICLE],
    #                 'month_buy_links': operations[SeoConst.OPERATION_LINK],
    #                 'total_buy_links': self.agg_month_plan[month][request][SeoConst.OPERATION_LINK],
    #                 'link_cost': self.link_cost(request),
    #                 'position': self.month_request_position[month][request],
    #                 'initial_position': self.get_initial_position(request),
    #                 'has_article': self.has_article(month, request),
    #                 'has_article_initially': bool(self.request_target[request]),
    #                 'total_links': self.initial_donors(request) + self.agg_month_plan[month][request][SeoConst.OPERATION_LINK],
    #                 'initial_links': self.initial_donors(request),
    #                 'top3_avg_links': self.request_top_donors[request] + self.agg_month_request_top_donors_increase[month][request],
    #                 'initial_top3_avg_links': self.request_top_donors[request]
    #             },
    #             ...
    #         ]
    #     },
    #     ...
    # ]

    seo_plan_request_history = seo_plan.get_request_history()
    # [
    #     {
    #         'request': '...',
    #         'direct_info': {
    #             1: {
    #                 'mode_id': 1,
    #                 'frequency': int,
    #                 'special_cost': {float|None},
    #                 'first_cost': {float|None},
    #                 'guaranteed_cost': {float|None}
    #             },
    #             2: {...},
    #             3: {...},
    #             4: {...}
    #
    #         },
    #         'link_cost': float,
    #         'months': [
    #             {
    #                 'month': 0..,
    #                 'month_buy_links': int,
    #                 'total_buy_links': int,
    #                 'total_links': int,
    #                 'position': 1..51,
    #                 'month_buy_articles': int,
    #                 'has_article': bool,
    #                 'top3_avg_links': float
    #                 'traffic': float,
    #                 'extra_traffic': float
    #             },
    #             ...
    #         ]
    #     },
    #     ...
    # ]

    ############################################################################
    # LinksPlan
    links_plan = LinksPlan(actions_plan, seo_plan.domain)
    generated_links_plan = links_plan.generate_links_plan()
    # generated_links_plan = [
    #     {
    #         'request': '...',
    #         'request_id': 1..N,
    #         'cost': float,
    #         'day': 0..D,
    #         'url': '{url|#id#}',
    #         'ancor': '...',
    #         'extra_words': ['...', ...]
    #     },
    #     ...
    # ]

    ############################################################################
    # ArticlesPlan
    articles_plan = ArticlesPlan(actions_plan)
    generated_articles_plan = articles_plan.generate_articles_plan()
    # generated_articles_plan = {
    #     'h1': ['...(request)', ...],  # ordered by frequency desc
    #     'h2': ['...(request)', ...],  # ordered by frequency desc
    #     'not_used': ['...(request)', ...],  # ordered by frequency desc
    #     'plan': [
    #         {
    #             'day': 0..D,
    #             'id': 1..N,
    #             'request': '...',
    #             'url': '{url|#id#}',
    #             'cost': float,
    #             'operation': '{write|rewrite}',
    #             'internal_links': [
    #                 {
    #                     'ancor': '...',
    #                     'from_link': '{url|#id#}',
    #                     'to_link': '{url|#id#}'
    #                 },
    #                 ...
    #             ],
    #             'external_links': [
    #                 {
    #                     'ancor': '...',
    #                     'from_link': '{url|#id#}',
    #                     'to_link': '{url|#id#}'
    #                 },
    #                 ...
    #             ],
    #             'future_links_keywords': ['...', ...],
    #             'tags': {
    #                 'title': '...',
    #                 'keywords': '...',
    #                 'description': '...',
    #                 'h1': '...',
    #                 'h2': ['...', ...],
    #                 'strong_em': ['...', ...],
    #                 'img_alt_title': ['...', ...],
    #             }
    #         },
    #         ...
    #     ]
    # }

    return {
        'articles_plan': generated_articles_plan,
        'links_plan': generated_links_plan,
        'actions_plan': actions_plan,
        'history': seo_plan_history,
        'request_history': seo_plan_request_history,
        'request_bad_donors': seo_plan.requests_with_bad_donors,
        'request_all_targets': seo_plan.request_all_targets
    }