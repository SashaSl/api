from django.conf.urls import patterns, url
from proxy import views

urlpatterns = patterns('',
    url(r'^proxy/checker$', views.proxy_checker, name='api_proxy_checker'),
)