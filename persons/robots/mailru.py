# -*- coding: utf-8 -*-

import logging
import random
import datetime
from surfbot import WebApplication
from surfbot.common import random_string
from utils.common import Xvfb
import cfg
import sys
import os
import tempfile
from persons.models import Person, MailRu
from utils.common import KWObject

VISIBLE = cfg.persons_visible

def generator(visible):
    def signup(person, proxy=None):
        if person.using(MailRu): return

        cache_dir = os.path.join(tempfile.gettempdir(), 'webkit-network-cache-' + random_string())
        print cache_dir
        if not os.path.exists(cache_dir): os.makedirs(cache_dir)

        app = WebApplication(
            open_links_in_new_tabs=False,
            wait_for_page_load_when_open=True,
            log_level=logging.INFO,
            proxy=proxy,
            antigate_key=cfg.antigate_key,
            viewport_size=(1200, 600),
            user_agent='Mozilla/5.0',
            # cache_dir=os.path.join(tempfile.gettempdir(), 'webkit-network-cache-' + random_string()),
            cache_dir=cache_dir,
            homedir=os.path.join(tempfile.gettempdir(), 'webkit-home-dir-' + random_string())
        )

        app = WebApplication(
            open_links_in_new_tabs=False,
            wait_for_page_load_when_open=True,
            log_level=logging.INFO,
            proxy=proxy,
            antigate_key=cfg.antigate_key,
            viewport_size=(1200, 600),
            user_agent='Mozilla/5.0',
        )

        app.load_url_in_tab("http://m.mail.ru/cgi-bin/signup", True)
        page = app.get_current_web_page()

        login = person.generate_login()
        gender_key = 'Down' if not person.is_male else 'Down+Up'
        day = str(person.birthday.day)
        month = ['Down'] * person.birthday.month
        year = str(person.birthday.year)
        page.fill('input#Username', login).key('Tab+Tab').type(person.first_name).key('Tab').\
        type(person.last_name).key('Tab').key(gender_key).key('Tab').type(day).key('Tab').\
        key(month).key('Tab').type(year).type('\n')

        question = ['Down'] * random.randint(0, 8)
        answer = random_string()
        password = person.password
        page.fill('a[href*=RegStep]').wait_for_selector('select[name=Password_Question]').\
        sleep(1000).fill('input#password', password).key('Tab').key(question).key('Tab').type(answer).\
        type('\n').wait_for_page_loaded()

        captcha_text = page.solve_captcha('img[src*="swa.mail.ru"]')
        page.fill('input.input-captcha', captcha_text).type('\n').wait_for_selector('div.trash')

        mailru = MailRu.objects.create(
            person=person,
            login=login,
            mail='mail.ru',
            password=password,
            created=datetime.datetime.now(),
            last_login=datetime.datetime.now(),
            question_num=len(question),
            answer=answer
        )

        print >> sys.stderr, "SIGNUP_MAILRU: " + str(mailru.dict)

        app.exit()

    if not visible:
        signup = Xvfb(visible=VISIBLE, width=1200, height=600, timeout=15)(signup)

    def read_lastmsg(person, proxy=None):
        cache_dir = os.path.join(tempfile.gettempdir(), 'webkit-network-cache-' + random_string())
        print cache_dir
        if not os.path.exists(cache_dir): os.makedirs(cache_dir)

        app = WebApplication(
            open_links_in_new_tabs=False,
            wait_for_page_load_when_open=True,
            log_level=logging.INFO,
            proxy=proxy,
            antigate_key=cfg.antigate_key,
            viewport_size=(1200, 600),
            user_agent='Mozilla/5.0',
            # cache_dir=os.path.join(tempfile.gettempdir(), 'webkit-network-cache-' + random_string()),
            cache_dir=cache_dir,
            homedir=os.path.join(tempfile.gettempdir(), 'webkit-home-dir-' + random_string())
        )

        mailru = person.account(MailRu)

        app.load_url_in_tab("http://m.mail.ru/cgi-bin/login", True)
        page = app.get_current_web_page()

        page.fill('input[name=Login]', mailru.login).key('Tab+Tab').\
        type(mailru.password).fill('input[type=submit]')

        '''
        if page.find_elements('p.error'):
            mailru.delete()
            twitter = person.account(Twitter)
            twitter.delete()
            raise Exception('Corrupted mailru.')
        '''

        page.fill('table#m_msglistcontainer a[href*=readmsg]:nth-child(1)').\
        wait_for_selector('div.Section1')

        content = unicode(page.find_elements('div.Section1')[0].html)

        print >> sys.stderr, "READ_LAST_MSG_MAILRU: " + content.encode('utf-8')

        MailRu.objects.filter(id=mailru.id).update(lastmsg=content)

        app.exit()

        return content

    if not visible:
        read_lastmsg = Xvfb(visible=VISIBLE, width=1200, height=600, timeout=15)(read_lastmsg)

    return KWObject(
        signup=signup,
        read_lastmsg=read_lastmsg
    )
