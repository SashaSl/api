# -*- coding: utf-8 -*-

from django.db import models

class Region(models.Model):
    LEVEL_CITY = "city"
    LEVEL_REGION = "region"
    LEVEL_COUNTRY = "country"
    LEVEL_CHOICE = (
        (LEVEL_CITY, u"Город"),
        (LEVEL_REGION, u"Регион"),
        (LEVEL_COUNTRY, u"Страна")
    )

    name_ru = models.TextField(verbose_name=u"Название на русском", unique=True)
    name_en = models.TextField(verbose_name=u"Название на английском", unique=True)
    name_translit = models.TextField(verbose_name=u"Название транслитом", unique=True)
    level = models.TextField(verbose_name=u"Уровень", choices=LEVEL_CHOICE, db_index=True)
    parent = models.ForeignKey('self', null=True, blank=True, verbose_name=u"Родитель")

    @classmethod
    def extract(cls, region_string):
        fields = ['pk', 'name_ru', 'name_en', 'name_translit']

        for field in fields:
            try:
                return cls.objects.get(**{field + "__iexact": region_string})
            except (ValueError, cls.DoesNotExist):
                pass

        raise cls.DoesNotExist

    def __unicode__(self):
        return u"{RU} ({TR})".format(
            RU = self.name_ru,
            TR = self.name_translit
        )
