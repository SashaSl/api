# -*- coding: utf-8 -*-

import os
import re
import sys
import cfg

MAX_TIMEOUT = 90
MIN_TIMEOUT = 10

MAX_SERP_POS = 50
MIN_SERP_POS = 10

################################################
SEARCH_URL = {
    'yandex': 'http://yandex.{DOMAIN}/yandsearch?p={PAGE}&lr={REGION}&text={REQUEST}',
    'google': 'http://www.google.{DOMAIN}/search?ie=UTF-8&hl=ru&q={REQUEST}&start={START_FROM}',
    'yandex_direct': 'https://direct.yandex.{DOMAIN}/search?charset=utf-8&text={REQUEST}&page={PAGE}',
    'mail': 'http://go.mail.{DOMAIN}/search?mailru=1&q={REQUEST}&rch=l&sf={PAGE}'
}

################################################

YA_CAPTCHA_IMG_XPATH = "//img[@class='b-captcha__image']"
YA_CAPTCHA_INPUT_FIELD_XPATH = "//input[@class='b-captcha__input']"
YA_CAPTCHA_SUBMIT_XPATH = "//input[@class='b-captcha__submit']"
YA_SERP_CAPTHA_INPUT_NAME = 'rep'
YA_SERP_XPATH = "//ol[contains(@class,'b-serp-list')]/li[contains(@class, 'b-serp-item') and not(contains(@class, 'z-'))]/h2[contains(@class, 'b-serp-item__title')]/a[contains(@class,'b-serp-item__title-link')]"

YA_INDICATOR_TEXT = 'yandex.st/lego'

################################################

MA_CAPTCHA_IMG_CSSPATH = '.block-antirobot form table tr td img'
MA_CAPTCHA_INPUT_FORM_NAME = "input_adr"
MA_CAPTCHA_INPUT_FIELD_NAME = 'SequreWord'

################################################

WORDSTAT_URL_BASE_2 = u"http://wordstat.yandex.ru/#!/?{PAGE}regions={REGION}&words={REQUEST}"
WORDSTAT_URL_BASE_2_NO_REGIONS = u"http://wordstat.yandex.ru/#!/?{PAGE}&words={REQUEST}"

WORDSTAT_URL = "http://wordstat.yandex.ru/?cmd=regions&scmd=&regsort=&t="
WORDSTAT_VISIBILITY = cfg.wordstat_visible

### TO EXPLORE: different browser - different captcha hosts or just randomized hosts
# WORDSTAT_CAPTCHA_IMG_XPATH = "//img[contains(@src,'http://u.captcha.yandex.net/image')]"
WORDSTAT_CAPTCHA_IMG_XPATH = "//img[contains(@src,'.captcha.yandex.net/image')]"

# WORDSTAT_CAPTCHA_IMG_CSS = 'img[src^="http://u.captcha.yandex.net/image"]'
WORDSTAT_CAPTCHA_IMG_CSS = 'img[src*=".captcha.yandex.net/image"]'

WORDSTAT_NEXT_PAGE_CSS = 'a[href="#next_page"]'
WORDSTAT_UPDATED_CSS = 'div[class="b-word-statistics__last_update"]'
WORDSTAT_UPDATED_MASK = re.compile("(\d{2})\.(\d{2})\.(\d{4})")

WORDSTAT_CAPTCHA_INPUT_XPATH = "//div[contains(@class,'b-page__captcha-popup')]//form//input[contains(@id,'uniq') and contains(@class,'b-form-input__input')]"
WORDSTAT_CAPTCHA_INPUT_CSS = 'div.b-page__captcha-popup form input.b-form-input__input'
WORDSTAT_INPUT_TEXT_CSS = 'input[name="text"]'
WORDSTAT_RESULTS_CSS = 'p[class="b-phrases__info"]'
WORDSTAT_URL_BASE = "http://wordstat.yandex.ru"
WORDSTAT_CAPTHA_INPUT_NAME = 'captcha_val'

WORDSTAT_FREQ_MASK = re.compile('<h4>Всего показов: (\d+)</h4>')
WORDSTAT_NEW_FREQ_MASK = re.compile("»—(\d+)")

WORDSTAT_FREQ_MASK_2 = re.compile('<h4>Всего показов: (\d+)</h4>')
WORDSTAT_NEW_FREQ_MASK_2 = re.compile("» — (\d+)")

WORDSTAT_FREQ_MASK_3 = re.compile('<h4>Всего показов: ([\d\s]+)</h4>')
WORDSTAT_NEW_FREQ_MASK_3 = re.compile(" — ([\d\s]+)")

WORDSTAT_REQUEST = '«(.+?)»'

################################################

G_CAPTCHA_IMG_XPATH = "//img[contains(@src,'/sorry/image')]"
G_CAPTCHA_INPUT_FIELD_XPATH = "//input[@name='captcha']"
G_CAPTCHA_SUBMIT_XPATH = "//input[@name='submit']"

G_INDICATOR_TEXT = 'window.google'

# G_SERP_XPATH = "//div[@id='ires']/ol/li[@class='g']//h3[@class='r']/a"
G_SERP_XPATH = "//div[@id='ires']/ol/li[@class='g']/div[@class='rc']/h3[@class='r']/a"

G_SERP_URL_MASK = re.compile("^https?://(?:www\.)?(?:(?:maps|news)\.)?google\.(?:ru|ua|com|com\.tr|net|kz|by)(?:$|[?/])")

################################################

ANTI_CAPTCHA_KEY     = "112db67fc60962a9bdc82574e28991f6"
ANTI_CAPTCHA_NOT_READY = "CAPCHA_NOT_READY"
ANTI_CAPTCHA_BOUNDARY = "----------33849tv34098um893um398n3yx98nty3784xyr8"
ANTI_CAPTCHA_DOMAIN = "antigate.com"
ANTI_CAPTCHA_API_URL = "http://%s/res.php" % ANTI_CAPTCHA_DOMAIN
ANTI_CAPTCHA_SEND_URL = "/in.php"
ANTI_CAPTCHA_SEND_METHOD = "POST"
ANTI_CAPTCHA_TIMEOUT = 5

################################################

ROOT_DIR = os.path.dirname(__file__)
ROOT_DIR = os.path.abspath(os.path.join(ROOT_DIR, '..'))
sys.path.append(ROOT_DIR)

VISIBILITY = ROOT_DIR + '/serp/visibility.js'

PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))
SCREENSHOTS_URI = cfg.media_url + 'screenshots/'
SCREENSHOTS_PATH = PROJECT_PATH + SCREENSHOTS_URI

if not os.path.exists(SCREENSHOTS_PATH):
    os.makedirs(SCREENSHOTS_PATH)

################################################


REGIONS = {
    'moscow': {
        'yandex': {
            'domain': 'ru',
            'code': '213',
        },
        'google': {
            'domain': 'ru',
            'code': '0J6sXuwc',
        },
    },
    'peterburg': {
        'yandex': {
            'domain': 'ru',
            'code': '2',
        },
        'google': {
            'domain': 'ru',
            'code': '10L_QtdGC0LXRgNCx0YPRgNCz',
        },
    },
    'ekaterinburg': {
        'yandex': {
            'domain': 'ru',
            'code': '54',
        },
        'google': {
            'domain': 'ru',
            'code': '10JXQutCw0YLQtdGA0LjQvdCx0YPRgNCz',
        },
    },
    'kazan': {
        'yandex': {
            'domain': 'ru',
            'code': '43',
        },
        'google': {
            'domain': 'ru',
            'code': '10LrQsNC30LDQvdGM',
        },
    },
    'novosibirsk': {
        'yandex': {
            'domain': 'ru',
            'code': '65',
        },
        'google': {
            'domain': 'ru',
            'code': '10L3QvtCy0L7RgdC40LHQuNGA0YHQug',
        },
    },
    'krasnodar': {
        'yandex': {
            'domain': 'ru',
            'code': '35',
        },
        'google': {
            'domain': 'ru',
            'code': '10LrRgNCw0YHQvdC-0LTQsNGA',
        },
    },
    'rostov': {
        'yandex': {
            'domain': 'ru',
            'code': '39',
        },
        'google': {
            'domain': 'ru',
            'code': '10YDQvtGB0YLQvtCy',
        },
    },
    'voronezh': {
        'yandex': {
            'domain': 'ru',
            'code': '193',
        },
        'google': {
            'domain': 'ru',
            'code': '10LLQvtGA0L7QvdC10LY',
        },
    },
    'ufa': {
        'yandex': {
            'domain': 'ru',
            'code': '172',
        },
        'google': {
            'domain': 'ru',
            'code': '10YPRhNCw',
        },
    },
    'omsk': {
        'yandex': {
            'domain': 'ru',
            'code': '66',
        },
        'google': {
            'domain': 'ru',
            'code': '10L7QvNGB0Lo',
        },
    },
    'astana': {
        'yandex': {
            'domain': 'kz',
            'code': '163',
        },
        'google': {
            'domain': 'kz',
            'code': '10JDRgdGC0LDQvdCw',
        },
    },
    'kiev': {
        'yandex': {
            'domain': 'ua',
            'code': '143',
        },
        'google': {
            'domain': 'com.ua',
            'code': '10JrQuNC10LI',
        },
    },
    'minsk': {
        'yandex': {
            'domain': 'by',
            'code': '157',
        },
        'google': {
            'domain': 'by',
            'code': '10JzQuNC90YHQug',
        },
    },
}


TMP_DIR = "/tmp"

if not os.path.exists(TMP_DIR):
    os.makedirs(TMP_DIR)
