# encoding: utf-8
import random
import cfg
from django.db import models
from datetime import datetime, date, timedelta
from django.db.models import Q


class User(models.Model):
    login = models.CharField(max_length=50, verbose_name=u"Логин", unique=True, null=False)
    password = models.CharField(max_length=32, verbose_name=u"Пароль")
    limit_reached_at = models.DateTimeField(verbose_name=u"Время достижения лимита", null=True, default=None, blank=True)

    first_usage_date = models.DateTimeField(verbose_name=u"Дата первого запроса", null=True, default=None, blank=True)
    success_counter = models.IntegerField(verbose_name=u"Количество успехов", null=True, default=None, blank=True)
    fail_counter = models.IntegerField(verbose_name=u"Количество фейлов", null=True, default=None, blank=True)
    last_fail_date = models.DateTimeField(verbose_name=u"Дата последнего фейла", null=True, default=None, blank=True)
    last_success_date = models.DateTimeField(verbose_name=u"Дата последнего успеха", null=True, default=None, blank=True)
    is_used = models.BooleanField(verbose_name=u"Аккаунт используется", default=False)

    @classmethod
    def get_active_user(cls):
        active_users = cls.objects.filter(Q(limit_reached_at__isnull=True) | Q(limit_reached_at__lt=date.today()))
        if active_users:
            return random.choice(active_users)
        return None

    @classmethod
    def get_api_budget_user(cls):
        for p in xrange(cfg.safety_period, -1, -1):
            period = date.today()-timedelta(p)

            cond0 = Q(is_used=False)
            cond1 = Q(last_fail_date__lte=period, last_success_date__lte=period)
            cond2 = Q(last_fail_date__isnull=True, last_success_date__isnull=True)
            cond3 = Q(last_fail_date__isnull=True, last_success_date__lte=period)
            cond4 = Q(last_fail_date__lte=period, last_success_date__isnull=True)

            queryset = cls.objects.filter(cond0 & (cond1 | cond2 | cond3 | cond4))

            if queryset.count() > 0:
                return queryset.order_by('?')[0]

    @classmethod
    def limit_reached_for(cls, login):
        cls.objects.filter(login=login).update(limit_reached_at=datetime.now())

    def __unicode__(self):
        return u"{login}".format(login=self.login)
