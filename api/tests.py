# -*- coding: utf-8 -*-

from django.test.utils import override_settings
from django.test.client import Client
from django.test import TestCase
from celery.result import AsyncResult
import json
import cfg
import time
from parser.tasks import full_suggest_parse, structure_generate
import os

class ApiTestCase(TestCase):
    fixtures = [
        'region.json',
        'engineregionalparams.json'
    ]

    def setUp(self):
        self.client = Client()

    @override_settings(
        TEST_RUNNER='djcelery.contrib.test_runner.CeleryTestSuiteRunner',
        BROKER_BACKEND='memory',
        CELERY_ALWAYS_EAGER=True,
        CELERY_EAGER_PROPAGATES_EXCEPTIONS=True
    )
    def test_structure_generate(self):
        test_file_name = os.path.join(cfg.ROOT_DIR, 'api/tests_data/avia.txt')
        with open(test_file_name) as test_file:
            requests = test_file.read().strip().decode('utf-8').split('\n')
            requests = [request.strip() for request in requests if request.strip()]

        result = structure_generate.delay(
            requests
        )

        data = result.get()
        self.assertIn(u'авиабилеты гоа', data['structure'].keys())

    def test_task_status_1(self):
        response = self.client.get('/api/v1/task/status', {'task_id': '123', 'key': '123'})
        self.assertEqual(response.status_code, 200)

    def test_task_status_2(self):
        response = self.client.get('/api/v1/task/status', {'task_id': '123', 'key': '123'})
        data = json.loads(response.content)
        self.assertEqual(data['success'], False, data.get('errors', None))

    def test_task_status_3(self):
        response = self.client.get('/api/v1/task/status', {'task_id': '123', 'key': cfg.api_key})
        data = json.loads(response.content)
        self.assertEqual(data['success'], True, data.get('errors', None))

    @override_settings(
        TEST_RUNNER='djcelery.contrib.test_runner.CeleryTestSuiteRunner',
        BROKER_BACKEND='memory',
        CELERY_ALWAYS_EAGER=True,
        CELERY_EAGER_PROPAGATES_EXCEPTIONS=True
    )
    def test_parser_suggest(self):
        response = self.client.post('/api/v1/parser/suggest/parse', {'key': cfg.api_key, 'request': 'v'})
        data = json.loads(response.content)
        self.assertEqual(data['success'], True, data.get('errors', None))
        task_id = data['task_id']

        # TODO: Status checking doesn't work in tests. Fix this artificially.
        '''
        while True:
            response = self.client.get('/api/v1/task/status', {'key': cfg.api_key, 'task_id': task_id})
            data = json.loads(response.content)
            self.assertEqual(data['success'], True, data.get('errors', None))

            if data['status'] == 'success':
                break

            self.assertNotIn(data['status'], ['revoked', 'failure'], data.get('errors', None))
            time.sleep(0.1)
            print data, task_id
        '''

        response = self.client.get('/api/v1/parser/suggest/get', {'key': cfg.api_key, 'request': 'v'})
        data = json.loads(response.content)
        self.assertEqual(data['success'], True, data.get('errors', None))
        self.assertIn('vk', data['result'], data.get('errors', None))

    # @override_settings(
    #     TEST_RUNNER='djcelery.contrib.test_runner.CeleryTestSuiteRunner',
    #     BROKER_BACKEND='memory',
    #     CELERY_ALWAYS_EAGER=True,
    #     CELERY_EAGER_PROPAGATES_EXCEPTIONS=True
    # )
    # def test_parser_related(self):
    #     response = self.client.post('/api/v1/parser/related/parse', {'key': cfg.api_key, 'request': 'фотокнига'})
    #     data = json.loads(response.content)
    #     self.assertEqual(data['success'], True, data.get('errors', None))
    #     task_id = data['task_id']
    #
    #     # TODO: Status checking doesn't work in tests. Fix this artificially.
    #
    #     response = self.client.get('/api/v1/parser/related/get', {'key': cfg.api_key, 'request': 'фотокнига'})
    #     data = json.loads(response.content)
    #     self.assertEqual(data['success'], True, data.get('errors', None))
    #     self.assertIn(u'фотоальбом', data['related'], data.get('errors', None))

    @override_settings(
        TEST_RUNNER='djcelery.contrib.test_runner.CeleryTestSuiteRunner',
        BROKER_BACKEND='memory',
        CELERY_ALWAYS_EAGER=True,
        CELERY_EAGER_PROPAGATES_EXCEPTIONS=True
    )
    def test_parser_serp_parse(self):
        # TODO: There were some problems with Tor decorator in test environment. Fix later.
        # NOTE: This problem ocured only on my laptop T_T fix fuckin' later, yep

        '''
        response = self.client.post('/api/v1/parser/serp/parse', {'key': cfg.api_key, 'request': 'vkontakte'})
        data = json.loads(response.content)
        self.assertEqual(data['success'], True, data.get('errors', None))
        task_id = data['task_id']

        # TODO: Status checking doesn't work in tests. Fix this artificially.

        response = self.client.get('/api/v1/parser/serp/get', {'key': cfg.api_key, 'request': 'vkontakte'})
        data = json.loads(response.content)
        self.assertEqual(data['success'], True, data.get('errors', None))
        self.assertEqual(u'http://vk.com/', data['serp'][0]['url'], data.get('errors', None))
        #print data
        '''
        pass

    # @override_settings(
    #     TEST_RUNNER='djcelery.contrib.test_runner.CeleryTestSuiteRunner',
    #     BROKER_BACKEND='memory',
    #     CELERY_ALWAYS_EAGER=True,
    #     CELERY_EAGER_PROPAGATES_EXCEPTIONS=True
    # )
    # def test_full_suggest(self):
    #     result = full_suggest_parse.delay(
    #         [u'в россии найдено нечто чему 6 000 лет ученые на грани обморока фото'],
    #         'request',
    #         'target'
    #     )
    #     data = result.get()
    #     self.assertEqual(1, len(data))