import json
import os
import urllib2
import httplib
import cfg

from direct_api.models import User

ACCOUNTS_DIR = os.path.join(cfg.ROOT_DIR, 'api_data', 'direct_api')

class YandexApiBudget:
    def __init__(self, keyfile, certfile, proxy=None):
        KEYFILE = keyfile
        CERTFILE = certfile

        class YandexCertConnection(httplib.HTTPSConnection):
            def __init__(self, host, port=None, key_file=KEYFILE, cert_file=CERTFILE, timeout=30):
                httplib.HTTPSConnection.__init__(self, host, port, key_file, cert_file)

        class YandexCertHandler(urllib2.HTTPSHandler):
            def https_open(self, req):
                return self.do_open(YandexCertConnection, req)
            https_request = urllib2.AbstractHTTPHandler.do_request_
        if proxy:
            _proxy = urllib2.ProxyHandler({'http': proxy})
            self.urlopener = urllib2.build_opener(*[YandexCertHandler(), _proxy])
        else:
            self.urlopener = urllib2.build_opener(*[YandexCertHandler()])
        self.url = 'https://api.direct.yandex.ru/json-api/v4/'

    def task_start(self, phrases, geo):
        data = {
           'method': 'CreateNewForecast',
           'param': {
           'Phrases': phrases,
           'GeoID': geo,
           },
        }
        jdata = json.dumps(data, ensure_ascii=False).encode('utf8')
        response = self.urlopener.open(self.url, jdata)
        return json.loads(response.read().decode('utf8'))

    def task_status(self):
        status = {
            "method": "GetForecastList",
        }
        jdata = json.dumps(status, ensure_ascii=False).encode('utf8')
        response = self.urlopener.open(self.url, jdata)
        return json.loads(response.read().decode('utf8'))

    def task_result(self, id):
        result = {
           "method": "GetForecast",
           "param": id,
        }
        jdata = json.dumps(result, ensure_ascii=False).encode('utf8')
        response = self.urlopener.open(self.url, jdata)
        return json.loads(response.read().decode('utf8'))

    def task_wait(self, id):
        check = True
        while check:
            status = self.task_status()
            for forecast in status['data']:
                if forecast['ForecastID'] == id and forecast['StatusForecast'] == 'Done':
                    check = False
                    break

    def delete_report(self, id):
        data = {
           'method': 'CreateNewForecast',
           'param': id,
        }
        jdata = json.dumps(data, ensure_ascii=False).encode('utf8')
        self.urlopener.open(self.url, jdata)

def acc_randomizer():
    while True:
        user = User.get_api_budget_user()
        keyfile = ACCOUNTS_DIR + '/' + user.login + '/private.key'
        certfile = ACCOUNTS_DIR + '/' + user.login + '/cert.crt'
        if os.path.exists(keyfile) and os.path.exists(certfile): break

    return keyfile, certfile, user.id

