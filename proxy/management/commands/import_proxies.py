# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
import cfg
import urllib
from proxy.models import Proxy
from dblog.models import errors_to_db


class Command(BaseCommand):
    @errors_to_db
    def handle(self, *args, **options):
        proxies = urllib.urlopen(cfg.proxy_import_url).read().strip().split('\n')
        proxies = [tuple(proxy.strip().split(':')) for proxy in proxies if proxy.strip()]

        all_proxies = set([(ip, str(port)) for ip, port in Proxy.objects.all().values_list('ip', 'port')])
        proxies = list(set(proxies) - all_proxies)
        proxies = [Proxy(ip=ip, port=port) for ip, port in proxies]

        Proxy.objects.bulk_create(proxies, batch_size=100)