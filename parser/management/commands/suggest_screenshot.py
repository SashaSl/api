# -*- coding: utf-8 -*-

import os
import sys
import time
import urllib
import datetime

from django.core.management.base import BaseCommand

from optparse import make_option
from utils.parser.serp import YandexBrowser
from parser.models import Parser
from utils.common import datetime_to_ts, parser_task_hash, random_string, ts_to_datetime, ts_to_date
from geobase.models import Region
from pprint import pprint

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--request',
            action='store',
            dest='request',
            default='Rihanna',
            help='Target request.'),
        make_option('--proxy',
            action='store',
            dest='proxy',
            default=None,
            help='Proxy.'),
        make_option('--region',
            action='store',
            dest='region',
            default=None,
            help='Region.'),
        make_option('--engine',
            action='store',
            dest='engine',
            default='yandex',
            help='Engine.'),    )

    def handle(self, *args, **options):
        request = urllib.unquote_plus(options.get('request')).decode("utf-8")
        proxy = options.get('proxy')
        region = options.get('region')
        engine = options.get('engine')

        ya = YandexBrowser(proxy=proxy, useragent="Mozilla/5.0")
        [screenshot_path, screenshot_size, filesize] = ya._suggest_screenshot(request, region=region)

        final_result = {
            "success": True,
            "created": datetime_to_ts(datetime.datetime.now()),
            "path": screenshot_path,
            "width": screenshot_size[0],
            "height": screenshot_size[1],
            "size": filesize,
        }

        params = {
            'engine': engine,
            'request': request.encode('utf-8'),
            'type': Parser.MODE_SUGGEST_SCREENSHOT,
        }

        if region:
            region = Region.extract(region)
            params['region'] = region.name_en

        operation_hash = parser_task_hash(params)

        Parser.add_and_save(Parser.MODE_SUGGEST_SCREENSHOT, operation_hash, request, region, engine, final_result)

