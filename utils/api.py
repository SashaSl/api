# -*- coding: utf-8 -*-

import cfg
import json
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from common import exception_to_str
import utils.parser.serp.config

MAX_SERP = utils.parser.serp.config.MAX_SERP_POS
MAX_KERNEL = cfg.kernel_max_page
CACHE_TIMEOUT = cfg.cache_timeout
CACHE_TIMEOUT_FREQUENCY = cfg.cache_timeout_frequency

def api(url, data=None):
    import json
    import urllib
    if data is None:
        result = urllib.urlopen(url).read()
    else:
        post_data = urllib.urlencode(data)
        result = urllib.urlopen(url, post_data).read()
    return json.loads(result)

# for views
def check_api_key(method, _json=False):
    def _decorator(func):
        def wrapper(request, *args, **kwargs):
            if _json:
                if json.loads(request.body).get('key') == cfg.api_key:
                    return func(request, *args, **kwargs)
            else:
                if getattr(request, method).get('key') == cfg.api_key:
                    return func(request, *args, **kwargs)

            return {
                "success": False,
                "errors": "Invalid key!"
            }
        return wrapper
    return _decorator

#for views
def wrap_json(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return HttpResponse(content=json.dumps(result, indent=4), mimetype="application/json")
    return wrapper

def wrap_does_not_exist(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except ObjectDoesNotExist:
            return {
                "success": False,
                "errors": "Object does not exist!"
            }
        
    return wrapper

def wrap_exception(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:
            return {
                "success": False,
                "errors": exception_to_str()
            }

    return wrapper

def check_and_pass_common(method, _json=False, region_engine=True, prefix_depth=False, page=False, maxpage=False, timestamp=False, max_serp_pos=True, check_tor_disable=False,
                          kernel_wordstat_pages=False, serp_extra_data=False, machine_type=False, queue=False, cache_timeout=False, max_direct_pos=False, zero=False,
                          money=False, site=False, period=False, seo_kernel=False, login=False, password=False, days=False, iterations=False, mode=False, marker_sites=False,
                          direct_check=False, ads_check=False, top_block_size=False, bottom_block_size=False, side_block_size=False, extended=False, date1=False, date2=False, counter_name=False,
                          metrika_method=False, denial=False, counter_id=False, articles_tags_normal_form=False):
    def _decorator(func):
        def wrapper(request, *args, **kwargs):
            from geobase.models import Region
            from engine.models import EngineRegionalParams, Engine

            if _json:
                query_dict = json.loads(request.body)
            else:
                query_dict = getattr(request, method)

            if region_engine:
                _region = query_dict.get('region', 'moscow')
                try:
                    _region = Region.extract(_region)
                except Region.DoesNotExist:
                    return {
                        "success": False,
                        "errors": "Target region doesn't exist."
                    }

                _engine = query_dict.get('engine', Engine.ENGINE_YANDEX)
                try:
                    EngineRegionalParams.objects.get(region=_region, engine=_engine)
                except EngineRegionalParams.DoesNotExist:
                    return {
                        "success": False,
                        "errors": "Target engineregionalparams doesn't exist."
                    }

                kwargs['region'] = _region
                kwargs['engine'] = _engine

            if prefix_depth:
                _prefix_depth = query_dict.get('prefix_depth', 1)
                try:
                    _prefix_depth = int(_prefix_depth)
                    if _prefix_depth <= 0:
                        return {
                            "success": False,
                            "errors": "Incorrect prefix_depth (<=0)."
                        }
                except ValueError:
                    return {
                        "success": False,
                        "errors": "Incorrect prefix_depth."
                    }
                kwargs['prefix_depth'] = _prefix_depth

            if page:
                _page = query_dict.get('page', 1)
                try:
                    _page = int(_page)
                    if _page <= 0:
                        return {
                            "success": False,
                            "errors": "Incorrect page (<=0)."
                        }
                except ValueError:
                    return {
                        "success": False,
                        "errors": "Incorrect page."
                    }
                kwargs['page'] = _page

            if maxpage:
                _maxpage = query_dict.get('maxpage', 1)
                try:
                    _maxpage = int(_maxpage)
                    if _maxpage <= 0:
                        return {
                            "success": False,
                            "errors": "Incorrect maxpage (<=0)."
                        }
                except ValueError:
                    return {
                        "success": False,
                        "errors": "Incorrect maxpage."
                    }
                kwargs['maxpage'] = _maxpage

            if timestamp:
                _ts = query_dict.get('timestamp', None)
                try:
                    _ts = None if _ts is None else int(_ts)
                    if _ts is not None and _ts <= 0:
                        return {
                            "success": False,
                            "errors": "Incorrect timestamp (<=0)."
                        }
                except ValueError:
                    return {
                        "success": False,
                        "errors": "Incorrect timestamp."
                    }
                kwargs['timestamp'] = _ts

            if max_serp_pos:
                _max_serp_pos = query_dict.get('max_serp_pos', None)
                try:
                    _max_serp_pos = MAX_SERP if _max_serp_pos is None else int(_max_serp_pos)
                    if _max_serp_pos is not None and _max_serp_pos <= 0:
                        return {
                            "success": False,
                            "errors": "Incorrect max_serp_pos (<=0)."
                        }
                except ValueError:
                    return {
                        "success": False,
                        "errors": "Incorrect max_serp_pos."
                    }
                kwargs['max_serp_pos'] = _max_serp_pos

            if check_tor_disable:
                _tor_disable = query_dict.get('tor_disable', 'False')

                if _tor_disable == 'True': kwargs['tor_disable'] = True
                if _tor_disable == 'False': kwargs['tor_disable'] = False

            if kernel_wordstat_pages:
                _kernel_wordstat_pages = query_dict.get('kernel_wordstat_pages', None)
                try:
                    _kernel_wordstat_pages = MAX_KERNEL if _kernel_wordstat_pages is None else int(_kernel_wordstat_pages)
                    if _kernel_wordstat_pages <= 0:
                        return {
                            "success": False,
                            "errors": "Incorrect kernel_wordstat_pages (<=0)."
                        }
                except ValueError:
                    return {
                        "success": False,
                        "errors": "Incorrect kernel_wordstat_pages."
                    }
                kwargs['kernel_wordstat_pages'] = _kernel_wordstat_pages
            
            if machine_type:
                _machine_type = query_dict.get('machine_type', 'selenium')
                kwargs['machine_type'] = _machine_type
                
            if serp_extra_data:
                _serp_extra_data = query_dict.get('serp_extra_data', 'False')
                if _serp_extra_data == 'True': _serp_extra_data = True
                if _serp_extra_data == 'False': _serp_extra_data = False
                kwargs['serp_extra_data'] = _serp_extra_data

            if queue:
                kwargs['queue'] = query_dict.get('queue', 'default')
            
            if cache_timeout:
                if zero:
                    kwargs['cache_timeout'] = str(query_dict.get('cache_timeout', 0))
                elif extended:
                    kwargs['cache_timeout'] = str(query_dict.get('cache_timeout', CACHE_TIMEOUT_FREQUENCY))
                else:
                    kwargs['cache_timeout'] = str(query_dict.get('cache_timeout', CACHE_TIMEOUT))

            if login:
                kwargs['login'] = query_dict.get('login', 'False')

            if password:
                kwargs['password'] = query_dict.get('password', 'False')

            if max_direct_pos:
                _max_direct_pos = query_dict.get('max_direct_pos', None)
                if _max_direct_pos is not None: _max_direct_pos = int(_max_direct_pos)
                kwargs['max_direct_pos'] = _max_direct_pos

            if money:
                kwargs['money'] = float(query_dict.get('money'))

            if period:
                kwargs['period'] = int(query_dict.get('period'))
            if days:
                kwargs['days'] = int(query_dict.get('days'))

            if site:
                kwargs['site'] = query_dict.get('site')
            if seo_kernel:
                kwargs['seo_kernel'] = query_dict.getlist('seo_kernel', [])
            if mode:
                kwargs['mode'] = request.POST.getlist('mode', ['wordstat'])
            if iterations:
                kwargs['iterations'] = int(query_dict.get('iterations', 1))
            if marker_sites:
                kwargs['marker_sites'] = json.loads(query_dict.get('marker_sites', '[]'))
            if direct_check:
                _direct_check = query_dict.get('direct_check', 'False')
                if _direct_check == 'True': _direct_check = True
                if _direct_check == 'False': _direct_check = False
                kwargs['direct_check'] = _direct_check
            if ads_check:
                _ads_check = query_dict.get('ads_check', 'False')
                if _ads_check == 'True': _ads_check = True
                if _ads_check == 'False': _ads_check = False
                kwargs['ads_check'] = _ads_check
            if top_block_size:
                kwargs['top_block_size'] = int(query_dict.get('top_block_size', 1))
            if bottom_block_size:
                kwargs['bottom_block_size'] = int(query_dict.get('bottom_block_size', 1))
            if side_block_size:
                kwargs['side_block_size'] = int(query_dict.get('side_block_size', 1))
            if date1:
                kwargs['date1'] = query_dict.get('date1', None)
            if date2:
                kwargs['date2'] = query_dict.get('date2', None)
            if counter_name:
                kwargs['counter_name'] = query_dict.get('counter_name', None)
            if counter_id:
                kwargs['counter_id'] = query_dict.get('counter_id', None)
            if metrika_method:
                kwargs['metrika_method'] = query_dict.get('metrika_method', None)
            if denial:
                kwargs['denial'] = int(query_dict.get('denial', None))
            if articles_tags_normal_form:
                _articles_tags_normal_form = query_dict.get('articles_tags_normal_form', 'False')
                if _articles_tags_normal_form == 'True': _articles_tags_normal_form = True
                if _articles_tags_normal_form == 'False': _articles_tags_normal_form = False
                kwargs['articles_tags_normal_form'] = _articles_tags_normal_form
            return func(request, *args, **kwargs)
        return wrapper
    return _decorator