# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'CookieData'
        db.delete_table('cookie_cookiedata')

        # Adding model 'Cookie'
        db.create_table('cookie_cookie', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.TextField')(db_column='name')),
            ('value', self.gf('django.db.models.fields.TextField')(db_column='value')),
            ('path', self.gf('django.db.models.fields.TextField')(db_column='path')),
            ('domain', self.gf('django.db.models.fields.TextField')(db_column='domain')),
            ('secure', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='secure')),
            ('expires', self.gf('django.db.models.fields.DateTimeField')(db_column='expires')),
        ))
        db.send_create_signal('cookie', ['Cookie'])


    def backwards(self, orm):
        # Adding model 'CookieData'
        db.create_table('cookie_cookiedata', (
            ('domain', self.gf('django.db.models.fields.TextField')(db_column='domain')),
            ('name', self.gf('django.db.models.fields.TextField')(db_column='name')),
            ('path', self.gf('django.db.models.fields.TextField')(db_column='path')),
            ('expires', self.gf('django.db.models.fields.DateTimeField')(db_column='expires')),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.TextField')(db_column='value')),
            ('secure', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='secure')),
        ))
        db.send_create_signal('cookie', ['CookieData'])

        # Deleting model 'Cookie'
        db.delete_table('cookie_cookie')


    models = {
        'cookie.cookie': {
            'Meta': {'object_name': 'Cookie'},
            'domain': ('django.db.models.fields.TextField', [], {'db_column': "'domain'"}),
            'expires': ('django.db.models.fields.DateTimeField', [], {'db_column': "'expires'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'db_column': "'name'"}),
            'path': ('django.db.models.fields.TextField', [], {'db_column': "'path'"}),
            'secure': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'secure'"}),
            'value': ('django.db.models.fields.TextField', [], {'db_column': "'value'"})
        }
    }

    complete_apps = ['cookie']