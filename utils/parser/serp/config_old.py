# -*- coding: utf-8 -*-

import random
import re
import os
# from suggest_seo import settings

IMPLICITLY_WAIT = 1.0
ADMIN_MAILS = ['dkuryakin@gmail.com']
FROM_MAIL = 'info@evristic.ru'

YA_DIRECT_ITEM_XPATH = "//div[contains(@class,'__list') and contains(@class,'b-serp-list')]/div[contains(@class,'__item') and contains(@class,'b-serp-item')]"
YA_DIRECT_DOMAIN_XPATH = "//div[contains(@class,'b-serp-item__links')]/div[contains(@class,'b-serp-url')]/span[contains(@class,'b-serp-url__item')]"
YA_DIRECT_CLICKABLE_XPATH = "//a[contains(@class,'b-serp-item__title-link')]"
ADS_CLICK_CHANCE = 0.0
ADS_MAX_PAGE = 5

##################################################################################

PRICE_CONST = 20.0/17.5
RABBITMQ_URL = '127.0.0.1'
FREQ_BOOST = 2.0
DATEPICKER_FMT = "%d.%m.%Y"

FREQS_HOUR_IN_DAY = [
    23, 22, 21, 21, 23, 29,
    36, 45, 54, 58, 59, 61,
    56, 50, 44, 38, 38, 42,
    43, 42, 36, 30, 26, 25
]

FREQS_DAY_IN_WEEK = [
    50, 50, 50, 50, 50, 50, 50
]

FREQS_DAY_IN_MONTH = [
    50, 50, 50, 50, 50, 50, 50,
    50, 50, 50, 50, 50, 50, 50,
    50, 50, 50, 50, 50, 50, 50,
    50, 50, 50, 50, 50, 50, 50,
    50, 50, 50
]

FREQS_MONTH_IN_YEAR = [
    50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50
]

##################################################################################

SMSC_LOGIN = 'qevristic'
SMSC_PASSWORD = 'qpravsecretkey11'

MYIP = '127.0.0.1'
TEST_STRING = 'wefyuj234YJWED6345SDFsdf'
HEADERS_SERVER_PORT = 2208
HEADERS_TEST_URL = 'http://' + MYIP + ':' + str(HEADERS_SERVER_PORT) + '/'

PERSON_LOCAL = False
XVFB_DEBUG = False

# ROOT_DIR = settings.ROOT_DIR
TMP_DIR = "/tmp"
INSTALL_DIR = ROOT_DIR + "/install"
SCHEMAS_DIR = INSTALL_DIR + "/initial_schemas"
HUMANIZER_JQUERY = ROOT_DIR + "/utils/humanizer/jquery.js"
VISIBILITY = ROOT_DIR + '/static/js/visibility.js'

SCHEMA_PROXY_REQUESTS = SCHEMAS_DIR + "/proxy_requests.txt"
SCHEMA_REFERERS = SCHEMAS_DIR + "/referers.txt"
SCHEMA_USERAGENTS = SCHEMAS_DIR + "/useragents.txt"
SCHEMA_YA_DOMAINS = SCHEMAS_DIR + "/se_domains.txt"
SCHEMA_G_DOMAINS = SCHEMAS_DIR + "/g_domains.txt"
SCHEMA_CASUAL_REQUESTS = SCHEMAS_DIR + "/casual_requests.txt"

SCHEMA_NAME_PROXY_REQUESTS = "__proxy_requests"
SCHEMA_NAME_REFERERS = "__referers"
SCHEMA_NAME_USERAGENTS = "__useragents"
SCHEMA_NAME_YA_DOMAINS = "__se_domains"
SCHEMA_NAME_G_DOMAINS = "__g_domains"
SCHEMA_NAME_CASUAL_REQUESTS = "__casual_requests"

################################################################################
##### Yandex conf
################################################################################

SERVERS = {
    "morda": "morda",
    "serp": "www",
    }

REGIONS = {
    "moscow": "213",
    }

SUGGEST_URL = "http://suggest.yandex.ru/suggest-ya.cgi?lr={{REGION}}&srv={{SERVER}}&part={{PREFIX}}"
RESULT_MASK = re.compile("^suggest.apply\(\[(.*),\[.*\]\],\[.*\]\)$")

YA_WAIT_FOR_CURRENT_URL_TIMEOUT = 1.5
YA_CLICKABLE_CHANCE = 1.0
YA_CLICKABLE_XPATH = "//*[@onclick] | //*[@onmousedown] | //a"

YA_TEST_SERP_URL = "http://yandex.ru/yandsearch?text=qwe"
DEFAULT_TEST_URL = "http://m.baidu.com"
DEFAULT_TEST_URL_INDICATOR = "uid="
YA_SERP_INDICATOR_TEXT = "b-serp-item__title"
YA_INPUT_FIELD_XPATH = "//input[@name='text']"
YA_SUGGEST_XPATH = "//div/div/ul/li[contains(@class,'b-autocomplete-item')]"
#YA_SERP_XPATH = "//ol[contains(@class,'b-serp-list')]//a[contains(@class,'b-serp-item__title-link')]"
YA_SERP_XPATH = "//ol[contains(@class,'b-serp-list')]/li[contains(@class, 'b-serp-item') and not(contains(@class, 'z-'))]/h2[contains(@class, 'b-serp-item__title')]/a[contains(@class,'b-serp-item__title-link')]"
YA_TARGET_URL_EXCLUDE = ["http://yaca.yandex.ru/", "http://yabs.yandex.ru/"]

YA_SERP_PAGE_CHANCE_TO_CLICK = 1.0
YA_SERP_PAGE_XPATH = "//a[contains(@class, 'b-pager__page')]"
YA_NEXT_PAGE_XPATH = "//a[contains(@class, 'b-pager__next')]"
YA_PREV_PAGE_XPATH = "//a[contains(@class, 'b-pager__prev')]"

YA_MSP_SIMPLE_CHANCE = 35.0
YA_MSP_AUTO_CHANCE = 5.0
YA_MSP_AUTO_BACK_CHANCE = 55.0
YA_MSP_SIMPLE_XPATH = "//div[contains(@class, 'z-misspell')]//a"
YA_MSP_AUTO_XPATH = "//div[contains(@class, 'b-misspell')]//a"
YA_MSP_AUTO_BACK_XPATH = "//div[contains(@class, 'z-misspell__text')]//a"

YA_SUGGEST_PROBABILITY = 15.0
YA_MAX_SUGGEST_SPLIT_POS = 6
YA_MIN_SUGGEST_SPLIT_POS = 1
YA_SUGGEST_LOAD_TIMEOUT = 2.5
YA_BEFORE_SERP_TIMEOUT = 1.5
YA_AFTER_SEARCH_TIMEOUT = 1.5

YA_COOKIES_DOMAINS = [u"yandex.ru", u".yandex.ru"]
YANDEXUID_COOKIE_NAME = u"yandexuid"
YANDEXGID_COOKIE_NAME = u"yandex_gid"

MAX_SERP_POS = 50
YA_SEARCH_URL = "http://yandex.ru/yandsearch?lr=213&numdoc={NUMDOC}&text=".format(NUMDOC=MAX_SERP_POS)

GOOGLE_SEARCH_URL = "http://www.google.com/search?ie=UTF-8&q="
GOOGLE_SERP_MASK_1 = "<a href=\"/url\?q=([^\"&]+)"
GOOGLE_SERP_MASK_2 = " href=\"([^\"]+)\""
PROXY_GRABBER_MASK = "(\d+\.\d+\.\d+\.\d+:\d+)"

########################################

MAX_TIMEOUT = 90
MIN_TIMEOUT = 10
SEND_KEYS_TIMEOUT = 0.5

PROXY_GRABBER_PROCESSES = 240
PROXY_GRABBER_IP_COUNT_TO_SEARCH = 10
PROCESS_PROXY_SEARCHES = 20
PROCESS_PROXY_PROCESSES = 20
PROCESS_PROXY_SESSIONS = 1000000
PROCESS_PROXY_TIMEOUT_STEP = 1

MAX_PROXY_FAIL_COUNTER = 50

CHANCE_TO_GENERATE_NEW_USER = 0.5
MAX_USERS = 1

DISPLAY_X = 1200
DISPLAY_Y = 600
DISPLAY_VISIBLE = 1

########################################

# check proxy list options
IS_WHITE_URL = "http://yandex.ru/yandsearch?text=qwe"
IS_WHITE_INDICATORS = ["yandexuid=", "spravka="]
IS_GOOD_URL = "http://m.baidu.com/"
IS_GOOD_INDICATORS = ["BAIDUID="]

########################################

ANTI_CAPTCHA_KEY     = "112db67fc60962a9bdc82574e28991f6"
ANTI_CAPTCHA_NOT_READY = "CAPCHA_NOT_READY"
ANTI_CAPTCHA_BOUNDARY = "----------33849tv34098um893um398n3yx98nty3784xyr8"
ANTI_CAPTCHA_DOMAIN = "antigate.com"
ANTI_CAPTCHA_API_URL = "http://%s/res.php" % ANTI_CAPTCHA_DOMAIN
ANTI_CAPTCHA_SEND_URL = "/in.php"
ANTI_CAPTCHA_SEND_METHOD = "POST"
ANTI_CAPTCHA_TIMEOUT = 5

YA_CAPTCHA_IMG_XPATH = "//img[@class='b-captcha__image']"
YA_CAPTCHA_INPUT_FIELD_XPATH = "//input[@class='b-captcha__input']"
YA_CAPTCHA_SUBMIT_XPATH = "//input[@class='b-captcha__submit']"

YA_TEST_RE = re.compile("b-serp-item__title-link|b-captcha__input")

YA_SERP_CAPTHA_INPUT_NAME = 'rep'

WORDSTAT_URL = "http://wordstat.yandex.ru/?cmd=regions&scmd=&regsort=&t="
WORDSTAT_CAPTCHA_IMG_XPATH = "//img[contains(@src,'http://u.captcha.yandex.net/image')]"
WORDSTAT_CAPTCHA_INPUT_XPATH = "//div[contains(@class,'b-page__captcha-popup')]//form//input[contains(@id,'uniq') and contains(@class,'b-form-input__input')]"
WORDSTAT_CAPTCHA_IMG_CSS = 'img[src^="http://u.captcha.yandex.net/image"]'
WORDSTAT_CAPTCHA_INPUT_CSS = 'div.b-page__captcha-popup form input.b-form-input__input'
WORDSTAT_INPUT_TEXT_CSS = 'input[name="text"]'
WORDSTAT_RESULTS_CSS = 'p[class="b-phrases__info"]'
WORDSTAT_URL_BASE = "http://wordstat.yandex.ru"
WORDSTAT_CAPTHA_INPUT_NAME = 'captcha_val'
WORDSTAT_FREQ_MASK = re.compile('<h4>Всего показов: (\d+)</h4>')
WORDSTAT_NEW_FREQ_MASK = re.compile("»—(\d+)")

YA_INDICATOR_TEXT = 'suggest.yandex.ru'

######## PRICER CONF
SOME_PRICE = 30000.0
SOME_SEARCHES = 337577
MIN_PRICE = 200000.0
MIN_SEARCHES = 3375770

######## dolbilka servers
DOLBILKA_SERVERS = []
MAX_SEARCHES_PER_INSTANCE = 1200000

######## scalaxy conf
SCALAXY_API_HOST = "https://www.scalaxy.ru/api/"
SCALAXY_LOGIN = "qinfo@evristic.ru"
SCALAXY_PASSWORD = "qpravsecretkey11"
SCALAXY_LIST_PROJECTS = "projects.json"
SCALAXY_LIST_INSTANCES = "projects/{PROJECT_ID}/instances.json"
SCALAXY_STOP_INSTANCE = "projects/{PROJECT_ID}/instances/{INSTANCE_ID}/terminate.json"
SCALAXY_RUN_INSTANCE = "projects/{PROJECT_ID}/instances/{INSTANCE_ID}/run.json"
SCALAXY_REBOOT_INSTANCE = "projects/{PROJECT_ID}/instances/{INSTANCE_ID}/reboot.json"

AWMPROXY_URL = "http://awmproxy.com/socks_proxy.txt"

INITIAL_BALANCE = 0
MIN_BALANCE_PROPORTION = 0.25
ACTION_LOG_THRESHOLD = 10

################################################################################
##### Bing conf
################################################################################

G_REGIONS = {
    "moscow": "0J6sXuwc",
    }
G_RESULT_MASK = re.compile("^window\.google\.ac\.h\((.*)\)$")
G_WAIT_FOR_CURRENT_URL_TIMEOUT = YA_WAIT_FOR_CURRENT_URL_TIMEOUT * 5
G_INPUT_FIELD_XPATH = "//input[@name='q']"
G_SUGGEST_XPATH = "//div[@class='gsq_a']//span"
G_SERP_XPATH = "//div[@id='ires']/ol/li[@class='g']//h3[@class='r']/a"
#G_SERP_XPATH = "//div[@id='ires']/ol/li[@class='g']//h3/a"

G_SERP_PAGE_XPATH = "//table[@id='nav']/tbody/tr/td/a[@class='fl']"
G_NEXT_PAGE_XPATH = "//table[@id='nav']/tbody/tr/td[@class='b'][2]/a | //a[@id='pnnext']"
G_PREV_PAGE_XPATH = "//table[@id='nav']/tbody/tr/td[@class='b'][1]/a | //a[@id='pnprev']"

G_MSP_SIMPLE_XPATH = "//div[@id='topstuff']//a | //a[@class='spell']"
G_MSP_AUTO_XPATH = "//span[@class='spell_orig']/a | //a[@class='spell_orig']"
G_MSP_AUTO_BACK_XPATH = "//div[@id='topstuff']//a | //a[@class='spell']"

G_COOKIE_NAME = u"PREF"
GUID_MASK = re.compile(u'ID=([^:]*):')

#G_SEARCH_URL = "http://www.google.ru/search?ie=UTF-8&hl=ru&num={NUMDOC}&q=".format(NUMDOC=MAX_SERP_POS)
G_SEARCH_URL = "http://www.google.ru/search?ie=UTF-8&hl=ru&q="

G_CAPTCHA_IMG_XPATH = "//img[contains(@src,'/sorry/image')]"
G_CAPTCHA_INPUT_FIELD_XPATH = "//input[@name='captcha']"
G_CAPTCHA_SUBMIT_XPATH = "//input[@name='submit']"

G_SERP_CAPTHA_INPUT_NAME = 'captcha'

G_INDICATOR_TEXT = 'window.google'

################################################################################
##### Mail conf
################################################################################

M_REGIONS = {
    "moscow": "70",
    }

G_RESULT_MASK = re.compile("^window\.google\.ac\.h\((.*)\)$")
M_WAIT_FOR_CURRENT_URL_TIMEOUT = YA_WAIT_FOR_CURRENT_URL_TIMEOUT
M_INPUT_FIELD_XPATH = "//input[@name='q']"
M_SUGGEST_XPATH = "//div[@id='go-suggests']//div[@class='go-suggests__item__content']/span"
M_SERP_XPATH = "//div[@id='ires']/ol/li[@class='g']//h3[@class='r']/a"

G_SERP_PAGE_XPATH = "//table[@id='nav']/tbody/tr/td/a[@class='fl']"
G_NEXT_PAGE_XPATH = "//table[@id='nav']/tbody/tr/td[@class='b'][2]/a | //a[@id='pnnext']"
G_PREV_PAGE_XPATH = "//table[@id='nav']/tbody/tr/td[@class='b'][1]/a | //a[@id='pnprev']"

G_MSP_SIMPLE_XPATH = "//div[@id='topstuff']//a | //a[@class='spell']"
G_MSP_AUTO_XPATH = "//span[@class='spell_orig']/a | //a[@class='spell_orig']"
G_MSP_AUTO_BACK_XPATH = "//div[@id='topstuff']//a | //a[@class='spell']"

G_COOKIE_NAME = u"PREF"
GUID_MASK = re.compile(u'ID=([^:]*):')

M_SEARCH_URL = "http://go.mail.ru/search?num=10&ge={REGION}&rch=l&q="

G_CAPTCHA_IMG_XPATH = "//img[contains(@src,'/sorry/image')]"
G_CAPTCHA_INPUT_FIELD_XPATH = "//input[@name='captcha']"
G_CAPTCHA_SUBMIT_XPATH = "//input[@name='submit']"

G_SERP_CAPTHA_INPUT_NAME = 'captcha'

G_INDICATOR_TEXT = 'http://go.imgsmail.ru/favicon.ico'

################################################################################
##### Google conf
################################################################################

G_REGIONS = {
    "moscow": "0J6sXuwc",
    }
G_RESULT_MASK = re.compile("^window\.google\.ac\.h\((.*)\)$")
G_WAIT_FOR_CURRENT_URL_TIMEOUT = YA_WAIT_FOR_CURRENT_URL_TIMEOUT * 5
G_INPUT_FIELD_XPATH = "//input[@name='q']"
G_SUGGEST_XPATH = "//div[@class='gsq_a']//span"
G_SERP_XPATH = "//div[@id='ires']/ol/li[@class='g']//h3[@class='r']/a"
#G_SERP_XPATH = "//div[@id='ires']/ol/li[@class='g']//h3/a"

G_SERP_PAGE_XPATH = "//table[@id='nav']/tbody/tr/td/a[@class='fl']"
G_NEXT_PAGE_XPATH = "//table[@id='nav']/tbody/tr/td[@class='b'][2]/a | //a[@id='pnnext']"
G_PREV_PAGE_XPATH = "//table[@id='nav']/tbody/tr/td[@class='b'][1]/a | //a[@id='pnprev']"

G_MSP_SIMPLE_XPATH = "//div[@id='topstuff']//a | //a[@class='spell']"
G_MSP_AUTO_XPATH = "//span[@class='spell_orig']/a | //a[@class='spell_orig']"
G_MSP_AUTO_BACK_XPATH = "//div[@id='topstuff']//a | //a[@class='spell']"

G_COOKIE_NAME = u"PREF"
GUID_MASK = re.compile(u'ID=([^:]*):')

#G_SEARCH_URL = "http://www.google.ru/search?ie=UTF-8&hl=ru&num={NUMDOC}&q=".format(NUMDOC=MAX_SERP_POS)
G_SEARCH_URL = "http://www.google.ru/search?ie=UTF-8&hl=ru&q="

G_CAPTCHA_IMG_XPATH = "//img[contains(@src,'/sorry/image')]"
G_CAPTCHA_INPUT_FIELD_XPATH = "//input[@name='captcha']"
G_CAPTCHA_SUBMIT_XPATH = "//input[@name='submit']"

G_SERP_CAPTHA_INPUT_NAME = 'captcha'

G_INDICATOR_TEXT = 'window.google'

################################################################################
################################################################################
################################################################################


######## very advanced options
FIREFOX_PROFILE_DIR = "firefox_profile"
PROFILE_NAME_PREFIX = "suggest_seo_"
BASE_PROFILES_DIR = ROOT_DIR + "/tmp/profiles"
BACKUP_TARGETS = [
    ".macromedia",
    "firefox_profile",
]

KEY_BACKUP_TARGETS = [
    "firefox_profile/cookies.sqlite",
]

EXCEPT_TARGETS = [
    "firefox_profile/x86",
    "firefox_profile/amd64",
    "firefox_profile/Cache",
    "firefox_profile/places.sqlite",
    "firefox_profile/places.sqlite-shm",
    "firefox_profile/places.sqlite-wal",
    "firefox_profile/OfflineCache",
    "firefox_profile/extensions/fxdriver@googlecode.com",
    "firefox_profile/startupCache",
    "firefox_profile/profiler_log*",
    "firefox_profile/driver_log*",
    "firefox_profile/browser_log*",
    "firefox_profile/thumbnails",
    "firefox_profile/lock",
    "firefox_profile/parent.lock",
    "firefox_profile/.parentlock",
]

MAX_SUGGESTS = 10

SERP_DISTR = {
    'click_weight': [
        64.0, 32.0, 16.0, 8.0, 8.0, 8.0, 4.0, 4.0, 4.0, 4.0,
        4.0, 4.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0,
        2.0, 2.0, 2.0, 2.0, 2.0, 1.0, 1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
    ],
    'if_target_click_weight': [
        128.0, 64.0, 32.0, 16.0, 16.0, 16.0, 8.0, 8.0, 8.0, 8.0,
        8.0, 8.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0,
        4.0, 4.0, 4.0, 4.0, 4.0, 2.0, 2.0, 2.0, 2.0, 2.0,
        2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0,
        2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0,
    ],
    'cancel_perc': [
        10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0,
        10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0,
        10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0,
        10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0,
        10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0,
    ],
    'if_target_cancel_perc': [
        3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0,
        3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0,
        3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0,
        3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0,
        3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0,
    ],
    'time_min': [
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    ],
    'if_target_time_min': [
        15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0,
        15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0,
        15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0,
        15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0,
        15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0, 15.0,
    ],
    'time_max': [
        60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0,
        60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0,
        60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0,
        60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0,
        60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0,
    ],
    'if_target_time_max': [
        120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0,
        120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0,
        120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0,
        120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0,
        120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0, 120.0,
    ],
    'depth_min': [
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    ],
    'if_target_depth_min': [
        1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
    ],
    'depth_max': [
        3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0,
        3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0,
        3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0,
        3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0,
        3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0,
    ],
    'if_target_depth_max': [
        5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0,
        5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0,
        5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0,
        5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0,
        5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0,
    ],
}

MORPHER_CAPITALIZE_CHANCE = 4.0
MORPHER_UPPER_CHANCE = 10.0
MORPHER_TITLE_CHANCE = 7.0
MORPHER_REMOVE_CHANCE = 2.0
MORPHER_REPLACE_CHANCE = 2.0
MORPHER_KB_LAYOUT_CHANCE = 2.0
MORPHER_TRANSLIT_CHANCE = 2.0

SERP_DEFAULT_CHANCE = 75.0
SERP_SIZE = 10
SERP_CLICKS_MIN = 1
SERP_CLICKS_MAX = 3

YA_CURR_PAGE_XPATH = "//b[contains(@class, 'b-pager__current')]"
G_CURR_PAGE_XPATH = "//table[@id='nav']/tbody/tr/td/b | //table[@id='nav']/tbody/tr/td[@class='cur']"
YA_SERP_URL_MASK = re.compile("^https?://(?:www\.)?(?:(?:maps|market|news|slovari|blogs|video|images)\.)?yandex\.(?:ru|ua|com|com\.tr|net|kz|by)(?:$|[?/])")
G_SERP_URL_MASK = re.compile("^https?://(?:www\.)?(?:(?:maps|news)\.)?google\.(?:ru|ua|com|com\.tr|net|kz|by)(?:$|[?/])")

UPDATE_CACHES_PROCESSES = 16
XROBOT_MOUSE_MOVE_TIMEOUT = 10
CLICKABLE_ATTEMPTS = 10

SERP_CLICKS_REPEAT = False
MY_PART_PERC = 50.0

################################################################################
################################################################################
################################################################################
###########################   PRESETS!!!   #####################################
################################################################################
################################################################################
################################################################################

PRESET_DEFAULT = SERP_DISTR.copy()
PRESET_DEFAULT.update({
    'serp_clicks': SERP_DEFAULT_CHANCE,
    'serp_clicks_min': SERP_CLICKS_MIN,
    'serp_clicks_max': SERP_CLICKS_MAX,
    'serp_clicks_repeat': SERP_CLICKS_REPEAT,
    'suggest_clicks': YA_SUGGEST_PROBABILITY,
    'suggest_min_split_pos': YA_MIN_SUGGEST_SPLIT_POS,
    'suggest_max_split_pos': YA_MAX_SUGGEST_SPLIT_POS,
    'page_clicks': YA_SERP_PAGE_CHANCE_TO_CLICK,
    'clickable_clicks': YA_CLICKABLE_CHANCE,
    'msp_simple': YA_MSP_SIMPLE_CHANCE,
    'msp_auto': YA_MSP_AUTO_CHANCE,
    'msp_auto_back': YA_MSP_AUTO_BACK_CHANCE,
    'morpher_capitalize': MORPHER_CAPITALIZE_CHANCE,
    'morpher_upper': MORPHER_UPPER_CHANCE,
    'morpher_title': MORPHER_TITLE_CHANCE,
    'morpher_remove': MORPHER_REMOVE_CHANCE,
    'morpher_replace': MORPHER_REPLACE_CHANCE,
    'morpher_kb_layout': MORPHER_KB_LAYOUT_CHANCE,
    'morpher_translit': MORPHER_TRANSLIT_CHANCE,
    'ads_clicks': ADS_CLICK_CHANCE,
    'ads_domain': '',
    'ads_max_page': ADS_MAX_PAGE,
})

PRESET_SUGGEST_1 = {
    'serp_clicks': 50,
    'serp_clicks_min': 1,
    'serp_clicks_max': 1,
    'serp_clicks_repeat': True,
    'suggest_clicks': 15,
    'suggest_min_split_pos': 1,
    'suggest_max_split_pos': 6,
    'page_clicks': 1,
    'clickable_clicks': 1,
    'msp_simple': 35,
    'msp_auto': 5,
    'msp_auto_back': 55,
    'morpher_capitalize': 4,
    'morpher_upper': 10,
    'morpher_title': 7,
    'morpher_remove': 2,
    'morpher_replace': 2,
    'morpher_kb_layout': 2,
    'morpher_translit': 2,
    'ads_clicks': 0,
    'ads_domain': '',
    'ads_max_page': 5,
    'click_weight': [100.0] * MAX_SERP_POS,
    'if_target_click_weight': [0.0] * MAX_SERP_POS,
    'cancel_perc': [100.0] * MAX_SERP_POS,
    'if_target_cancel_perc': [100] * MAX_SERP_POS,
    'time_min': [0.0] * MAX_SERP_POS,
    'if_target_time_min': [0.0] * MAX_SERP_POS,
    'time_max': [0.0] * MAX_SERP_POS,
    'if_target_time_max': [0.0] * MAX_SERP_POS,
    'depth_min': [0.0] * MAX_SERP_POS,
    'if_target_depth_min': [0.0] * MAX_SERP_POS,
    'depth_max': [0.0] * MAX_SERP_POS,
    'if_target_depth_max': [0.0] * MAX_SERP_POS,
}

PRESET_SUGGEST_2 = PRESET_SUGGEST_1.copy()
PRESET_SUGGEST_2['if_target_click_weight'] = ([100.0] * SERP_SIZE) + ([0.0] * (MAX_SERP_POS - SERP_SIZE))
PRESET_SUGGEST_2['serp_clicks_repeat'] = False

PRESET_SUGGEST_3 = PRESET_SUGGEST_2.copy()
PRESET_SUGGEST_3['if_target_time_min'] = [15.0] * MAX_SERP_POS
PRESET_SUGGEST_3['if_target_time_max'] = [30.0] * MAX_SERP_POS
PRESET_SUGGEST_3['if_target_depth_min'] = [0] * MAX_SERP_POS
PRESET_SUGGEST_3['if_target_depth_max'] = [1] * MAX_SERP_POS
PRESET_SUGGEST_3['if_target_cancel_perc'] = [15.0] * MAX_SERP_POS

########################################################################################################################

if not os.path.exists(TMP_DIR):
    os.makedirs(TMP_DIR)
if not os.path.exists(BASE_PROFILES_DIR):
    os.makedirs(BASE_PROFILES_DIR)
