# -*- coding: utf-8 -*-

import cfg
import datetime
from django.core.management.base import BaseCommand

from dblog.models import Error


class Command(BaseCommand):
    def handle(self, *args, **options):
        errors_to_delete = Error.objects.filter(created__range=[datetime.date(2011,01,01), datetime.datetime.now()-datetime.timedelta(cfg.errors_age)])
        errors_to_delete.delete()



