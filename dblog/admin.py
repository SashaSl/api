from django.contrib import admin
from models import *

class ErrorAdmin(admin.ModelAdmin):
    list_display = ['created', 'tag']

admin.site.register(Error, ErrorAdmin)
