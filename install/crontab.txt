# m    h    dom mon dow   command
*    *    *   *   *     cd %ROOT% && sudo [ "$(ps ax | grep "manage.py celerybeat" | grep -v grep | wc -l)" -eq 0 ] && sudo ./manage.py celerybeat --traceback -v 3 -l info -f celery.log
*    *    *   *   *     sudo rabbitmq-server -detached
*    *    *   *   *     cd %ROOT% && [ "0" -eq "$(ps ax | grep "./manage.py celerycam" | grep -v grep | wc -l)" ] && ( rm celeryev.pid ; sudo ./manage.py celerycam )
*    *    *   *   *     cd %ROOT% && [ "0" -eq "$(ps ax | grep "./manage.py celerymon" | grep -v grep | wc -l)" ] && ( rm celerymon.pid ; sudo ./manage.py celerymon )

#*    *    *   *   *     cd %ROOT% && sudo [ "$(ps ax | grep "gunicorn" | grep -v grep | wc -l)" -eq 0 ] && sudo gunicorn -w 24 -b :80 --timeout 600 --log-file gunicorn.log --access-logfile access.log system.wsgi:application
*    *    *   *   *     cd %ROOT% && ./manage.py runserver 0.0.0.0:80

*/5  *    *   *   *     cd %ROOT% && ./manage.py collectstatic --noinput

*    *    *   *   *     cd %ROOT% && [[ 25 < $(ps auxff | grep "python ./manage.py celerycam" | grep -v grep | awk '{print $4}') ]] && kill -9 $(ps auxff | grep "python ./manage.py celerycam" | grep -v grep | awk '{print $2}') && ( rm celeryev.pid ; sudo ./manage.py celerycam & )