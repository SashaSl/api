# -*- coding: utf-8 -*-

import random
import datetime

def generate_birthday():
    min_days_ago = 365 * 12
    max_days_ago = 365 * 50
    mu = (min_days_ago + max_days_ago) * 0.5
    sigma = (max_days_ago - mu) / 2
    seed = random.uniform(mu, sigma)
    seed = max(min_days_ago, seed)
    seed = min(max_days_ago, seed)
    return (datetime.datetime.now() - datetime.timedelta(days=int(seed))).date()

def generate_is_male():
    return random.random() < 0.45

def generate_login(first_name, last_name, birthday):
    from utils.morpher import translit
    a = translit(first_name)
    b = translit(last_name)
    year = birthday.year

    p = [a, b, str(year)]
    random.shuffle(p)

    seed = random.randint(1, 30)
    if seed == 1:
        login = '%s.%s.%s' % (p[0], p[1], p[2])
    elif seed == 2:
        login = '%s%s.%s' % (p[0], p[1], p[2])
    elif seed == 3:
        login = '%s.%s%s' % (p[0], p[1], p[2])
    elif seed == 4:
        login = '%s%s%s' % (p[0], p[1], p[2])
    elif seed == 5:
        login = '%s.%s.%s' % (p[0][0], p[1], p[2])
    elif seed == 6:
        login = '%s%s.%s' % (p[0][0], p[1], p[2])
    elif seed == 7:
        login = '%s.%s%s' % (p[0][0], p[1], p[2])
    elif seed == 8:
        login = '%s%s%s' % (p[0][0], p[1], p[2])
    elif seed == 9:
        login = '%s.%s.%s' % (p[0], p[1][0], p[2])
    elif seed == 10:
        login = '%s%s.%s' % (p[0], p[1][0], p[2])
    elif seed == 11:
        login = '%s.%s%s' % (p[0], p[1][0], p[2])
    elif seed == 12:
        login = '%s%s%s' % (p[0], p[1][0], p[2])
    elif seed == 13:
        login = '%s.%s.%s' % (p[0], p[1], p[2][0])
    elif seed == 14:
        login = '%s%s.%s' % (p[0], p[1], p[2][0])
    elif seed == 15:
        login = '%s.%s%s' % (p[0], p[1], p[2][0])
    elif seed == 16:
        login = '%s%s%s' % (p[0], p[1], p[2][0])
    elif seed == 17:
        login = '%s.%s.%s' % (p[0][:4], p[1][:4], p[2])
    elif seed == 18:
        login = '%s%s.%s' % (p[0][:4], p[1][:4], p[2])
    elif seed == 19:
        login = '%s.%s%s' % (p[0][:4], p[1][:4], p[2])
    elif seed == 20:
        login = '%s%s%s' % (p[0][:4], p[1][:4], p[2])
    elif seed == 21:
        login = '%s.%s.%s' % (p[0][:4], p[1], p[2][:4])
    elif seed == 22:
        login = '%s%s.%s' % (p[0][:4], p[1], p[2][:4])
    elif seed == 23:
        login = '%s.%s%s' % (p[0][:4], p[1], p[2][:4])
    elif seed == 24:
        login = '%s%s%s' % (p[0][:4], p[1], p[2][:4])
    elif seed == 25:
        login = '%s.%s.%s' % (p[0], p[1][:4], p[2][:4])
    elif seed == 26:
        login = '%s%s.%s' % (p[0], p[1][:4], p[2][:4])
    elif seed == 27:
        login = '%s.%s%s' % (p[0], p[1][:4], p[2][:4])
    elif seed == 28:
        login = '%s%s%s' % (p[0], p[1][:4], p[2][:4])
    elif seed == 29:
        login = '%s.%s' % (p[0], p[1])
    elif seed == 30:
        login = '%s%s' % (p[0], p[1])

    _login = ''
    for c in login:
        seed = random.randint(1, 3)
        if c != '.':
            _login = _login + c
            continue

        if seed == 1:
            _login = _login + '.'
        elif seed == 2:
            _login = _login + '_'
        elif seed == 3:
            _login = _login + '-'

    return _login.lower().replace('\'', '')