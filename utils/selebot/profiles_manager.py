# -*- coding: utf-8 -*-

import sys
import os
import shutil
import copy
import cfg
from utils.common import random_string, cp, rm
from selenium import webdriver

class ProfilesManager:
    FIREFOX_PROFILE_DIR = cfg.firefox_profile_dir
    PROFILE_NAME_PREFIX = cfg.profile_name_prefix
    BASE_PROFILES_DIR = cfg.ROOT_DIR + "/" + cfg.base_profiles_dir
    BACKUP_TARGETS = cfg.backup_targets
    KEY_BACKUP_TARGETS = cfg.key_backup_targets
    EXCEPT_TARGETS = cfg.except_targets

    class FirefoxProfile(webdriver.FirefoxProfile):
        def __init__(self,profile_directory=None):
            self.default_preferences = copy.deepcopy(
                webdriver.FirefoxProfile.DEFAULT_PREFERENCES)
            self.tempfolder = None
            self.profile_dir = profile_directory
            if self.profile_dir is None:
                self.profile_dir = self._create_tempfolder()
            else:
                self._read_existing_userjs()
            self.extensionsDir = os.path.join(self.profile_dir, "extensions")
            self.userPrefs = os.path.join(self.profile_dir, "user.js")
            self.selebot_set_speedups()

        def selebot_set_proxy(self, proxy=None, fake=False):  # proxy in format HOST:PORT or None
            if proxy is None or fake:
                self.set_preference("network.proxy.type", 0)
            else:
                host, port = proxy.rsplit(":", 1)
                port = int(port)
                self.set_preference("network.proxy.type", 1)
                self.set_preference("network.proxy.http", host)
                self.set_preference("network.proxy.http_port", port)
                self.set_preference("network.proxy.no_proxies_on", "")
                self.set_preference("network.proxy.share_proxy_settings", True)
            self.update_preferences()
            self.selebot_proxy = proxy

        def selebot_set_useragent(self, useragent='Mozilla/5.0'):
            self.set_preference("general.useragent.override", useragent)
            self.update_preferences()
            self.selebot_useragent = useragent

        def selebot_set_speedups(self):
            '''
            self.set_preference("browser.shell.checkDefaultBrowser", False)
            self.set_preference("extensions.update.autoUpdateDefault", True)
            self.set_preference("extensions.checkCompatibility", False)
            self.set_preference("extensions.checkUpdateSecurity", False)
            self.set_preference("extensions.disabledObsolete", True)
            self.set_preference("extensions.update.autoUpdateEnabled", False)
            self.set_preference("extensions.update.enabled", False)
            self.set_preference("extensions.update.notifyUser", False)
            self.set_preference("app.update.enabled", False)
            self.set_preference("extensions.update.enabled", False)
            self.set_preference("lightweightThemes.update.enabled", False)
            self.set_preference("services.sync.prefs.sync.extensions.update.enabled", False)

            self.set_preference("browser.search.update", False)
            self.set_preference("extensions.shownSelectionUI", True)
            self.set_preference("extensions.autoDisableScopes", 11)
            '''

            '''
            self.set_preference("nglayout.initialpaint.delay", 0)
            self.set_preference("content.notify.interval", 500000)
            self.set_preference("content.notify.ontimer", True)
            self.set_preference("content.max.tokenizing.time", 1500000)
            self.set_preference("content.interrupt.parsing", True)
            self.set_preference("content.switch.threshold", 500000)
            self.set_preference("content.maxtextrun", 8191)
            self.set_preference("content.notify.backoffcount", 5)
            self.set_preference("browser.search.openintab", True)
            self.set_preference("layout.word_select.eat_space_to_next_word", False)
            self.set_preference("network.http.max-connections", 48)
            self.set_preference("network.http.max-connections-per-server", 16)
            self.set_preference("network.http.max-persistent-connections-per-server", 8)
            self.set_preference("network.http.request.max-start-delay", 0)
            self.set_preference("network.http.pipelining", True)
            self.set_preference("network.http.proxy.pipelining", True)  # ???
            self.set_preference("network.http.pipelining.maxrequests", 30)  # 8
            self.set_preference("network.prefetch-next", False)
            self.set_preference("browser.cache.memory.enable", True)
            self.set_preference("browser.cache.memory.capacity", 18432)
            self.set_preference("browser.cache.disk.capacity", 18432)  # ???
            self.set_preference("browser.sessionhistory.max_entries", 5)
            self.set_preference("browser.sessionhistory.max_total_viewers", 0)
            self.set_preference("config.trim_on_minimize", True)
            self.set_preference("signed.applets.codebase_principal_support", True)
            self.set_preference("plugin.expose_full_path", True)
            self.set_preference("ui.submenuDelay", 0)
            self.set_preference("network.http.keep-alive", True)
            self.set_preference("network.http.keep-alive.timeout", 600)
            self.set_preference("network.dnsCacheExpiration", 3600)
            self.set_preference("network.dnsCacheEntries", 1000)
            '''

            self.update_preferences()
            pass

    def __init__(self, profile_name=None):
        if profile_name is None:
            profile_name = self.__class__.get_available_profile_name()
        self.profile_name = profile_name
        ff_profile_dir = self.get_firefox_profile_dir()
        if not os.path.exists(ff_profile_dir):
            os.makedirs(ff_profile_dir)
        os.environ['HOME'] = self.get_profile_dir()

    def __del__(self):
        self.delete_profile()

    def __iter_existed(self, relative_to=None):
        profile_dir = self.get_profile_dir() if relative_to is None else relative_to
        for item in self.BACKUP_TARGETS:
            target = profile_dir + "/" + item
            if os.path.exists(target):
                yield item

    def __copy_to_temp(self):
        profile_dir = self.get_profile_dir()
        temp_dir = profile_dir + "/tmp_for_backup_" + random_string()
        os.makedirs(temp_dir)
        for target in self.__iter_existed():
            target = profile_dir + "/" + target
            cp(target, temp_dir)
        self.__rm_except_targets(temp_dir)
        return temp_dir

    def __rm_except_targets(self, path):
        for except_target in self.EXCEPT_TARGETS:
            target = path + "/" + except_target
            rm(target)

    def __rm_existed(self):
        profile_dir = self.get_profile_dir()
        for item in self.__iter_existed():
            target = profile_dir + "/" + item
            rm(target)

    def dumps_profile(self):
        if not list(self.__iter_existed()):
            return None
        profile_dir = self.get_profile_dir()
        for key_target in self.KEY_BACKUP_TARGETS:
            path = profile_dir + "/" + key_target
            if not os.path.exists(path):
                return None

        arc_name = profile_dir + "/dumps_profile_" + random_string() + ".tar.gz"
        temp_dir = self.__copy_to_temp()
        os.system('tar -C "{TEMP_DIR}" -zcvf "{ARC_NAME}" {TARGETS}'.format(
            TEMP_DIR=temp_dir,
            ARC_NAME=arc_name,
            TARGETS=" ".join(self.__iter_existed(relative_to=temp_dir))
        ))
        dump = open(arc_name, "rb").read()
        os.remove(arc_name)
        rm(temp_dir)
        return dump

    def loads_profile(self, dump=None):
        if dump is None:
            return
        profile_dir = self.get_profile_dir()
        arc_name = profile_dir + "/loads_profile_" + random_string() + ".tar.gz"
        dump_file = open(arc_name, "wb")
        dump_file.write(dump)
        dump_file.close()
        self.__rm_existed()
        os.system('tar -C "{PROFILE_DIR}" -zxvf "{ARC_NAME}"'.format(
            PROFILE_DIR=profile_dir,
            ARC_NAME=arc_name
        ))
        self.__rm_except_targets(profile_dir)
        os.remove(arc_name)

    def get_profile_dir(self):
        return self.BASE_PROFILES_DIR + "/" + self.profile_name

    @classmethod
    def list_profiles(cls):
        profile_names = []
        if not os.path.exists(cls.BASE_PROFILES_DIR):
            os.makedirs(cls.BASE_PROFILES_DIR)
        for item in os.listdir(cls.BASE_PROFILES_DIR):
            profile_name = os.path.basename(item)
            if profile_name.startswith(cls.PROFILE_NAME_PREFIX):
                profile_names.append(profile_name)
        return profile_names

    @classmethod
    def generate_profile_name(cls):
        return cls.PROFILE_NAME_PREFIX + random_string()

    @classmethod
    def delete_all_profiles(cls):
        for profile_name in cls.list_profiles():
            cls(profile_name).delete_profile()

    def delete_profile(self):
        if self.profile_name in self.__class__.list_profiles():
            shutil.rmtree(self.get_profile_dir())
            result = True
        else:
            result = False
        print >> sys.stderr, "Delete profile %s, status %s." % (self.profile_name, str(result))
        return result

    def get_firefox_profile_dir(self):
        return  self.get_profile_dir() + "/" + self.FIREFOX_PROFILE_DIR

    def get_firefox_profile(self):
        return self.FirefoxProfile(self.get_firefox_profile_dir())

    @classmethod
    def get_available_profile_name(cls):
        while True:
            profile_names = set(cls.list_profiles())
            profile_name = cls.generate_profile_name()
            if profile_name not in profile_names:
                return profile_name
