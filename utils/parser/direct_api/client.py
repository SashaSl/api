# encoding: utf-8

import json
import urllib2
import httplib
import os.path
import cfg
import errors

ACCOUNTS_DIR = os.path.join(cfg.ROOT_DIR, 'api_data', 'direct_api')
DEFAULT_LOGIN = os.listdir(ACCOUNTS_DIR)[0]


class YandexDirectApiClient:
    """
    Пример использования:

        api = YandexDirectApiClient()

        print api.call('GetClientInfo', ['besposchadnii'])
        print api.call('GetClientsUnits', ['besposchadnii'])

        api.set_account('anotherone123')
    """
    _locale = 'ru'
    _url = 'https://api.direct.yandex.ru/json-api/v4/'
    _login = DEFAULT_LOGIN

    def __init__(self, login):
        if login:
            self.set_account(login)
        else:
            self.reload_opener()

    def call(self, method, params=None):
        data = {'method': method, 'locale': self._locale}
        if params:
            data['param'] = params
        json_data = json.dumps(data, ensure_ascii=False).encode('utf-8')
        response = self.opener.open(self._url, json_data)
        json_response = response.read().decode('utf-8')
        ret = json.loads(json_response, encoding='utf-8')
        if 'error_code' in ret:
            raise errors.ErrorFactory.create_error(ret['error_code'], ret['error_str'])
        return ret

    def reload_opener(self):
        class YandexCertConnection(httplib.HTTPSConnection):
            key_file = self.get_key_file()
            cert_file = self.get_cert_file()

            def __init__(self, host, port=None, key_file=None, cert_file=None, timeout=30):
                key_file = key_file or self.key_file
                cert_file = cert_file or self.cert_file
                httplib.HTTPSConnection.__init__(self, host, port, key_file, cert_file)

        class YandexCertHandler(urllib2.HTTPSHandler):
            def https_open(self, req):
                return self.do_open(YandexCertConnection, req)

            https_request = urllib2.AbstractHTTPHandler.do_request_

        self.opener = urllib2.build_opener(*[YandexCertHandler()])

    def set_account(self, login):
        self._login = login
        self.reload_opener()

    def get_key_dir(self):
        return os.path.join(ACCOUNTS_DIR, self._login)

    def get_key_file(self):
        return os.path.join(self.get_key_dir(), 'private.key')

    def get_cert_file(self):
        return os.path.join(self.get_key_dir(), 'cert.crt')

