# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Counters.name'
        db.alter_column('yandex_counters', 'name', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Counters.permission'
        db.alter_column('yandex_counters', 'permission', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Counters.owner_login'
        db.alter_column('yandex_counters', 'owner_login', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Counters.site'
        db.alter_column('yandex_counters', 'site', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Counters.code_status'
        db.alter_column('yandex_counters', 'code_status', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Counters.type'
        db.alter_column('yandex_counters', 'type', self.gf('django.db.models.fields.TextField')(null=True))

    def backwards(self, orm):

        # Changing field 'Counters.name'
        db.alter_column('yandex_counters', 'name', self.gf('django.db.models.fields.CharField')(max_length=40, null=True))

        # Changing field 'Counters.permission'
        db.alter_column('yandex_counters', 'permission', self.gf('django.db.models.fields.CharField')(max_length=40, null=True))

        # Changing field 'Counters.owner_login'
        db.alter_column('yandex_counters', 'owner_login', self.gf('django.db.models.fields.CharField')(max_length=40, null=True))

        # Changing field 'Counters.site'
        db.alter_column('yandex_counters', 'site', self.gf('django.db.models.fields.CharField')(max_length=40, null=True))

        # Changing field 'Counters.code_status'
        db.alter_column('yandex_counters', 'code_status', self.gf('django.db.models.fields.CharField')(max_length=40, null=True))

        # Changing field 'Counters.type'
        db.alter_column('yandex_counters', 'type', self.gf('django.db.models.fields.CharField')(max_length=40, null=True))

    models = {
        'geobase.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'name_en': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_ru': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'name_translit': ('django.db.models.fields.TextField', [], {'unique': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']", 'null': 'True', 'blank': 'True'})
        },
        'yandex.counters': {
            'Meta': {'object_name': 'Counters'},
            'code_status': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'counter_id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'owner_login': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'permission': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'site': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['yandex.YandexUser']"})
        },
        'yandex.searchrequest': {
            'Meta': {'object_name': 'SearchRequest'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phrase': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'yandex.searchrequestbudget': {
            'Meta': {'unique_together': "(('search_request', 'region'),)", 'object_name': 'SearchRequestBudget'},
            'clicks_firstplace': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'clicks_guaranteed': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'clicks_premium': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ctr_firstplace': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'ctr_guaranteed': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'ctr_premium': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price_firstplace': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'price_guaranteed': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'price_premium': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']"}),
            'search_request': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['yandex.SearchRequest']"}),
            'shows': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'yandex.searchrequestbudgethistory': {
            'Meta': {'object_name': 'SearchRequestBudgetHistory'},
            'clicks_firstplace': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'clicks_guaranteed': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'clicks_premium': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ctr_firstplace': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'ctr_guaranteed': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'ctr_premium': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price_firstplace': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'price_guaranteed': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'price_premium': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '4', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geobase.Region']"}),
            'search_request': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['yandex.SearchRequest']"}),
            'shows': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'yandex.yandexuser': {
            'Meta': {'object_name': 'YandexUser'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32', 'db_index': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'sertificate_expires': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sertificate_path': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'token_expires': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['yandex']