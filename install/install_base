#!/bin/bash

set -xe

ROOT="$(readlink -m "$(dirname "$0")/..")"
cd "$ROOT"

apt-get -y update
apt-get -y install python psmisc python-software-properties mc \
    screen curl bmon python-imaging wget unzip proxycheck python-pip \
    xvfb xserver-xephyr tightvncserver xdotool flashplugin-nonfree \
    python-qt4 python-psycopg2 python-lxml python-mechanize rabbitmq-server \
    memcached sshpass cython python-dev cx-freeze firefox imagemagick tor \
    privoxy g++ gcc libicu48 bash

pip install -r install/requirements.txt

# Fix locales. Need ot correct work of xdotool.
locale-gen --purge ru_RU.UTF-8 en_US.UTF-8
localedef -c -i ru_RU -f UTF-8 ru_RU.UTF-8
update-locale LANG=ru_RU.UTF-8
dpkg-reconfigure locales

# Downgrade firefox
apt-get -y remove firefox
tools/install_firefox "$($ROOT/cfg.py firefox_arch)" "$($ROOT/cfg.py firefox_version)" "$($ROOT/cfg.py firefox_lang)"

# Cache conf
cp install/memcashed.conf /etc/memcached.conf
service memcached restart

if [ "prod" == "$1" ] ; then
    printf prod > $ROOT/install_type
    cat install/crontab.txt | awk -v "ROOT=$ROOT" '{gsub(/%ROOT%/, ROOT); print}' | crontab -
    cat install/cron.d/api-common | awk -v "ROOT=$ROOT" '{gsub(/%ROOT%/, ROOT); print}' > /etc/cron.d/api-common
    #cat install/cron.d/accs_filler | awk -v "ROOT=$ROOT" '{gsub(/%ROOT%/, ROOT); print}' > /etc/cron.d/accs_filler
    cat install/cron.d/monitoring | awk -v "ROOT=$ROOT" '{gsub(/%ROOT%/, ROOT); print}' > /etc/cron.d/monitoring
elif [ "worker" == "$1" ] ; then
    printf worker > $ROOT/install_type
    if [ "sync" == "$2" ] ; then
        [ -e ~/.ssh/id_rsa ] || ssh-keygen
        ssh-copy-id root@$($ROOT/cfg.py my_ip)
        scp -r root@$($ROOT/cfg.py my_ip):$ROOT/$($ROOT/cfg.py data_dir)/fast_suggest $ROOT/$($ROOT/cfg.py data_dir)
        scp -r root@$($ROOT/cfg.py my_ip):$ROOT/$($ROOT/cfg.py data_dir)/full_suggest $ROOT/$($ROOT/cfg.py data_dir)
    fi
    cat install/worker_crontab.txt | awk -v "ROOT=$ROOT" '{gsub(/%ROOT%/, ROOT); print}' | crontab -
elif [ "local" == "$1" ] ; then
    printf local > $ROOT/install_type
fi