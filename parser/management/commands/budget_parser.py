# -*- coding: utf-8 -*-


import json
import urllib
import datetime
from django.core.management.base import BaseCommand
from optparse import make_option
from utils.parser.serp import YandexBrowser
from dblog.models import Error
from parser.models import Parser
from geobase.models import Region
from utils.common import datetime_to_ts, parser_task_hash



class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--request',
            action='store',
            dest='request',
            default='Rihanna',
            help='Target request.'),
        make_option('--region',
            action='store',
            dest='region',
            default=None,
            help='Region.'),
        make_option('--engine',
            action='store',
            dest='engine',
            default='yandex',
            help='Engine.'),
        make_option('--reqs_dict',
            action='store',
            dest='reqs_dict',
            default=None),
    )

    def handle(self, *args, **options):
        try:
            request = urllib.unquote_plus(options.get('request')).decode("utf-8")
            region = options.get('region')

            reqs_dict = options.get('reqs_dict')
            if reqs_dict:
                with open(reqs_dict) as f:
                    reqs_dict = json.load(f)

            ya = YandexBrowser()
            ya.budget_surfbot(request, region, reqs_dict, tor_pid=None)
        except Exception:
            Error.to_db()
            pass


