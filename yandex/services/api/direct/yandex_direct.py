# -*- coding: utf-8 -*-

import os
import json
import urllib2
import httplib

from yandex.models import *

class YandexDirectException(Exception):
    pass

def process_user(agent_login, agent_password, client_login, client_info_param):
    from yandex.tasks import api_direct_process_request
    user = YandexUser.objects.filter(login=client_login, agent_login__login=agent_login)
    if user:
        return {'agent_login': agent_login,
                'client_login': client_login}
    else:
        response = api_direct_process_request.apply(kwargs={'login': agent_login,
                                                            'password': agent_password,
                                                            'direct_method': 'CreateNewSubclient',
                                                            'direct_params': client_info_param
        }).result
        if 'error_code' in response:
            return response
        client_login = response['data']['Login']
        client_password = response['data']['Password']
        ya_user = YandexUser.objects.create(login=client_login, password=client_password, agent_login=YandexUser.objects.get(login=agent_login))
        return {'agent_login': agent_login,
                'client_login': client_login}

class YandexDirectApi:
    def __init__(self, path_to_sertificate=None):
        #адрес для отправки json-запросов
        self.url = 'https://api.direct.yandex.ru/json-api/v4/'
        #адрес для отправки LIVE json-запросов
        self.live_url = 'https://api.direct.yandex.ru/live/v4/json/'
        if path_to_sertificate:
            sertif_folder = path_to_sertificate
            self.keyfile = sertif_folder + 'private.key'
            self.certfile = sertif_folder + 'cert.crt'
        else:
            self.sertificate_status = {
                'status': 'reload sertificate',
                'details': 'no sertificate file',
            }
        that = self
        if os.path.exists(self.keyfile) and os.path.exists(self.certfile):
            # configure urlopener
            class YandexCertConnection(httplib.HTTPSConnection):
                def __init__(self, host, port=None, key_file=that.keyfile, cert_file=that.certfile, timeout=30):
                    httplib.HTTPSConnection.__init__(self, host, port, key_file, cert_file)
            class YandexCertHandler(urllib2.HTTPSHandler):
                def https_open(self, req):
                    return self.do_open(YandexCertConnection, req)
                https_request = urllib2.AbstractHTTPHandler.do_request_
            self.urlopener = urllib2.build_opener(*[YandexCertHandler()])
            self.sertificate_status = self.check_sertificate()
        else:
            self.sertificate_status = {
                'status': 'reload sertificate',
                'details': 'no sertificate file',
            }

    def check_sertificate(self):
        resp = self.make_action("GetReportList")
        if 'error_code'in resp:
            if 'error_detail' in resp:
                details = resp['error_detail']
            else:
                details = ''
            if int(resp['error_code']) == 53:
                return {
                    'status': 'reload sertificate',
                    'details': details,
                }
            else:
                return {
                    'status': 'failure',
                    'details': details,
                }
        return {
            'status': 'success',
            'details': 'success'
        }

    def get_response(self, data, url):
        jdata = json.dumps(data, ensure_ascii=False).encode('utf8')
        response = self.urlopener.open(url, jdata)
        return response

    def make_action(self, method, live_url=None, param=None):
        if method in ['CreateOrUpdateCampaign', 'CreateNewSubclient', 'CreateOrUpdateBanners', 'ModerateBanners',
                      'GetCampaignsParams', 'GetCampaignsList', 'AdImage', 'AdImageAssociation']:
            live_url = True
        if live_url:
            direct_url = self.live_url
        else:
            direct_url = self.url
        data = {
            "method": method,
        }
        if param:
            data["param"] = param
        response = self.get_response(data, direct_url)
        return json.load(response)